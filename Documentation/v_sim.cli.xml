<?xml version="1.0" encoding="utf-8"?>
<commandLine>
  <option name="export" short="e" version="3.0">
    <description arg="file">make an image from the fileToRender argument. The format is specified through the extension of the argument or by the -o fileFormatId=id option (get the id of available file formats with -o list).</description>
  </option>
  <option name="resources" short="r" version="3.4">
    <description arg="file">load the given resources file on startup instead of looking for a valid resources file in the standard locations.</description>
  </option>
  <option name="help" short="h" version="3.0">
    <description>show this little help.</description>
  </option>
  <option name="geometry" short="g" version="3.0">
    <description arg="&lt;w&gt;x&lt;h&gt;" default="600x600">specify the size of the rendering window, the size argument must have the following format: &lt;width&gt;x&lt;height&gt; with positive non null values.</description>
  </option>
  <option name="spin-file" short="s" version="3.1">
    <description arg="file">use the given argument as a spin indicator. If this option is used, V_Sim switches automatically to spin rendering whatever method is specified in the parameter file.</description>
  </option>
  <option name="hiding-mode" short="m" version="3.2">
    <description arg="id" default="0">policy used to show or not null modulus spins possible values are positives.</description>
  </option>
  <option name="spin-and-atomic" short="a" version="3.3">
    <description>always draws atomic rendering on node position in addition to spin rendering.</description>
  </option>
  <option name="colorize" short="c" version="3.1">
    <description arg="file">the argument fileToRender must be called, then the given file of the option is used to colorize the elements.</description>
  </option>
  <option name="use-column" short="u" version="3.1">
    <description arg="l:m:n">it specifies the columns to use from the data file for the three colour channels [l;m;n]. Columns are counted from 1. Use -3, -2, -1 and 0 to use the special values, constant 1, coord. x, coord. y, and coord. z, respectively.</description>
  </option>
  <option name="color-preset" short="d" version="3.1">
    <description arg="id">this option can be used with the &apos;--colorize&apos; one or the &apos;--build-map&apos; one. It chooses a preset color scheme. The id argument is an integer that corresponds to a defined color shade (ranging from 0).</description>
  </option>
  <option name="translate" short="t" version="3.3">
    <description arg="x:y:z">a file must be loaded. It applies the given translations to the loaded file. The units are those of the file. This is available for periodic file formats only.</description>
  </option>
  <option name="expand" short="x" version="3.4">
    <description arg="x:y:z">a file must be loaded. It applies the given expansion to the loaded file. The values are given in box coordinates. This is available for periodic file formats only.</description>
  </option>
  <option name="planes" short="p" version="3.2">
    <description arg="file">the argument fileToRender must be called, then the given file of the option is parsed as a list of planes and they are rendered.</description>
  </option>
  <option name="scalar-field" short="f" version="3.3">
    <description arg="file">the argument fileToRender must be called, then the given file of the option is parsed as a scalar field and loaded.</description>
  </option>
  <option name="iso-values" short="v" version="3.3">
    <description arg="v[:v]">must be used with the &apos;--scalar-field&apos; option, then the given surfaces are built and rendered. If a name is appended to a value using &apos;#&apos; as a separator, this name is used as the name for the iso-surface (i.e. 0.25#Blue).</description>
  </option>
  <option name="iso-surfaces" short="i" version="3.2">
    <description arg="file">the argument fileToRender must be given, then the given file of the option is parsed and surfaces are rendered.</description>
  </option>
  <option name="build-map" short="b" version="3.4">
    <description arg="id[:id]">the argument fileToRender must be given, as the &apos;--planes&apos;, &apos;--color-preset&apos; and &apos;--scalar-field&apos; options used, then the given plane &apos;id&apos; is replaced by a coloured map using given scalar field and shade. &apos;id&apos; ranges from 0. If several ids are given, several maps are built.</description>
  </option>
  <option name="log-scale" version="3.4">
    <description arg="id">select the scaling method to use with gradients (0: linear, 1: log scaled and 2 is zero-centred log scale), default is linear scale.</description>
  </option>
  <option name="n-iso-lines" short="n" version="3.4">
    <description arg="val">when positive, val isolines are plotted on the coloured map.</description>
  </option>
  <option name="color-iso-lines" version="3.5">
    <description arg="[R:G:B] or auto" default="[0:0:0]">when given, generated iso-lines are colourised [R:G:B] or auto with the values. The specific value &apos;auto&apos; will produced iso-lines in inversed colours.</description>
  </option>
  <option name="fit-to-box" version="3.3">
    <description arg="val" default="TRUE">if val is not TRUE, the surfaces use their own bounding box.</description>
  </option>
  <option name="bg-image" version="3.4">
    <description arg="file">draw the given image on the background.</description>
  </option>
  <option name="option" short="o" version="3.3">
    <description arg="id=value">this is a generic way to give extended option. to V_Sim. As much as -o can be used. Each one store a key and its value (boolean, integer or float).</description>
  </option>
  <option name="window-mode" short="w" version="3.5">
    <description arg="mode" default="classic">used to choose the windowing mode. By default the command panel and the rendering window are separated. In the &apos;oneWindow&apos; mode they are joined. In the &apos;renderOnly&apos; mode, the command panel is not used.</description>
  </option>
  <option name="i-set" version="3.5">
    <description arg="i" default="0">this flag is used to choose the id of the loaded file if the format has support for multiple ids in one file (see XYZ format or -posi.d3 ones).</description>
  </option>
  <option name="value-file" version="3.5">
    <description arg="file">specify an XML file with some value information for V_Sim, like a list of planes, highlighted nodes... It replaces and extend the previous --planes option.</description>
  </option>
  <option name="map-precision" version="3.5">
    <description arg="prec" default="100">Give the precision in percent to render the coloured map.</description>
  </option>
  <option name="map-clamp" version="3.6">
    <description arg="min:max or auto" default="auto">Set the minimum and maximum values for the coloured map rendering.</description>
  </option>
  <option name="introspect-dump" version="3.6">
    <description arg="fileToDump">Dump object signals and properties for introspection.</description>
  </option>
  <option name="color-clamp" version="3.7">
    <description arg="col#min:max or auto" default="auto">Range to adjust values into for colourisation. col specifiesthe column to apply the range to. Use -2, -1 and 0 for x, yand z directions respectively.</description>
  </option>
  <option name="scaling-column" version="3.7">
    <description arg="id">used with a data file (see -c), it specifies the column id to be used to scale the nodes.</description>
  </option>
</commandLine>
