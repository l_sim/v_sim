#!/bin/sh

echo "internationalisation"
intltoolize --force --copy
echo "libtoolize"
libtoolize --copy --force
echo "aclocal"
aclocal -I m4
echo "gtkdocize --flavour=no-tmpl"
gtkdocize --docdir Documentation/reference --flavour no-tmpl --copy || exit 1
echo "autoheader"
autoheader
echo "automake"
automake --add-missing --copy --force-missing
echo "autoconf"
autoconf

