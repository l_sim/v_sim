/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2011)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors: Damien CALISTE, L_Sim laboratory, (2001-2011)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at
        Documentation/licence.en.txt.
*/

#include <string.h>

#include <support.h>
#include <visu_basic.h>
#include <visu_rendering.h>
#include <visu_data.h>
#include <renderingMethods/renderingAtomic_ascii.h>
#include <coreTools/toolMatrix.h>
#include <coreTools/toolPhysic.h>
#include <extraFunctions/scalarFields.h>
#include <extraGtkFunctions/gtk_toolPanelWidget.h>
#include <extraGtkFunctions/gtk_numericalEntryWidget.h>
#include <extraGtkFunctions/gtk_elementComboBox.h>
#include <panelModules/panelSurfaces.h>
#include <gtk_main.h>
#include <visu_gtk.h>

#include <bigdft.h>

#include "bigdftplug.h"

enum
  {
    LR_OBJECT,
    LR_LABEL,
    LR_ID,
    LR_SHOW_GRID_C,
    LR_SHOW_GRID_F,
    LR_COLOR,
    LR_N_COLUMNS
  };
static GtkListStore *lrdata;

/* Local methods. */
static void update_lzd(BigDFT_Lzd *lzd);

/* Callbacks. */
static void onDataLoaded(GObject *obj, VisuData *dataObj, gpointer data);
static void onGridToggled(GtkCellRendererToggle *cell_renderer, gchar *path,
                          gpointer user_data);

void bdft_system_init()
{
  lrdata = gtk_list_store_new(LR_N_COLUMNS, G_TYPE_OBJECT, G_TYPE_STRING,
                              G_TYPE_UINT, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN,
                              GDK_TYPE_PIXBUF);

  g_signal_connect(VISU_OBJECT_INSTANCE, "dataLoaded",
                   G_CALLBACK(onDataLoaded), NULL);
}
static void onLzdDefined(BigDFT_Lzd *lzd, gpointer data _U_)
{
  update_lzd(lzd);
}
static void onDataLoaded(GObject *obj _U_, VisuData *dataObj, gpointer data _U_)
{
  BigDFT_Wf *wf;

  if (!dataObj)
    return;

  wf = (BigDFT_Wf*)g_object_get_data(G_OBJECT(dataObj), "BigDFT_wf");
  update_lzd(NULL);
  if (wf)
    g_signal_connect(G_OBJECT(wf->lzd), "defined",
                     G_CALLBACK(onLzdDefined), (gpointer)0);

  g_debug("BigDFT system: 'DataLoaded' signal OK.");
}

GtkWidget* bdft_system_tab(BigDFT_Lzd *lzd)
{
  GtkWidget *wd, *ct, *vbox;
  GtkCellRenderer *render;
  GtkTreeViewColumn *col;

  vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);

  ct = gtk_scrolled_window_new((GtkAdjustment*)0, (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(ct),
                                 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(ct),
				      GTK_SHADOW_ETCHED_IN);
  gtk_box_pack_start(GTK_BOX(vbox), ct, TRUE, TRUE, 0);
  wd = gtk_tree_view_new_with_model(GTK_TREE_MODEL(lrdata));
  gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(wd), FALSE);
  gtk_container_add(GTK_CONTAINER(ct), wd);
  /* Columns. */
  render = gtk_cell_renderer_text_new();
  col = gtk_tree_view_column_new_with_attributes("", render, "markup", LR_LABEL, NULL);
  gtk_tree_view_column_set_expand(col, TRUE);
  render = gtk_cell_renderer_pixbuf_new();
  gtk_tree_view_column_pack_end(col, render, FALSE);
  gtk_tree_view_column_add_attribute(col, render, "pixbuf", LR_COLOR);
  gtk_tree_view_append_column(GTK_TREE_VIEW(wd), col);
  render = gtk_cell_renderer_toggle_new();
  g_signal_connect(G_OBJECT(render), "toggled",
                   G_CALLBACK(onGridToggled), (gpointer)LR_SHOW_GRID_C);
  col = gtk_tree_view_column_new_with_attributes("", render, "active", LR_SHOW_GRID_C, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(wd), col);
  render = gtk_cell_renderer_toggle_new();
  g_signal_connect(G_OBJECT(render), "toggled",
                   G_CALLBACK(onGridToggled), (gpointer)LR_SHOW_GRID_F);
  col = gtk_tree_view_column_new_with_attributes("", render, "active", LR_SHOW_GRID_F, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(wd), col);

  update_lzd(lzd);

  return vbox;
}

static void update_lzd(BigDFT_Lzd *lzd)
{
  GtkTreeIter iter;
  gchar *lbl;
  guint i;
  GdkPixbuf *pixbuf;

  gtk_list_store_clear(lrdata);
  if (!lzd)
    return;

  /* Global region. */
  lbl = g_strdup_printf("Global region\n<span size=\"smaller\">"
                        "   %d / %d (coarse / fine grid pts)</span>",
                        lzd->parent.nvctr_c, lzd->parent.nvctr_f);
  gtk_list_store_append(lrdata, &iter);
  gtk_list_store_set(lrdata, &iter, LR_OBJECT, &lzd->parent,
                     LR_LABEL, lbl, LR_ID, 0, LR_SHOW_GRID_C,
                     FALSE, LR_SHOW_GRID_F, FALSE, -1);
  g_free(lbl);
  /* Local regions. */
  if (lzd->nlr > 1)
    for (i = 0; i < lzd->nlr; i++)
      {
        lbl = g_strdup_printf("Local region %d\n<span size=\"smaller\">"
                              "   %d / %d (coarse / fine grid pts)</span>",
                              i + 1, lzd->Llr[i]->nvctr_c, lzd->Llr[i]->nvctr_f);
        pixbuf = tool_color_get_stamp(tool_color_new_bright(i), FALSE);
        gtk_list_store_append(lrdata, &iter);
        gtk_list_store_set(lrdata, &iter, LR_OBJECT, lzd->Llr[i],
                           LR_LABEL, lbl, LR_ID, i + 1, LR_COLOR, pixbuf,
                           LR_SHOW_GRID_C, FALSE, LR_SHOW_GRID_F, FALSE, -1);
        g_free(lbl);
        g_object_unref(pixbuf);
      }
}

static void onGridToggled(GtkCellRendererToggle *cell_renderer, gchar *path,
                          gpointer user_data)
{
  GtkTreePath* tpath = gtk_tree_path_new_from_string(path);
  GtkTreeIter iter;
  gboolean valid;
  BigDFT_Locreg *lr;
  GArray *gcoord;
  BigDFT_Wf *wf;
  BigDFT_LocregIter iseg;
  float vect[3];
  guint nseg, tseg, ilr;
  VisuData *dataObj;
  VisuGlView *view;
  gchar *lbl;
  
  valid = gtk_tree_model_get_iter(GTK_TREE_MODEL(lrdata), &iter, tpath);
  gtk_tree_path_free(tpath);
  g_return_if_fail(valid);

  dataObj = visu_ui_panel_getData(VISU_UI_PANEL(panelBigDFT));
  view = visu_ui_panel_getView(VISU_UI_PANEL(panelBigDFT));
  g_return_if_fail(dataObj && view);
  wf = (BigDFT_Wf*)g_object_get_data(G_OBJECT(dataObj), "BigDFT_wf");
  g_return_if_fail(wf);

  gtk_tree_model_get(GTK_TREE_MODEL(lrdata), &iter,
                     LR_OBJECT, &lr, LR_ID, &ilr, -1);
  
  if (GPOINTER_TO_INT(user_data) == LR_SHOW_GRID_C)
    {
      nseg = lr->nseg_c;
      tseg = GRID_COARSE;
      lbl = "g";
    }
  else
    {
      nseg = lr->nseg_f;
      tseg = GRID_FINE;
      lbl = "G";
    }

  valid = !gtk_cell_renderer_toggle_get_active(cell_renderer);
  gtk_list_store_set(lrdata, &iter, GPOINTER_TO_INT(user_data), valid, -1);
  if (valid)
    {
      gcoord = g_array_sized_new(FALSE, FALSE, sizeof(gfloat) * 3, 2 * nseg);
      for (valid = bigdft_lzd_iter_new(wf->lzd, &iseg, tseg, ilr); valid;
           valid = bigdft_lzd_iter_next(&iseg))
        {
          vect[0] = iseg.x0;
          vect[1] = iseg.y;
          vect[2] = iseg.z;
          g_array_append_val(gcoord, vect);
          if (iseg.x1 != iseg.x0)
            {
              vect[0] = iseg.x1;
              vect[1] = iseg.y;
              vect[2] = iseg.z;
              g_array_append_val(gcoord, vect);
            }
        }
    }
  else
    gcoord = g_array_sized_new(FALSE, FALSE, sizeof(gfloat) * 3, 1);

  bigdft_show_grid(dataObj, gcoord, lbl, ilr);

  g_array_unref(gcoord);
  g_object_unref(lr);

  VISU_REDRAW_ADD;
}
