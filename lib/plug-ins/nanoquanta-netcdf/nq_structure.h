#ifndef NQ_STRUCTURE_H
#define NQ_STRUCTURE_H

#include <visu_dataloader.h>

/**
 * nqStructuralInit:
 * 
 * Routine used to create a new loading method for the ETSF file
 * format.
 *
 * Returns: a newly created rendering method.
 */
VisuDataLoader* visu_data_loader_ETSF_getStatic();

#endif
