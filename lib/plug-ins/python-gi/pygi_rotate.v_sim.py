#!/usr/bin/env python

from gi.repository import v_sim
import math, time

render = v_sim.uiMainClass_getDefaultRendering()
data = render.getVisuData()

camera = data.getOpenGLView().camera
(theta0, gross0) = (camera.theta, camera.gross)

dumps = v_sim.dump_getAllModules()
for dump in dumps:
  if dump.fileType.match("a.png"):
    format = dump

NB = 100.
for i in range(0, int(NB) + 1):
  data.setAngleOfView(theta0 + 360. * i / NB, 0, 0, v_sim.CAMERA_THETA)
  data.setZoomOfView(gross0 + 0.5 * math.sin(i * math.pi * 2. / NB))
  v_sim.object_redraw("My script")
  #time.sleep(0.01)
  #render.dump(format, "rotation%03d.png" % i, 450, 450, None, None)

