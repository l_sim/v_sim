# -*- Mode: Python; py-indent-offset: 2 -*-
# -*- coding: utf-8 -*-
# vim: tabstop=2 shiftwidth=2 expandtab
# vim: set fileencoding=utf-8
#
# EXTRAITS DE LA LICENCE
#    Copyright CEA, contributeurs : Damien CALISTE, (2001-2010)
#
#    Adresse mèl :
#    CALISTE, damien P caliste AT cea P fr.
#
#    Ce logiciel est un programme informatique servant à visualiser des
#    structures atomiques dans un rendu pseudo-3D. 
#
#    Ce logiciel est régi par la licence CeCILL soumise au droit français et
#    respectant les principes de diffusion des logiciels libres. Vous pouvez
#    utiliser, modifier et/ou redistribuer ce programme sous les conditions
#    de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
#    sur le site "http://www.cecill.info".
#
#    Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
#    pris connaissance de la licence CeCILL, et que vous en avez accepté les
#    termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).


# LICENCE SUM UP
#    Copyright CEA, contributors: Damien CALISTE, (2001-2010)
#
#    E-mail address:
#    CALISTE, damien P caliste AT cea P fr.
#
#    This software is a computer program whose purpose is to visualize atomic
#    configurations in 3D.
#
#    This software is governed by the CeCILL  license under French law and
#    abiding by the rules of distribution of free software.  You can  use, 
#    modify and/ or redistribute the software under the terms of the CeCILL
#    license as circulated by CEA, CNRS and INRIA at the following URL
#    "http://www.cecill.info". 
#
#    The fact that you are presently reading this means that you have had
#    knowledge of the CeCILL license and that you accept its terms. You can
#    find a copy of this licence shipped with this software at
#    Documentation/licence.en.txt.

from ..module import get_introspection_module

v_sim = get_introspection_module('v_sim')

class VisuNodeArrayIter:
  def __init__(self, it):
    self.iter = it

  def __iter__(self):
    return self

  def next(self):
    if not(self.iter.next()):
      raise StopIteration
    return self.iter

def __NodeArrayIter__(it):
  return VisuNodeArrayIter(it)
setattr(v_sim.NodeArrayIter, "__iter__", __NodeArrayIter__)

class VisuNodeArrayIter2:
  def __init__(self, nodes):
    self.iter1 = nodes.iter_new()
    self.iter2 = nodes.iter_new()

  def __iter__(self):
    return self

  def next(self):
    if not(self.iter1.next2(self.iter2)):
      raise StopIteration
    return (self.iter1, self.iter2)

def newIt(nodes, kind = v_sim.NodeArrayIterType.NODES_BY_TYPE, elename = None):
  it = nodes.iter_new()
  it.type = kind
  if elename is not None:
    it.element = v_sim.Element.lookup(elename)
  return it
def __NodeArrayIter__(nodes):
  return VisuNodeArrayIter(newIt(nodes))
def __NodeArrayIterByNumber__(nodes):
  return VisuNodeArrayIter(newIt(nodes, v_sim.NodeArrayIterType.NODES_BY_NUMBER))
def __NodeArrayIterElements__(nodes):
  return VisuNodeArrayIter(newIt(nodes, v_sim.NodeArrayIterType.ELEMENTS))
def __NodeArrayIterVisible__(nodes):
  return VisuNodeArrayIter(newIt(nodes, v_sim.NodeArrayIterType.NODES_VISIBLE))
def __NodeArrayIterOriginal__(nodes):
  return VisuNodeArrayIter(newIt(nodes, v_sim.NodeArrayIterType.NODES_ORIGINAL))
def __NodeArrayIterForElement__(nodes, elename):
  return VisuNodeArrayIter(newIt(nodes, v_sim.NodeArrayIterType.NODES_FOR_ELEMENT, elename = elename))
def __NodeArrayIterByPairs__(nodes):
  return VisuNodeArrayIter2(nodes)


def __DataAllNodePositions__(data):
  nodes = []
  for it in data:
    nodes.append((data.getNodeCoordinates(it.node), \
                  it.node.number, it.element.getName()))
  return nodes

setattr(v_sim.NodeArray, "__iter__", __NodeArrayIter__)
setattr(v_sim.NodeArray, "__iterByNumber__", __NodeArrayIterByNumber__)
setattr(v_sim.NodeArray, "__iterElements__", __NodeArrayIterElements__)
setattr(v_sim.NodeArray, "__iterVisible__", __NodeArrayIterVisible__)
setattr(v_sim.NodeArray, "__iterOriginal__", __NodeArrayIterOriginal__)
setattr(v_sim.NodeArray, "__iterForElement__", __NodeArrayIterForElement__)
setattr(v_sim.NodeArray, "__iterByPairs__", __NodeArrayIterByPairs__)
setattr(v_sim.Data, "getAllNodePositions", __DataAllNodePositions__)

def __SceneShow__(scene, withToolbar = False):
  from gi.repository import GLib, Gtk

  try:
    v_sim.basic_initConfigFiles()
  except:
    pass
  
  r = v_sim.UiRenderingWindow.new_withGlScene(scene, withToolbar)

  w = Gtk.Window.new(Gtk.WindowType.TOPLEVEL)
  w.add(r)
  w.show_all()

  l = GLib.MainLoop.new(None, False)
  w.connect_object("destroy", GLib.MainLoop.quit, l)
  l.run()

setattr(v_sim.GlNodeScene, "show", __SceneShow__)

try:
  v_sim.basic_init()
  v_sim.plugins_init()
except:
  pass
