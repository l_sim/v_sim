/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)

	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D.

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use,
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info".

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "toolDbg.h"

#include <config.h>

#include <stdio.h>
#if DEBUG == 1 && PLATFORM_X11 == 1
#define __USE_GNU
#include <dlfcn.h>
#endif

/**
 * SECTION: toolDbg
 * @short_description: A wrapper around #GObject for debugging
 * purposes
 *
 * <para>This is a thin wrapper around #GObject to count allocation
 * and deallocation and check memory leaks of objects created by V_Sim.</para>
 *
 * Since: 3.8
 */

/**
 * ToolDbgObj:
 *
 * Opaque structure used to encapsulate #GObject with debug counters.
 *
 * Since: 3.8
 */

static GHashTable *_objSet;

static void tool_dbg_obj_finalize(GObject* obj);

G_DEFINE_TYPE(ToolDbgObj, tool_dbg_obj, G_TYPE_OBJECT)

static void tool_dbg_obj_class_init(ToolDbgObjClass *klass)
{
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->finalize = tool_dbg_obj_finalize;

  _objSet = g_hash_table_new(g_direct_hash, g_direct_equal);
}

static void tool_dbg_obj_init(ToolDbgObj *obj)
{
  g_hash_table_add(_objSet, obj);
}

static void tool_dbg_obj_finalize(GObject* obj)
{
  g_hash_table_remove(_objSet, obj);

  G_OBJECT_CLASS(tool_dbg_obj_parent_class)->finalize(obj);
}

/**
 * tool_dbg_obj_class_summarize:
 *
 * Debug function displaying the currently in-memory #ToolDbgObj.
 *
 * Since: 3.8
 **/
void tool_dbg_obj_class_summarize(void)
{
  GHashTableIter iter;
  gpointer key, value;

  g_debug("Number of allocated GObjects: %d", g_hash_table_size(_objSet));

  g_hash_table_iter_init(&iter, _objSet);
  while (g_hash_table_iter_next(&iter, &key, &value))
    g_debug("- %p: %s (%d ref counts)", key, G_OBJECT_TYPE_NAME(value), G_OBJECT(value)->ref_count);
}

#if DEBUG == 1 && PLATFORM_X11 == 1

#pragma GCC diagnostic push    /* Save actual diagnostics state */
#pragma GCC diagnostic ignored "-Wpedantic"    /* Disable pedantic */
#undef g_object_ref
gpointer g_object_ref(gpointer obj)
{
  static gpointer (*ref)(gpointer);

  if (!ref)
    ref = (gpointer (*)(gpointer))dlsym(RTLD_NEXT, "g_object_ref");
  if (TOOL_IS_DBG_OBJ(obj))
    g_debug("Tool Dbg: ref %p (%d).", obj, G_OBJECT(obj)->ref_count);
  return ref(obj);
}

void g_object_unref(gpointer obj)
{
  static void (*unref)(gpointer);

  if (TOOL_IS_DBG_OBJ(obj))
    g_debug("Tool Dbg: unref %p (%d).", obj, G_OBJECT(obj)->ref_count);
  if (!unref)
    unref = (void (*)(gpointer))dlsym(RTLD_NEXT, "g_object_unref");
  unref(obj);
}
#pragma GCC diagnostic pop    /* Restore diagnostics state */

#endif
