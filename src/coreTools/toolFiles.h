/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresses mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail addresses :
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef TOOLFILES_H
#define TOOLFILES_H

#include <glib-object.h>

#include "visu_tools.h"

/**
 * TOOL_TYPE_FILES:
 *
 * return the type of #ToolFiles.
 */
#define TOOL_TYPE_FILES	     (tool_files_get_type ())
/**
 * FILES:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #ToolFiles type.
 */
#define TOOL_FILES(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, TOOL_TYPE_FILES, ToolFiles))
/**
 * TOOL_FILES_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #ToolFilesClass.
 */
#define TOOL_FILES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, TOOL_TYPE_FILES, ToolFilesClass))
/**
 * TOOL_IS_FILES:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #ToolFiles object.
 */
#define TOOL_IS_FILES(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, TOOL_TYPE_FILES))
/**
 * TOOL_IS_FILES_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #ToolFilesClass class.
 */
#define TOOL_IS_FILES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, TOOL_TYPE_FILES))
/**
 * TOOL_FILES_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define TOOL_FILES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, TOOL_TYPE_FILES, ToolFilesClass))

typedef struct _ToolFiles        ToolFiles;
typedef struct _ToolFilesPrivate ToolFilesPrivate;
typedef struct _ToolFilesClass   ToolFilesClass;

/**
 * ToolFiles:
 *
 * An opaque structure for the scalar field.
 */
struct _ToolFiles
{
  VisuObject parent;

  ToolFilesPrivate *priv;
};

/**
 * ToolFilesClass:
 * @parent: the parent class.
 *
 * An opaque structure for the class.
 */
struct _ToolFilesClass
{
  VisuObjectClass parent;
};

/**
 * tool_scalar_field_operator_get_type:
 *
 * This method returns the type of #ToolFiles, use TOOL_TYPE_FILES instead.
 *
 * Returns: the type of #ToolFiles.
 */
GType tool_files_get_type(void);

ToolFiles* tool_files_new();

gboolean tool_files_open(ToolFiles *file, const gchar *filename, GError **error);
void tool_files_fromMemory(ToolFiles *file, const gchar *data);
void tool_files_setEncoding(ToolFiles *file, const gchar *encoding);

GIOStatus tool_files_read(ToolFiles *file, void *buffer, gsize count, GError **error);
GIOStatus tool_files_skip(ToolFiles *file, gsize count, GError **error);
GIOStatus tool_files_read_line_string(ToolFiles *file, GString *buffer,
                                      gsize *terminator_pos, GError **error);

gboolean tool_files_atEnd(ToolFiles *file);
GIOStatus tool_files_rewind(ToolFiles *file, GError **error);

gboolean tool_files_supportCompression(void);

#endif
