/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeur : Damien CALISTE,
	laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien CALISTE,
	laboratoire L_Sim, (2001-2005)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef DUMPTHROUGHGDKPIXBUF_H
#define DUMPTHROUGHGDKPIXBUF_H

#include "glDump.h"

/**
 * visu_dump_png_getStatic:
 *
 * This routine returns the PNG dump object.
 *
 * Returns: (transfer none): a newly created dump object to create PNG files.
 */
const VisuDump* visu_dump_png_getStatic();
/**
 * visu_dump_jpeg_getStatic:
 *
 * This routine returns the JPG dump object.
 *
 * Returns: (transfer none): a newly created dump object to create JPEG files.
 */
const VisuDump* visu_dump_jpeg_getStatic();

#endif
