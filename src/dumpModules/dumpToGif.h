/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef DUMPTOGIF_H
#define DUMPTOGIF_H

#include "glDump.h"

/**
 * dumpToGif_init:
 *
 * This routine should not be used since it inialised the module
 * and is already called when V_Sim is launched.
 *
 * Returns: (transfer none): a newly created dump object to create GIF files.
 */
const VisuDump* dumpToGif_init();

/**
 * ColorPacket:
 * @red: red value ;
 * @green: green value ;
 * @blue: blue value ;
 * @index: ??.
 *
 * A private structure used by the GIF conversion. It is usefull for all
 * convertion from RGB to color table.
 */
typedef struct _ColorPacket {
  unsigned char red, green, blue;
  unsigned short index;
} ColorPacket;

/**
 * Image:
 * @columns: number of columns ;
 * @rows: number of rows ;
 * @colors: number of colours ;
 * @colormap: an array of colours ;
 * @pixels: the definition for each pixels ;
 * @packet: ?? ;
 * @packets: ??.
 *
 * A private structure used by the GIF conversion. It is usefull for all
 * convertion from RGB to color table.
 */
typedef struct _Image {
  guint columns, rows, colors;
  ColorPacket *colormap, *pixels, *packet;
  unsigned long packets;    
} Image;

/**
 * dumpToGif_quantizeImage:
 * @number_colors: the number of desired colours ;
 * @error: an error location ;
 * @functionWait: a method that will be called during the quantize process ;
 * @data: an argument to give to the wait function.
 *
 * This routine transform an RGB image to an indexed colours image. To set the image
 * to quantize, use setImage().
 *
 * Returns: 0 if everithing went right.
 */
guint dumpToGif_quantizeImage(guint number_colors, GError **error,
				     ToolVoidDataFunc functionWait, gpointer data);
/**
 * dumpToGif_syncImage:
 *
 * Do something in the GIF exoprt process.
 */
void dumpToGif_syncImage(void);
/**
 * dumpToGif_setImage:
 * @data: a #Image structure.
 *
 * Set the image to be manipulated by the GIF convertor (see
 * dumpToGif_quantizeImage()).
 */
void dumpToGif_setImage(Image *data);


#endif
