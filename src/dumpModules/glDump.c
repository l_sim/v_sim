/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "glDump.h"

/**
 * SECTION:glDump
 * @short_description: A generic class defining interface to export
 * #VisuGlNodeScene into image formats.
 *
 * <para>Instance of this class can be used to export any #VisuGlNodeScene
 * into image formats.</para>
 */

struct _VisuDumpScenePrivate
{
  VisuDumpSceneWriteFunc writeFunc;
  gboolean hasAlpha;
};

/* Local callbacks */

G_DEFINE_TYPE_WITH_CODE(VisuDumpScene, visu_dump_scene, VISU_TYPE_DUMP,
                        G_ADD_PRIVATE(VisuDumpScene))

static void visu_dump_scene_class_init(VisuDumpSceneClass *klass _U_)
{
  g_debug("Visu Node Scene: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */
}

static void visu_dump_scene_init(VisuDumpScene *self)
{
  self->priv = visu_dump_scene_get_instance_private(self);
  self->priv->writeFunc = NULL;
  self->priv->hasAlpha = FALSE;
}

/**
 * visu_dump_scene_new:
 * @descr: a description.
 * @patterns: (array zero-terminated=1): a list of file patterns.
 * @method: (scope call): the write method.
 * @hasAlpha: a boolean.
 *
 * Creates a new #VisuDumpScene object.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuDumpScene object.
 **/
VisuDumpScene* visu_dump_scene_new(const gchar* descr, const gchar** patterns,
                                   VisuDumpSceneWriteFunc method, gboolean hasAlpha)
{
  VisuDumpScene *dump;

  dump = VISU_DUMP_SCENE(g_object_new(VISU_TYPE_DUMP_SCENE,
                                     "name", descr, "ignore-type", FALSE, NULL));
  tool_file_format_setPatterns(TOOL_FILE_FORMAT(dump), patterns);
  dump->priv->writeFunc = method;
  dump->priv->hasAlpha = hasAlpha;
  
  return dump;
}

/**
 * visu_dump_scene_getAlphaStatus:
 * @dump: a #VisuDump method.
 *
 * Retrieve if @dump use alpha channel or not.
 *
 * Returns: TRUE if @dump has an alpha channel.
 **/
gboolean visu_dump_scene_getAlphaStatus(VisuDumpScene *dump)
{
  g_return_val_if_fail(VISU_IS_DUMP_SCENE(dump), FALSE);

  return dump->priv->hasAlpha;
}

/**
 * visu_dump_scene_write:
 * @dump: a #VisuDump object ;
 * @fileName: (type filename): a string that defined the file to write
 * to ;
 * @scene: the #VisuGlNodeScene to be exported.
 * @width: an integer ;
 * @height: an integer ;
 * @functionWait: (allow-none) (closure data) (scope call): a method
 * to call periodically during the dump ;
 * @data: (closure): some pointer on object to be passed to the wait
 * function.
 * @error: a location to store some error (not NULL) ;
 *
 * Use the write function of @dump to export the @scene to file @fileName.
 *
 * Since: 3.6
 *
 * Returns: TRUE if dump succeed.
 */
gboolean visu_dump_scene_write(VisuDumpScene *dump, const char* fileName,
                               VisuGlNodeScene *scene, guint width, guint height,
                               ToolVoidDataFunc functionWait, gpointer data, GError **error)
{
  gboolean res;

  g_return_val_if_fail(VISU_IS_DUMP_SCENE(dump) && dump->priv->writeFunc, FALSE);

  visu_gl_addHint(VISU_GL(scene), VISU_GL_OFFSCREEN);
  res = dump->priv->writeFunc(TOOL_FILE_FORMAT(dump), fileName,
                              scene, width, height, error, functionWait, data);
  visu_gl_removeHint(VISU_GL(scene), VISU_GL_OFFSCREEN);
  
  return res;
}
