/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DUMP_SCENE_H
#define VISU_DUMP_SCENE_H

#include <glib.h>
#include <glib-object.h>

#include <visu_dump.h>
#include <visu_glnodescene.h>

G_BEGIN_DECLS

/***************/
/* Public part */
/***************/

/**
 * VisuDumpSceneWriteFunc:
 * @format: a #ToolFileFormat object, corresponding to the write method ;
 * @fileName: a string that defined the file to write to ;
 * @scene: the #VisuGlNodeScene to be exported ;
 * @width: the desired width.
 * @height: the desired height.
 * @error: (allow-none): a location to store some error (not NULL) ;
 * @functionWait: (allow-none) (scope call): a method to call
 * periodically during the dump ;
 * @data: (closure): some pointer on object to be passed to the wait function.
 *
 * This is a prototype of a method implemented by a dumping extension that is called
 * when the current rendering must be dumped to a file.
 *
 * Returns: TRUE if everything went right.
 */
typedef gboolean (*VisuDumpSceneWriteFunc) (ToolFileFormat *format, const char* fileName,
                                            VisuGlNodeScene *scene, guint width, guint height,
                                            GError **error, ToolVoidDataFunc functionWait, gpointer data);

/**
 * VISU_TYPE_DUMP_SCENE:
 *
 * return the type of #VisuDumpScene.
 */
#define VISU_TYPE_DUMP_SCENE	     (visu_dump_scene_get_type ())
/**
 * VISU_DUMP_SCENE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDumpScene type.
 */
#define VISU_DUMP_SCENE(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DUMP_SCENE, VisuDumpScene))
/**
 * VISU_DUMP_SCENE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDumpSceneClass.
 */
#define VISU_DUMP_SCENE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DUMP_SCENE, VisuDumpSceneClass))
/**
 * VISU_IS_DUMP_SCENE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDumpScene object.
 */
#define VISU_IS_DUMP_SCENE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DUMP_SCENE))
/**
 * VISU_IS_DUMP_SCENE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDumpSceneClass class.
 */
#define VISU_IS_DUMP_SCENE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DUMP_SCENE))
/**
 * VISU_DUMP_SCENE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DUMP_SCENE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DUMP_SCENE, VisuDumpSceneClass))

/**
 * VisuDumpScenePrivate:
 * 
 * Private data for #VisuDumpScene objects.
 */
typedef struct _VisuDumpScenePrivate VisuDumpScenePrivate;

/**
 * VisuDumpScene:
 * 
 * Common name to refer to a #_VisuDumpScene.
 */
typedef struct _VisuDumpScene VisuDumpScene;
struct _VisuDumpScene
{
  VisuDump parent;

  VisuDumpScenePrivate *priv;
};

/**
 * VisuDumpSceneClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuDumpSceneClass.
 */
typedef struct _VisuDumpSceneClass VisuDumpSceneClass;
struct _VisuDumpSceneClass
{
  VisuDumpClass parent;
};

/**
 * visu_dump_scene_get_type:
 *
 * This method returns the type of #VisuDumpScene, use
 * VISU_TYPE_DUMP_SCENE instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuDumpScene.
 */
GType visu_dump_scene_get_type(void);

VisuDumpScene* visu_dump_scene_new(const gchar* descr, const gchar** patterns,
                                   VisuDumpSceneWriteFunc method, gboolean hasAlpha);
gboolean visu_dump_scene_getAlphaStatus(VisuDumpScene *dump);

gboolean visu_dump_scene_write(VisuDumpScene *dump, const char* fileName,
                               VisuGlNodeScene *scene, guint width, guint height,
                               ToolVoidDataFunc functionWait, gpointer data, GError **error);

G_END_DECLS

#endif
