/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2013)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "frame.h"

#include <string.h>

#include <epoxy/gl.h>

#include <coreTools/toolColor.h>
#include <coreTools/toolWriter.h>

/**
 * SECTION:frame
 * @short_description: Draw a frame with the representation of a color frame.
 *
 * <para>This extension draws a frame on top of the rendering area
 * with a color frame. One can setup printed values and draw
 * additional marks inside the frame.</para>
 *
 * Since: 3.7
 */

/**
 * VisuGlExtFrameClass:
 * @parent: the parent class;
 * @render: the render method for the content of this frame.
 * @draw: the draw method for the content of this frame.
 *
 * A short way to identify #_VisuGlExtFrameClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtFrame:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtFramePrivate:
 *
 * Private fields for #VisuGlExtFrame objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtFramePrivate
{
  gboolean dispose_has_run;

  /* Rendering parameters. */
  guint requisition[2];
  float scale;
  float xpos, ypos, xmargin, ymargin, xpad, ypad;
  float bgRGBA[4];
  gchar *title;
  guint xoffset, yoffset;
  guint frameBuf, titleTex;

  /* Object dependencies. */
  VisuGlView *view;
  gulong widthHeight_signal;
};

enum
  {
    PROP_0,
    XPOS_PROP,
    YPOS_PROP,
    XPAD_PROP,
    YPAD_PROP,
    XREQ_PROP,
    YREQ_PROP,
    TITLE_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

static int refMask[4] = {TOOL_COLOR_MASK_R, TOOL_COLOR_MASK_G,
                         TOOL_COLOR_MASK_B, TOOL_COLOR_MASK_A};
static float bgRGBADefault[4] = {1.f, 1.f, 1.f, 0.4f};
static float fontRGBDefault[4] = {0.f, 0.f, 0.f, 1.};
static float xmarginDefault = 10.f, ymarginDefault = 10.f;
static float xpadDefault = 5.f, ypadDefault = 5.f;

static GObject* visu_gl_ext_frame_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_frame_finalize(GObject* obj);
static void visu_gl_ext_frame_dispose(GObject* obj);
static void visu_gl_ext_frame_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec);
static void visu_gl_ext_frame_set_property(GObject* obj, guint property_id,
                                           const GValue *value, GParamSpec *pspec);
static void visu_gl_ext_frame_rebuild(VisuGlExt *ext);
static void visu_gl_ext_frame_render(const VisuGlExt *ext);
static void visu_gl_ext_frame_render_text(const VisuGlExt *ext);
static void visu_gl_ext_frame_draw(VisuGlExt *ext);
static gboolean visu_gl_ext_frame_setGlView(VisuGlExt *frame, VisuGlView *view);

static void onViewChanged(VisuGlView *view, gpointer data);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtFrame, visu_gl_ext_frame, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtFrame))

static void visu_gl_ext_frame_class_init(VisuGlExtFrameClass *klass)
{
  g_debug("Extension Frame: creating the class of the object.");
  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_frame_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_frame_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_frame_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_frame_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_frame_get_property;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_frame_rebuild;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_frame_render;
  VISU_GL_EXT_CLASS(klass)->renderText = visu_gl_ext_frame_render_text;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_frame_draw;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_frame_setGlView;

  /**
   * VisuGlExtFrame::x-pos:
   *
   * Store the position along x of the frame.
   *
   * Since: 3.8
   */
  properties[XPOS_PROP] = g_param_spec_float("x-pos", "x position",
                                             "position along x axis",
                                             0., 1., 0.,
                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  /**
   * VisuGlExtFrame::y-pos:
   *
   * Store the position along y of the frame.
   *
   * Since: 3.8
   */
  properties[YPOS_PROP] = g_param_spec_float("y-pos", "y position",
                                             "position along y axis",
                                             0., 1., 1.,
                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  /**
   * VisuGlExtFrame::x-padding:
   *
   * Store the padding along x of the frame.
   *
   * Since: 3.8
   */
  properties[XPAD_PROP] = g_param_spec_float("x-padding", "x padding",
                                             "padding along x axis",
                                             0., 30., xpadDefault,
                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  /**
   * VisuGlExtFrame::y-padding:
   *
   * Store the padding along y of the frame.
   *
   * Since: 3.8
   */
  properties[YPAD_PROP] = g_param_spec_float("y-padding", "y padding",
                                             "padding along y axis",
                                             0., 30., ypadDefault,
                                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  /**
   * VisuGlExtFrame::title:
   *
   * Store the title for the frame.
   *
   * Since: 3.9
   */
  properties[TITLE_PROP] = g_param_spec_string("title", "Title", "title",
                                               NULL, G_PARAM_READWRITE);

  /**
   * VisuGlExtFrame::x-requisition:
   *
   * Store the desired width of the frame.
   *
   * Since: 3.9
   */
  properties[XREQ_PROP] = g_param_spec_uint("x-requisition", "desired width",
                                            "requisition along x axis",
                                            0, G_MAXUINT, 0,
                                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
  /**
   * VisuGlExtFrame::y-requisition:
   *
   * Store the desired height of the frame.
   *
   * Since: 3.9
   */
  properties[YREQ_PROP] = g_param_spec_uint("y-requisition", "desired height",
                                            "requisition along y axis",
                                            0, G_MAXUINT, 0,
                                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP,
                                    properties);
}

static GObject* visu_gl_ext_frame_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, g_value_get_uint(props[i].value) + 1);
    }

  return G_OBJECT_CLASS(visu_gl_ext_frame_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_frame_init(VisuGlExtFrame *obj)
{
  g_debug("Extension Frame: initializing a new object (%p).",
	      (gpointer)obj);

  obj->width      = 0;
  obj->height     = 0;
  obj->fontRGB[0] = fontRGBDefault[0];
  obj->fontRGB[1] = fontRGBDefault[1];
  obj->fontRGB[2] = fontRGBDefault[2];
  obj->fontRGB[3] = 1.f;
  
  obj->priv = visu_gl_ext_frame_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->requisition[0] = 0;
  obj->priv->requisition[1] = 0;
  obj->priv->scale      = 1.f;
  obj->priv->xpos       = 0.f;
  obj->priv->ypos       = 0.f;
  obj->priv->xmargin    = xmarginDefault;
  obj->priv->ymargin    = ymarginDefault;
  obj->priv->xpad       = xpadDefault;
  obj->priv->ypad       = ypadDefault;
  obj->priv->bgRGBA[0]  = bgRGBADefault[0];
  obj->priv->bgRGBA[1]  = bgRGBADefault[1];
  obj->priv->bgRGBA[2]  = bgRGBADefault[2];
  obj->priv->bgRGBA[3]  = bgRGBADefault[3];
  obj->priv->title      = g_strdup("");
  obj->priv->view               = (VisuGlView*)0;
  obj->priv->widthHeight_signal = 0;
  obj->priv->frameBuf   = G_MAXUINT;
  obj->priv->titleTex   = G_MAXUINT;
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_frame_dispose(GObject* obj)
{
  VisuGlExtFrame *frame;

  g_debug("Extension Frame: dispose object %p.", (gpointer)obj);

  frame = VISU_GL_EXT_FRAME(obj);
  if (frame->priv->dispose_has_run)
    return;
  frame->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_frame_setGlView(VISU_GL_EXT(obj), (VisuGlView*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_frame_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_frame_finalize(GObject* obj)
{
  VisuGlExtFrame *frame;

  g_return_if_fail(obj);

  g_debug("Extension Frame: finalize object %p.", (gpointer)obj);

  frame = VISU_GL_EXT_FRAME(obj);
  /* Free privs elements. */
  if (frame->priv)
    {
      g_debug("Extension Frame: free private frame.");
      g_free(frame->priv->title);
    }

  /* Chain up to the parent class */
  g_debug("Extension Frame: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_frame_parent_class)->finalize(obj);
  g_debug("Extension Frame: freeing ... OK.");
}

static void visu_gl_ext_frame_get_property(GObject* obj, guint property_id,
                                          GValue *value, GParamSpec *pspec)
{
  VisuGlExtFrame *self = VISU_GL_EXT_FRAME(obj);

  g_debug("Extension Frame: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case XPOS_PROP:
      g_value_set_float(value, self->priv->xpos);
      g_debug("%g.", self->priv->xpos);
      break;
    case YPOS_PROP:
      g_value_set_float(value, self->priv->ypos);
      g_debug("%g.", self->priv->ypos);
      break;
    case XPAD_PROP:
      g_value_set_float(value, self->priv->xpad);
      g_debug("%g.", self->priv->xpad);
      break;
    case YPAD_PROP:
      g_value_set_float(value, self->priv->ypad);
      g_debug("%g.", self->priv->ypad);
      break;
    case TITLE_PROP:
      g_value_set_string(value, self->priv->title);
      g_debug("%s.", self->priv->title);
      break;
    case XREQ_PROP:
      g_value_set_uint(value, self->priv->requisition[0]);
      g_debug("%u.", self->priv->requisition[0]);
      break;
    case YREQ_PROP:
      g_value_set_uint(value, self->priv->requisition[1]);
      g_debug("%u.", self->priv->requisition[1]);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_frame_set_property(GObject* obj, guint property_id,
                                          const GValue *value, GParamSpec *pspec)
{
  VisuGlExtFrame *self = VISU_GL_EXT_FRAME(obj);

  g_debug("Extension Frame: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case XPOS_PROP:
      visu_gl_ext_frame_setPosition(self, g_value_get_float(value), self->priv->ypos);
      g_debug("%g.", self->priv->xpos);
      break;
    case YPOS_PROP:
      visu_gl_ext_frame_setPosition(self, self->priv->xpos, g_value_get_float(value));
      g_debug("%g.", self->priv->ypos);
      break;
    case XPAD_PROP:
      g_debug("%g.", g_value_get_float(value));
      visu_gl_ext_frame_setPadding(self, g_value_get_float(value), self->priv->ypad);
      break;
    case YPAD_PROP:
      g_debug("%g.", g_value_get_float(value));
      visu_gl_ext_frame_setPadding(self, self->priv->xpad, g_value_get_float(value));
      break;
    case TITLE_PROP:
      g_debug("%s.", g_value_get_string(value));
      visu_gl_ext_frame_setTitle(self, g_value_get_string(value));
      break;
    case XREQ_PROP:
      g_debug("%u.", g_value_get_uint(value));
      visu_gl_ext_frame_setRequisition(self, g_value_get_uint(value), self->priv->requisition[1]);
      break;
    case YREQ_PROP:
      g_debug("%u.", g_value_get_uint(value));
      visu_gl_ext_frame_setRequisition(self, self->priv->requisition[0], g_value_get_uint(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static gboolean visu_gl_ext_frame_setGlView(VisuGlExt *frame, VisuGlView *view)
{
  VisuGlExtFramePrivate *priv = VISU_GL_EXT_FRAME(frame)->priv;

  if (priv->view == view)
    return FALSE;

  if (priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(priv->view), priv->widthHeight_signal);
      g_object_unref(priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      priv->widthHeight_signal =
        g_signal_connect(G_OBJECT(view), "WidthHeightChanged",
                         G_CALLBACK(onViewChanged), (gpointer)frame);
    }
  else
    priv->widthHeight_signal = 0;
  priv->view = view;

  visu_gl_ext_setDirty(frame, VISU_GL_DRAW_REQUIRED);
  return TRUE;
}

/**
 * visu_gl_ext_frame_setPosition:
 * @frame: the #VisuGlExtFrame object to modify.
 * @xpos: the reduced y position (1 to the left).
 * @ypos: the reduced y position (1 to the bottom).
 *
 * Change the position of the frame representation.
 *
 * Since: 3.7
 *
 * Returns: TRUE if any position is actually changed.
 **/
gboolean visu_gl_ext_frame_setPosition(VisuGlExtFrame *frame, float xpos, float ypos)
{
  gboolean changed;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  xpos = CLAMP(xpos, 0.f, 1.f);
  ypos = CLAMP(ypos, 0.f, 1.f);
  changed = FALSE;

  g_object_freeze_notify(G_OBJECT(frame));
  if (xpos != frame->priv->xpos)
    {
      frame->priv->xpos = xpos;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[XPOS_PROP]);
      changed = TRUE;
    }
  if (ypos != frame->priv->ypos)
    {
      frame->priv->ypos = ypos;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[YPOS_PROP]);
      changed = TRUE;
    }
  if (changed)
    visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  g_object_thaw_notify(G_OBJECT(frame));

  return changed;
}
/**
 * visu_gl_ext_frame_setPadding:
 * @frame: the #VisuGlExtFrame object to modify.
 * @xpad: the padding along x.
 * @ypad: the padding along y.
 *
 * Change the padding of the frame representation.
 *
 * Since: 3.8
 *
 * Returns: TRUE if any position is actually changed.
 **/
gboolean visu_gl_ext_frame_setPadding(VisuGlExtFrame *frame, float xpad, float ypad)
{
  gboolean changed;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  xpad = CLAMP(xpad, 0.f, 30.f);
  ypad = CLAMP(ypad, 0.f, 30.f);
  changed = FALSE;

  g_object_freeze_notify(G_OBJECT(frame));
  if (xpad != frame->priv->xpad)
    {
      frame->priv->xpad = xpad;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[XPAD_PROP]);
      changed = TRUE;
    }
  if (ypad != frame->priv->ypad)
    {
      frame->priv->ypad = ypad;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[YPAD_PROP]);
      changed = TRUE;
    }
  if (changed)
    visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  g_object_thaw_notify(G_OBJECT(frame));

  return changed;
}
#define SET_RGBA_I(S, I, V, M) {if (M & refMask[I] && S[I] != V[I]) \
      {S[I] = V[I]; diff = TRUE;}}
#define SET_RGBA(S, V, M) {SET_RGBA_I(S, 0, V, M);                      \
    SET_RGBA_I(S, 1, V, M); SET_RGBA_I(S, 2, V, M); SET_RGBA_I(S, 3, V, M); }
#define SET_RGB(S, V, M) {SET_RGBA_I(S, 0, V, M);                      \
    SET_RGBA_I(S, 1, V, M); SET_RGBA_I(S, 2, V, M); }
/**
 * visu_gl_ext_frame_setBgRGBA:
 * @frame: the #VisuGlExtFrame to update.
 * @rgba: (array fixed-size=4): a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue color and the alpha channel. Only values
 * specified by the @mask are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B, #TOOL_COLOR_MASK_A or a combinaison to indicate
 * what values in the @rgba array must be taken into account.
 *
 * Change the colour to represent the background of the frame.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 */
gboolean visu_gl_ext_frame_setBgRGBA(VisuGlExtFrame *frame, float rgba[4], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);
    
  SET_RGBA(frame->priv->bgRGBA, rgba, mask);
  if (!diff)
    return FALSE;

  visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_frame_setFontRGB:
 * @frame: the #VisuGlExtFrame to update.
 * @rgb: (array fixed-size=3): a four floats array with values (0 <= values <= 1) for the
 * red, the green, the blue color. Only values specified by the @mask
 * are really relevant.
 * @mask: use #TOOL_COLOR_MASK_R, #TOOL_COLOR_MASK_G,
 * #TOOL_COLOR_MASK_B or a combinaison to indicate what values in the
 * @rgb array must be taken into account.
 *
 * Change the colour to represent the font of the frame.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 */
gboolean visu_gl_ext_frame_setFontRGB(VisuGlExtFrame *frame, float rgb[3], int mask)
{
  gboolean diff = FALSE;

  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);
    
  SET_RGB(frame->fontRGB, rgb, mask);
  if (!diff)
    return FALSE;

  visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_frame_setScale:
 * @frame: the #VisuGlExtFrame to update.
 * @scale: a positive value.
 *
 * Change the zoom level for the rendering of the legend.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_frame_setScale(VisuGlExtFrame *frame, float scale)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (frame->priv->scale == scale)
    return FALSE;

  frame->priv->scale = CLAMP(scale, 0.01f, 10.f);

  visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_frame_setTitle:
 * @frame: the #VisuGlExtFrame object to modify.
 * @title: a title.
 *
 * Change the title of the box legend.
 *
 * Since: 3.7
 *
 * Returns: TRUE if title is actually changed.
 **/
gboolean visu_gl_ext_frame_setTitle(VisuGlExtFrame *frame, const gchar *title)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame) && title, FALSE);

  if (!g_strcmp0(title, frame->priv->title))
    return FALSE;
  g_free(frame->priv->title);
  frame->priv->title = g_strdup(title);

  g_object_notify_by_pspec(G_OBJECT(frame), properties[TITLE_PROP]);
  visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);

  return TRUE;
}
/**
 * visu_gl_ext_frame_setRequisition:
 * @frame: the #VisuGlExtFrame object to modify.
 * @width: the desired width.
 * @height: the desired height.
 *
 * Set the size of the frame in pixels. Use
 * visu_gl_ext_frame_setScale() to adjust the size if necessary.
 *
 * Since: 3.7
 *
 * Returns: TRUE if requested size is changed.
 **/
gboolean visu_gl_ext_frame_setRequisition(VisuGlExtFrame *frame, guint width, guint height)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), FALSE);

  if (frame->priv->requisition[0] == width &&
      frame->priv->requisition[1] == height)
    return FALSE;

  if (frame->priv->requisition[0] != width)
    {
      frame->priv->requisition[0] = width;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[XREQ_PROP]);
    }
  if (frame->priv->requisition[1] != height)
    {
      frame->priv->requisition[1] = height;
      g_object_notify_by_pspec(G_OBJECT(frame), properties[YREQ_PROP]);
    }

  visu_gl_ext_setDirty(VISU_GL_EXT(frame), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}

/**
 * visu_gl_ext_frame_getScale:
 * @frame: a #VisuGlExtFrame object.
 *
 * Frames are rendered with a scaling factor of 1. by default.
 *
 * Since: 3.7
 *
 * Returns: the scaling factor used to represent frames.
 **/
float visu_gl_ext_frame_getScale(const VisuGlExtFrame *frame)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_FRAME(frame), 1.f);

  return frame->priv->scale;
}
/**
 * visu_gl_ext_frame_getPosition:
 * @frame: the #VisuGlExtFrame object to inquire.
 * @xpos: (out) (allow-none): a location to store the x position.
 * @ypos: (out) (allow-none): a location to store the y position.
 *
 * Inquire the position of the representation of the frame.
 *
 * Since: 3.7
 **/
void visu_gl_ext_frame_getPosition(const VisuGlExtFrame *frame,
                                   float *xpos, float *ypos)
{
  g_return_if_fail(VISU_IS_GL_EXT_FRAME(frame));

  if (xpos)
    *xpos = frame->priv->xpos;
  if (ypos)
    *ypos = frame->priv->ypos;
}

static void onViewChanged(VisuGlView *view _U_, gpointer data)
{
  visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
}

static void visu_gl_ext_frame_rebuild(VisuGlExt *ext)
{
  GError *error = (GError*)0;
  guint iProg;

  g_object_get(G_OBJECT(ext), "nGlProg", &iProg, NULL);
  if (!visu_gl_ext_setShaderById(ext, iProg - 1, VISU_GL_SHADER_ORTHO, &error))
    {
      g_warning("Cannot create frame shader: %s", error->message);
      g_clear_error(&error);
    }
}

static void visu_gl_ext_frame_render(const VisuGlExt *ext)
{
  VisuGlExtFrame *frame;
  VisuGlExtFrameClass *klass;

  frame = VISU_GL_EXT_FRAME(ext);

  klass = VISU_GL_EXT_FRAME_GET_CLASS(frame);
  if ((!klass->render && !klass->renderText) || !frame->priv->view ||
      (klass->isValid && !klass->isValid(frame)))
    return;

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  visu_gl_ext_renderBuffer(ext, frame->priv->frameBuf);

  if (!klass->render)
    return;

  visu_gl_ext_setViewport(ext, frame->priv->xoffset, frame->priv->yoffset, frame->width, frame->height);
  klass->render(frame);

  /* We set viewport back. */
  g_debug("Visu GlExt Frame: put back viewport at %dx%d.",
          visu_gl_view_getWidth(frame->priv->view),
          visu_gl_view_getHeight(frame->priv->view));
  glViewport(0, 0, visu_gl_view_getWidth(frame->priv->view),
             visu_gl_view_getHeight(frame->priv->view));
}

static void visu_gl_ext_frame_render_text(const VisuGlExt *ext)
{
  VisuGlExtFrame *frame;
  VisuGlExtFrameClass *klass;
  ToolGlMatrix P;

  frame = VISU_GL_EXT_FRAME(ext);

  klass = VISU_GL_EXT_FRAME_GET_CLASS(frame);
  if (!frame->priv->view || (klass->isValid && !klass->isValid(frame)))
    return;

  tool_gl_matrix_setIdentity(&P);
  visu_gl_ext_setUniformTexMVP(ext, &P);

  /* We draw the title, if any. */
  if (frame->priv->title && frame->priv->title[0])
    {
      GLfloat at[2];
      
      visu_gl_ext_setUniformTexRGBA(ext, frame->fontRGB);
      at[0] = (GLfloat)(frame->priv->xoffset + frame->width * 0.5f) / visu_gl_view_getWidth(frame->priv->view);
      at[1] = (GLfloat)(frame->priv->yoffset + frame->height + frame->priv->ypad * frame->priv->scale) / visu_gl_view_getHeight(frame->priv->view);
      visu_gl_ext_blitTextureOnScreen(ext, frame->priv->titleTex, at,
                                      VISU_GL_CENTER, VISU_GL_LEFT);
    }
  if (!klass->renderText)
    return;
  visu_gl_ext_setViewport(ext, frame->priv->xoffset, frame->priv->yoffset, frame->width, frame->height);
  klass->renderText(frame);
  glViewport(0, 0, visu_gl_view_getWidth(frame->priv->view),
             visu_gl_view_getHeight(frame->priv->view));
}

static void visu_gl_ext_frame_draw(VisuGlExt *ext)
{
  float requisition[2];
  VisuGlExtFrame *frame;
  VisuGlExtFrameClass *klass;
  int dx, dy;
  guint titleHeight, titleWidth, iProg;
  GArray *bgData;
  GLfloat data[3] = {0.f, 0.f, 0.f};

  g_return_if_fail(VISU_IS_GL_EXT_FRAME(ext));
  frame = VISU_GL_EXT_FRAME(ext);

  g_debug("Visu GlExt Frame: drawing the extension '%p'.", (gpointer)frame);

  klass = VISU_GL_EXT_FRAME_GET_CLASS(frame);
  if (!frame->priv->view || (klass->isValid && !klass->isValid(frame)))
    return;

  titleHeight = titleWidth = 0;
  if (frame->priv->title && frame->priv->title[0])
    {
      ToolWriter *writer;
      GArray *buf;
      guint width, height;

      if (frame->priv->titleTex == G_MAXUINT)
        frame->priv->titleTex = visu_gl_ext_addTextures(ext, 1);
      writer = tool_writer_getStatic();
      tool_writer_setSize(writer, TOOL_WRITER_FONT_NORMAL);
      buf = tool_writer_layout(writer, frame->priv->title, &width, &height);
      visu_gl_ext_setTexture2D(ext, frame->priv->titleTex,
                               (const unsigned char*)buf->data, width, height);
      g_array_unref(buf);
      g_object_unref(writer);
      titleHeight = height + frame->priv->ypad;
      titleWidth = width + 2 * frame->priv->xpad;
    }

  /* We get the size of the current viewport. */
  g_debug("Visu GlExt Frame: current viewport is %dx%d.",
          visu_gl_view_getWidth(frame->priv->view),
          visu_gl_view_getHeight(frame->priv->view));

  /* We change the viewport. */
  g_debug("Visu GlExt Frame: requisition is %ux%u.",
          frame->priv->requisition[0], frame->priv->requisition[1]);
  requisition[0] = (float)MAX(frame->priv->requisition[0], titleWidth);
  requisition[1] = (float)frame->priv->requisition[1] + titleHeight;
  frame->width = MIN((guint)(requisition[0] * frame->priv->scale),
                     visu_gl_view_getWidth(frame->priv->view) - (guint)(2.f * frame->priv->xmargin));
  frame->height = MIN((guint)(requisition[1] * frame->priv->scale),
                      visu_gl_view_getHeight(frame->priv->view) - (guint)(2.f * frame->priv->ymargin));
  g_debug("Visu GlExt Frame: size is %ux%u.", frame->width, frame->height);
  frame->priv->xoffset = frame->priv->xmargin +
    ((float)visu_gl_view_getWidth(frame->priv->view) - 2.f * frame->priv->xmargin - frame->width) * frame->priv->xpos;
  frame->priv->yoffset = frame->priv->ymargin +
    ((float)visu_gl_view_getHeight(frame->priv->view) - 2.f * frame->priv->ymargin - frame->height) * frame->priv->ypos;

  g_object_get(G_OBJECT(ext), "nGlProg", &iProg, NULL);
  if (frame->priv->frameBuf == G_MAXUINT)
    frame->priv->frameBuf = visu_gl_ext_addBuffers(ext, 1);
  bgData = visu_gl_ext_startBuffer(ext, iProg - 1, frame->priv->frameBuf, VISU_GL_RGBA_XYZ, VISU_GL_TRIANGLE_FAN);
  data[0] = frame->priv->xoffset;
  data[1] = frame->priv->yoffset;
  g_array_append_vals(bgData, frame->priv->bgRGBA, 4);
  g_array_append_vals(bgData, data, 3);
  data[0] += frame->width;
  g_array_append_vals(bgData, frame->priv->bgRGBA, 4);
  g_array_append_vals(bgData, data, 3);
  data[1] += frame->height;
  g_array_append_vals(bgData, frame->priv->bgRGBA, 4);
  g_array_append_vals(bgData, data, 3);
  data[0] -= frame->width;
  g_array_append_vals(bgData, frame->priv->bgRGBA, 4);
  g_array_append_vals(bgData, data, 3);
  visu_gl_ext_takeBuffer(ext, frame->priv->frameBuf, bgData);

  /* We change again the viewport to adjust to padding. */
  dx = frame->priv->xpad * frame->priv->scale;
  dy = frame->priv->ypad * frame->priv->scale;
  frame->priv->xoffset += dx;
  frame->priv->yoffset += dy;
  frame->width -= 2.f * dx * frame->priv->scale;
  frame->height -= (2.f * dy  + titleHeight) * frame->priv->scale;
  g_debug("Visu GlExt Frame: frame actual size is %dx%d.",
          frame->width, frame->height);
}
