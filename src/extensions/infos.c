/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
        Copyright CEA, contributeurs : Damien
        CALISTE, laboratoire L_Sim, (2001-2013)
  
        Adresse mèl :
        CALISTE, damien P caliste AT cea P fr.

        Ce logiciel est un programme informatique servant à visualiser des
        structures atomiques dans un rendu pseudo-3D. 

        Ce logiciel est régi par la licence CeCILL soumise au droit français et
        respectant les principes de diffusion des logiciels libres. Vous pouvez
        utiliser, modifier et/ou redistribuer ce programme sous les conditions
        de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
        sur le site "http://www.cecill.info".

        Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
        pris connaissance de la licence CeCILL, et que vous en avez accepté les
        termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
        Copyright CEA, contributors : Damien
        CALISTE, laboratoire L_Sim, (2001-2013)

        E-mail address:
        CALISTE, damien P caliste AT cea P fr.

        This software is a computer program whose purpose is to visualize atomic
        configurations in 3D.

        This software is governed by the CeCILL  license under French law and
        abiding by the rules of distribution of free software.  You can  use, 
        modify and/ or redistribute the software under the terms of the CeCILL
        license as circulated by CEA, CNRS and INRIA at the following URL
        "http://www.cecill.info". 

        The fact that you are presently reading this means that you have had
        knowledge of the CeCILL license and that you accept its terms. You can
        find a copy of this licence shipped with this software at COPYING.
*/
#include "infos.h"

#include <epoxy/gl.h>

#include <renderingMethods/elementAtomic.h>

/**
 * SECTION:infos
 * @short_description: give the capability to draw some information
 * near each node.
 *
 * <para>This part is used to draw some information near the
 * nodes. This information can be the one of a #VisuNodeProperty or
 * something else. When read from a #VisuNodeProperty, just giving the
 * name will produce the right output. In other cases a print routine
 * must be given.</para>
 */

/* Interface for a routine to draw the informations into a label. */
typedef void (*DrawInfosFunc)(gchar lbl[64], VisuData *data, VisuElement *element,
                              VisuNode *node, VisuNodeValues *values);
static void drawNumber(gchar lbl[64], VisuData *data, VisuElement *element,
                       VisuNode *node, VisuNodeValues *dataNode);
static void drawElement(gchar lbl[64], VisuData *data, VisuElement *element,
                        VisuNode *node, VisuNodeValues *dataNode);
static void drawProps(gchar lbl[64], VisuData *data, VisuElement *element,
                      VisuNode *node, VisuNodeValues *values);

/**
 * VisuGlExtInfosClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtInfosClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtInfos:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtInfosPrivate:
 *
 * Private fields for #VisuGlExtInfos objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtInfosPrivate
{
  gboolean dispose_has_run;

  /* What to draw. */
  GArray *nodes;
  DrawInfosFunc draw;
  
  /* Object signals. */
  VisuNodeArrayRenderer *renderer;
  gulong popDec, popInc, pos_sig, col_sig, vis_sig, siz_sig;
  gulong thetaChg, phiChg, omegaChg;
  VisuNodeValues *values;
  gulong nodeChgd;
};

enum
  {
    PROP_0,
    SELECTION_PROP,
    VALUES_PROP,
    N_PROP
  };
static GParamSpec *_properties[N_PROP];

static GObject* visu_gl_ext_infos_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_infos_finalize(GObject* obj);
static void visu_gl_ext_infos_dispose(GObject* obj);
static void visu_gl_ext_infos_get_property(GObject* obj, guint property_id,
                                            GValue *value, GParamSpec *pspec);
static void visu_gl_ext_infos_set_property(GObject* obj, guint property_id,
                                            const GValue *value, GParamSpec *pspec);
static void visu_gl_ext_infos_draw(VisuGlExt *infos);
static void visu_gl_ext_infos_render_text(const VisuGlExt *infos);

/* Callbacks. */
static void onPopulationIncrease(VisuNodeArrayRenderer *renderer, GArray *newNodes, gpointer user_data);
static void onPopulationDecrease(VisuNodeArrayRenderer *renderer, GArray *oldNodes, gpointer user_data);
static void onNodeValuesChanged(VisuNodeValues *values, VisuNode *node, gpointer user_data);

static void _setNodeValues(VisuGlExtInfos *infos, VisuNodeValues *values);
static void _setDirty(VisuGlExt *ext);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtInfos, visu_gl_ext_infos, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtInfos))

static void visu_gl_ext_infos_class_init(VisuGlExtInfosClass *klass)
{
  g_debug("Extension Infos: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  /* g_debug("                - adding new resources ;"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor  = visu_gl_ext_infos_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_infos_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_infos_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_infos_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_infos_get_property;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_infos_draw;
  VISU_GL_EXT_CLASS(klass)->renderText = visu_gl_ext_infos_render_text;

  /**
   * VisuGlExtInfos::selection:
   *
   * The ids of selected nodes.
   *
   * Since: 3.8
   */
  _properties[SELECTION_PROP] = g_param_spec_boxed
    ("selection", "Selection", "ids of selected nodes.",
     G_TYPE_ARRAY, G_PARAM_READWRITE);
  
  /**
   * VisuGlExtInfos::values:
   *
   * Some #VisuNodeValues to display the values of.
   *
   * Since: 3.8
   */
  _properties[VALUES_PROP] = g_param_spec_object
    ("values", "Values", "some node values to display.",
     VISU_TYPE_NODE_VALUES, G_PARAM_READWRITE);
  
  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, _properties);
}

static GObject* visu_gl_ext_infos_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_HIGH);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "withFog"))
        g_value_set_boolean(props[i].value, TRUE);
    }

  return G_OBJECT_CLASS(visu_gl_ext_infos_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_infos_init(VisuGlExtInfos *obj)
{
  g_debug("Extension Infos: initializing a new object (%p).",
              (gpointer)obj);

  obj->priv = visu_gl_ext_infos_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->nodes    = (GArray*)0;
  obj->priv->draw     = drawNumber;
  obj->priv->values   = (VisuNodeValues*)0;
  obj->priv->renderer = (VisuNodeArrayRenderer*)0;
}
static void visu_gl_ext_infos_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec)
{
  g_debug("Extension Infos: get property '%s' -> ",
              g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SELECTION_PROP:
      g_value_set_boxed(value, VISU_GL_EXT_INFOS(obj)->priv->nodes);
      g_debug("%p.", g_value_get_boxed(value));
      break;
    case VALUES_PROP:
      g_value_set_object(value, VISU_GL_EXT_INFOS(obj)->priv->values);
      g_debug("%p.", g_value_get_object(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_infos_set_property(GObject* obj, guint property_id,
                                           const GValue *value, GParamSpec *pspec)
{
  VisuGlExtInfos *self = VISU_GL_EXT_INFOS(obj);

  g_debug("Extension Infos: set property '%s'.",
              g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SELECTION_PROP:
      if (self->priv->nodes)
        g_array_unref(self->priv->nodes);
      self->priv->nodes = g_value_dup_boxed(value);
      break;
    case VALUES_PROP:
      _setNodeValues(self, VISU_NODE_VALUES(g_value_get_object(value)));
      self->priv->draw = g_value_get_object(value) ? drawProps : NULL;
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
  visu_gl_ext_setDirty(VISU_GL_EXT(obj), VISU_GL_DRAW_REQUIRED);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_infos_dispose(GObject* obj)
{
  VisuGlExtInfos *infos;

  g_debug("Extension Infos: dispose object %p.", (gpointer)obj);

  infos = VISU_GL_EXT_INFOS(obj);
  if (infos->priv->dispose_has_run)
    return;
  infos->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_infos_setDataRenderer(infos, (VisuNodeArrayRenderer*)0);
  _setNodeValues(infos, (VisuNodeValues*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_infos_parent_class)->dispose(obj);
}
/* This method is called once only. */
static void visu_gl_ext_infos_finalize(GObject* obj)
{
  VisuGlExtInfos *infos;

  g_return_if_fail(obj);

  g_debug("Extension Infos: finalize object %p.", (gpointer)obj);

  infos = VISU_GL_EXT_INFOS(obj);
  /* Free privs elements. */
  g_debug("Extension Infos: free private infos.");
  if (infos->priv->nodes)
    g_array_unref(infos->priv->nodes);

  /* Chain up to the parent class */
  g_debug("Extension Infos: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_infos_parent_class)->finalize(obj);
  g_debug("Extension Infos: freeing ... OK.");
}
/**
 * visu_gl_ext_infos_new:
 * @name: (allow-none): the name of the #VisuGlExt.
 *
 * Create a new #VisuGlExt to represent information on nodes.
 *
 * Since: 3.7
 *
 * Returns: a new #VisuGlExtInfos object.
 **/
VisuGlExtInfos* visu_gl_ext_infos_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_INFOS_ID;
  char *description = _("Draw informations on nodes.");

  g_debug("Extension Infos: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_INFOS,
                      "name", (name)?name:name_, "label", _(name),
                      "description", description, NULL);
}

/********************/
/* Public routines. */
/********************/
/**
 * visu_gl_ext_infos_setDataRenderer:
 * @infos: The #VisuGlExtInfos to attached to.
 * @renderer: the #VisuNodeArrayRenderer displaying the data.
 *
 * Attach a #VisuNodeArrayRenderer to render to and setup the infos.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the model was actually changed.
 **/
gboolean visu_gl_ext_infos_setDataRenderer(VisuGlExtInfos *infos,
                                           VisuNodeArrayRenderer *renderer)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  if (renderer == infos->priv->renderer)
    return FALSE;

  if (infos->priv->renderer)
    {
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->vis_sig);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->pos_sig);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->siz_sig);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->col_sig);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->popDec);
      g_signal_handler_disconnect(G_OBJECT(infos->priv->renderer), infos->priv->popInc);
      g_object_unref(infos->priv->renderer);
    }
  if (renderer)
    {
      g_object_ref(renderer);
      infos->priv->vis_sig = g_signal_connect_swapped(G_OBJECT(renderer), "nodes::visibility",
                                                      G_CALLBACK(_setDirty), (gpointer)infos);
      infos->priv->pos_sig = g_signal_connect_swapped(G_OBJECT(renderer), "nodes::position",
                                                      G_CALLBACK(_setDirty), (gpointer)infos);
      infos->priv->siz_sig = g_signal_connect_swapped(G_OBJECT(renderer), "element-size-changed",
                                                      G_CALLBACK(_setDirty), (gpointer)infos);
      infos->priv->col_sig = g_signal_connect_swapped(G_OBJECT(renderer), "element-notify::color",
                                                      G_CALLBACK(_setDirty), (gpointer)infos);
      infos->priv->popDec = g_signal_connect(G_OBJECT(renderer), "nodes::population-decrease",
                                             G_CALLBACK(onPopulationDecrease), (gpointer)infos);
      infos->priv->popInc = g_signal_connect(G_OBJECT(renderer), "nodes::population-increase",
                                             G_CALLBACK(onPopulationIncrease), (gpointer)infos);
    }
  infos->priv->renderer = renderer;

  visu_gl_ext_setDirty(VISU_GL_EXT(infos), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
static void _setNodeValues(VisuGlExtInfos *infos, VisuNodeValues *values)
{
  g_return_if_fail(VISU_IS_GL_EXT_INFOS(infos));

  if (values == infos->priv->values)
    return;

  if (infos->priv->values)
    {
      g_signal_handler_disconnect(G_OBJECT(infos->priv->values), infos->priv->nodeChgd);
      g_object_unref(infos->priv->values);
    }
  if (values)
    {
      g_object_ref(values);
      infos->priv->nodeChgd =
        g_signal_connect(G_OBJECT(values), "changed",
                         G_CALLBACK(onNodeValuesChanged), (gpointer)infos);
    }
  infos->priv->values = values;
  g_object_notify_by_pspec(G_OBJECT(infos), _properties[VALUES_PROP]);
}
/**
 * visu_gl_ext_infos_drawIds:
 * @infos: the #VisuGlExtInfos object to update.
 * @nodes: (element-type guint) (transfer full): an integer list.
 *
 * With this extension,
 * some the number of nodes will be drawn on them. Numbers can be drawn and
 * all nodes (set @nodes to a NULL pointer), or to a restricted list of nodes
 * represented by their numbers. In this case, @nodes can have whatever length
 * but must be terminated by a negative integer. This array is then owned by the
 * extension and should not be freed.
 *
 * Returns: TRUE if the status was actually changed.
 */
gboolean visu_gl_ext_infos_drawIds(VisuGlExtInfos *infos, GArray *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  if (infos->priv->nodes)
    g_array_unref(infos->priv->nodes);
  infos->priv->nodes    = nodes ? g_array_ref(nodes) : (GArray*)0;
  g_object_notify_by_pspec(G_OBJECT(infos), _properties[SELECTION_PROP]);
  infos->priv->draw     = drawNumber;
  _setNodeValues(infos, (VisuNodeValues*)0);

  visu_gl_ext_setDirty(VISU_GL_EXT(infos), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_infos_drawElements:
 * @infos: the #VisuGlExtInfos object to update.
 * @nodes: (element-type guint) (transfer full): an integer list.
 *
 * As visu_gl_ext_infos_drawIds(), but draw the names of elements instead of their
 * numbers.
 *
 * Returns: TRUE if the status was actually changed.
 */
gboolean visu_gl_ext_infos_drawElements(VisuGlExtInfos *infos, GArray *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos), FALSE);

  if (infos->priv->nodes)
    g_array_unref(infos->priv->nodes);
  infos->priv->nodes    = nodes ? g_array_ref(nodes) : (GArray*)0;
  g_object_notify_by_pspec(G_OBJECT(infos), _properties[SELECTION_PROP]);
  infos->priv->draw     = drawElement;
  _setNodeValues(infos, (VisuNodeValues*)0);

  visu_gl_ext_setDirty(VISU_GL_EXT(infos), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_infos_drawNodeProperties:
 * @infos: the #VisuGlExtInfos object to update.
 * @values: the #VisuNodeValues to render on nodes.
 * @nodes: (element-type guint) (transfer full): an integer list.
 *
 * Draw @values on selected @nodes.
 *
 * Since: 3.8
 *
 * Returns: TRUE.
 **/
gboolean visu_gl_ext_infos_drawNodeProperties(VisuGlExtInfos *infos,
                                              VisuNodeValues *values, GArray *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_INFOS(infos) &&
                       VISU_IS_NODE_VALUES(values), FALSE);

  if (infos->priv->nodes)
    g_array_unref(infos->priv->nodes);
  infos->priv->nodes    = nodes ? g_array_ref(nodes) : (GArray*)0;
  g_object_notify_by_pspec(G_OBJECT(infos), _properties[SELECTION_PROP]);
  infos->priv->draw     = (DrawInfosFunc)drawProps;
  _setNodeValues(infos, values);

  visu_gl_ext_setDirty(VISU_GL_EXT(infos), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}

static void visu_gl_ext_infos_draw(VisuGlExt *infos)
{
  float material[5];
  VisuNodeArray *nodes;
  VisuNodeArrayRendererIter iter;
  guint i;
  gboolean valid;
  VisuGlExtInfosPrivate *priv = VISU_GL_EXT_INFOS(infos)->priv;
  VisuGlExtLabel lbl;
  GArray *labels;

  visu_gl_ext_clearBuffers(infos);
  visu_gl_ext_clearLabels(infos);
  
  /* Nothing to draw; */
  if(!priv->renderer || !priv->draw)
    return;

  labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));
  lbl.offset[0] = 0.f;
  lbl.offset[1] = 0.f;

  /* If nodes is NULL, we draw for all nodes. */
  nodes = visu_node_array_renderer_getNodeArray(priv->renderer);
  if (!priv->nodes || !priv->nodes->len)
    {
      g_debug(" | use all the nodes.");
      for (valid = visu_node_array_renderer_iter_new(priv->renderer, &iter, TRUE);
           valid; valid = visu_node_array_renderer_iter_next(&iter))
        if (visu_element_getRendered(iter.element))
          {
            visu_element_renderer_getColorization(iter.renderer, VISU_ELEMENT_RENDERER_INVERT,
                                                  lbl.rgba, material);
            lbl.offset[2] = visu_element_renderer_getExtent(iter.renderer);
            for(visu_node_array_iterRestartNode(iter.parent.array, &iter.parent);
                iter.parent.node;
                visu_node_array_iterNextNode(iter.parent.array, &iter.parent))
              if (iter.parent.node->rendered)
                {
                  visu_node_array_getNodePosition(iter.parent.array,
                                                  iter.parent.node, lbl.xyz);
                  priv->draw(lbl.lbl, VISU_DATA(nodes), iter.element, iter.parent.node,
                             priv->values);
                  g_array_append_val(labels, lbl);
                }
          }
    }
  else
    {
      g_debug(" | use a restricted list of nodes.");
      /* nodes is not NULL, we draw for the given nodes only. */
      for (i = 0; i < priv->nodes->len; i++)
        {
          g_debug(" | %d", g_array_index(priv->nodes, guint, i));
          iter.parent.node = visu_node_array_getFromId
            (nodes, g_array_index(priv->nodes, guint, i));
          g_return_if_fail(iter.parent.node);
          iter.element = visu_node_array_getElement(nodes, iter.parent.node);
          if (visu_element_getRendered(iter.element) && iter.parent.node->rendered)
            {
              iter.renderer = visu_node_array_renderer_get(priv->renderer, iter.element);
              visu_element_renderer_getColorization(iter.renderer, VISU_ELEMENT_RENDERER_INVERT, lbl.rgba, material);
              lbl.offset[2] = visu_element_renderer_getExtent(iter.renderer);
              visu_node_array_getNodePosition(nodes, iter.parent.node, lbl.xyz);
              priv->draw(lbl.lbl, VISU_DATA(nodes), iter.element, iter.parent.node,
                         priv->values);
              g_array_append_val(labels, lbl);
            }
        }
    }
  visu_gl_ext_setLabels(infos, labels, TOOL_WRITER_FONT_NORMAL, TRUE);
  g_array_unref(labels);
}
static void visu_gl_ext_infos_render_text(const VisuGlExt *infos)
{
  visu_gl_ext_blitLabels(infos);
}

/*********************/
/* Private routines. */
/*********************/
static void drawNumber(gchar lbl[64], VisuData *data _U_, VisuElement *element _U_,
                       VisuNode *node, VisuNodeValues *dataNode _U_)
{
  sprintf(lbl, "%d", node->number + 1);
}
static void drawElement(gchar lbl[64], VisuData *data _U_, VisuElement *element,
                        VisuNode *node _U_, VisuNodeValues *dataNode _U_)
{
  sprintf(lbl, "%s", element->name);
}
static void drawProps(gchar lbl[64], VisuData *data _U_, VisuElement *element _U_,
                      VisuNode *node, VisuNodeValues *values)
{
  gchar *label;

  label = visu_node_values_toString(values, node);
  if (label)
    sprintf(lbl, "%s", label);
  else
    lbl[0] = '\0';
  g_free(label);
}

/*************/
/* Callbacks */
/*************/
static void _setDirty(VisuGlExt *ext)
{
  visu_gl_ext_setDirty(ext, VISU_GL_DRAW_REQUIRED);
}
static void onPopulationIncrease(VisuNodeArrayRenderer *renderer _U_, GArray *newNodes _U_,
                                 gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);

  /* If we draw all nodes, then we must redraw. */
  if (!infos->priv->nodes || !infos->priv->nodes->len)
    visu_gl_ext_setDirty(VISU_GL_EXT(user_data), VISU_GL_DRAW_REQUIRED);
}
static void onPopulationDecrease(VisuNodeArrayRenderer *renderer _U_, GArray *oldNodes, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);
  guint i;

  g_debug("Extension Informations: caught the 'PopulationDecrease'"
              " signal, update and rebuild in list nodes case.");
  /* If we draw a list of nodes, then we must remove some and redraw. */

  /* We remove all old nodes. */
  if (infos->priv->nodes)
    {
      for (i = 0; i < oldNodes->len; i++)
        g_array_remove_index_fast(infos->priv->nodes, g_array_index(oldNodes, guint, i));
      g_object_notify_by_pspec(G_OBJECT(infos), _properties[SELECTION_PROP]);
    }
  visu_gl_ext_setDirty(VISU_GL_EXT(user_data), VISU_GL_DRAW_REQUIRED);
}
static void onNodeValuesChanged(VisuNodeValues *values _U_, VisuNode *node, gpointer user_data)
{
  VisuGlExtInfos *infos = VISU_GL_EXT_INFOS(user_data);
  guint i;

  g_debug("Extension Informations: caught the 'changed'"
          " signal, rebuild.");
  if (!infos->priv->nodes || !infos->priv->nodes->len || !node)
    {
      visu_gl_ext_setDirty(VISU_GL_EXT(user_data), VISU_GL_DRAW_REQUIRED);
      return;
    }

  for (i = 0; i < infos->priv->nodes->len; i++)
    if (g_array_index(infos->priv->nodes, guint, i) == node->number)
      {
        visu_gl_ext_setDirty(VISU_GL_EXT(user_data), VISU_GL_DRAW_REQUIRED);
        return;
      }
}
