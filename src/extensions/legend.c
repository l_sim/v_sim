/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "legend.h"

#include <epoxy/gl.h>

#include <visu_configFile.h>

/**
 * SECTION:legend
 * @short_description: Draw a frame with the representation of each
 * atom species, its name and the number of elements.
 *
 * <para>This extension draws a frame on top of the rendering area with an
 * item per #VisuElement currently rendered. For each #VisuElement, a
 * small representation of its OpenGL shape is drawn, its label is
 * printed and the number of #VisuNode associated to this
 * element.</para>
 * <para>This extension defines one resource entry labeled
 * "legend_is_on" to control if the legend is printed or not.</para>
 *
 * Since: 3.5
 */

#define FLAG_RESOURCE_LEGEND_USED   "legend_is_on"
#define DESC_RESOURCE_LEGEND_USED   "Control if the legend is drawn ; boolean (0 or 1)"
static gboolean DEFAULT_LEGEND_USED = FALSE;

/**
 * VisuGlExtLegendClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtLegendClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtLegend:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtLegendPrivate:
 *
 * Private fields for #VisuGlExtLegend objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtLegendPrivate
{
  gboolean dispose_has_run;

  /* Legend definition. */
  VisuNodeArrayRenderer *nodes;
  gulong pop_sig, siz_sig, ren_sig;
};
static VisuGlExtLegend* defaultLegend;

enum
  {
   SHADER_OBJECT,
   SHADER_POINT,
   N_SHADERS
  };

static GObject* visu_gl_ext_legend_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_legend_dispose(GObject* obj);
static void visu_gl_ext_legend_render(const VisuGlExtFrame *self);
static void visu_gl_ext_legend_render_text(const VisuGlExtFrame *self);
static void visu_gl_ext_legend_draw(VisuGlExt *legend);
static void visu_gl_ext_legend_rebuild(VisuGlExt *legend);
static gboolean visu_gl_ext_legend_isValid(const VisuGlExtFrame *legend);

/* Local callbacks. */
static void setDirty(VisuGlExt *ext);
static void onEntryUsed(VisuGlExtLegend *lg, VisuConfigFileEntry *entry, VisuConfigFile *obj);

/* Local routines. */
static void exportResources(GString *data, VisuData *dataObj);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtLegend, visu_gl_ext_legend, VISU_TYPE_GL_EXT_FRAME,
                        G_ADD_PRIVATE(VisuGlExtLegend))

static void visu_gl_ext_legend_class_init(VisuGlExtLegendClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  g_debug("Extension Legend: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  g_debug("                - adding new resources ;");
  resourceEntry = visu_config_file_addBooleanEntry(VISU_CONFIG_FILE_RESOURCE,
                                                   FLAG_RESOURCE_LEGEND_USED,
                                                   DESC_RESOURCE_LEGEND_USED,
                                                   &DEFAULT_LEGEND_USED, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.5f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
				   exportResources);

  defaultLegend = (VisuGlExtLegend*)0;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_legend_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_legend_dispose;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_legend_rebuild;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_legend_draw;
  VISU_GL_EXT_FRAME_CLASS(klass)->render = visu_gl_ext_legend_render;
  VISU_GL_EXT_FRAME_CLASS(klass)->renderText = visu_gl_ext_legend_render_text;
  VISU_GL_EXT_FRAME_CLASS(klass)->isValid = visu_gl_ext_legend_isValid;
}

static void visu_gl_ext_legend_init(VisuGlExtLegend *obj)
{
  g_debug("Extension Legend: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_legend_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->nodes = (VisuNodeArrayRenderer*)0;

  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_LEGEND_USED,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);

  if (!defaultLegend)
    defaultLegend = obj;
}

static GObject* visu_gl_ext_legend_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LAST);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, N_SHADERS);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "x-pos"))
        g_value_set_float(props[i].value, 0.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-pos"))
        g_value_set_float(props[i].value, 1.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "x-padding"))
        g_value_set_float(props[i].value, 5.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-padding"))
        g_value_set_float(props[i].value, 3.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "x-requisition"))
        g_value_set_uint(props[i].value, 102400);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-requisition"))
        g_value_set_uint(props[i].value, 30);
    }

  return G_OBJECT_CLASS(visu_gl_ext_legend_parent_class)->constructor(gtype, nprops, props);
}

/* This method can be called several times.
   It should unref all of its reference to
   GObjects. */
static void visu_gl_ext_legend_dispose(GObject* obj)
{
  VisuGlExtLegend *legend;

  g_debug("Extension Legend: dispose object %p.", (gpointer)obj);

  legend = VISU_GL_EXT_LEGEND(obj);
  if (legend->priv->dispose_has_run)
    return;
  legend->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_legend_setNodes(legend, (VisuNodeArrayRenderer*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_legend_parent_class)->dispose(obj);
}

/**
 * visu_gl_ext_legend_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_LEGEND_ID).
 *
 * Creates a new #VisuGlExt to draw a legend.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtLegend* visu_gl_ext_legend_new(const gchar *name)
{
  char *name_ = VISU_GL_EXT_LEGEND_ID;
  char *description = _("Draw the name and the shape of available elements on screen.");

  g_debug("Extension Legend: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_LEGEND, "active", DEFAULT_LEGEND_USED,
                      "name", (name)?name:name_, "label", _(name),
                      "description", description, NULL);
}
/**
 * visu_gl_ext_legend_setNodes:
 * @legend: The #VisuGlExtLegend to attached to.
 * @nodes: the nodes to get the population of.
 *
 * Attach an #VisuGlView to render to and setup the legend to get the
 * node population also.
 *
 * Since: 3.7
 *
 * Returns: TRUE if model has been changed.
 **/
gboolean visu_gl_ext_legend_setNodes(VisuGlExtLegend *legend,
                                     VisuNodeArrayRenderer *nodes)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LEGEND(legend), FALSE);

  if (legend->priv->nodes == nodes)
    return FALSE;

  if (legend->priv->nodes)
    {
      g_signal_handler_disconnect(G_OBJECT(legend->priv->nodes), legend->priv->ren_sig);
      g_signal_handler_disconnect(G_OBJECT(legend->priv->nodes), legend->priv->pop_sig);
      g_signal_handler_disconnect(G_OBJECT(legend->priv->nodes), legend->priv->siz_sig);
      g_object_unref(legend->priv->nodes);
    }
  legend->priv->nodes = nodes;
  if (nodes)
    {
      g_object_ref(nodes);
      legend->priv->ren_sig =
        g_signal_connect_swapped(G_OBJECT(nodes), "element-notify",
                                 G_CALLBACK(setDirty), (gpointer)legend);
      legend->priv->pop_sig =
        g_signal_connect_swapped(G_OBJECT(nodes), "nodes::population",
                                 G_CALLBACK(setDirty), (gpointer)legend);
      legend->priv->siz_sig =
        g_signal_connect_swapped(G_OBJECT(nodes), "element-size-changed",
                                 G_CALLBACK(setDirty), (gpointer)legend);
    }

  visu_gl_ext_setDirty(VISU_GL_EXT(legend), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_legend_getNodes:
 * @legend: a #VisuGlExtLegend object.
 *
 * @legend is displaying a label showing the element of a
 * #VisuNodeArray using the rendering properties of a #VisuNodeArrayRenderer.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): the #VisuNodeArrayRenderer this legend is
 * based on.
 **/
VisuNodeArrayRenderer* visu_gl_ext_legend_getNodes(VisuGlExtLegend *legend)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LEGEND(legend), (VisuNodeArrayRenderer*)0);
  return legend->priv->nodes;
}
static void onEntryUsed(VisuGlExtLegend *lg, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_setActive(VISU_GL_EXT(lg), DEFAULT_LEGEND_USED);
}
static void setDirty(VisuGlExt *ext)
{
  visu_gl_ext_setDirty(ext, VISU_GL_DRAW_REQUIRED);
}

static gboolean visu_gl_ext_legend_isValid(const VisuGlExtFrame *frame)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_LEGEND(frame), FALSE);
  
  return (VISU_GL_EXT_LEGEND(frame)->priv->nodes != (VisuNodeArrayRenderer*)0);
}

static void visu_gl_ext_legend_render(const VisuGlExtFrame *self)
{
  VisuGlExtLegend *legend;
  guint i, nEle;
  ToolGlMatrix P, MV;
  const gfloat cam[3] = {0.f, 0.f, 9999.f};

  legend = VISU_GL_EXT_LEGEND(self);

  glEnable(GL_CULL_FACE);
  tool_gl_matrix_setIdentity(&MV);
  tool_gl_matrix_setOrtho(&P, self->width, self->height);
  visu_gl_ext_setUniformMVP(VISU_GL_EXT(self), SHADER_OBJECT, &P);
  visu_gl_ext_setUniformMVP(VISU_GL_EXT(self), SHADER_POINT, &P);
  visu_gl_ext_setUniformMV(VISU_GL_EXT(self), SHADER_OBJECT, &MV);
  visu_gl_ext_setUniformCamera(VISU_GL_EXT(self), SHADER_OBJECT, cam);
  /* The elements. */
  visu_node_array_renderer_getMaxElementSize(legend->priv->nodes, &nEle);
  for (i = 0; i < nEle; i++)
    visu_gl_ext_renderBuffer(VISU_GL_EXT(self), i + 1);
}

static void visu_gl_ext_legend_render_text(const VisuGlExtFrame *self)
{
  /* The labels. */
  visu_gl_ext_blitLabelsOnScreen(VISU_GL_EXT(self));
}

static void visu_gl_ext_legend_rebuild(VisuGlExt *legend)
{
  GError *error = (GError*)0;

  if (!visu_gl_ext_setShaderById(legend, SHADER_OBJECT, VISU_GL_SHADER_MATERIAL, &error))
    {
      g_warning("Cannot create legend shader: %s", error->message);
      g_clear_error(&error);
    }
  if (!visu_gl_ext_setShaderById(legend, SHADER_POINT, VISU_GL_SHADER_FLAT, &error))
    {
      g_warning("Cannot create legend shader: %s", error->message);
      g_clear_error(&error);
    }
  VISU_GL_EXT_CLASS(visu_gl_ext_legend_parent_class)->rebuild(legend);
}
struct _BufClosure
{
  VisuGlExt *ext;
  guint iBuf;
  VisuElement *element;
  VisuElementRenderer *renderer;
};
static void _commitBuf(const GArray *vertices, guint id _U_, const gfloat rgba[4] _U_, const gfloat material[5] _U_, gpointer data)
{
  struct _BufClosure *closure = (struct _BufClosure*)data;
  gfloat rgba_[4], material_[5];

  if (visu_element_getRendered(closure->element))
    visu_element_renderer_getColorization(closure->renderer, VISU_ELEMENT_RENDERER_NO_EFFECT, rgba_, material_);
  else
    visu_element_renderer_getColorization(closure->renderer, VISU_ELEMENT_RENDERER_FLATTEN_DARK, rgba_, material_);
  visu_gl_ext_layoutBufferWithColor(closure->ext, closure->iBuf, vertices, rgba_, material_);
}
static void visu_gl_ext_legend_draw(VisuGlExt *self)
{
  VisuGlExtLegend *legend;
  guint nEle, j;
  VisuNodeArrayRendererIter iter;
  gboolean valid;
  gfloat offset;
  gfloat scale, mSize, dw;
  GArray *labels;
  VisuGlExtFrame *frame;
  gfloat xyz[3];
  struct _BufClosure closure;

  VISU_GL_EXT_CLASS(visu_gl_ext_legend_parent_class)->draw(self);

  legend = VISU_GL_EXT_LEGEND(self);
  if (!legend->priv->nodes)
    return;

  mSize = visu_node_array_renderer_getMaxElementSize(legend->priv->nodes, &nEle);
  visu_gl_ext_bookBuffers(self, nEle + 1);
  frame = VISU_GL_EXT_FRAME(self);
  dw = MAX(frame->width / nEle, frame->height + 10 + 60);
  xyz[1] = 0.5f * frame->height;
  xyz[2] = 0.f;
  scale = 0.5f * frame->height / mSize;
  offset = (gfloat)(frame->height + 5) / frame->width;

  closure.ext = self;
  labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));
  for (valid = visu_node_array_renderer_iter_new(legend->priv->nodes, &iter, TRUE), j = 1;
       valid; valid = visu_node_array_renderer_iter_next(&iter), j+= 1)
    {
      VisuGlExtPackingModels packing;
      VisuGlExtPrimitives primitive;
      GArray *vertices[10];
      guint nBuf, iBuf;
      VisuGlExtLabel lbl;
      sprintf(lbl.lbl, "%s (%d)", visu_element_getName(iter.element),
              iter.nStoredNodes);
      lbl.xyz[0] = offset + (gfloat)(j - 1) / nEle;
      lbl.xyz[1] = 0.5f;
      memcpy(lbl.rgba, frame->fontRGB, sizeof(gfloat) * 4);
      lbl.align[0] = VISU_GL_LEFT;
      lbl.align[1] = VISU_GL_CENTER;
      g_array_append_val(labels, lbl);

      nBuf = visu_element_renderer_getVerticeBufferDim(iter.renderer);
      closure.element = iter.element;
      closure.renderer = iter.renderer;
      closure.iBuf = j;
      xyz[0] = 0.5f * frame->height + (j - 1) * dw;
      for (iBuf = 0; iBuf < nBuf - 1; iBuf++)
        vertices[iBuf] = (GArray*)0;
      visu_element_renderer_getVerticeLayout(iter.renderer, nBuf - 1, &packing, &primitive);
      if (packing == VISU_GL_XYZ)
        vertices[nBuf - 1] = visu_gl_ext_startBuffer(self, SHADER_POINT, j, packing, primitive);
      else if (packing == VISU_GL_XYZ_NRM)
        vertices[nBuf - 1] = visu_gl_ext_startBuffer(self, SHADER_OBJECT, j, packing, primitive);
      else
        g_return_if_reached();
      visu_element_renderer_addVerticesAt(iter.renderer, xyz, scale,
                                          vertices, _commitBuf, &closure);
      visu_gl_ext_takeBuffer(self, j, vertices[nBuf - 1]);
    }
  visu_gl_ext_setLabels(self, labels, TOOL_WRITER_FONT_NORMAL, FALSE);
  g_array_unref(labels);
}

static void exportResources(GString *data, VisuData *dataObj _U_)
{
  if (!defaultLegend)
    return;

  visu_config_file_exportComment(data, DESC_RESOURCE_LEGEND_USED);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_LEGEND_USED, NULL,
                               "%d", visu_gl_ext_getActive(VISU_GL_EXT(defaultLegend)));
  visu_config_file_exportComment(data, "");
}
