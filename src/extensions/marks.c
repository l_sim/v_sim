/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include <glib.h>
#include <epoxy/gl.h>
#include <math.h>

#include "marks.h"
#include <visu_elements.h>
#include <visu_dataloadable.h>
#include <visu_configFile.h>
#include <visu_extension.h>
#include <openGLFunctions/view.h>
#include <coreTools/toolShape.h>

/**
 * SECTION:marks
 * @short_description: Draw features on nodes, like measurement marks
 * or highlights.
 *
 * <para>#VisuGlExtMarks is used to store a set of mark on a list of
 * nodes. A mark can be a distance measurement, an angle measurement
 * or an highlight. The measurement marks are automatically updated by
 * listening to the #VisuInteractive::node-selection signal. On the
 * contrary, highlights are set, unset or toggled using
 * visu_gl_ext_marks_setHighlight().</para>
 * <para>In addition, #VisuGlExtMarks can be export to or loaded from an
 * XML file thanks to visu_gl_ext_marks_exportXMLFile() and visu_gl_ext_marks_parseXMLFile().</para>
 *
 * Since: 3.6
 */

enum
  {
   BUF_SMALL_POINTS,
   BUF_BIG_POINTS,
   BUF_LINES,
   BUF_POINT_ANGLES,
   BUF_LINE_ANGLES,
   BUF_ANGLES,
   BUF_RECT,
   N_BUFFERS
  };

enum
  {
   SHADER_MARKS,
   SHADER_SELECTION,
   N_SHADERS
  };

typedef enum
  {
    MARK_BIG_SQUARE,
    MARK_SMALL_SQUARE,
    MARK_HIGHLIGHT,
    MARK_DISTANCE,
    MARK_ANGLE,
  } VisuMarkType;

struct MarkInfo_struct
{
  /* Mark type. */
  VisuMarkType type;

  /* Id used to address the VisuNode when the
     pointer node1 has changed, for example when a new VisuData
     is loaded with the same geometry and we want to apply this mark. */
  guint idNode1;
  /* Idem for a second node. */
  guint idNode2;
  /* Idem for a third node. */
  guint idNode3;
};

/**
 * VisuNodeInfo:
 * @id: an id;
 * @dist: a float.
 *
 * Some data.
 *
 * Since: 3.5
 */
typedef struct _VisuNodeInfo VisuNodeInfo;
struct _VisuNodeInfo
{
  guint id;
  float dist;
};

typedef struct _ExtNode ExtNode;
struct _ExtNode
{
  VisuGlExt parent;

  VisuGlExtMarks *marks;
};

struct _VisuGlExtMarksPrivate
{
  gboolean dispose_has_run;
  ExtNode *extNode;

  /* A reference on the #VisuData and #VisuGlView it refers to. */
  VisuGlView *view;
  VisuNodeArrayRenderer *renderer;
  gulong nodePosition, nodeRender, nodeMaterial, nodePopDec, nodePopInc, siz_sig, dat_sig;

  /* Interactive object to get measurement signals. */
  VisuInteractive *inter;
  gulong nodeSelection, rectSelection;

  /* A list of MarkInfo_struct elements. */
  GList *storedMarks;
  gboolean drawValues;
  GArray *cachedHighlighted;
  gint x1, y1, x2, y2;

  /* Miscellaneous. */
  float infoRange;
  VisuGlExtMarksHidingModes hidingMode;
};


enum
  {
    MEASUREMENT_CHANGE_SIGNAL,
    NB_SIGNAL
  };
static guint signals[NB_SIGNAL];
enum
  {
    PROP_0,
    VIEW_PROP,
    INTER_PROP,
    HIDING_PROP,
    HIGHLIGHT_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

/* Local variables. */
static float highlightFactor = 1.25f;

/* Local methods. */
static ExtNode* ext_node_new(VisuGlExtMarks *marks);
static struct MarkInfo_struct* markNew(VisuMarkType type);
static VisuMarkType markRemove(VisuGlExtMarks *marks, GList *rmList);
static void addDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type);
static gboolean toggleDistance(VisuGlExtMarks *marks, guint nodeRefId,
			       guint nodeId, gboolean set);
static gboolean toggleHighlight(VisuGlExtMarks *marks, guint nodeId,
				VisuGlExtMarksStatus status, gboolean *finalStatus);
static gboolean setInformation(VisuGlExtMarks *marks, VisuData *dataObj, guint node);

static GObject* visu_gl_ext_marks_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_marks_dispose(GObject *obj);
static void visu_gl_ext_marks_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec);
static void visu_gl_ext_marks_set_property(GObject* obj, guint property_id,
                                           const GValue *value,
                                           GParamSpec *pspec);
static void visu_gl_ext_marks_rebuild(VisuGlExt *ext);
static void visu_gl_ext_marks_draw(VisuGlExt *ext);
static void visu_gl_ext_marks_render(const VisuGlExt *ext);
static gboolean visu_gl_ext_marks_setGlView(VisuGlExt *ext, VisuGlView *view);
static void visu_node_masker_interface_init(VisuNodeMaskerInterface *iface);
static gboolean _maskApply(const VisuNodeMasker *self, VisuNodeArray *array);
static VisuNodeInfo* _getDistanceList(VisuData *data, guint nodeId, float *minVal);

/* Local callbacks. */
static void _setDirty(VisuGlExtMarks *marks);
static void onNotifyData(VisuGlExtMarks *marks, GParamSpec *pspec,
                         VisuNodeArrayRenderer *renderer);
static void _populationDecrease(VisuGlExtMarks *marks, GArray *nodes);
static gboolean onNodeSelection(VisuInteractive *inter, VisuInteractivePick kind,
                                VisuData *dataObje, VisuNode *node1,
                                VisuNode *node2, VisuNode *node3, gpointer data);
static void onRectSelection(VisuInteractive *inter, gint x1, gint y1,
                            gint x2, gint y2, gpointer data);

/* V_Sim resources and paramaters. */
#define FLAG_RESOURCE_FACTOR "highlight_radiusFactor"
#define DESC_RESOURCE_FACTOR "Give the factor for the highlight radius ; one float (> 1.)"
static void exportResources(GString *data, VisuData *dataObj);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtMarks, visu_gl_ext_marks, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtMarks)
                        G_IMPLEMENT_INTERFACE(VISU_TYPE_NODE_MASKER,
                                              visu_node_masker_interface_init))

static void visu_gl_ext_marks_class_init(VisuGlExtMarksClass *klass)
{
  float rg[2] = {1.01f, G_MAXFLOAT};
  VisuConfigFileEntry *conf;

  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_marks_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_marks_dispose;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_marks_get_property;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_marks_set_property;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_marks_rebuild;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_marks_draw;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_marks_render;
  VISU_GL_EXT_CLASS(klass)->renderText = visu_gl_ext_blitLabels;
  VISU_GL_EXT_CLASS(klass)->setGlView = visu_gl_ext_marks_setGlView;

  g_debug("Visu Marks: installing signals.");
  /**
   * VisuGlExtMarks::measurementChanged:
   * @marks: the object emitting the signal.
   * @data: the #VisuData the measurement is done on.
   *
   * The list of measurements has been changed.
   *
   * Since: 3.6
   */
  signals[MEASUREMENT_CHANGE_SIGNAL] = 
    g_signal_new("measurementChanged", G_TYPE_FROM_CLASS(klass),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__OBJECT,
                 G_TYPE_NONE, 1, VISU_TYPE_DATA);

  /**
   * VisuGlExtMarks::gl-view:
   *
   * Store to which #VisuGlView the marks labels are aligned with.
   *
   * Since: 3.8
   */
  properties[VIEW_PROP] = g_param_spec_object("gl-view", "GlView",
                                              "GlView mark labels are aligned with",
                                              VISU_TYPE_GL_VIEW, G_PARAM_READWRITE);
  /**
   * VisuGlExtMarks::interactive:
   *
   * Store which #VisuInteractive the marks should react on.
   *
   * Since: 3.8
   */
  properties[INTER_PROP] = g_param_spec_object("interactive", "Interactive",
                                               "Interactive the marks react on",
                                               VISU_TYPE_INTERACTIVE, G_PARAM_READWRITE);
  /**
   * VisuGlExtMarks::hiding-mode:
   *
   * Mode used to hide nodes.
   *
   * Since: 3.8
   */
  properties[HIDING_PROP] = g_param_spec_uint("hiding-mode", "Hiding mode",
                                              "nodes hiding property",
                                              0, HIDE_NON_HIGHLIGHT, HIDE_NONE,
                                              G_PARAM_READWRITE);
  /**
   * VisuGlExtMarks::highlight:
   *
   * Set of node ids with highlight.
   *
   * Since: 3.8
   */
  properties[HIGHLIGHT_PROP] = g_param_spec_boxed("highlight", "Highlight",
                                                  "node ids with highlight mark.",
                                                  G_TYPE_ARRAY, G_PARAM_READWRITE);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROP, properties);

  conf = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                             FLAG_RESOURCE_FACTOR,
                                             DESC_RESOURCE_FACTOR,
                                             1, &highlightFactor, rg, FALSE);
  visu_config_file_entry_setVersion(conf, 3.6f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResources);
}
static void visu_node_masker_interface_init(VisuNodeMaskerInterface *iface)
{
  iface->apply = _maskApply;
}

static GObject* visu_gl_ext_marks_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, N_BUFFERS);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, N_SHADERS);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LAST);
    }

  return G_OBJECT_CLASS(visu_gl_ext_marks_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_marks_init(VisuGlExtMarks *marks)
{
  g_debug("Visu Marks: create a new object, %p.",
	      (gpointer)marks);

  marks->priv = visu_gl_ext_marks_get_instance_private(marks);
  marks->priv->dispose_has_run = FALSE;

  marks->priv->renderer           = (VisuNodeArrayRenderer*)0;
  marks->priv->view               = (VisuGlView*)0;
  marks->priv->storedMarks        = (GList*)0;
  marks->priv->cachedHighlighted  = (GArray*)0;
  marks->priv->drawValues         = TRUE;
  marks->priv->infoRange          = 1.44f;
  marks->priv->extNode            = ext_node_new(marks);
  marks->priv->inter              = (VisuInteractive*)0;
  marks->priv->nodeSelection      = 0;
  marks->priv->rectSelection      = 0;
  marks->priv->hidingMode         = HIDE_NONE;
}

static void visu_gl_ext_marks_dispose(GObject *obj)
{
  VisuGlExtMarks *marks;

  g_debug("Visu Marks: dispose object %p.", (gpointer)obj);

  marks = VISU_GL_EXT_MARKS(obj);

  if (marks->priv->dispose_has_run)
    return;
  marks->priv->dispose_has_run = TRUE;

  visu_gl_ext_marks_setDataRenderer(marks, (VisuNodeArrayRenderer*)0);
  g_debug(" | release VisuGlView.");
  visu_gl_ext_marks_setGlView(VISU_GL_EXT(marks), (VisuGlView*)0);
  g_debug(" | free internal VisuGlExt.");
  g_object_unref(G_OBJECT(marks->priv->extNode));

  g_debug(" | unconnect signals.");
  visu_gl_ext_marks_setInteractive(marks, (VisuInteractive*)0);

  /* Chain up to the parent class */
  g_debug(" | chain up.");
  G_OBJECT_CLASS(visu_gl_ext_marks_parent_class)->dispose(obj);
  g_debug(" | OK.");
}
static void visu_gl_ext_marks_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec)
{
  VisuGlExtMarks *self = VISU_GL_EXT_MARKS(obj);

  g_debug("Extension Marks: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case VIEW_PROP:
      g_value_set_object(value, self->priv->view);
      g_debug("%p.", (gpointer)self->priv->view);
      break;
    case INTER_PROP:
      g_value_set_object(value, self->priv->inter);
      g_debug("%p.", (gpointer)self->priv->inter);
      break;
    case HIDING_PROP:
      g_value_set_uint(value, self->priv->hidingMode);
      g_debug("%d.", self->priv->hidingMode);
      break;
    case HIGHLIGHT_PROP:
      g_value_set_boxed(value, visu_gl_ext_marks_getHighlighted(self));
      g_debug("%p.", g_value_get_boxed(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_marks_set_property(GObject* obj, guint property_id,
                                           const GValue *value, GParamSpec *pspec)
{
  VisuGlExtMarks *self = VISU_GL_EXT_MARKS(obj);

  g_debug("Extension Marks: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case VIEW_PROP:
      visu_gl_ext_marks_setGlView(VISU_GL_EXT(obj),
                                  VISU_GL_VIEW(g_value_get_object(value)));
      g_debug("%p.", (gpointer)self->priv->view);
      break;
    case INTER_PROP:
      g_debug("%p.", g_value_get_object(value));
      visu_gl_ext_marks_setInteractive
        (self, VISU_INTERACTIVE(g_value_get_object(value)));
      break;
    case HIDING_PROP:
      g_debug("%d.", g_value_get_uint(value));
      visu_gl_ext_marks_setHidingMode(self, g_value_get_uint(value));
      break;
    case HIGHLIGHT_PROP:
      g_debug("%p.", g_value_get_boxed(value));
      visu_gl_ext_marks_setHighlight(self, g_value_get_boxed(value), MARKS_STATUS_SET);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * visu_gl_ext_marks_new:
 * @name: (allow-none): a possible name for the #VisuGlExt.
 *
 * Create a new #VisuGlExtMarks object. Make it listen to
 * #VisuInteractive::node-selection signal to update itself
 * automatically.
 *
 * Returns: the newly created object.
 */
VisuGlExtMarks* visu_gl_ext_marks_new(const gchar *name)
{
  char *name_ = "MarksInv";
  char *description = _("Draw some marks on element in video inverse.");

  return g_object_new(VISU_TYPE_GL_EXT_MARKS,
                      "name", name ? name : name_, "label", _(name),
                      "description", description, NULL);
}

/**
 * visu_gl_ext_marks_getInternalList:
 * @marks: a #VisuGlExtMarks object.
 *
 * Return an additional list used internaly.
 *
 * Since: 3.7
 *
 * Returns: (transfer none): a #VisuGlExt object.
 **/
VisuGlExt* visu_gl_ext_marks_getInternalList(VisuGlExtMarks *marks)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), (VisuGlExt*)0);

  return VISU_GL_EXT(marks->priv->extNode);
}

static struct MarkInfo_struct* markNew(VisuMarkType type)
{
  struct MarkInfo_struct *mark;

  mark          = g_malloc(sizeof(struct MarkInfo_struct));
  mark->type    = type;
  mark->idNode1 = -1;
  mark->idNode2 = -1;
  mark->idNode3 = -1;

  return (struct MarkInfo_struct*)mark;
}
static VisuMarkType markRemove(VisuGlExtMarks *marks, GList *rmList)
{
  struct MarkInfo_struct *mark;
  VisuMarkType type;

  mark = (struct MarkInfo_struct*)rmList->data;
  g_debug("Visu Marks: remove mark (%d %d %d).",
	      mark->idNode1, mark->idNode2, mark->idNode3);
  type = mark->type;
  if (mark->type == MARK_HIGHLIGHT && marks->priv->cachedHighlighted)
    {
      g_array_unref(marks->priv->cachedHighlighted);
      marks->priv->cachedHighlighted = (GArray*)0;
    }
  g_free(mark);
  marks->priv->storedMarks = g_list_delete_link(marks->priv->storedMarks, rmList);
  
  return type;
}

/**
 * visu_gl_ext_marks_setInteractive:
 * @marks: a #VisuGlExtMarks object.
 * @inter: (transfer full) (allow-none): a #VisuInteractive object.
 *
 * Listen to #VisuInteractive::node-selection signal to update @marks.
 *
 * Since: 3.7
 **/
void visu_gl_ext_marks_setInteractive(VisuGlExtMarks *marks, VisuInteractive *inter)
{
  g_return_if_fail(VISU_IS_GL_EXT_MARKS(marks));

  g_debug("Visu Marks: set interactive object.");
  if (marks->priv->inter)
    {
      g_debug(" | old interactive (%p) has %d ref counts.",
                  (gpointer)marks->priv->inter, G_OBJECT(marks->priv->inter)->ref_count);
      g_signal_handler_disconnect(G_OBJECT(marks->priv->inter), marks->priv->nodeSelection);
      g_signal_handler_disconnect(G_OBJECT(marks->priv->inter), marks->priv->rectSelection);
      g_object_unref(marks->priv->inter);
    }
  if (inter)
    {
      g_object_ref(inter);
      marks->priv->nodeSelection = g_signal_connect(G_OBJECT(inter), "node-selection",
                                              G_CALLBACK(onNodeSelection), (gpointer)marks);
      marks->priv->rectSelection = g_signal_connect(G_OBJECT(inter), "rectangle-selection",
                                              G_CALLBACK(onRectSelection), (gpointer)marks);
      g_debug(" | new interactive (%p) has %d ref counts.",
                  (gpointer)inter, G_OBJECT(inter)->ref_count);
    }
  else
    marks->priv->nodeSelection = 0;
      
  marks->priv->inter = inter;
  g_object_notify_by_pspec(G_OBJECT(marks), properties[INTER_PROP]);
}
static gboolean visu_gl_ext_marks_setGlView(VisuGlExt *ext, VisuGlView *view)
{
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(ext);

  g_debug(" | marks has %d ref counts.",
              G_OBJECT(marks)->ref_count);
  /* We attach a new VisuGlView object. */
  if (marks->priv->view == view)
    return FALSE;

  /* We unref the previous VisuView and disconnect signals. */
  if (marks->priv->view)
    g_object_unref(marks->priv->view);
  marks->priv->view = view;
  if (marks->priv->view)
    g_object_ref(marks->priv->view);
  g_object_notify_by_pspec(G_OBJECT(marks), properties[VIEW_PROP]);
  return TRUE;
}
static void _setData(VisuGlExtMarks *marks, VisuNodeArray *array)
{
  GList *list, *rmList;
  struct MarkInfo_struct*mark;
  gboolean remove;
  gboolean invDirty, dirDirty;
  VisuMarkType type;

  invDirty = dirDirty = FALSE;
  /* Mark list as dirty because of position changed, if necessary. */
  for (list = marks->priv->storedMarks; list; list = g_list_next(list))
    if (((struct MarkInfo_struct*)list->data)->type == MARK_HIGHLIGHT)
      dirDirty = TRUE;
    else
      invDirty = TRUE;

  /* We update the list of nodes with marks. */
  if (array)
    {
      /* Try to match previous marks on new VisuData. */
      list = marks->priv->storedMarks;
      while(list)
	{
	  mark = (struct MarkInfo_struct*)list->data;
	  g_debug(" | old mark %p of type %d.",
		      (gpointer)mark, mark->type);
	  rmList = list;
	  /* We remove mark if one of the node can't be matched. */
	  switch (mark->type)
	    {
	    case MARK_BIG_SQUARE:
	    case MARK_SMALL_SQUARE:
	    case MARK_HIGHLIGHT:
	      remove = !visu_node_array_getFromId(array, mark->idNode1);
	      break;
	    case MARK_DISTANCE:
	      remove =
		!visu_node_array_getFromId(array, mark->idNode1) ||
		!visu_node_array_getFromId(array, mark->idNode2);
	      break;
	    case MARK_ANGLE:
	      remove =
		!visu_node_array_getFromId(array, mark->idNode1) ||
		!visu_node_array_getFromId(array, mark->idNode2) ||
		!visu_node_array_getFromId(array, mark->idNode3);
	      break;
	    default:
              remove = FALSE;
	      g_warning("TODO implementation required.");
	    }
	  list = g_list_next(list);
	  if (remove)
	    {
	      g_debug(" | delete old mark %p of type %d.",
			  (gpointer)mark, mark->type);
	      type = markRemove(marks, rmList);
              if (type == MARK_HIGHLIGHT)
                dirDirty = TRUE;
              else
                invDirty = TRUE;
	    }
	}
    }
  else
    {
      invDirty = dirDirty = (g_list_length(marks->priv->storedMarks) > 0);
      for (list = marks->priv->storedMarks; list; list = g_list_next(list))
	g_free(list->data);
      g_list_free(marks->priv->storedMarks);
      marks->priv->storedMarks   = (GList*)0;
      if (marks->priv->cachedHighlighted)
        g_array_unref(marks->priv->cachedHighlighted);
      marks->priv->cachedHighlighted = (GArray*)0;
    }
  g_debug(" | New mark list is pointing to %p.",
	      (gpointer)marks->priv->storedMarks);
  /* Signal changes. */
  g_object_notify_by_pspec(G_OBJECT(marks), properties[HIGHLIGHT_PROP]);
  g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, array);
  if (invDirty)
    visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
  if (dirDirty)
    visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
}
/**
 * visu_gl_ext_marks_setDataRenderer:
 * @marks: a #VisuGlExtMarks object.
 * @renderer: a #VisuNodeArrayRenderer object.
 *
 * Attach the given @marks to @data. @marks will be updated if @data
 * is changed and internal list of marks is updated with the new nodes
 * of @data.
 */
void visu_gl_ext_marks_setDataRenderer(VisuGlExtMarks *marks, VisuNodeArrayRenderer *renderer)
{
  g_debug("Visu Marks: set new VisuNodeArrayRenderer %p (%p).",
	      (gpointer)renderer, (gpointer)marks->priv->renderer);

  if (marks->priv->renderer == renderer)
    return;

  /* We unref the previous VisuData and disconnect signals. */
  if (marks->priv->renderer)
    {
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->nodePosition);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->nodeRender);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->nodeMaterial);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->nodePopDec);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->nodePopInc);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->siz_sig);
      g_signal_handler_disconnect(marks->priv->renderer, marks->priv->dat_sig);
      g_object_unref(marks->priv->renderer);
    }
  marks->priv->renderer = renderer;
  if (renderer)
    {
      /* We ref the new given data. */
      g_object_ref(renderer);

      /* Connect a signal on file change to remove everything. */
      marks->priv->nodePopDec =
        g_signal_connect_swapped(renderer, "nodes::population-decrease",
                                 G_CALLBACK(_populationDecrease), (gpointer)marks);
      marks->priv->nodePopInc =
        g_signal_connect_swapped(renderer, "nodes::population-increase",
                                 G_CALLBACK(onNotifyData), (gpointer)marks);
      marks->priv->nodePosition =
        g_signal_connect_swapped(renderer, "nodes::position",
                                 G_CALLBACK(_setDirty), (gpointer)marks);
      marks->priv->nodeRender =
        g_signal_connect_swapped(renderer, "nodes::visibility",
                                 G_CALLBACK(_setDirty), (gpointer)marks);
      marks->priv->nodeMaterial =
        g_signal_connect_swapped(renderer, "element-notify::color",
                                 G_CALLBACK(_setDirty), (gpointer)marks);
      marks->priv->siz_sig =
        g_signal_connect_swapped(renderer, "element-size-changed",
                                 G_CALLBACK(_setDirty), (gpointer)marks);
      marks->priv->dat_sig =
        g_signal_connect_swapped(renderer, "notify::data",
                                 G_CALLBACK(onNotifyData), (gpointer)marks);
    }
  _setData(marks, (renderer) ? visu_node_array_renderer_getNodeArray(renderer) : (VisuNodeArray*)0);
}

static void onNotifyData(VisuGlExtMarks *marks, GParamSpec *pspec _U_,
                         VisuNodeArrayRenderer *renderer)
{
  _setData(marks, visu_node_array_renderer_getNodeArray(renderer));
}
static void _populationDecrease(VisuGlExtMarks *marks, GArray *nodes)
{
  guint i;
  GList *list, *rmList;
  struct MarkInfo_struct*mark;
  gboolean remove;
  gboolean invDirty, dirDirty;
  VisuMarkType type;

  g_debug("Visu Marks: caught 'PopulationDecrease' signal (%p).", (gpointer)marks);
  
  invDirty = dirDirty = FALSE;

  /* Run through the mark list to get all nodeId and look into nodes
     to find if mark must be removed or not. */
  g_debug(" | list contains %d elements",
	      g_list_length(marks->priv->storedMarks));
  list = marks->priv->storedMarks;
  while(list)
    {
      mark = (struct MarkInfo_struct*)list->data;
      g_debug(" | mark %p of type %d.",
		  (gpointer)mark, mark->type);
      /* We remove mark if one of the node is in the list. */
      remove = FALSE;
      rmList = list;
      for (i = 0; !remove && i < nodes->len; i++)
	remove = remove ||
	  (g_array_index(nodes, guint, i) == mark->idNode1) ||
	  (g_array_index(nodes, guint, i) == mark->idNode2) ||
	  (g_array_index(nodes, guint, i) == mark->idNode3);
      list = g_list_next(list);
      if (remove)
	{
	  g_debug(" | delete mark %p of type %d.",
		      (gpointer)mark, mark->type);
          type = markRemove(marks, rmList);
          if (type == MARK_HIGHLIGHT)
            dirDirty = TRUE;
          else
            invDirty = TRUE;
	}
    }

  if (invDirty)
    visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
  if (dirDirty)
    visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
}
static void _setDirty(VisuGlExtMarks *marks)
{
  visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
  visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
}
static gboolean _setReference(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  for (GList *list = marks->priv->storedMarks; list; list = g_list_next(list))
    {
      struct MarkInfo_struct *item = (struct MarkInfo_struct*)list->data;
      if (item->type == type)
        {
          if (item->idNode1 == nodeId)
            return FALSE;
          markRemove(marks, list);
          break;
        }
    }

  /* Add new one. */
  addDot(marks, nodeId, type);

  visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
static gboolean onNodeSelection(VisuInteractive *inter _U_, VisuInteractivePick pick,
                                VisuData *dataObj, VisuNode *node0, VisuNode *node1,
                                VisuNode *node2, gpointer data)
{
  gboolean set, status;
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(data);

  g_debug("Visu Marks: get a 'node-selection' signals -> %d.", pick);
  
  switch (pick)
    {
    case PICK_DISTANCE:
      if (marks->priv->drawValues)
	{
	  toggleDistance(marks, node1->number, node0->number, FALSE);
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, dataObj);
          visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
	}
      return TRUE;
    case PICK_ANGLE:
      if (marks->priv->drawValues)
	{
	  visu_gl_ext_marks_setAngle(marks, node1->number, node2->number, node0->number, MARKS_STATUS_TOGGLE);
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, dataObj);
          visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
	}
      return TRUE;
    case PICK_REFERENCE_1:
      _setReference(marks, node0->number, MARK_BIG_SQUARE);
      return TRUE;
    case PICK_REFERENCE_2:
      _setReference(marks, node0->number, MARK_SMALL_SQUARE);
      return TRUE;
    case PICK_UNREFERENCE_1:
    case PICK_UNREFERENCE_2:
      for (GList *list = marks->priv->storedMarks; list; list = g_list_next(list))
        {
          struct MarkInfo_struct *item = (struct MarkInfo_struct*)list->data;
          if ((item->type == MARK_BIG_SQUARE ||
               item->type == MARK_SMALL_SQUARE) &&
              (item->idNode1 == node0->number))
            {
              markRemove(marks, list);
              visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
              break;
            }
        }
      return TRUE;
    case PICK_HIGHLIGHT:
      set = toggleHighlight(marks, node0->number, MARKS_STATUS_TOGGLE, &status);
      if (set)
	{
          g_object_notify_by_pspec(G_OBJECT(marks), properties[HIGHLIGHT_PROP]);
          if (marks->priv->hidingMode != HIDE_NONE)
            visu_node_masker_emitDirty(VISU_NODE_MASKER(marks));
          visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
	}
      return TRUE;
    case PICK_INFORMATION:
      set = setInformation(marks, dataObj, node0->number);
      if (set)
	{
          g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, dataObj);
          visu_gl_ext_setDirty(VISU_GL_EXT(data), VISU_GL_DRAW_REQUIRED);
	}
      return TRUE;
    case PICK_SELECTED:
    default:
      return TRUE;
    }
}
static void onRectSelection(VisuInteractive *inter _U_, gint x1, gint y1,
                            gint x2, gint y2, gpointer data)
{
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(data);

  marks->priv->x1 = x1;
  marks->priv->y1 = y1;
  marks->priv->x2 = x2;
  marks->priv->y2 = y2;
  
  visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
}

/* Method that add/remove  mark to the list of drawn marks. */
static void addDot(VisuGlExtMarks *marks, guint nodeId, VisuMarkType type)
{
  struct MarkInfo_struct *mark;

  g_return_if_fail((type == MARK_BIG_SQUARE ||
		    type == MARK_SMALL_SQUARE ||
		    type == MARK_HIGHLIGHT));

  g_debug("Visu Marks: add a new mark of type %d.", type);
  mark          = markNew(type);
  mark->idNode1 = nodeId;
  marks->priv->storedMarks = g_list_prepend(marks->priv->storedMarks, (gpointer)mark);
  if (type == MARK_HIGHLIGHT && marks->priv->cachedHighlighted)
    {
      g_array_unref(marks->priv->cachedHighlighted);
      marks->priv->cachedHighlighted = (GArray*)0;
    }
  return;
}
static gboolean toggleDistance(VisuGlExtMarks *marks, guint nodeRefId,
			       guint nodeId, gboolean set)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(marks, FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_DISTANCE &&
	  ((mark->idNode1 == nodeRefId && mark->idNode2 == nodeId) ||
	   (mark->idNode2 == nodeRefId && mark->idNode1 == nodeId)))
	{
	  g_debug("Visu Marks: found a distance mark.");
	  if (!set)
	    markRemove(marks, tmpLst);
	  return set;
	}
    }
  /* Found none, create a new one. */
  mark          = markNew(MARK_DISTANCE);
  mark->idNode1 = nodeRefId;
  mark->idNode2 = nodeId;
  g_debug("Visu Marks: add a distance mark.");
  marks->priv->storedMarks = g_list_prepend(marks->priv->storedMarks, (gpointer)mark);
  return TRUE;
}
/**
 * visu_gl_ext_marks_setAngle:
 * @marks: a #VisuGlExtMarks object.
 * @ref: a node id.
 * @node1: a node id.
 * @node2: a node id.
 * @status: the action to do.
 *
 * Change the angle visibility centred on @ref between @node1 and
 * @node2, depending on @status.
 *
 * Since: 3.9
 *
 * Returns: TRUE if the angle has been toggled.
 */
gboolean visu_gl_ext_marks_setAngle(VisuGlExtMarks *marks,
                                    guint ref, guint node1, guint node2,
                                    VisuGlExtMarksStatus status)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_ANGLE &&
	  mark->idNode1 == ref &&
	  ((mark->idNode2 == node1 && mark->idNode3 == node2) ||
	   (mark->idNode3 == node1 && mark->idNode2 == node2)))
	{
	  g_debug("Visu Marks: found an angle mark.");
	  if (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET)
	    markRemove(marks, tmpLst);
	  return status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET;
	}
    }
  /* Found none, create a new one. */
  if (status == MARKS_STATUS_SET || status == MARKS_STATUS_TOGGLE)
    {
      mark          = markNew(MARK_ANGLE);
      mark->idNode1 = ref;
      mark->idNode2 = node1;
      mark->idNode3 = node2;
      g_debug("Visu Marks: add an angle mark.");
      marks->priv->storedMarks = g_list_prepend(marks->priv->storedMarks, (gpointer)mark);
      return TRUE;
    }
  return FALSE;
}
static gboolean toggleHighlight(VisuGlExtMarks *marks, guint nodeId,
				VisuGlExtMarksStatus status, gboolean *finalStatus)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  /* Look for the mark. */
  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_HIGHLIGHT &&
	  mark->idNode1 == nodeId)
	{
	  g_debug("Visu Marks: found a highlight mark (%d).", nodeId);
	  if (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET)
	    markRemove(marks, tmpLst);
	  if (finalStatus)
	    *finalStatus = (status != MARKS_STATUS_TOGGLE && status != MARKS_STATUS_UNSET);
	  /* Return if it has been changed. */
	  return (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_UNSET);
	}
    }
  if (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET)
    /* Found none, create a new one. */
    addDot(marks, nodeId, MARK_HIGHLIGHT);
  if (finalStatus)
    *finalStatus = (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET);
  /* Return if it has been changed. */
  return (status == MARKS_STATUS_TOGGLE || status == MARKS_STATUS_SET);
}
/**
 * visu_gl_ext_marks_unHighlight:
 * @marks: a #VisuGlExtMarks object.
 *
 * Remove all highlight marks.
 *
 * Since: 3.8
 *
 * Returns: TRUE if anything was highlighted.
 **/
gboolean visu_gl_ext_marks_unHighlight(VisuGlExtMarks *marks)
{
  struct MarkInfo_struct *mark;
  GList *tmpLst, *rmLst;
  gboolean changed;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  /* Look for the mark. */
  changed = FALSE;
  tmpLst = marks->priv->storedMarks;
  while (tmpLst)
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      rmLst = tmpLst;
      tmpLst = g_list_next(tmpLst);
      if (mark->type == MARK_HIGHLIGHT)
	{
          markRemove(marks, rmLst);
          changed = TRUE;
	}
    }
  if (changed)
    {
      g_object_notify_by_pspec(G_OBJECT(marks), properties[HIGHLIGHT_PROP]);
      if (marks->priv->hidingMode != HIDE_NONE)
        visu_node_masker_emitDirty(VISU_NODE_MASKER(marks));
      visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
    }
  return changed;
}
static gboolean setInformation(VisuGlExtMarks *marks, VisuData *dataObj, guint node)
{
  VisuNodeInfo *infos;
  float min;
  GList *lst, *tmpLst, *tmpLst2;
  int i, n;
  gboolean redraw;
  float *dists;
  guint *ids;
  float xyz1[3], xyz2[3];

  g_return_val_if_fail(marks, FALSE);

  g_debug("Visu Marks: compute and print distances"
	      " and angles for %d.", node);
  infos = _getDistanceList(dataObj, node, &min);

  lst = (GList*)0;
  redraw = FALSE;
  for (i = 0; infos[i].id != node; i+= 1)
    if (infos[i].dist < marks->priv->infoRange * min)
      {
	toggleDistance(marks, node, infos[i].id, TRUE);
	lst = g_list_prepend(lst, GINT_TO_POINTER(infos[i].id));
	redraw = TRUE;
      }
  g_free(infos);

  n = g_list_length(lst);
  if (n > 1)
    {
      n = n * (n - 1 ) / 2;
      g_debug("Visu Marks: there are %d angles at max.", n);
      ids = g_malloc(sizeof(guint) * n * 2);
      dists = g_malloc(sizeof(float) * n);
      i = 0;
      min = G_MAXFLOAT;
      for (tmpLst = lst; tmpLst; tmpLst = g_list_next(tmpLst))
	for (tmpLst2 = tmpLst->next; tmpLst2; tmpLst2 = g_list_next(tmpLst2))
	  {
	    ids[i * 2 + 0] = (guint)GPOINTER_TO_INT(tmpLst->data);
	    ids[i * 2 + 1] = (guint)GPOINTER_TO_INT(tmpLst2->data);
	    visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                        ids[i * 2 + 0], xyz1);
	    visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                        ids[i * 2 + 1], xyz2);
	    dists[i] =
	      (xyz1[0] - xyz2[0]) * (xyz1[0] - xyz2[0]) +
	      (xyz1[1] - xyz2[1]) * (xyz1[1] - xyz2[1]) +
	      (xyz1[2] - xyz2[2]) * (xyz1[2] - xyz2[2]);
	    g_debug(" | %d (%d %d) -> %g", i, ids[i * 2 + 0],
			ids[i * 2 + 1], dists[i]);
	    min = MIN(min, dists[i]);
	    i += 1;
	  }
  
      for (i = 0; i < n; i++)
	if (dists[i] < (2.75f * min))
	  {
	    g_debug(" | %d (%d %d) -> %g OK", i, ids[i * 2 + 0],
			ids[i * 2 + 1], dists[i]);
	    visu_gl_ext_marks_setAngle(marks, node, ids[i * 2 + 0],
                                       ids[i * 2 + 1], MARKS_STATUS_SET);
	  }
      g_free(ids);
      g_free(dists);
    }
  g_list_free(lst);

  return redraw;
}
/**
 * visu_gl_ext_marks_getActive:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 *
 * Retrieve if @nodeId is implied any measurement marks stored in @mark.
 *
 * Returns: TRUE if @nodeId participate to any mark (distance,
 * angle...).
 */
gboolean visu_gl_ext_marks_getActive(VisuGlExtMarks *marks, guint nodeId)
{
  GList *list;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks, FALSE);

  for (list = marks->priv->storedMarks; list; list = g_list_next(list))
    {
      mark = (struct MarkInfo_struct*)list->data;
      if ((mark->type == MARK_DISTANCE &&
	   mark->idNode1 == nodeId) ||
	  (mark->type == MARK_ANGLE &&
	   mark->idNode1 == nodeId))
	return TRUE;
    }
  return FALSE;
}
/**
 * visu_gl_ext_marks_getHighlighted:
 * @marks: a #VisuGlExtMarks object ;
 *
 * @marks has a list of mark for some nodes. These marks are only
 * highlight marks.
 *
 * Returns: (element-type guint) (transfer none): list of
 * highlighted nodes (starting from 0), should not be freed.
 *
 * Since: 3.6
 */
GArray* visu_gl_ext_marks_getHighlighted(const VisuGlExtMarks *marks)
{
  GList *tmpLst;
  struct MarkInfo_struct* mark;

  g_return_val_if_fail(marks, (GArray*)0);

  if (!marks->priv->cachedHighlighted)
    {
      marks->priv->cachedHighlighted = g_array_new(FALSE, FALSE, sizeof(guint));
      for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
        {
          mark = (struct MarkInfo_struct*)(tmpLst->data);
          if (mark->type == MARK_HIGHLIGHT)
            g_array_append_val(marks->priv->cachedHighlighted, mark->idNode1);
        }
    }
  return marks->priv->cachedHighlighted;
}
/**
 * visu_gl_ext_marks_getHighlightStatus:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id (ranging from 0).
 *
 * Nodes can be highlighted.
 *
 * Since: 3.7
 *
 * Returns: TRUE if @nodeId has an highlight.
 **/
gboolean visu_gl_ext_marks_getHighlightStatus(VisuGlExtMarks *marks, guint nodeId)
{
  GList *tmpLst;
  struct MarkInfo_struct* mark;

  g_return_val_if_fail(marks, FALSE);

  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)(tmpLst->data);
      if (mark->type == MARK_HIGHLIGHT && mark->idNode1 == nodeId)
        return TRUE;
    }
  return FALSE;
}
/**
 * visu_gl_ext_marks_setHighlight:
 * @marks: a #VisuGlExtMarks object ;
 * @nodes: (element-type guint32): a set of node ids (0 started) ;
 * @status: changing command.
 *
 * @marks has a list of mark for some nodes. These marks can be
 * highlight (or distance, angles...). Depending on @status values,
 * the mark may be switch on or off.
 *
 * Returns: TRUE if redraw needed.
 *
 * Since: 3.6
 */
gboolean visu_gl_ext_marks_setHighlight(VisuGlExtMarks *marks, GArray *nodes,
                                        VisuGlExtMarksStatus status)
{
  gboolean redraw, res;
  guint i;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  if (!nodes || !nodes->len)
    return FALSE;
  
  g_debug("Visu Marks: set highlight status of list to %d.",
	      status);
  redraw = FALSE;
  for (i = 0; i < nodes->len; i++)
    {
      res = toggleHighlight(marks, g_array_index(nodes, guint, i),
			    status, (gboolean*)0);
      g_debug(" | %d -> %d", g_array_index(nodes, guint, i), res);
      redraw = redraw || res;
    }
  g_debug("Visu Marks: resulting redraw %d.",
	      redraw);
  if (redraw)
    {
      g_object_notify_by_pspec(G_OBJECT(marks), properties[HIGHLIGHT_PROP]);
      if (marks->priv->hidingMode != HIDE_NONE)
        visu_node_masker_emitDirty(VISU_NODE_MASKER(marks));
      visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
    }
  return redraw;
}
/**
 * visu_gl_ext_marks_setDrawValues:
 * @marks: a #VisuGlExtMarks object.
 * @status: a boolean.
 *
 * Change if the measurements are printed or not (distance length, or
 * angles...).
 *
 * Returns: TRUE if @marks is modified.
 */
gboolean visu_gl_ext_marks_setDrawValues(VisuGlExtMarks *marks, gboolean status)
{
  g_return_val_if_fail(marks, FALSE);

  if (marks->priv->drawValues == status)
    return FALSE;

  marks->priv->drawValues = status;
  return TRUE;
}
/**
 * visu_gl_ext_marks_removeMeasures:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 *
 * This routine scans the @mark to remove all marks of distance or
 * angle where @nodeId is implied in.
 *
 * Returns: TRUE is @mark is changed.
 */
gboolean visu_gl_ext_marks_removeMeasures(VisuGlExtMarks *marks, gint nodeId)
{
  gboolean redraw;
  GList *list, *tmpLst;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks, FALSE);

  g_debug("Visu Marks: remove measures (%d).", nodeId);
  redraw = FALSE;
  for (list = marks->priv->storedMarks; list; list = tmpLst)
    {
      tmpLst = g_list_next(list);
      mark = (struct MarkInfo_struct*)list->data;
      if ((mark->type == MARK_DISTANCE ||
	   mark->type == MARK_ANGLE) &&
	  (nodeId < 0 || mark->idNode1 == (guint)nodeId))
	{
	  markRemove(marks, list);
	  redraw = TRUE;
	}
    }
  if (!redraw)
    return FALSE;

  visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_marks_setInfos:
 * @marks: a #VisuGlExtMarks object.
 * @nodeId: a node id.
 * @status: a boolean.
 *
 * Depending on @status, it removes all measurements from @nodeId or
 * it calculate all first neighbour relations of @nodeId.
 *
 * Return: TRUE if @marks is changed.
 */
gboolean visu_gl_ext_marks_setInfos(VisuGlExtMarks *marks, guint nodeId, gboolean status)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);
  g_return_val_if_fail(marks->priv->renderer, FALSE);

  g_debug("Visu Marks: set info for node %d.", nodeId);

  if (status)
    {
      if (setInformation(marks, VISU_DATA(visu_node_array_renderer_getNodeArray(marks->priv->renderer)), nodeId))
	{
          visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
	  return TRUE;
	}
    }
  else
    return visu_gl_ext_marks_removeMeasures(marks, nodeId);
    
  return FALSE;
}


/*************************/
/* Resources management. */
/*************************/
static void exportResources(GString *data, VisuData *dataObj _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_FACTOR);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_FACTOR, NULL,
                               "%f", highlightFactor);
  visu_config_file_exportComment(data, "");
}


/****************************/
/* List drawing management. */
/****************************/
static void visu_gl_ext_marks_rebuild(VisuGlExt *ext)
{
  GError *error = (GError*)0;

  if (!visu_gl_ext_setShaderById(ext, SHADER_MARKS, VISU_GL_SHADER_FLAT, &error))
    {
      g_warning("Cannot create marks shader: %s", error->message);
      g_clear_error(&error);
    }
  if (!visu_gl_ext_setShaderById(ext, SHADER_SELECTION, VISU_GL_SHADER_ORTHO, &error))
    {
      g_warning("Cannot create selection shader: %s", error->message);
      g_clear_error(&error);
    }
}
static gboolean getNodePosition(const VisuNodeArray *array, guint nodeId, gfloat xyz[3])
{
  VisuNode *node;
  VisuElement *ele;

  node = visu_node_array_getFromId(array, nodeId);
  g_return_val_if_fail(node, FALSE);

  if (!node->rendered)
    return FALSE;
  ele = visu_node_array_getElement(array, node);
  if (!visu_element_getRendered(ele))
    return FALSE;

  g_debug("Visu Mark: draw a mark on node %d.", node->number);

  visu_node_array_getNodePosition(array, node, xyz);
  return TRUE;
}
static void visu_gl_ext_marks_draw(VisuGlExt *ext)
{
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(ext);
  struct MarkInfo_struct *mark;
  GList *tmpLst;
  guint id;
  VisuData *dataObj;
  gfloat xyz[3], xyz2[3], xyz3[3];
  GHashTable *dots;
  const gfloat white[4] = {1.f, 1.f, 1.f, 1.f};
  const gfloat material[5] = {1.f, 1.f, 1.f, 1.f, 1.f};
  ToolUnits units;

  GArray *smallDots, *bigDots, *lines, *pangles, *langles, *angles, *labels;

  g_debug("Visu Marks: update the list %p"
	      " of all marks.", (gpointer)marks->priv->storedMarks);

  visu_gl_ext_clearBuffers(ext);
  visu_gl_ext_clearLabels(ext);

  if (marks->priv->x1 >= 0 && marks->priv->y1 >= 0)
    {
      GArray *rect;
      GLfloat at[3] = {0.f, 0.f, 0.f};
      const GLfloat transp[4] = {1.f, 1.f, 1.f, 1.f};

      rect = visu_gl_ext_startBuffer(ext, SHADER_SELECTION, BUF_RECT, VISU_GL_RGBA_XYZ, VISU_GL_TRIANGLE_FAN);
      at[0] = marks->priv->x1;
      at[1] = visu_gl_view_getHeight(marks->priv->view) - marks->priv->y1;
      g_array_append_vals(rect, transp, 4);
      g_array_append_vals(rect, at, 3);
      at[0] = marks->priv->x2;
      at[1] = visu_gl_view_getHeight(marks->priv->view) - marks->priv->y1;
      g_array_append_vals(rect, transp, 4);
      g_array_append_vals(rect, at, 3);
      at[0] = marks->priv->x2;
      at[1] = visu_gl_view_getHeight(marks->priv->view) - marks->priv->y2;
      g_array_append_vals(rect, transp, 4);
      g_array_append_vals(rect, at, 3);
      at[0] = marks->priv->x1;
      at[1] = visu_gl_view_getHeight(marks->priv->view) - marks->priv->y2;
      g_array_append_vals(rect, transp, 4);
      g_array_append_vals(rect, at, 3);
      visu_gl_ext_takeBuffer(ext, BUF_RECT, rect);
    }

  if (!marks->priv->storedMarks || !marks->priv->renderer)
    return;

  dataObj = VISU_DATA(visu_node_array_renderer_getNodeArray(marks->priv->renderer));
  units = visu_box_getUnit(visu_boxed_getBox(VISU_BOXED(dataObj)));

  smallDots = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_SMALL_POINTS, VISU_GL_XYZ, VISU_GL_POINTS);
  bigDots = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_BIG_POINTS, VISU_GL_XYZ, VISU_GL_POINTS);
  lines = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_LINES, VISU_GL_XYZ, VISU_GL_LINES);
  pangles = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_POINT_ANGLES, VISU_GL_XYZ, VISU_GL_POINTS);
  langles = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_LINE_ANGLES, VISU_GL_XYZ, VISU_GL_LINES);
  angles = visu_gl_ext_startBuffer(ext, SHADER_MARKS, BUF_ANGLES, VISU_GL_XYZ, VISU_GL_TRIANGLE_FAN);
  labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));

  id = 0;
  dots = g_hash_table_new(g_direct_hash, g_direct_equal);
  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {      
      mark = (struct MarkInfo_struct*)tmpLst->data;
      g_debug(" | draw mark of type %d.", mark->type);
      switch (mark->type)
        {
        case MARK_BIG_SQUARE:
          if (g_hash_table_add(dots, GINT_TO_POINTER(mark->idNode1)) &&
              getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode1, xyz))
            g_array_append_vals(bigDots, xyz, 3);
          break;
        case MARK_SMALL_SQUARE:
          if (getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode1, xyz))
            g_array_append_vals(smallDots, xyz, 3);
          break;
        case MARK_DISTANCE:
          if (getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode1, xyz)
              && getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode2, xyz2))
            {
              VisuGlExtLabel lbl;
              gfloat dxyz[3] = {xyz2[0] - xyz[0], xyz2[1] - xyz[1], xyz2[2] - xyz[2]};
              gfloat dist = sqrt(dxyz[0] * dxyz[0] + dxyz[1] * dxyz[1] + dxyz[2] * dxyz[2]);
              sprintf(lbl.lbl, "%7.3f %s", dist, tool_physic_getUnitLabel(units));
              lbl.xyz[0] = 0.5f * (xyz[0] + xyz2[0]);
              lbl.xyz[1] = 0.5f * (xyz[1] + xyz2[1]);
              lbl.xyz[2] = 0.5f * (xyz[2] + xyz2[2]);
              lbl.offset[0] = lbl.offset[1] = lbl.offset[2] = 0.f;
              memcpy(lbl.rgba, white, sizeof(gfloat) * 4);
              g_array_append_val(labels, lbl);
              if (g_hash_table_add(dots, GINT_TO_POINTER(mark->idNode1)))
                g_array_append_vals(bigDots, xyz, 3);
              if (g_hash_table_add(dots, GINT_TO_POINTER(mark->idNode2)))
                g_array_append_vals(bigDots, xyz2, 3);
              g_array_append_vals(lines, xyz, 3);
              g_array_append_vals(lines, xyz2, 3);
              if (visu_box_getInside(visu_boxed_getBox(VISU_BOXED(dataObj)),
                                     dxyz, 0.501f, TRUE))
                {
                  gfloat mid[3];
                  g_array_append_vals(lines, xyz, 3);
                  mid[0] = xyz[0] + 0.5f * dxyz[0];
                  mid[1] = xyz[1] + 0.5f * dxyz[1];
                  mid[2] = xyz[2] + 0.5f * dxyz[2];
                  g_array_append_vals(lines, mid, 3);
                  for (guint i = 0; i < 6; i++)
                    {
                      mid[0] += 0.02f * dxyz[0];
                      mid[1] += 0.02f * dxyz[1];
                      mid[2] += 0.02f * dxyz[2];
                      g_array_append_vals(lines, mid, 3);
                    }
                  g_array_append_vals(lines, xyz2, 3);
                  mid[0] = xyz2[0] - 0.5f * dxyz[0];
                  mid[1] = xyz2[1] - 0.5f * dxyz[1];
                  mid[2] = xyz2[2] - 0.5f * dxyz[2];
                  g_array_append_vals(lines, mid, 3);
                  for (guint i = 0; i < 6; i++)
                    {
                      mid[0] -= 0.02f * dxyz[0];
                      mid[1] -= 0.02f * dxyz[1];
                      mid[2] -= 0.02f * dxyz[2];
                      g_array_append_vals(lines, mid, 3);
                    }
                  dist = sqrt(dxyz[0] * dxyz[0] + dxyz[1] * dxyz[1] + dxyz[2] * dxyz[2]);
                  sprintf(lbl.lbl, "%7.3f %s", dist, tool_physic_getUnitLabel(units));
                  lbl.xyz[0] = xyz[0] + 0.25f * dxyz[0];
                  lbl.xyz[1] = xyz[1] + 0.25f * dxyz[1];
                  lbl.xyz[2] = xyz[2] + 0.25f * dxyz[2];
                  lbl.offset[0] = lbl.offset[1] = lbl.offset[2] = 0.f;
                  memcpy(lbl.rgba, white, sizeof(gfloat) * 4);
                  g_array_append_val(labels, lbl);
                }
            }
          break;
        case MARK_ANGLE:
          if (getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode2, xyz2)
              && getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode3, xyz3)
              && getNodePosition(VISU_NODE_ARRAY(dataObj), mark->idNode1, xyz))
            {
              VisuGlExtLabel lbl;
              const ToolColor *bright = tool_color_new_bright(id++);
              gfloat theta = tool_drawAngle(angles, xyz, xyz2, xyz3, lbl.xyz);
              lbl.offset[0] = lbl.offset[1] = lbl.offset[2] = 0.f;
              visu_gl_ext_layoutBufferWithColor(ext, BUF_ANGLES, angles, bright->rgba, material);
              sprintf(lbl.lbl, "%5.1f\302\260", theta * 180.f / G_PI);
              memcpy(lbl.rgba, bright->rgba, sizeof(gfloat) * 4);
              g_array_append_val(labels, lbl);
              g_array_append_vals(langles, xyz, 3);
              g_array_append_vals(langles, xyz2, 3);
              g_array_append_vals(langles, xyz, 3);
              g_array_append_vals(langles, xyz3, 3);
              visu_gl_ext_layoutBufferWithColor(ext, BUF_LINE_ANGLES, langles, bright->rgba, material);
              g_array_append_vals(pangles, xyz2, 3);
              g_array_append_vals(pangles, xyz3, 3);
              visu_gl_ext_layoutBufferWithColor(ext, BUF_POINT_ANGLES, pangles, bright->rgba, material);
            }
          break;
        default:
          break;
        }
    }
  g_hash_table_destroy(dots);
  visu_gl_ext_layoutBufferWithColor(ext, BUF_SMALL_POINTS, smallDots, white, material);
  visu_gl_ext_takeBuffer(ext, BUF_SMALL_POINTS, smallDots);
  visu_gl_ext_layoutBufferWithColor(ext, BUF_BIG_POINTS, bigDots, white, material);
  visu_gl_ext_takeBuffer(ext, BUF_BIG_POINTS, bigDots);
  visu_gl_ext_layoutBufferWithColor(ext, BUF_LINES, lines, white, material);
  visu_gl_ext_takeBuffer(ext, BUF_LINES, lines);
  visu_gl_ext_takeBuffer(ext, BUF_POINT_ANGLES, pangles);
  visu_gl_ext_takeBuffer(ext, BUF_LINE_ANGLES, langles);
  visu_gl_ext_takeBuffer(ext, BUF_ANGLES, angles);
  visu_gl_ext_setLabels(ext, labels, TOOL_WRITER_FONT_NORMAL, TRUE);

  g_array_unref(labels);
}
static void visu_gl_ext_marks_render(const VisuGlExt *ext)
{
  VisuGlExtMarks *marks = VISU_GL_EXT_MARKS(ext);
  const gfloat black[4] = {0.f, 0.f, 0.f, 1.f};

  glEnable(GL_DEPTH_TEST);
  glClear(GL_DEPTH_BUFFER_BIT);
  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);

  glPointSize(4);
  visu_gl_ext_renderBuffer(ext, BUF_SMALL_POINTS);
  glPointSize(8);
  visu_gl_ext_renderBuffer(ext, BUF_BIG_POINTS);
  glLineWidth(1);
  visu_gl_ext_renderBuffer(ext, BUF_LINES);

  if (marks->priv->x1 >= 0 && marks->priv->x2 >= 0)
    {
      glDepthMask(GL_FALSE);
      glLineWidth(2);
      glBlendFunc(GL_DST_COLOR, GL_SRC_COLOR);
      visu_gl_ext_renderBuffer(ext, BUF_RECT);
      glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO);
      visu_gl_ext_renderBufferWithPrimitive(ext, BUF_RECT, VISU_GL_LINE_LOOP, (const gfloat*)0);
      glDepthMask(GL_TRUE);
    }

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  visu_gl_ext_renderBuffer(ext, BUF_ANGLES);
  glLineWidth(1);
  visu_gl_ext_renderBuffer(ext, BUF_LINE_ANGLES);
  glPointSize(4);
  visu_gl_ext_renderBuffer(ext, BUF_POINT_ANGLES);
  glLineWidth(2.f);
  visu_gl_ext_renderBufferWithPrimitive(ext, BUF_ANGLES, VISU_GL_LINE_LOOP, black);
}

/******************************************/
/* XML files for marks and other exports. */
/******************************************/

/**
 * visu_gl_ext_marks_getMeasurementLabels:
 * @marks: a #VisuGlExtMarks object.
 *
 * Exports as a string the ids of nodes for measurement marks.
 *
 * Since: 3.6
 *
 * Returns: a newly allocated string.
 */
gchar* visu_gl_ext_marks_getMeasurementLabels(VisuGlExtMarks *marks)
{
  GString *str;
  GList *tmpLst;
  struct MarkInfo_struct *mark;
  guint i;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), (gchar*)0);

  str = g_string_new("#");

  /* Look for the mark. */
  for (tmpLst = marks->priv->storedMarks, i = 0; tmpLst && i < 6;
       tmpLst = g_list_next(tmpLst), i += 1)
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_DISTANCE)
        g_string_append_printf(str, "      %4d-%4d", mark->idNode1 + 1, mark->idNode2 + 1);
      else if (mark->type == MARK_ANGLE)
        g_string_append_printf(str, " %4d-%4d-%4d",
                               mark->idNode3 + 1, mark->idNode1 + 1, mark->idNode2 + 1);
    }
  if (!tmpLst)
    g_string_append(str, "\n");
  else
    g_string_append(str, " (truncated list)\n");

  return g_string_free(str, FALSE);
}
/**
 * visu_gl_ext_marks_getMeasurementStrings:
 * @marks: a #VisuGlExtMarks object.
 *
 * Exports as a string all measurements stored in @marks.
 *
 * Since: 3.6
 *
 * Returns: a newly allocated string.
 */
gchar* visu_gl_ext_marks_getMeasurementStrings(VisuGlExtMarks *marks)
{
  VisuData *dataObj;
  GString *str;
  GList *tmpLst;
  struct MarkInfo_struct *mark;
  float posSelect[3], posRef1[3], posRef2[3], dx, dy, dz, dr;
  float dx1, dy1, dz1, dx2, dy2, dz2, dr1, dr2, ang;
  gchar *lbl;
  gboolean export;
  guint i;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks) && marks->priv->renderer, (gchar*)0);

  str = g_string_new(" ");
  export = FALSE;
  dataObj = VISU_DATA(visu_node_array_renderer_getNodeArray(marks->priv->renderer));
  /* Look for the mark. */
  for (tmpLst = marks->priv->storedMarks, i = 0; tmpLst && i < 6;
       tmpLst = g_list_next(tmpLst), i += 1)
    {
      mark = (struct MarkInfo_struct*)tmpLst->data;
      if (mark->type == MARK_DISTANCE
          && visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                         mark->idNode1, posRef1)
          && visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                         mark->idNode2, posSelect))
        {
          g_debug("Visu Marks: export this distance.");
          dx = posSelect[0] - posRef1[0];
          dy = posSelect[1] - posRef1[1];
          dz = posSelect[2] - posRef1[2];
          dr = sqrt(dx*dx + dy*dy + dz*dz);
          g_string_append_printf(str, "   %12.6g", dr);
          export = TRUE;
	}
      else if (mark->type == MARK_ANGLE
               && visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                              mark->idNode1, posRef1)
               && visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                              mark->idNode2, posRef2)
               && visu_node_array_getPosition(VISU_NODE_ARRAY(dataObj),
                                              mark->idNode3, posSelect))
        {
	  g_debug("Visu Marks: export this angle.");
          dx1 = posSelect[0] - posRef1[0];
          dy1 = posSelect[1] - posRef1[1];
          dz1 = posSelect[2] - posRef1[2];
          dx2 = posRef2[0] - posRef1[0];
          dy2 = posRef2[1] - posRef1[1];
          dz2 = posRef2[2] - posRef1[2];
          dr1 = sqrt(dx1*dx1 + dy1*dy1 + dz1*dz1);
          dr2 = sqrt(dx2*dx2 + dy2*dy2 + dz2*dz2);
          ang = acos((dx2*dx1+dy2*dy1+dz2*dz1)/(dr2*dr1))/TOOL_PI180;
          g_string_append_printf(str, "   %12.6g", ang);
          export = TRUE;
        }
    }
  if (!export)
    {
      g_string_free(str, TRUE);
      return (gchar*)0;
    }
  if (VISU_IS_DATA_LOADABLE(dataObj))
    {
      g_object_get(dataObj, "label", &lbl, NULL);
      g_string_append_printf(str, " # %s\n", lbl);
      g_free(lbl);
    }

  return g_string_free(str, FALSE);
}

/**
 * visu_gl_ext_marks_getHidingMode:
 * @marks: a #VisuGlExtMarks object.
 *
 * Retrieves the hiding mode of @marks, see #VisuGlExtMarksHidingModes.
 *
 * Since: 3.8
 *
 * Returns: a #VisuGlExtMarksHidingModes value.
 **/
VisuGlExtMarksHidingModes visu_gl_ext_marks_getHidingMode(const VisuGlExtMarks *marks)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), HIDE_NONE);

  return marks->priv->hidingMode;
}
/**
 * visu_gl_ext_marks_setHidingMode:
 * @marks: a #VisuGlExtMarks object.
 * @mode: a #VisuGlExtMarksHidingModes value.
 *
 * Change the hiding mode of @marks.
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_marks_setHidingMode(VisuGlExtMarks *marks,
                                         VisuGlExtMarksHidingModes mode)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks), FALSE);

  g_debug("Visu Ext Marks: setting hiding mode to %d (%d).",
              mode, marks->priv->hidingMode);

  if (marks->priv->hidingMode == mode)
    return FALSE;

  marks->priv->hidingMode = mode;
  g_object_notify_by_pspec(G_OBJECT(marks), properties[HIDING_PROP]);
  visu_node_masker_emitDirty(VISU_NODE_MASKER(marks));

  return TRUE;
}

static gboolean _maskApply(const VisuNodeMasker *self, VisuNodeArray *array)
{
  VisuGlExtMarks *marks;
  gboolean redraw;
  GArray *ids;
  guint i;
  guint nHl, *hl, j;
  guint min, max;
  VisuNodeArrayIter iter;
  gboolean hide;

  g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(self), FALSE);

  marks = VISU_GL_EXT_MARKS(self);

  if (marks->priv->hidingMode == HIDE_NONE)
    return FALSE;

  redraw = FALSE;
  ids = visu_gl_ext_marks_getHighlighted(marks);
  if (marks->priv->hidingMode == HIDE_NON_HIGHLIGHT)
    {
      /* We switch off all nodes except the highlighted ones. To avoid
         to run the list of highlighted nodes to many times, we create
         a temporary array and store the min and max indexes. */
      if (!ids || !ids->len)
        return FALSE;

      visu_node_array_iter_new(array, &iter);

      hl = g_malloc(sizeof(gint) * ids->len);
      nHl = 0;
      min = iter.nAllStoredNodes;
      max = 0;
      for (i = 0; i < ids->len; i++)
        {
          hl[nHl] = g_array_index(ids, guint, i);
          min = MIN(min, hl[nHl]);
          max = MAX(max, hl[nHl]);
          nHl += 1;
        }

      for (visu_node_array_iterStart(array, &iter); iter.node;
           visu_node_array_iterNext(array, &iter))
        if (iter.node->number >= min && iter.node->number <= max)
          {
            hide = TRUE;
            for (j = 0; hide && j < nHl; j++)
              hide = (iter.node->number != hl[j]);
            if (hide)
              redraw = visu_node_setVisibility(iter.node, FALSE) || redraw;
          }
        else
          redraw = visu_node_setVisibility(iter.node, FALSE) || redraw;

      g_free(hl);
    }
  else if (marks->priv->hidingMode == HIDE_HIGHLIGHT)
    for (i = 0; i < ids->len; i++)
      {
        redraw = visu_node_setVisibility
          (visu_node_array_getFromId(array, g_array_index(ids, guint, i)),
           FALSE) || redraw;
      }

  return redraw;
}

/**
 * visu_data_getDistanceList:
 * @data: a #VisuData object ;
 * @nodeId: a node id.
 * @minVal: a location for a float.
 *
 * This routine creates an array of #VisuNodeInfo, storing for each
 * node its node id and its distance to @nodeId. The periodicity is
 * NOT taken into account. The array is not distance sorted, but if
 * @minVal is provided, it will contain the minimal distance between
 * @nodeId and the other nodes.
 *
 * Since: 3.5
 *
 * Returns: an array of #VisuNodeInfo of size the number of nodes. It
 * is terminated by @nodeId value itself.
 */
static VisuNodeInfo* _getDistanceList(VisuData *data, guint nodeId, float *minVal)
{
  VisuNodeInfo *infos;
  int nNodes;
  VisuDataIter iter;
  VisuNode *nodeRef;
  float xyzRef[3], min;

  if (minVal)
    *minVal = -1.f;
  
  g_return_val_if_fail(VISU_IS_DATA(data), (VisuNodeInfo*)0);

  g_debug("Visu Data: get distance list for node %d.", nodeId);
  nodeRef = visu_node_array_getFromId(VISU_NODE_ARRAY(data), nodeId);
  g_return_val_if_fail(nodeRef, (VisuNodeInfo*)0);

  nNodes = visu_node_array_getNNodes(VISU_NODE_ARRAY(data));
  infos = g_malloc(sizeof(VisuNodeInfo) * nNodes);
  
  visu_node_array_getNodePosition(VISU_NODE_ARRAY(data), nodeRef, xyzRef);

  min = G_MAXFLOAT;
  nNodes = 0;
  for(visu_data_iter_new(data, &iter, ITER_NODES_VISIBLE);
      visu_data_iter_isValid(&iter); visu_data_iter_next(&iter))
    {
      infos[nNodes].id = iter.parent.node->number;
      infos[nNodes].dist =
	(iter.xyz[0] - xyzRef[0]) * (iter.xyz[0] - xyzRef[0]) +
	(iter.xyz[1] - xyzRef[1]) * (iter.xyz[1] - xyzRef[1]) +
	(iter.xyz[2] - xyzRef[2]) * (iter.xyz[2] - xyzRef[2]);
      if (infos[nNodes].dist > 0.0001f)
	{
	  min = MIN(min, infos[nNodes].dist);
	  nNodes += 1;
	}
    }
  infos[nNodes].id = nodeId;

  g_debug(" | min value = %g.", min);
  if (minVal)
    *minVal = min;

  return infos;
}


/*
   <pick data-mode="selected" data-info="">
     <node id="23" />
     <node id="16" />
     <distance ref="45" id="23" />
   </pick>
*/

/* Known elements. */
#define PICK_PARSER_ELEMENT_PICK  "pick"
#define PICK_PARSER_ELEMENT_NODE  "node"
#define PICK_PARSER_ELEMENT_DIST  "distance"
#define PICK_PARSER_ELEMENT_ANGL  "angle"
/* Known attributes. */
#define PICK_PARSER_ATTRIBUTES_MODE "info-mode"
#define PICK_PARSER_ATTRIBUTES_INFO "info-data"
#define PICK_PARSER_ATTRIBUTES_ID   "id"
#define PICK_PARSER_ATTRIBUTES_REF  "ref"
#define PICK_PARSER_ATTRIBUTES_REF2 "ref2"
#define PICK_PARSER_ATTRIBUTES_HLT  "highlight"

static gboolean startPick;
static VisuGlExtInfosDrawId mode;
static guint info;

/* This method is called for every element that is parsed.
   The user_data must be a GList of _pick_xml. When a 'surface'
   element, a new struct instance is created and prepend in the list.
   When 'hidden-by-planes' or other qualificative elements are
   found, the first surface of the list is modified accordingly. */
static void pickXML_element(GMarkupParseContext *context _U_,
			    const gchar         *element_name,
			    const gchar        **attribute_names,
			    const gchar        **attribute_values,
			    gpointer             user_data,
			    GError             **error)
{
  GList **pickList;
  int i, n;
  guint val, val2, val3;
  gboolean highlight;

  g_return_if_fail(user_data);
  pickList = (GList **)user_data;

  g_debug("Pick parser: found '%s' element.", element_name);
  if (!g_strcmp0(element_name, PICK_PARSER_ELEMENT_PICK))
    {
      /* Initialise the pickList. */
      if (*pickList)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
		      _("DTD error: element '%s' should appear only once."),
		      PICK_PARSER_ELEMENT_PICK);
	  return;
	}
      *pickList = (GList*)0;
      startPick = TRUE;
      /* Should have 2 mandatory attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_MODE))
	    {
	      if (!g_strcmp0(attribute_values[i], "never"))
		mode = DRAW_NEVER;
	      else if (!g_strcmp0(attribute_values[i], "selected"))
		mode = DRAW_SELECTED;
	      else if (!g_strcmp0(attribute_values[i], "always"))
		mode = DRAW_ALWAYS;
	      else
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_MODE, attribute_values[i]);
		  return;
		}
	    }
	  else if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_INFO))
	    {
	      n = sscanf(attribute_values[i], "%u", &info);
	      if (n != 1 || info < 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_INFO, attribute_values[i]);
		  return;
		}
	    }
	}
    }
  else if (!g_strcmp0(element_name, PICK_PARSER_ELEMENT_NODE))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_NODE);
	  return;
	}
      highlight = FALSE;
      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_HLT))
	    highlight = (!g_strcmp0(attribute_values[i], "yes") ||
			 !g_strcmp0(attribute_values[i], "Yes"));
	}
      if (highlight)
	*pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_HIGHLIGHT));
      else
	*pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_SELECTED));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (!g_strcmp0(element_name, PICK_PARSER_ELEMENT_DIST))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_DIST);
	  return;
	}

      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF))
	    {
	      n = sscanf(attribute_values[i], "%u", &val2);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF, attribute_values[i]);
		  return;
		}
	    }
	}
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_DISTANCE));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val2));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (!g_strcmp0(element_name, PICK_PARSER_ELEMENT_ANGL))
    {
      if (!startPick)
	{
	  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_INVALID_CONTENT,
		      _("DTD error: parent element '%s' of element '%s' is missing."),
		      PICK_PARSER_ELEMENT_PICK, PICK_PARSER_ELEMENT_ANGL);
	  return;
	}

      /* We parse the attributes. */
      for (i = 0; attribute_names[i]; i++)
	{
	  if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_ID))
	    {
	      n = sscanf(attribute_values[i], "%u", &val);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_ID, attribute_values[i]);
		  return;
		}
	    }
	  else if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF))
	    {
	      n = sscanf(attribute_values[i], "%u", &val2);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF, attribute_values[i]);
		  return;
		}
	    }
	  else if (!g_strcmp0(attribute_names[i], PICK_PARSER_ATTRIBUTES_REF2))
	    {
	      n = sscanf(attribute_values[i], "%u", &val3);
	      if (n != 1)
		{
		  g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_PARSE,
			      _("DTD error: attribute '%s' has an unknown value '%s'."),
			      PICK_PARSER_ATTRIBUTES_REF2, attribute_values[i]);
		  return;
		}
	    }
	}
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(PICK_ANGLE));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val3));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val2));
      *pickList = g_list_prepend(*pickList, GINT_TO_POINTER(val));
    }
  else if (startPick)
    {
      /* We silently ignore the element if pickList is unset, but
	 raise an error if pickList has been set. */
      g_set_error(error, G_MARKUP_ERROR, G_MARKUP_ERROR_UNKNOWN_ELEMENT,
		  _("Unexpected element '%s'."), element_name);
    }
}

/* Check when a element is closed that everything required has been set. */
static void pickXML_end(GMarkupParseContext *context _U_,
			const gchar         *element_name,
			gpointer             user_data _U_,
			GError             **error _U_)
{
  if (!g_strcmp0(element_name, PICK_PARSER_ELEMENT_PICK))
    startPick = FALSE;
}

/* What to do when an error is raised. */
static void pickXML_error(GMarkupParseContext *context _U_,
			  GError              *error,
			  gpointer             user_data)
{
  g_debug("Pick parser: error raised '%s'.", error->message);
  g_return_if_fail(user_data);

  /* We free the current list of pick. */
  g_list_free(*(GList**)user_data);
}

/**
 * visu_gl_ext_marks_parseXMLFile:
 * @marks: a #VisuGlExtMarks object.
 * @filename: a location to save to.
 * @infos: (element-type guint32) (out): a location to a #GList.
 * @drawingMode: a location to a flag.
 * @drawingInfos: a location to a flag.
 * @error: a location to store an error.
 *
 * This routines read from an XML file the description of selected
 * nodes, @mark is updated accordingly.
 *
 * Since: 3.5
 *
 * Returns: TRUE if no error.
 */
gboolean visu_gl_ext_marks_parseXMLFile(VisuGlExtMarks *marks, const gchar* filename,
                                        GList **infos, VisuGlExtInfosDrawId *drawingMode,
                                        guint *drawingInfos, GError **error)
{
  GMarkupParseContext* xmlContext;
  GMarkupParser parser;
  gboolean status;
  gchar *buffer;
  gsize size;
  GList *tmpLst;
  guint id1, id2, id3;
  VisuData *dataObj;

  g_return_val_if_fail(filename, FALSE);
  g_return_val_if_fail(infos && drawingMode && drawingInfos, FALSE);

  buffer = (gchar*)0;
  if (!g_file_get_contents(filename, &buffer, &size, error))
    return FALSE;

  /* Create context. */
  *infos = (GList*)0;
  parser.start_element = pickXML_element;
  parser.end_element   = pickXML_end;
  parser.text          = NULL;
  parser.passthrough   = NULL;
  parser.error         = pickXML_error;
  xmlContext = g_markup_parse_context_new(&parser, 0, infos, NULL);

  /* Parse data. */
  startPick = FALSE;
  status = g_markup_parse_context_parse(xmlContext, buffer, size, error);

  /* Free buffers. */
  g_markup_parse_context_free(xmlContext);
  g_free(buffer);

  if (!status)
    return FALSE;

  if (!*infos)
    {
      *error = g_error_new(G_MARKUP_ERROR, G_MARKUP_ERROR_EMPTY,
			  _("No picked node found."));
      return FALSE;
    }

  /* Need to reverse the list since elements have been prepended. */
  *infos        = g_list_reverse(*infos);
  *drawingMode  = mode;
  *drawingInfos = info;

  /* Update the current marks. */
  if (marks)
    {
      g_return_val_if_fail(VISU_IS_GL_EXT_MARKS(marks) && marks->priv->renderer, FALSE);
      
      dataObj = VISU_DATA(visu_node_array_renderer_getNodeArray(marks->priv->renderer));
      tmpLst = *infos;
      while(tmpLst)
	{
	  if (GPOINTER_TO_INT(tmpLst->data) == PICK_SELECTED)
	    tmpLst = g_list_next(tmpLst);
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_HIGHLIGHT)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id1))
		toggleHighlight(marks, id1, MARKS_STATUS_SET, (gboolean*)0);
	    }
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_DISTANCE)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id2 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id1) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id2))
		toggleDistance(marks, id1, id2, TRUE);
	    }
	  else if (GPOINTER_TO_INT(tmpLst->data) == PICK_ANGLE)
	    {
	      tmpLst = g_list_next(tmpLst);
	      id1 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id2 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      tmpLst = g_list_next(tmpLst);
	      id3 = (guint)GPOINTER_TO_INT(tmpLst->data) - 1;
	      /* We silently ignore out of bound values. */
	      if (visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id1) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id2) &&
		  visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), id3))
		visu_gl_ext_marks_setAngle(marks, id2, id1, id3, MARKS_STATUS_SET);
	    }
	  else
	    {
	      g_error("Should not be here!");
	    }
      
	  tmpLst = g_list_next(tmpLst);
	}

      g_object_notify_by_pspec(G_OBJECT(marks), properties[HIGHLIGHT_PROP]);
      if (marks->priv->hidingMode != HIDE_NONE)
        visu_node_masker_emitDirty(VISU_NODE_MASKER(marks));
      g_signal_emit(G_OBJECT(marks), signals[MEASUREMENT_CHANGE_SIGNAL], 0, dataObj);

      visu_gl_ext_setDirty(VISU_GL_EXT(marks), VISU_GL_DRAW_REQUIRED);
      visu_gl_ext_setDirty(VISU_GL_EXT(marks->priv->extNode), VISU_GL_DRAW_REQUIRED);
    }

  return TRUE;
}

/**
 * visu_gl_ext_marks_exportXMLFile:
 * @marks: a #VisuGlExtMarks object.
 * @filename: a location to save to.
 * @nodes: (element-type uint): an array of node ids.
 * @drawingMode: a flag.
 * @drawingInfos: a flag.
 * @error: a location to store an error.
 *
 * This routines export to an XML file a description of selected
 * @nodes. If @nodes is NULL, the nodes stored in the @mark will be
 * used instead.
 *
 * Since: 3.5
 *
 * Returns: TRUE if no error.
 */
gboolean visu_gl_ext_marks_exportXMLFile(VisuGlExtMarks *marks, const gchar* filename,
                                         GArray *nodes, VisuGlExtInfosDrawId drawingMode,
                                         guint drawingInfos, GError **error)
{
  gboolean valid, set;
  GString *output;
  guint i;
  char *modes[] = {"never", "selected", "always"};
  GList *tmpLst;
  struct MarkInfo_struct *mark;

  g_return_val_if_fail(marks && filename, FALSE);

  /*
   <pick data-mode="selected" data-info="0">
     <node id="23" />
     <node id="16" highlight="yes" />
     <distance ref="45" id="23" />
     <angle ref="45" ref2="65" id="23" />
   </pick>
  */

  output = g_string_new("  <pick");
  g_string_append_printf(output, " data-mode=\"%s\" data-infos=\"%d\">\n",
			 modes[drawingMode], drawingInfos);
  if (nodes)
    for (i = 0; i < nodes->len; i++)
      {
	set = FALSE;
	/* We print them only if they are not part of marks. */
	for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
	  {
	    mark = (struct MarkInfo_struct*)(tmpLst->data);
	    set = set || ( (mark->type == MARK_DISTANCE &&
			    g_array_index(nodes, guint, i) == mark->idNode2) ||
			   (mark->type == MARK_HIGHLIGHT &&
			    g_array_index(nodes, guint, i) == mark->idNode1) );
	  }
	if (!set)
	  g_string_append_printf(output, "    <node id=\"%d\" />\n", g_array_index(nodes, guint, i) + 1);
      }
  for (tmpLst = marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
    {
      mark = (struct MarkInfo_struct*)(tmpLst->data);
      if (mark->type == MARK_DISTANCE)
	g_string_append_printf(output, "    <distance ref=\"%d\" id=\"%d\" />\n",
			       mark->idNode1 + 1, mark->idNode2 + 1);
      else if (mark->type == MARK_ANGLE)
	g_string_append_printf(output,
			       "    <angle ref=\"%d\" ref2=\"%d\" id=\"%d\" />\n",
			       mark->idNode1 + 1, mark->idNode2 + 1,
			       mark->idNode3 + 1);
      else if (mark->type == MARK_HIGHLIGHT)
	g_string_append_printf(output, "    <node id=\"%d\" highlight=\"yes\" />\n",
			       mark->idNode1 + 1);
    }
  g_string_append(output, "  </pick>");

  valid = tool_XML_substitute(output, filename, "pick", error);
  if (!valid)
    {
      g_string_free(output, TRUE);
      return FALSE;
    }
  
  valid = g_file_set_contents(filename, output->str, -1, error);
  g_string_free(output, TRUE);
  return valid;
}


/* Local VisuGlExt for direct node marks. */
typedef struct _ExtNodeClass ExtNodeClass;
struct _ExtNodeClass
{
  VisuGlExtClass parent;
};

static GObject* ext_node_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void ext_node_rebuild(VisuGlExt *ext);
static void ext_node_render(const VisuGlExt *ext);
static void ext_node_draw(VisuGlExt *ext);

#define TYPE_EXT_NODE          (ext_node_get_type())
GType ext_node_get_type(void);

#define EXT_NODE(obj)          (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_EXT_NODE, ExtNode))

G_DEFINE_TYPE (ExtNode, ext_node, VISU_TYPE_GL_EXT)

static void ext_node_class_init(ExtNodeClass *klass)
{
  g_debug("Visu Marks: create internal ExtNode class.");
  G_OBJECT_CLASS(klass)->constructor = ext_node_constructor;
  VISU_GL_EXT_CLASS(klass)->rebuild = ext_node_rebuild;
  VISU_GL_EXT_CLASS(klass)->render = ext_node_render;
  VISU_GL_EXT_CLASS(klass)->draw = ext_node_draw;
}

static GObject* ext_node_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, 1);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, 1);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "withFog"))
        g_value_set_boolean(props[i].value, TRUE);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LOW);
    }

  return G_OBJECT_CLASS(ext_node_parent_class)->constructor(gtype, nprops, props);
}

static void ext_node_init(ExtNode *marks)
{
  g_debug("Visu Marks: initialise new ExtNode object.");
  marks->marks = (VisuGlExtMarks*)0;
}

static ExtNode* ext_node_new(VisuGlExtMarks *marks)
{
  ExtNode *ext;

  ext = EXT_NODE(g_object_new(TYPE_EXT_NODE,
                              "name", "Marks", "label", _("Marks - classical"),
                              "description", _("Draw some marks on element."), NULL));
  
  ext->marks = marks;

  return ext;
}

static void ext_node_rebuild(VisuGlExt *ext)
{
  GError *error = (GError*)0;

  if (!visu_gl_ext_setShaderById(ext, 0, VISU_GL_SHADER_MATERIAL, &error))
    {
      g_warning("Cannot create highlight shader: %s", error->message);
      g_clear_error(&error);
    }
}

static void ext_node_draw(VisuGlExt *ext)
{
  const ExtNode *extNode = EXT_NODE(ext);

  visu_gl_ext_clearBuffers(ext);
  if (extNode->marks->priv->storedMarks
      && extNode->marks->priv->renderer && extNode->marks->priv->view) {
    GArray *gpuData;
    VisuData *dataObj = VISU_DATA(visu_node_array_renderer_getNodeArray(extNode->marks->priv->renderer));
    GList *tmpLst;

    gpuData = visu_gl_ext_startBuffer(ext, 0, 0, VISU_GL_XYZ_NRM, VISU_GL_TRIANGLES);

    for (tmpLst = extNode->marks->priv->storedMarks; tmpLst; tmpLst = g_list_next(tmpLst))
      {
        struct MarkInfo_struct *mark = (struct MarkInfo_struct*)tmpLst->data;
        if (mark->type == MARK_HIGHLIGHT)
          {
            VisuNode *node;
            VisuElement *ele;
            VisuElementRenderer *eleRenderer;
            float eleSize;
            int nlat;
            float xyz[3];
            gfloat rgba[4], material[5];

            g_debug(" | draw highlight mark for node %d.", mark->idNode1);
            node = visu_node_array_getFromId(VISU_NODE_ARRAY(dataObj), mark->idNode1);
            g_return_if_fail(node);

            if (!node->rendered)
              continue;
            ele = visu_node_array_getElement(VISU_NODE_ARRAY(dataObj), node);
            if (!visu_element_getRendered(ele))
              continue;

            eleRenderer = visu_node_array_renderer_get(extNode->marks->priv->renderer, ele);
            visu_element_renderer_getColorization(eleRenderer, VISU_ELEMENT_RENDERER_HIGHLIGHT_SEMI, rgba, material);
            eleSize = visu_element_renderer_getExtent(eleRenderer);
            nlat = visu_gl_view_getDetailLevel(extNode->marks->priv->view,
                                               eleSize * highlightFactor);

            visu_node_array_getNodePosition(VISU_NODE_ARRAY(dataObj), node, xyz);
            tool_drawSphere(gpuData, xyz, eleSize * highlightFactor, 2 * nlat);
            visu_gl_ext_layoutBufferWithColor(ext, 0, gpuData, rgba, material);
          }
      }

    visu_gl_ext_takeBuffer(ext, 0, gpuData);
  }
}

static void ext_node_render(const VisuGlExt *ext)
{
  glEnable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  
  glDepthMask(GL_FALSE);
  visu_gl_ext_renderBuffer(ext, 0);
  glDepthMask(GL_TRUE);
}
