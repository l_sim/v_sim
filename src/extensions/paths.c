/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "paths.h"

#include <epoxy/gl.h>

#include <visu_configFile.h>

/**
 * SECTION:paths
 * @short_description: Defines methods to draw paths.
 *
 * <para>Create a #VisuGlExt object to handle #VisuPaths drawing.</para>
 */

/**
 * VisuGlExtPathsClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtPathsClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPaths:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtPathsPrivate:
 *
 * Private fields for #VisuGlExtPaths objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtPathsPrivate
{
  gboolean dispose_has_run;

  /* Related objects. */
  VisuPaths *obj;
  float width;
};

static GObject* visu_gl_ext_paths_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_paths_dispose(GObject* obj);
static void visu_gl_ext_paths_rebuild(VisuGlExt *paths);
static void visu_gl_ext_paths_render(const VisuGlExt *ext);
static void visu_gl_ext_paths_draw(VisuGlExt *paths);

#define FLAG_RESOURCE_WIDTH "path_lineWidth"
#define DESC_RESOURCE_WIDTH "Line width for drawing of paths ; float (positive)"
static void exportResources(GString *data, VisuData *dataObj);

/* Local variables. */
static float pathWidth = 3.f;

/* Local callbacks */
static void onEntryUsed(VisuGlExtPaths *paths, VisuConfigFileEntry *entry, VisuConfigFile *obj);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtPaths, visu_gl_ext_paths, VISU_TYPE_GL_EXT,
                        G_ADD_PRIVATE(VisuGlExtPaths))

static void visu_gl_ext_paths_class_init(VisuGlExtPathsClass *klass)
{
  float rg[2] = {0.01f, 10.f};
  VisuConfigFileEntry *conf;

  g_debug("Extension Paths: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_paths_constructor;
  G_OBJECT_CLASS(klass)->dispose  = visu_gl_ext_paths_dispose;
  VISU_GL_EXT_CLASS(klass)->rebuild = visu_gl_ext_paths_rebuild;
  VISU_GL_EXT_CLASS(klass)->render = visu_gl_ext_paths_render;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_paths_draw;

  g_debug("Extension Paths: set the conf entries for this class.");
  conf = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                             FLAG_RESOURCE_WIDTH,
                                             DESC_RESOURCE_WIDTH,
                                             1, &pathWidth, rg, FALSE);
  visu_config_file_entry_setVersion(conf, 3.7f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResources);
}

static GObject* visu_gl_ext_paths_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, 1);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlProg"))
        g_value_set_uint(props[i].value, 1);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_NODES - 1);
    }

  return G_OBJECT_CLASS(visu_gl_ext_paths_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_paths_init(VisuGlExtPaths *obj)
{
  g_debug("Extension Paths: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_paths_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  /* Private data. */
  obj->priv->obj      = (VisuPaths*)0;
  obj->priv->width    = pathWidth;

  g_signal_connect_object(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_WIDTH,
                          G_CALLBACK(onEntryUsed), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_gl_ext_paths_dispose(GObject* obj)
{
  VisuGlExtPaths *paths;

  g_debug("Extension Paths: dispose object %p.", (gpointer)obj);

  paths = VISU_GL_EXT_PATHS(obj);
  if (paths->priv->dispose_has_run)
    return;
  paths->priv->dispose_has_run = TRUE;

  /* Disconnect signals. */
  visu_gl_ext_paths_set(paths, (VisuPaths*)0);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_gl_ext_paths_parent_class)->dispose(obj);
}

/**
 * visu_gl_ext_paths_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_PATHS_ID).
 *
 * Creates a new #VisuGlExt to draw paths.
 *
 * Since: 3.7
 *
 * Returns: (transfer full): a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtPaths* visu_gl_ext_paths_new(const gchar *name)
{
  g_debug("Extension Paths: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_PATHS,
                      "name", name ? name : VISU_GL_EXT_PATHS_ID, "label", _(name),
                      "description", _("Representation of paths."), NULL);
}

/**
 * visu_gl_ext_paths_set:
 * @paths: the #VisuGlExtPaths object to modify.
 * @obj: (allow-none) (transfer none): a #VisuPaths object.
 *
 * Set the #VisuPaths to be drawn.
 *
 * Since: 3.7
 *
 * Returns: TRUE if the model was actually changed.
 **/
gboolean visu_gl_ext_paths_set(VisuGlExtPaths *paths, VisuPaths *obj)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_PATHS(paths), FALSE);

  g_debug("Extension Paths: set a new path %p (%p).",
              (gpointer)obj, (gpointer)paths->priv->obj);
  if (obj == paths->priv->obj)
    return FALSE;

  if (paths->priv->obj)
    visu_paths_unref(paths->priv->obj);
  paths->priv->obj = obj;
  if (obj)
    visu_paths_ref(obj);

  visu_gl_ext_setDirty(VISU_GL_EXT(paths), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_paths_setWidth:
 * @paths: a #VisuGlExtPaths object.
 * @value: a positive float lower than 10.
 *
 * Change the rendering width of the @paths.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the width has changed.
 **/
gboolean visu_gl_ext_paths_setWidth(VisuGlExtPaths *paths, float value)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_PATHS(paths) && value > 0.f, FALSE);

  value = CLAMP(value, 0.f, 10.f);
  if (value == paths->priv->width)
    return FALSE;

  paths->priv->width = value;
  visu_gl_ext_setDirty(VISU_GL_EXT(paths), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_paths_getWidth:
 * @paths: a #VisuGlExtPaths object.
 *
 * Inquire the width used to render the @paths.
 *
 * Since: 3.8
 *
 * Returns: the width used to render the path.
 **/
float visu_gl_ext_paths_getWidth(VisuGlExtPaths *paths)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_PATHS(paths), pathWidth);

  return paths->priv->width;
}

/****************/
/* Private part */
/****************/
static void visu_gl_ext_paths_rebuild(VisuGlExt *ext)
{
  GError *error = (GError*)0;

  if (!visu_gl_ext_setShaderById(ext, 0, VISU_GL_SHADER_SIMPLE, &error))
    {
      g_warning("Cannot create marks shader: %s", error->message);
      g_clear_error(&error);
    }
}

static void visu_gl_ext_paths_render(const VisuGlExt *ext)
{
  glDepthMask(0);
  glLineWidth(VISU_GL_EXT_PATHS(ext)->priv->width);
  glPointSize(VISU_GL_EXT_PATHS(ext)->priv->width);
  glEnable(GL_POINT_SMOOTH);

  visu_gl_ext_renderBuffer(ext, 0);
  visu_gl_ext_renderBufferWithPrimitive(ext, 0, VISU_GL_POINTS, (const gfloat*)0);

  glDepthMask(1);
  glDisable(GL_POINT_SMOOTH);
}

static void visu_gl_ext_paths_draw(VisuGlExt *paths)
{
  visu_gl_ext_clearBuffers(paths);
  if (VISU_GL_EXT_PATHS(paths)->priv->obj)
    {
      GArray *data = visu_gl_ext_startBuffer(paths, 0, 0, VISU_GL_RGBA_XYZ, VISU_GL_LINE_STRIP);
      VisuPathsIter *iter = visu_paths_iter_new(VISU_GL_EXT_PATHS(paths)->priv->obj);
      do
        {
          visu_paths_iter_addSegment(iter, data);
          visu_gl_ext_layoutBuffer(paths, 0, data);
        } while (visu_paths_iter_next(iter));
      visu_gl_ext_takeBuffer(paths, 0, data);
    }
}

/*************************/
/* Resources management. */
/*************************/
static void exportResources(GString *data, VisuData *dataObj _U_)
{
  visu_config_file_exportComment(data, DESC_RESOURCE_WIDTH);
  visu_config_file_exportEntry(data, FLAG_RESOURCE_WIDTH, NULL,
                               "%f", pathWidth);

  visu_config_file_exportComment(data, "");
}
static void onEntryUsed(VisuGlExtPaths *paths, VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  visu_gl_ext_paths_setWidth(paths, pathWidth);
}
