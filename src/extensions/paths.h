/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef PATHS_H
#define PATHS_H

#include <visu_extension.h>
#include <extraFunctions/geometry.h>

/**
 * VISU_TYPE_GL_EXT_PATHS:
 *
 * return the type of #VisuGlExtPaths.
 *
 * Since: 3.7
 */
#define VISU_TYPE_GL_EXT_PATHS	     (visu_gl_ext_paths_get_type ())
/**
 * VISU_GL_EXT_PATHS:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtPaths type.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PATHS(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_PATHS, VisuGlExtPaths))
/**
 * VISU_GL_EXT_PATHS_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtPathsClass.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PATHS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_PATHS, VisuGlExtPathsClass))
/**
 * VISU_IS_GL_EXT_PATHS:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtPaths object.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PATHS(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_PATHS))
/**
 * VISU_IS_GL_EXT_PATHS_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtPathsClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_GL_EXT_PATHS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_PATHS))
/**
 * VISU_GL_EXT_PATHS_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_GL_EXT_PATHS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_PATHS, VisuGlExtPathsClass))

typedef struct _VisuGlExtPaths        VisuGlExtPaths;
typedef struct _VisuGlExtPathsPrivate VisuGlExtPathsPrivate;
typedef struct _VisuGlExtPathsClass   VisuGlExtPathsClass;

struct _VisuGlExtPaths
{
  VisuGlExt parent;

  VisuGlExtPathsPrivate *priv;
};

struct _VisuGlExtPathsClass
{
  VisuGlExtClass parent;
};

/**
 * VISU_GL_EXT_PATHS_ID:
 *
 * The id used to identify this extension, see
 * visu_gl_ext_rebuild() for instance.
 */
#define VISU_GL_EXT_PATHS_ID "Paths"

/**
 * visu_gl_ext_paths_get_type:
 *
 * This method returns the type of #VisuGlExtPaths, use
 * VISU_TYPE_GL_EXT_PATHS instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtPaths.
 */
GType visu_gl_ext_paths_get_type(void);

VisuGlExtPaths* visu_gl_ext_paths_new(const gchar *name);

gboolean visu_gl_ext_paths_set(VisuGlExtPaths *paths, VisuPaths *obj);
gboolean visu_gl_ext_paths_setWidth(VisuGlExtPaths *paths, float value);
float visu_gl_ext_paths_getWidth(VisuGlExtPaths *paths);

#endif
