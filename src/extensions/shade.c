/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2009)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#include "shade.h"

#include <string.h>

#include <epoxy/gl.h>
#include <coreTools/toolColor.h>

/**
 * SECTION:shade
 * @short_description: Draw a frame with the representation of a color shade.
 *
 * <para>This extension draws a frame on top of the rendering area
 * with a color shade. One can setup printed values and draw
 * additional marks inside the shade.</para>
 *
 * Since: 3.7
 */

/**
 * VisuGlExtShadeClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtShadeClass structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtShade:
 *
 * An opaque structure.
 *
 * Since: 3.7
 */
/**
 * VisuGlExtShadePrivate:
 *
 * Private fields for #VisuGlExtShade objects.
 *
 * Since: 3.7
 */
struct _VisuGlExtShadePrivate
{
  /* Shade definition. */
  ToolShade *shade;

  /* Parameters. */
  float minMax[2];
  ToolMatrixScalingFlag scaling;
  GArray *marks;
};

enum
  {
   BUF_SHADE,
   BUF_TICKS,
   BUF_FRAME,
   BUF_MARKS,
   N_BUFFERS
  };
enum {
  PROP_0,
  PROP_SHADE,
  PROP_MM,
  N_PROPS
};
static GParamSpec *_properties[N_PROPS];

static GObject* visu_gl_ext_shade_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_gl_ext_shade_finalize(GObject* obj);
static void visu_gl_ext_shade_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec);
static void visu_gl_ext_shade_set_property(GObject* obj, guint property_id,
                                           const GValue *value, GParamSpec *pspec);
static gboolean visu_gl_ext_shade_isValid(const VisuGlExtFrame *frame);
static void visu_gl_ext_shade_render(const VisuGlExtFrame *self);
static void visu_gl_ext_shade_render_text(const VisuGlExtFrame *self);
static void visu_gl_ext_shade_draw(VisuGlExt *self);

G_DEFINE_TYPE_WITH_CODE(VisuGlExtShade, visu_gl_ext_shade, VISU_TYPE_GL_EXT_FRAME,
                        G_ADD_PRIVATE(VisuGlExtShade))

static void visu_gl_ext_shade_class_init(VisuGlExtShadeClass *klass)
{
  g_debug("Extension Shade: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */

  /* g_debug("                - adding new resources ;"); */

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor = visu_gl_ext_shade_constructor;
  G_OBJECT_CLASS(klass)->finalize = visu_gl_ext_shade_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_gl_ext_shade_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_gl_ext_shade_get_property;
  VISU_GL_EXT_CLASS(klass)->draw = visu_gl_ext_shade_draw;
  VISU_GL_EXT_FRAME_CLASS(klass)->render = visu_gl_ext_shade_render;
  VISU_GL_EXT_FRAME_CLASS(klass)->renderText = visu_gl_ext_shade_render_text;
  VISU_GL_EXT_FRAME_CLASS(klass)->isValid = visu_gl_ext_shade_isValid;

  /**
   * VisuGlExtShade::shade:
   *
   * The colour shade.
   *
   * Since: 3.8
   */
  _properties[PROP_SHADE] =
    g_param_spec_boxed("shade", "Shade", "colorization scheme",
                       TOOL_TYPE_SHADE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  /**
   * VisuGlExtShade::range-min-max:
   *
   * Min / max range as used to normalise data.
   *
   * Since: 3.8
   */
  _properties[PROP_MM] = g_param_spec_boxed("range-min-max", "Range min/max",
                                            "min / max range to normalise data",
                                            G_TYPE_ARRAY,
                                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties(G_OBJECT_CLASS(klass), N_PROPS, _properties);
}

static GObject* visu_gl_ext_shade_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
#define SHADE_LEGEND_WIDTH  20.f
#define SHADE_LEGEND_HEIGHT 175.f
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "nGlObj"))
        g_value_set_uint(props[i].value, N_BUFFERS);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "priority"))
        g_value_set_uint(props[i].value, VISU_GL_EXT_PRIORITY_LAST);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "x-pos"))
        g_value_set_float(props[i].value, 0.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-pos"))
        g_value_set_float(props[i].value, 0.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "x-padding"))
        g_value_set_float(props[i].value, 5.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-padding"))
        g_value_set_float(props[i].value, 5.f);
      else if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "y-requisition"))
        g_value_set_uint(props[i].value, SHADE_LEGEND_HEIGHT + 5 * 2);
    }

  return G_OBJECT_CLASS(visu_gl_ext_shade_parent_class)->constructor(gtype, nprops, props);
}

static void visu_gl_ext_shade_init(VisuGlExtShade *obj)
{
  g_debug("Extension Shade: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_gl_ext_shade_get_instance_private(obj);

  /* Private data. */
  obj->priv->marks      = g_array_new(FALSE, FALSE, sizeof(float));
  obj->priv->scaling    = TOOL_MATRIX_SCALING_LINEAR;
  obj->priv->minMax[0]  = G_MAXFLOAT;
  obj->priv->minMax[1]  = -G_MAXFLOAT;
  obj->priv->shade      = (ToolShade*)0;
}
static void visu_gl_ext_shade_get_property(GObject* obj, guint property_id,
                                           GValue *value, GParamSpec *pspec)
{
  VisuGlExtShade *self = VISU_GL_EXT_SHADE(obj);
  GArray *arr;

  g_debug("Extension Shade: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_SHADE:
      g_value_set_boxed(value, self->priv->shade);
      g_debug("%p.", (gpointer)self->priv->shade);
      break;
    case PROP_MM:
      g_debug("%g / %g.", self->priv->minMax[0], self->priv->minMax[1]);
      arr = g_array_sized_new(FALSE, FALSE, sizeof(float), 2);
      g_array_index(arr, float, 0) = self->priv->minMax[0];
      g_array_index(arr, float, 1) = self->priv->minMax[1];
      g_value_take_boxed(value, arr);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_gl_ext_shade_set_property(GObject* obj, guint property_id,
                                           const GValue *value, GParamSpec *pspec)
{
  VisuGlExtShade *self = VISU_GL_EXT_SHADE(obj);
  GArray *arr;

  g_debug("Visu Data Colorizer Fragment: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_SHADE:
      g_debug("%p.", g_value_get_boxed(value));
      visu_gl_ext_shade_setShade(self, (ToolShade*)g_value_get_boxed(value));
      break;
    case PROP_MM:
      g_debug("%p.", g_value_get_boxed(value));
      arr = (GArray*)g_value_get_boxed(value);
      visu_gl_ext_shade_setMinMax(self, g_array_index(arr, float, 0),
                                  g_array_index(arr, float, 1));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

static void visu_gl_ext_shade_finalize(GObject* obj)
{
  VisuGlExtShade *shade;

  g_return_if_fail(obj);

  g_debug("Extension Shade: finalize object %p.", (gpointer)obj);

  shade = VISU_GL_EXT_SHADE(obj);

  /* Free privs elements. */
  g_debug("Extension Shade: free private shade.");
  tool_shade_free(shade->priv->shade);
  g_array_unref(shade->priv->marks);

  /* Chain up to the parent class */
  g_debug("Extension Shade: chain to parent.");
  G_OBJECT_CLASS(visu_gl_ext_shade_parent_class)->finalize(obj);
  g_debug("Extension Shade: freeing ... OK.");
}

/**
 * visu_gl_ext_shade_new:
 * @name: (allow-none): the name to give to the extension (default is #VISU_GL_EXT_SHADE_ID).
 *
 * Creates a new #VisuGlExt to draw a shade.
 *
 * Since: 3.7
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtShade* visu_gl_ext_shade_new(const gchar *name)
{
  g_debug("Extension Shade: new object.");
  
  return g_object_new(VISU_TYPE_GL_EXT_SHADE,
                      "name", (name) ? name : VISU_GL_EXT_SHADE_ID, "label", _(name),
                      "description", _("Draw the legend of a color shade."), NULL);
}
/**
 * visu_gl_ext_shade_setShade:
 * @ext: The #VisuGlExtShade to attached to.
 * @shade: the shade to get the color of.
 *
 * Attach an #VisuGlView to render to and setup the shade.
 *
 * Since: 3.7
 *
 * Returns: TRUE if internal shade is changed.
 **/
gboolean visu_gl_ext_shade_setShade(VisuGlExtShade *ext, const ToolShade *shade)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SHADE(ext), FALSE);

  if (tool_shade_compare(ext->priv->shade, shade))
    return FALSE;

  tool_shade_free(ext->priv->shade);
  ext->priv->shade = tool_shade_copy(shade);

  g_object_notify_by_pspec(G_OBJECT(ext), _properties[PROP_SHADE]);

  visu_gl_ext_setDirty(VISU_GL_EXT(ext), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_shade_setMinMax:
 * @shade: the #VisuGlExtShade to update.
 * @minV: a value.
 * @maxV: another value.
 *
 * Change the minimum and maximum values used on the legend.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_shade_setMinMax(VisuGlExtShade *shade, float minV, float maxV)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SHADE(shade), FALSE);
  g_return_val_if_fail(minV <= maxV, FALSE);

  if (shade->priv->minMax[0] == minV && shade->priv->minMax[1] == maxV)
    return FALSE;

  shade->priv->minMax[0] = minV;
  shade->priv->minMax[1] = maxV;

  g_object_notify_by_pspec(G_OBJECT(shade), _properties[PROP_MM]);

  visu_gl_ext_setDirty(VISU_GL_EXT(shade), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_shade_setScaling:
 * @shade: the #VisuGlExtShade to update.
 * @scaling: a #ToolMatrixScalingFlag value.
 *
 * Change the scaling variation of the shade between the minimum and
 * the maximum values, see visu_gl_ext_shade_setMinMax().
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_shade_setScaling(VisuGlExtShade *shade, ToolMatrixScalingFlag scaling)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SHADE(shade), FALSE);

  if (shade->priv->scaling == scaling)
    return FALSE;

  shade->priv->scaling = scaling;

  visu_gl_ext_setDirty(VISU_GL_EXT(shade), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}
/**
 * visu_gl_ext_shade_setMarks:
 * @shade: the #VisuGlExtShade to update.
 * @marks: (array length=n): a list of float values in [0;1].
 * @n: the length of @marks.
 *
 * The legend can draw additional marks in the shade. Setup these
 * marks with this routine. The first and the last marks of the list will be
 * rendered bigger than the next ones.
 *
 * Since: 3.7
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_gl_ext_shade_setMarks(VisuGlExtShade *shade, float *marks, guint n)
{
  g_return_val_if_fail(VISU_IS_GL_EXT_SHADE(shade), FALSE);

  g_array_set_size(shade->priv->marks, n);
  memcpy(shade->priv->marks->data, marks, sizeof(float) * n);

  visu_gl_ext_setDirty(VISU_GL_EXT(shade), VISU_GL_DRAW_REQUIRED);
  return TRUE;
}

#define SHADE_LEGEND_N_QUADS 50u

static void visu_gl_ext_shade_draw(VisuGlExt *self)
{
  VisuGlExtShade *shade;
  GArray *labels;
  guint i, mWidth;
  tool_matrix_getScaledValue get_inv;
  gfloat yStep;
  GLfloat at[3], rgba[4];
  VisuGlExtLabel lbl;
  GArray *shadeData;

  shade = VISU_GL_EXT_SHADE(self);

  shadeData = visu_gl_ext_startBuffer(self, 0, BUF_SHADE, VISU_GL_RGBA_XYZ, VISU_GL_TRIANGLE_STRIP);
  yStep = (gfloat)SHADE_LEGEND_HEIGHT / (gfloat)SHADE_LEGEND_N_QUADS;
  for (i = 0; i <= SHADE_LEGEND_N_QUADS; i++)
    {
      tool_shade_valueToRGB(shade->priv->shade, rgba,
                            (float)i / (float)SHADE_LEGEND_N_QUADS);
      g_array_append_vals(shadeData, rgba, 4);
      at[0] = 0.f;
      at[1] = (GLfloat)i * yStep;
      at[2] = 0.f;
      g_array_append_vals(shadeData, at, 3);
      g_array_append_vals(shadeData, rgba, 4);
      at[0] = (GLfloat)SHADE_LEGEND_WIDTH;
      g_array_append_vals(shadeData, at, 3);
    }
  visu_gl_ext_takeBuffer(self, BUF_SHADE, shadeData);

  shadeData = visu_gl_ext_startBuffer(self, 0, BUF_FRAME, VISU_GL_RGBA_XYZ, VISU_GL_LINE_LOOP);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = at[1] = 0.5f;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] += SHADE_LEGEND_WIDTH;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[1] += SHADE_LEGEND_HEIGHT - 1;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] -= SHADE_LEGEND_WIDTH;
  g_array_append_vals(shadeData, at, 3);
  visu_gl_ext_takeBuffer(self, BUF_FRAME, shadeData);

  shadeData = visu_gl_ext_startBuffer(self, 0, BUF_TICKS, VISU_GL_RGBA_XYZ, VISU_GL_LINES);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH;
  at[1] = 0.5f;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH + 3;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH;
  at[1] = 0.5f + 0.33333333f * (SHADE_LEGEND_HEIGHT - 1);
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH + 3;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH;
  at[1] = 0.5f + 0.66666667f * (SHADE_LEGEND_HEIGHT - 1);
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH + 3;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH;
  at[1] = SHADE_LEGEND_HEIGHT;
  g_array_append_vals(shadeData, at, 3);
  g_array_append_vals(shadeData, VISU_GL_EXT_FRAME(self)->fontRGB, 4);
  at[0] = SHADE_LEGEND_WIDTH + 3;
  g_array_append_vals(shadeData, at, 3);
  visu_gl_ext_takeBuffer(self, BUF_TICKS, shadeData);

  if (shade->priv->marks)
    {
      GArray *marks
        = visu_gl_ext_startBuffer(self, 0, BUF_MARKS, VISU_GL_RGBA_XYZ, VISU_GL_LINES);
      GLfloat xbuf = 0.f, zbuf = 0.f;
      for (i = 0; i < shade->priv->marks->len; i++)
	{
          gfloat rgba[4];
          if (i == 0 || i == shade->priv->marks->len - 1)
            xbuf = 3.f;
          else if (i == 1)
            xbuf = 8.f;
	  yStep = CLAMP(g_array_index(shade->priv->marks, float, i), 0., 1.);
	  tool_shade_valueToRGB(shade->priv->shade, rgba, yStep);
	  rgba[0] = 1. - rgba[0];
	  rgba[1] = 1. - rgba[1];
	  rgba[2] = 1. - rgba[2];
	  yStep *= SHADE_LEGEND_HEIGHT;
          yStep = CLAMP(yStep, 2.f, SHADE_LEGEND_HEIGHT - 1.f);
          if (i == shade->priv->marks->len - 1)
            {
              g_array_prepend_val(marks, zbuf);
              g_array_prepend_val(marks, yStep);
              g_array_prepend_val(marks, xbuf);
              g_array_prepend_vals(marks, rgba, 4);
            }
          else
            {
              g_array_append_vals(marks, rgba, 4);
              g_array_append_val(marks, xbuf);
              g_array_append_val(marks, yStep);
              g_array_append_val(marks, zbuf);
            }
          xbuf = SHADE_LEGEND_WIDTH - xbuf;
          if (i == shade->priv->marks->len - 1)
            {
              g_array_prepend_val(marks, zbuf);
              g_array_prepend_val(marks, yStep);
              g_array_prepend_val(marks, xbuf);
              g_array_prepend_vals(marks, rgba, 4);
            }
          else
            {
              g_array_append_vals(marks, rgba, 4);
              g_array_append_val(marks, xbuf);
              g_array_append_val(marks, yStep);
              g_array_append_val(marks, zbuf);
            }
	}
      visu_gl_ext_takeBuffer(self, BUF_MARKS, marks);
    }

  switch (shade->priv->scaling)
    {
    case TOOL_MATRIX_SCALING_LINEAR:
      get_inv = tool_matrix_getScaledLinearInv;
      break;
    case TOOL_MATRIX_SCALING_LOG:
      get_inv = tool_matrix_getScaledLogInv;
      break;
    case TOOL_MATRIX_SCALING_ZERO_CENTRED_LOG:
      get_inv = tool_matrix_getScaledZeroCentredLogInv;
      break;
    default:
      get_inv = (tool_matrix_getScaledValue)0;
      break;
    }
  g_return_if_fail(get_inv);

  labels = g_array_new(FALSE, FALSE, sizeof(VisuGlExtLabel));
  sprintf(lbl.lbl, "%.3g", get_inv(0.f, shade->priv->minMax));
  lbl.xyz[0] = SHADE_LEGEND_WIDTH + 5.f;
  lbl.xyz[1] = 0.f;
  lbl.align[0] = VISU_GL_LEFT;
  lbl.align[1] = VISU_GL_RIGHT;
  memcpy(lbl.rgba, VISU_GL_EXT_FRAME(self)->fontRGB, sizeof(gfloat) * 4);
  g_array_append_val(labels, lbl);
  sprintf(lbl.lbl, "%.3g", get_inv(0.33333f, shade->priv->minMax));
  lbl.xyz[1] = 0.333333f;
  lbl.align[1] = VISU_GL_CENTER;
  g_array_append_val(labels, lbl);
  sprintf(lbl.lbl, "%.3g", get_inv(0.66667f, shade->priv->minMax));
  lbl.xyz[1] = 0.666667f;
  lbl.align[1] = VISU_GL_CENTER;
  g_array_append_val(labels, lbl);
  sprintf(lbl.lbl, "%.3g", get_inv(1.f, shade->priv->minMax));
  lbl.xyz[1] = 1.f;
  lbl.align[1] = VISU_GL_LEFT;
  g_array_append_val(labels, lbl);
  visu_gl_ext_setLabels(self, labels, TOOL_WRITER_FONT_NORMAL, FALSE);

  mWidth = 0;
  mWidth = MAX(g_array_index(labels, VisuGlExtLabel, 0).width, mWidth);
  mWidth = MAX(g_array_index(labels, VisuGlExtLabel, 1).width, mWidth);
  mWidth = MAX(g_array_index(labels, VisuGlExtLabel, 2).width, mWidth);
  mWidth = MAX(g_array_index(labels, VisuGlExtLabel, 3).width, mWidth);

  mWidth += (5 * 3 + SHADE_LEGEND_WIDTH);
  g_object_set(self, "x-requisition", mWidth, NULL);

  g_array_unref(labels);

  VISU_GL_EXT_CLASS(visu_gl_ext_shade_parent_class)->draw(self);
}

static gboolean visu_gl_ext_shade_isValid(const VisuGlExtFrame *frame)
{
  VisuGlExtShade *shade;

  g_return_val_if_fail(VISU_IS_GL_EXT_SHADE(frame), FALSE);
  shade = VISU_GL_EXT_SHADE(frame);

  return (shade->priv->shade && shade->priv->minMax[0] < shade->priv->minMax[1]);
}

static void visu_gl_ext_shade_render(const VisuGlExtFrame *self)
{
  VisuGlExtShade *shade = VISU_GL_EXT_SHADE(self);
  GLboolean wasEnabled;

  /* The shade */
  visu_gl_ext_renderBuffer(VISU_GL_EXT(self), BUF_SHADE);
  if (shade->priv->marks)
    {
      visu_gl_ext_startRenderShader(VISU_GL_EXT(self), 0, BUF_MARKS);
      glLineWidth(2);
      glDrawArrays(GL_LINES, 0, MIN(shade->priv->marks->len * 2, 4));
      glLineWidth(1);
      glDrawArrays(GL_LINES, 4, MAX(0, (gint)shade->priv->marks->len * 2 - 4));
      visu_gl_ext_stopRenderShader(VISU_GL_EXT(self), 0, BUF_MARKS);
    }

  /* the frame and ticks */
  glLineWidth(visu_gl_ext_frame_getScale(self));
  wasEnabled = glIsEnabled(GL_LINE_SMOOTH);
  glDisable(GL_LINE_SMOOTH);
  visu_gl_ext_renderBuffer(VISU_GL_EXT(self), BUF_FRAME);
  visu_gl_ext_renderBuffer(VISU_GL_EXT(self), BUF_TICKS);
  if (wasEnabled)
    glEnable(GL_LINE_SMOOTH);
}

static void visu_gl_ext_shade_render_text(const VisuGlExtFrame *self)
{
  /* The labels. */
  visu_gl_ext_blitLabelsOnScreen(VISU_GL_EXT(self));
}
