/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "vibrations.h"

/**
 * SECTION:vibrations
 * @short_description: Draw arrows at each node to represent vibrations.
 *
 * <para>A specialised #VisuGlExtNodeVectors to represent vibrations on nodes.</para>
 */

/**
 * VisuGlExtVibrationsClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuGlExtVibrationsClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuGlExtVibrations:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */

G_DEFINE_TYPE(VisuGlExtVibrations, visu_gl_ext_vibrations, VISU_TYPE_GL_EXT_NODE_VECTORS)

static void visu_gl_ext_vibrations_class_init(VisuGlExtVibrationsClass *klass _U_)
{
  g_debug("Visu GlExt Vibrations: creating the class of the object.");
  /* g_debug("                - adding new signals ;"); */
}

static void visu_gl_ext_vibrations_init(VisuGlExtVibrations *obj _U_)
{
  g_debug("Visu GlExt Vibrations: initializing a new object (%p).",
	      (gpointer)obj);
}

/**
 * visu_gl_ext_vibrations_new:
 * @name: (allow-none): the name to give to the extension.
 *
 * Creates a new #VisuGlExt to draw vibrations.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the #VisuGlExt it created or
 * NULL otherwise.
 */
VisuGlExtVibrations* visu_gl_ext_vibrations_new(const gchar *name)
{
  char *name_ = "Vibrations";
  char *description = _("Draw vibrations with vectors.");
  VisuGlExtNodeVectors *vibrations;

  g_debug("Visu GlExt Vibrations: new object.");
  vibrations = VISU_GL_EXT_NODE_VECTORS(g_object_new(VISU_TYPE_GL_EXT_VIBRATIONS,
                                                     "name", (name) ? name : name_,
                                                     "label", _(name_),
                                                     "description", description,
                                                     "rendering-size", 1.f,
                                                     "normalisation", -1.f,
                                                     NULL));
  visu_gl_ext_node_vectors_setCentering(vibrations, VISU_ARROW_TAIL_CENTERED);
  visu_gl_ext_node_vectors_setColor(vibrations, FALSE);
  visu_gl_ext_node_vectors_setArrow(vibrations, 0.5f, 0.2f, 10,
                                    0.5f, 0.3f, 10);
  visu_gl_ext_node_vectors_setVectorThreshold(vibrations, -0.05f); /* Value
                                                                      in percentage. */
  visu_gl_ext_node_vectors_setAddLength(vibrations, 2.5f);
  
  return VISU_GL_EXT_VIBRATIONS(vibrations);
}
