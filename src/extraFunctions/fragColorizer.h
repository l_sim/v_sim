/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef FRAGCOLORIZER_H
#define FRAGCOLORIZER_H

#include <glib.h>
#include <glib-object.h>

#include "colorizer.h"
#include "fragProp.h"
#include <iface_nodemasker.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_COLORIZER_FRAGMENT:
 *
 * return the type of #VisuDataColorizerFragment.
 */
#define VISU_TYPE_DATA_COLORIZER_FRAGMENT	     (visu_data_colorizer_fragment_get_type ())
/**
 * VISU_DATA_COLORIZER_FRAGMENT:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataColorizerFragment type.
 */
#define VISU_DATA_COLORIZER_FRAGMENT(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_COLORIZER_FRAGMENT, VisuDataColorizerFragment))
/**
 * VISU_DATA_COLORIZER_FRAGMENT_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataColorizerFragmentClass.
 */
#define VISU_DATA_COLORIZER_FRAGMENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_COLORIZER_FRAGMENT, VisuDataColorizerFragmentClass))
/**
 * VISU_IS_DATA_COLORIZER_FRAGMENT:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataColorizerFragment object.
 */
#define VISU_IS_DATA_COLORIZER_FRAGMENT(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_COLORIZER_FRAGMENT))
/**
 * VISU_IS_DATA_COLORIZER_FRAGMENT_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataColorizerFragmentClass class.
 */
#define VISU_IS_DATA_COLORIZER_FRAGMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_COLORIZER_FRAGMENT))
/**
 * VISU_DATA_COLORIZER_FRAGMENT_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_COLORIZER_FRAGMENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_COLORIZER_FRAGMENT, VisuDataColorizerFragmentClass))

/**
 * VisuDataColorizerFragmentPrivate:
 * 
 * Private data for #VisuDataColorizerFragment objects.
 */
typedef struct _VisuDataColorizerFragmentPrivate VisuDataColorizerFragmentPrivate;

/**
 * VisuDataColorizerFragment:
 * 
 * Common name to refer to a #_VisuDataColorizerFragment.
 */
typedef struct _VisuDataColorizerFragment VisuDataColorizerFragment;
struct _VisuDataColorizerFragment
{
  VisuDataColorizer parent;

  VisuDataColorizerFragmentPrivate *priv;
};

/**
 * VisuDataColorizerFragmentClass:
 * @parent: its parent.
 *
 * Interface for class that can represent #VisuDataColorizerFragment.
 *
 * Since: 3.8
 */
typedef struct _VisuDataColorizerFragmentClass VisuDataColorizerFragmentClass;
struct _VisuDataColorizerFragmentClass
{
  VisuDataColorizerClass parent;
};

/**
 * visu_data_colorizer_fragment_get_type:
 *
 * This method returns the type of #VisuDataColorizerFragment, use
 * VISU_TYPE_DATA_COLORIZER_FRAGMENT instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuDataColorizerFragment.
 */
GType visu_data_colorizer_fragment_get_type(void);

VisuDataColorizerFragment* visu_data_colorizer_fragment_new();

gboolean visu_data_colorizer_fragment_setNodeModel(VisuDataColorizerFragment *colorizer,
                                                   VisuNodeValuesFrag *model);

/**
 * VisuDataColorizerFragmentTypes:
 * @COLORIZATION_NONE: no per node colorization.
 * @COLORIZATION_PER_FRAGMENT: colorize each node of a given fragment
 * with the same colour.
 * @COLORIZATION_PER_TYPE: colorize ecah node of the same fragment
 * family with the same colour.
 *
 * The fragment colorization schemes.
 *
 * Since: 3.8
 */
typedef enum {
  COLORIZATION_NONE,
  COLORIZATION_PER_FRAGMENT,
  COLORIZATION_PER_TYPE
} VisuDataColorizerFragmentTypes;
gboolean visu_data_colorizer_fragment_setType(VisuDataColorizerFragment *colorizer,
                                              VisuDataColorizerFragmentTypes type);

gboolean visu_data_colorizer_fragment_setDefaultVisibility(VisuDataColorizerFragment *frag,
                                                           gboolean status);
gboolean visu_data_colorizer_fragment_setVisibility(VisuDataColorizerFragment *frag,
                                                    const gchar *label,
                                                    gboolean status);

G_END_DECLS

#endif
