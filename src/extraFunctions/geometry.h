/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <glib-object.h>

#include <visu_data.h>
#include <extraFunctions/vectorProp.h>
#include <coreTools/toolShade.h>
#include "gdiff.h"

typedef struct _VisuPaths VisuPaths;

#define VISU_TYPE_PATHS (visu_paths_get_type())
GType       visu_paths_get_type(void);

VisuPaths* visu_paths_new(float translation[3]);
VisuPaths* visu_paths_ref(VisuPaths *paths);
void        visu_paths_unref(VisuPaths *paths);
void        visu_paths_free(VisuPaths *paths);
void        visu_paths_empty(VisuPaths *paths);

gboolean    visu_paths_addFromDiff(VisuPaths *paths, VisuDataDiff *data, gdouble energy);
gboolean    visu_paths_addNodeStep(VisuPaths *paths, guint time, guint nodeId,
                                    float xyz[3], float dxyz[3], float energy);
void        visu_paths_pinPositions(VisuPaths *paths, VisuData *data);

void        visu_paths_constrainInBox(VisuPaths *paths, VisuData *data);
gboolean    visu_paths_exportXMLFile(const VisuPaths *paths,
                                    const gchar *filename, GError **error);
gboolean    visu_paths_parseFromXML(const gchar* filename, VisuPaths *paths,
                                     GError **error);

void        visu_paths_setTranslation(VisuPaths *paths, float cartCoord[3]);
gboolean    visu_paths_setToolShade(VisuPaths *paths, const ToolShade* shade);
const ToolShade*  visu_paths_getToolShade(const VisuPaths *paths);
guint       visu_paths_getLength(VisuPaths *paths);

/**
 * VisuPathsIter:
 * @parent: a pointer to the #VisuPaths this iter is running on.
 * @path: private
 * @item: private
 *
 * An iterator to run on the various segments stored in the path.
 *
 * Since: 3.9
 */
typedef struct _VisuPathsIter VisuPathsIter;
struct _VisuPathsIter
{
  const VisuPaths *parent;
  /* Private data */
  GList *path;
  guint item;
};

VisuPathsIter* visu_paths_iter_new(const VisuPaths *paths);
gboolean visu_paths_iter_next(VisuPathsIter *iter);
void visu_paths_iter_addSegment(const VisuPathsIter *iter, GArray *data);

#endif
