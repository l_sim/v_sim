/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "idProp.h"

/**
 * SECTION:idProp
 * @short_description: define a #VisuNodeValues object to access node idinates.
 *
 * <para>Defines a #VisuNodeValues object to be notified about
 * idinate changes.</para>
 */

static gboolean _getAt(const VisuNodeValues *vals, const VisuNode *node,
                       GValue *value);
static gchar* _serialize(const VisuNodeValues *vals, const VisuNode *node);


G_DEFINE_TYPE(VisuNodeValuesId, visu_node_values_id, VISU_TYPE_NODE_VALUES)

static void visu_node_values_id_class_init(VisuNodeValuesIdClass *klass)
{
  /* Connect the overloading methods. */
  VISU_NODE_VALUES_CLASS(klass)->getAt = _getAt;
  VISU_NODE_VALUES_CLASS(klass)->serialize = _serialize;
}

static void visu_node_values_id_init(VisuNodeValuesId *self _U_)
{
  g_debug("Visu ValuesId: creating new object %p.", (gpointer)self);
}

static gchar* _serialize(const VisuNodeValues *vals _U_, const VisuNode *node)
{
  return g_strdup_printf("%u", node ? node->number + 1 : 0);
}

/**
 * visu_node_values_id_new:
 * @array: a #VisuData object.
 *
 * Create a #VisuNodeValues object to handle idinates of nodes.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuNodeValuesId object.
 **/
VisuNodeValuesId* visu_node_values_id_new(VisuNodeArray *array)
{
  VisuNodeValuesId *vals;

  vals = VISU_NODE_VALUES_ID(g_object_new(VISU_TYPE_NODE_VALUES_ID,
                                          "label", _("Id"),
                                          "internal", TRUE,
                                          "nodes", array,
                                          "type", G_TYPE_UINT,
                                          "n-elements", 1,
                                          "editable", FALSE,
                                          NULL));
  return vals;
}

/**
 * visu_node_values_id_getAt:
 * @vect: a #VisuNodeValuesId object.
 * @node: a #VisuNode object.
 *
 * Retrieves the id hosted on @node.
 *
 * Since: 3.8
 *
 * Returns: the #VisuElement for @node.
 **/
guint visu_node_values_id_getAt(const VisuNodeValuesId *vect,
                                const VisuNode *node)
{
  g_return_val_if_fail(VISU_IS_NODE_VALUES_ID(vect), 0);

  return node ? node->number : 0;
}

static gboolean _getAt(const VisuNodeValues *vals _U_, const VisuNode *node,
                       GValue *value)
{
  g_value_set_uint(value, node ? node->number : 0);
  return TRUE;
}
