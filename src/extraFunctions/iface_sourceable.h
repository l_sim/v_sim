/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2019)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_SOURCEABLE_H
#define IFACE_SOURCEABLE_H

#include <glib.h>
#include <glib-object.h>
#include <visu_data.h>
#include "nodeProp.h"

G_BEGIN_DECLS

/* Sourceable interface. */
#define VISU_TYPE_SOURCEABLE                (visu_sourceable_get_type ())
#define VISU_SOURCEABLE(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_SOURCEABLE, VisuSourceable))
#define VISU_IS_SOURCEABLE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_SOURCEABLE))
#define VISU_SOURCEABLE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), VISU_TYPE_SOURCEABLE, VisuSourceableInterface))

typedef struct _VisuSourceableInterface VisuSourceableInterface;
typedef struct _VisuSourceable VisuSourceable;
typedef struct _VisuSourceableData VisuSourceableData;

/**
 * VisuSourceable:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuSourceableInterface:
 * @parent: its parent.
 * @getSource: a method to access the #VisuSourceableData storage that
 * a sourceable object should store.
 * @modelChanged: called whenever the underlying model is changed.
 *
 * The different routines common to objects implementing a #VisuSourceable interface.
 * 
 * Since: 3.8
 */
struct _VisuSourceableInterface
{
  GTypeInterface parent;

  VisuSourceableData** (*getSource)(VisuSourceable *self);
  void (*modelChanged)(VisuSourceable *self);
};

GType visu_sourceable_get_type(void);

void visu_sourceable_init(VisuSourceable *self);
void visu_sourceable_dispose(VisuSourceable *self);

gboolean visu_sourceable_setSource(VisuSourceable *self, const gchar *source);
const gchar* visu_sourceable_getSource(const VisuSourceable *self);

gboolean visu_sourceable_setNodeModel(VisuSourceable *self,
                                      VisuNodeValues *model);
VisuNodeValues* visu_sourceable_getNodeModel(VisuSourceable *self);
const VisuNodeValues* visu_sourceable_getConstNodeModel(const VisuSourceable *self);

gboolean visu_sourceable_follow(VisuSourceable *self, VisuData *data);

G_END_DECLS

#endif
