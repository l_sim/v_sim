/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeur : Damien
	CALISTE, laboratoire L_Sim, (2015)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributor : Damien
	CALISTE, laboratoire L_Sim, (2015)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef NEIGHBOURS_H
#define NEIGHBOURS_H

#include <glib.h>
#include <glib-object.h>

#include <visu_data.h>
#include <pairsModeling/link.h>
#include "surfaces.h"

/**
 * VISU_TYPE_NODE_NEIGHBOURS:
 *
 * return the type of #VisuNodeNeighbours.
 */
#define VISU_TYPE_NODE_NEIGHBOURS	     (visu_node_neighbours_get_type ())
/**
 * VISU_NODE_NEIGHBOURS:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeNeighbours type.
 */
#define VISU_NODE_NEIGHBOURS(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_NEIGHBOURS, VisuNodeNeighbours))
/**
 * VISU_NODE_NEIGHBOURS_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeNeighboursClass.
 */
#define VISU_NODE_NEIGHBOURS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_NEIGHBOURS, VisuNodeNeighboursClass))
/**
 * VISU_IS_NODE_NEIGHBOURS:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeNeighbours object.
 */
#define VISU_IS_NODE_NEIGHBOURS(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_NEIGHBOURS))
/**
 * VISU_IS_NODE_NEIGHBOURS_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeNeighboursClass class.
 */
#define VISU_IS_NODE_NEIGHBOURS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_NEIGHBOURS))
/**
 * VISU_NODE_NEIGHBOURS_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_NODE_NEIGHBOURS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_NEIGHBOURS, VisuNodeNeighboursClass))

/**
 * VisuNodeNeighboursPrivate:
 * 
 * Private data for #VisuNodeNeighbours objects.
 */
typedef struct _VisuNodeNeighboursPrivate VisuNodeNeighboursPrivate;

/**
 * VisuNodeNeighbours:
 * 
 * Common name to refer to a #_VisuNodeNeighbours.
 */
typedef struct _VisuNodeNeighbours VisuNodeNeighbours;
struct _VisuNodeNeighbours
{
  VisuObject parent;

  VisuNodeNeighboursPrivate *priv;
};

/**
 * VisuNodeNeighboursClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeNeighboursClass.
 */
typedef struct _VisuNodeNeighboursClass VisuNodeNeighboursClass;
struct _VisuNodeNeighboursClass
{
  VisuObjectClass parent;
};

/**
 * visu_node_neighbours_get_type:
 *
 * This method returns the type of #VisuNodeNeighbours, use
 * VISU_TYPE_NODE_NEIGHBOURS instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeNeighbours.
 */
GType visu_node_neighbours_get_type(void);

VisuNodeNeighbours* visu_node_neighbours_new(VisuData *data);
VisuNodeNeighbours* visu_node_neighbours_new_forLink(VisuData *data, VisuPairLink *link);

void visu_node_neighbours_addFilter(VisuNodeNeighbours *nei, const VisuElement *pair[2]);

gboolean visu_node_neighbours_getSurfaceAt(VisuNodeNeighbours *nei, guint nodeId,
                                           VisuSurfaceDefinition *def);
gboolean visu_node_neighbours_getSurfaceFrom(VisuNodeNeighbours *nei, guint nodeId,
                                             VisuSurfaceDefinition *def);

/**
 * VisuNodeNeighboursIter:
 * @index: current neighbour index.
 * @nnei: number of neighbours.
 * @neiId: node id of the current neighbour.
 * @dxyz: (array fixed-size=3): the vector, from origin to neighbour.
 * @nei: parent #VisuNodeNeighbours object.
 * @nodeId: node id of the current node.
 *
 * Iterator structure used to span over the neighbours of @nodeId.
 *
 * Since: 3.8
 */
typedef struct _VisuNodeNeighboursIter VisuNodeNeighboursIter;
struct _VisuNodeNeighboursIter
{
  guint index, nnei, neiId;
  gfloat dxyz[3];

  /* Private */
  VisuNodeNeighbours *nei;
  guint nodeId;
};

gboolean visu_node_neighbours_iter(VisuNodeNeighbours *nei,
                                   VisuNodeNeighboursIter *iter, guint nodeId);
void visu_node_neighbours_iter___iter__(const VisuNodeNeighboursIter *iter,
                                        VisuNodeNeighboursIter *out);
gboolean visu_node_neighbours_iter_next(VisuNodeNeighboursIter *iter);

#endif
