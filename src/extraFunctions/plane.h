/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_PLANE_H
#define VISU_PLANE_H

#include <visu_box.h>
#include <visu_data.h>
#include <coreTools/toolColor.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_PLANE:
 *
 * return the type of #VisuPlane.
 */
#define VISU_TYPE_PLANE	     (visu_plane_get_type ())
/**
 * VISU_PLANE:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPlane type.
 */
#define VISU_PLANE(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PLANE, VisuPlane))
/**
 * VISU_PLANE_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPlaneClass.
 */
#define VISU_PLANE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PLANE, VisuPlaneClass))
/**
 * VISU_IS_PLANE:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPlane object.
 */
#define VISU_IS_PLANE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PLANE))
/**
 * VISU_IS_PLANE_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPlaneClass class.
 */
#define VISU_IS_PLANE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PLANE))
/**
 * VISU_PLANE_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_PLANE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PLANE, VisuPlaneClass))

typedef struct _VisuPlaneClass VisuPlaneClass;
typedef struct _VisuPlane VisuPlane;

GType visu_plane_get_type(void);

/**
 * VisuPlane:
 *
 * All fields are private, use the access routines.
 */

/**
 * VisuPlaneClass:
 * @parent: the parent.
 *
 * An opaque structure.
 */
struct _VisuPlaneClass
{
  VisuObjectClass parent;
};

/**
 * VISU_PLANE_SIDE_PLUS
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, the side is the one pointed by the normal vector.
 */
#define VISU_PLANE_SIDE_PLUS +1
/**
 * VISU_PLANE_SIDE_MINUS
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, the side is the one at the opposite of the one pointed
 * by the normal vector.
 */
#define VISU_PLANE_SIDE_MINUS -1
/**
 * VISU_PLANE_SIDE_NONE
 *
 * This is a key that defines which side is hidden by the plane. For
 * this value, no node is hidden.
 */
#define VISU_PLANE_SIDE_NONE 0
VisuPlane* visu_plane_new(VisuBox *box, float vect[3], float dist,
                          const ToolColor *color);
VisuPlane* visu_plane_newUndefined(void);
gboolean visu_plane_setNormalVector(VisuPlane *plane, float vect[3]);
/**
 * visu_plane_setDistanceFromOrigin:
 * @plane: a #VisuPlane object ;
 * @dist: the distance between origin and intersection of the plane and the
 * line made by origin and normal vector.
 *
 * Change the position of the plane.
 *
 * Returns: 1 if the intersections should be recalculated by
 *          a call to planeComputeInter(), 0 if not. Or -1 if there is
 *          an error.
 */
gboolean visu_plane_setDistanceFromOrigin(VisuPlane *plane, float dist);
gboolean visu_plane_setOrigin(VisuPlane *plane, const float origin[3]);
gboolean visu_plane_setColor(VisuPlane *plane, const ToolColor *color);

void visu_plane_getBasis(const VisuPlane *plane, float xyz[2][3], float center[3]);
GList* visu_plane_getIntersection(const VisuPlane *plane);
void visu_plane_getCenter(const VisuPlane *plane, float center[3]);
float* visu_plane_getReducedIntersection(const VisuPlane *plane, guint *nVals);
/**
 * visu_plane_getNVectUser:
 * @plane: a #VisuPlane.
 * @vect: an already alloacted (size 3) float array.
 *
 * Stores the coordinates of the normal vector in @vec of the plane. It returns the values
 * given by the user, not the normalized vaues.
 */
void visu_plane_getNVectUser(const VisuPlane *plane, float vect[3]);
/**
 * visu_plane_getNVect:
 * @plane: a #VisuPlane.
 * @vect: an already alloacted (size 3) float array.
 *
 * Stores the coordinates of the normal vector in @vect of the plane.
 * It returns the normalized values.
 */
void visu_plane_getNVect(const VisuPlane *plane, float vect[3]);
gfloat visu_plane_getDistanceFromOrigin(const VisuPlane *plane);
gboolean visu_plane_getLineIntersection(const VisuPlane *plane, const float A[3],
                                        const float B[3], float *lambda);
gboolean visu_plane_getPlaneIntersection(const VisuPlane *plane1, const VisuPlane *plane2,
                                         float A[3], float B[3]);
const ToolColor* visu_plane_getColor(VisuPlane *plane);
gboolean visu_plane_setHiddenState(VisuPlane *plane, int side);
/**
 * visu_plane_getHiddenState:
 * @plane: a #VisuPlane.
 *
 * The plane can hide the nodes on one of its side. this
 * method get the status for the given @plane.
 * 
 * Returns: the state, defined by VISU_PLANE_SIDE_PLUS or VISU_PLANE_SIDE_MINUS or
 * VISU_PLANE_SIDE_NONE.
 */
int visu_plane_getHiddenState(const VisuPlane *plane);
gboolean visu_plane_setRendered(VisuPlane *plane, gboolean rendered);
gboolean visu_plane_getRendered(const VisuPlane *plane);
gboolean visu_plane_getVisibility(const VisuPlane *plane, float point[3]);

gfloat visu_plane_getOpacity(const VisuPlane *plane);
gboolean visu_plane_setOpacity(VisuPlane *plane, gfloat opacity);

/* No object method. */
/**
 * visu_plane_class_getOrderedIntersections:
 * @nVisuPlanes: the number of planes (must be consistent with the number of
 *           planes in listOfVisuPlanes!)
 * @listOfVisuPlanes: an array of #VisuPlane, NULL terminated ;
 * @pointA: three cartesian coordinates.
 * @pointB: three cartesian coordinates.
 * @inter: a pointer to the location to store the intersection points.
 *         Supposing you know the number of intersection points !
 * @index: a pointer to the location to store the indices of ordering
 *         of the planes.
 *
 * Compute the location of the intersection points of segment AB with list of
 * planes @listOfVisuPlanes. If there are several intersections, they are ordered
 * by the proximity to point A.
 *
 * Returns: TRUE if the intersections are found.
 */
gboolean visu_plane_class_getOrderedIntersections(int nVisuPlanes, VisuPlane **listOfVisuPlanes,
                                                  float pointA[3], float pointB[3], float *inter, int *index);

G_END_DECLS


#endif
