/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2018)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2018)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef POLEPROP_H
#define POLEPROP_H

#include <glib.h>
#include <glib-object.h>

#include "floatProp.h"

G_BEGIN_DECLS

#define VISU_TYPE_NODE_VALUES_POLE	      (visu_node_values_pole_get_type ())
#define VISU_NODE_VALUES_POLE(obj)	      (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_VALUES_POLE, VisuNodeValuesPole))
#define VISU_NODE_VALUES_POLE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_VALUES_POLE, VisuNodeValuesPoleClass))
#define VISU_IS_NODE_VALUES_POLE_TYPE(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_VALUES_POLE))
#define VISU_IS_NODE_VALUES_POLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_VALUES_POLE))
#define VISU_NODE_VALUES_POLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_VALUES_POLE, VisuNodeValuesPoleClass))

/**
 * VisuNodeValuesPole:
 * 
 * Common name to refer to a #_VisuNodeValuesPole.
 */
typedef struct _VisuNodeValuesPole VisuNodeValuesPole;
struct _VisuNodeValuesPole
{
  VisuNodeValuesFarray parent;
};

/**
 * VisuNodeValuesPoleClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuNodeValuesPoleClass.
 */
typedef struct _VisuNodeValuesPoleClass VisuNodeValuesPoleClass;
struct _VisuNodeValuesPoleClass
{
  VisuNodeValuesFarrayClass parent;
};

/**
 * visu_node_values_pole_get_type:
 *
 * This method returns the type of #VisuNodeValuesPole, use
 * VISU_TYPE_NODE_VALUES_POLE instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeValuesPole.
 */
GType visu_node_values_pole_get_type(void);

/**
 * VisuPoleOrder:
 * @VISU_MONOPOLE: a mono-pole.
 * @VISU_DIPOLE: a di-pole.
 * @VISU_QUADRUPOLE: a quadru-pole.
 *
 * Various pole orders.
 *
 * Since: 3.8
 */
typedef enum {
  VISU_MONOPOLE,
  VISU_DIPOLE,
  VISU_QUADRUPOLE,
} VisuPoleOrder;

VisuNodeValuesPole* visu_node_values_pole_new(VisuNodeArray *arr,
                                              const gchar *label);

const gfloat* visu_node_values_pole_getAt(const VisuNodeValuesPole *pole,
                                          const VisuNode *node,
                                          VisuPoleOrder order);

gboolean visu_node_values_pole_setMonoAt(VisuNodeValuesPole *pole,
                                         const VisuNode *node,
                                         gfloat val);
gboolean visu_node_values_pole_setDiAt(VisuNodeValuesPole *pole,
                                       const VisuNode *node,
                                       const gfloat val[3]);
gboolean visu_node_values_pole_setQuadAt(VisuNodeValuesPole *pole,
                                         const VisuNode *node,
                                         const gfloat val[5]);
gboolean visu_node_values_pole_setMonoAtDbl(VisuNodeValuesPole *pole,
                                            const VisuNode *node,
                                            gdouble val);
gboolean visu_node_values_pole_setDiAtDbl(VisuNodeValuesPole *pole,
                                          const VisuNode *node,
                                          const gdouble val[3]);
gboolean visu_node_values_pole_setQuadAtDbl(VisuNodeValuesPole *pole,
                                            const VisuNode *node,
                                            const gdouble val[5]);

G_END_DECLS

#endif
