/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at COPYING.
*/
#ifndef ROTATE_H
#define ROTATE_H

#include "mover.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_NODE_MOVER_ROTATION:
 *
 * return the type of #VisuNodeMoverRotation.
 *
 * Since: 3.8
 */
#define VISU_TYPE_NODE_MOVER_ROTATION	     (visu_node_mover_rotation_get_type ())
/**
 * VISU_NODE_MOVER_ROTATION:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuNodeMoverRotation type.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER_ROTATION(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_NODE_MOVER_ROTATION, VisuNodeMoverRotation))
/**
 * VISU_NODE_MOVER_ROTATION_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuNodeMoverRotationClass.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER_ROTATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_NODE_MOVER_ROTATION, VisuNodeMoverRotationClass))
/**
 * VISU_IS_NODE_MOVER_ROTATION:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuNodeMoverRotation object.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_MOVER_ROTATION(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_NODE_MOVER_ROTATION))
/**
 * VISU_IS_NODE_MOVER_ROTATION_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuNodeMoverRotationClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_NODE_MOVER_ROTATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_NODE_MOVER_ROTATION))
/**
 * VISU_NODE_MOVER_ROTATION_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_NODE_MOVER_ROTATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_NODE_MOVER_ROTATION, VisuNodeMoverRotationClass))

typedef struct _VisuNodeMoverRotation        VisuNodeMoverRotation;
typedef struct _VisuNodeMoverRotationPrivate VisuNodeMoverRotationPrivate;
typedef struct _VisuNodeMoverRotationClass   VisuNodeMoverRotationClass;

struct _VisuNodeMoverRotation
{
  VisuNodeMover parent;

  VisuNodeMoverRotationPrivate *priv;
};

struct _VisuNodeMoverRotationClass
{
  VisuNodeMoverClass parent;
};

/**
 * visu_node_mover_rotation_get_type:
 *
 * This method returns the type of #VisuNodeMoverRotation, use
 * VISU_TYPE_NODE_MOVER_ROTATION instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuNodeMoverRotation.
 */
GType visu_node_mover_rotation_get_type(void);

VisuNodeMoverRotation* visu_node_mover_rotation_new();
VisuNodeMoverRotation* visu_node_mover_rotation_new_full(const GArray *ids,
                                                         const gfloat axis[3],
                                                         const gfloat center[3],
                                                         gfloat angle);

gboolean visu_node_mover_rotation_setAngle(VisuNodeMoverRotation *rot, gfloat angle);
gboolean visu_node_mover_rotation_setCenter(VisuNodeMoverRotation *rot,
                                            const gfloat center[3]);
gboolean visu_node_mover_rotation_setAxis(VisuNodeMoverRotation *rot,
                                          const gfloat axis[3]);

gfloat visu_node_mover_rotation_getAngle(const VisuNodeMoverRotation *rot);
void visu_node_mover_rotation_getCenter(const VisuNodeMoverRotation *rot,
                                        gfloat center[3]);
void visu_node_mover_rotation_getAxis(const VisuNodeMoverRotation *rot,
                                      gfloat axis[3]);

G_END_DECLS

#endif
