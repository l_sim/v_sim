/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef SCALARFIELDS_H
#define SCALARFIELDS_H

#include <glib.h>
#include <gio/gio.h>

#include <iface_boxed.h>
#include <iface_pointset.h>
#include <coreTools/toolFileFormat.h>
#include <coreTools/toolOptions.h>
#include <coreTools/toolMatrix.h>

G_BEGIN_DECLS

/**
 * VisuScalarFieldMeshFlags:
 * @VISU_SCALAR_FIELD_MESH_UNIFORM: the mesh has constant divisions along x, y and z axis ;
 * @VISU_SCALAR_FIELD_MESH_NON_UNIFORM: the mesh has non linear divisions along x, y or z axis.
 *
 * flag (comment) standing at the begining of a Scalar field file,
 * that gives informations concerning the mesh.
 */
typedef enum
  {
    VISU_SCALAR_FIELD_MESH_UNIFORM,
    VISU_SCALAR_FIELD_MESH_NON_UNIFORM
  } VisuScalarFieldMeshFlags;

/**
 * VISU_TYPE_SCALAR_FIELD:
 *
 * return the type of #VisuScalarField.
 */
#define VISU_TYPE_SCALAR_FIELD	     (visu_scalar_field_get_type ())
/**
 * SCALAR_FIELD:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuScalarField type.
 */
#define VISU_SCALAR_FIELD(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_SCALAR_FIELD, VisuScalarField))
/**
 * VISU_SCALAR_FIELD_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuScalarFieldClass.
 */
#define VISU_SCALAR_FIELD_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_SCALAR_FIELD, VisuScalarFieldClass))
/**
 * VISU_IS_SCALAR_FIELD:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuScalarField object.
 */
#define VISU_IS_SCALAR_FIELD(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_SCALAR_FIELD))
/**
 * VISU_IS_SCALAR_FIELD_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuScalarFieldClass class.
 */
#define VISU_IS_SCALAR_FIELD_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_SCALAR_FIELD))
/**
 * VISU_SCALAR_FIELD_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_SCALAR_FIELD_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_SCALAR_FIELD, VisuScalarFieldClass))

typedef struct _VisuScalarField        VisuScalarField;
typedef struct _VisuScalarFieldPrivate VisuScalarFieldPrivate;
typedef struct _VisuScalarFieldClass   VisuScalarFieldClass;

/**
 * VisuScalarField:
 *
 * An opaque structure for the scalar field.
 */
struct _VisuScalarField
{
  VisuObject parent;

  VisuScalarFieldPrivate *priv;
};

/**
 * VisuScalarFieldClass:
 * @parent: the parent class.
 * @isEmpty: a method returning %TRUE if the scalar field has
 * no data yet.
 * @setGridSize: a method to define the grid size.
 * @getGridSize: a method returning the three grid dimensions.
 * @getAt: a method to get the value on a grid point.
 * @getValue: a method to get an interpolated value in space.
 * @getMinMax: a method to get the minimum and maximum value reached
 * by the scalar field.
 *
 * An opaque structure for the class.
 */
struct _VisuScalarFieldClass
{
  VisuObjectClass parent;

  gboolean (*isEmpty)(const VisuScalarField *field);
  gboolean (*setGridSize)(VisuScalarField *field, const guint grid[3]);
  void (*getGridSize)(const VisuScalarField *field, guint grid[3]);
  double (*getAt)(const VisuScalarField *field, int i, int j, int k);
  gboolean (*getValue)(const VisuScalarField *field, const float xyz[3],
                       double *value, const float extension[3]);
  void (*getMinMax)(const VisuScalarField *field, double minmax[2]);
};

/**
 * visu_scalar_field_get_type:
 *
 * This method returns the type of #VisuScalarField, use VISU_TYPE_SCALAR_FIELD instead.
 *
 * Returns: the type of #VisuScalarField.
 */
GType visu_scalar_field_get_type(void);

/**
 * VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE:
 *
 * Flag used to registered a gboolean property in a #VisuData object.
 * If this flag is TRUE, the file used to read the structure can be used to
 * read a density or a potential.
 */
#define VISU_SCALAR_FIELD_DEFINED_IN_STRUCT_FILE "fileFormat_hasPotentialOrDensity"

VisuScalarField* visu_scalar_field_new(const gchar *label);

gboolean visu_scalar_field_isEmpty(const VisuScalarField *field);

void visu_scalar_field_setCommentary(VisuScalarField *field, const gchar* comment);

void visu_scalar_field_getGridSize(const VisuScalarField *field, guint grid[3]);
gboolean visu_scalar_field_setGridSize(VisuScalarField *field, const guint grid[3]);
void visu_scalar_field_setMeshtype(VisuScalarField *field, VisuScalarFieldMeshFlags meshtype);

void visu_scalar_field_getMinMax(const VisuScalarField *field, double minmax[2]);
VisuScalarFieldMeshFlags visu_scalar_field_getMeshtype(const VisuScalarField *field);
double* visu_scalar_field_getMesh(const VisuScalarField *field, ToolXyzDir dir);
void visu_scalar_field_setMesh(VisuScalarField *field, const double *mesh, ToolXyzDir dir);
const gchar* visu_scalar_field_getLabel(VisuScalarField *field);
const gchar* visu_scalar_field_getCommentary(VisuScalarField *field);

void visu_scalar_field_getMeshInside(const VisuScalarField *field, guint grid[3],
                                     int i, int j, int k);
gboolean visu_scalar_field_getCoordInside(const VisuScalarField *field,
                                          guint grid[3], guint dgrid[3], gfloat factor[3],
                                          const gfloat xyz[3], const gfloat extension[3]);
double visu_scalar_field_getAt(const VisuScalarField *field, int i, int j, int k);
double visu_scalar_field_getGradAt(const VisuScalarField *field, int i, int j, int k, ToolXyzDir dir);
gboolean visu_scalar_field_getValue(const VisuScalarField *field, const float xyz[3],
                                    double *value, const float extension[3]);

void visu_scalar_field_addOption(VisuScalarField *field, ToolOption *option);
GList* visu_scalar_field_getAllOptions(VisuScalarField *field);

G_END_DECLS

#endif
