/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2019)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2019)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef SHADEDCOLORIZER_H
#define SHADEDCOLORIZER_H

#include <glib.h>
#include <glib-object.h>

#include "colorizer.h"
#include "floatProp.h"
#include <coreTools/toolShade.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_COLORIZER_SHADED:
 *
 * return the type of #VisuDataColorizerShaded.
 */
#define VISU_TYPE_DATA_COLORIZER_SHADED	     (visu_data_colorizer_shaded_get_type ())
/**
 * VISU_DATA_COLORIZER_SHADED:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuDataColorizerShaded type.
 */
#define VISU_DATA_COLORIZER_SHADED(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA_COLORIZER_SHADED, VisuDataColorizerShaded))
/**
 * VISU_DATA_COLORIZER_SHADED_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataColorizerShadedClass.
 */
#define VISU_DATA_COLORIZER_SHADED_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA_COLORIZER_SHADED, VisuDataColorizerShadedClass))
/**
 * VISU_IS_DATA_COLORIZER_SHADED:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuDataColorizerShaded object.
 */
#define VISU_IS_DATA_COLORIZER_SHADED(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA_COLORIZER_SHADED))
/**
 * VISU_IS_DATA_COLORIZER_SHADED_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataColorizerShadedClass class.
 */
#define VISU_IS_DATA_COLORIZER_SHADED_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA_COLORIZER_SHADED))
/**
 * VISU_DATA_COLORIZER_SHADED_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_COLORIZER_SHADED_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_COLORIZER_SHADED, VisuDataColorizerShadedClass))

/**
 * VisuDataColorizerShadedPrivate:
 * 
 * Private data for #VisuDataColorizerShaded objects.
 */
typedef struct _VisuDataColorizerShadedPrivate VisuDataColorizerShadedPrivate;

/**
 * VisuDataColorizerShaded:
 * 
 * Common name to refer to a #_VisuDataColorizerShaded.
 */
typedef struct _VisuDataColorizerShaded VisuDataColorizerShaded;
struct _VisuDataColorizerShaded
{
  VisuDataColorizer parent;

  VisuDataColorizerShadedPrivate *priv;
};

/**
 * VisuDataColorizerShadedClass:
 * @parent: its parent.
 * @toValues: a method that returns normalised values for @node.
 *
 * Interface for class that can represent #VisuDataColorizerShaded.
 *
 * Since: 3.8
 */
typedef struct _VisuDataColorizerShadedClass VisuDataColorizerShadedClass;
struct _VisuDataColorizerShadedClass
{
  VisuDataColorizerClass parent;

  gboolean (*toValues) (const VisuDataColorizerShaded *colorizer, gfloat values[3],
                        const VisuData *dataObj, const VisuNode *node);
};

/**
 * visu_data_colorizer_shaded_get_type:
 *
 * This method returns the type of #VisuDataColorizerShaded, use
 * VISU_TYPE_DATA_COLORIZER_SHADED instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuDataColorizerShaded.
 */
GType visu_data_colorizer_shaded_get_type(void);

gboolean visu_data_colorizer_shaded_setShade(VisuDataColorizerShaded *colorizer, const ToolShade *shade);
const ToolShade* visu_data_colorizer_shaded_getShade(const VisuDataColorizerShaded *colorizer);

G_END_DECLS

#endif
