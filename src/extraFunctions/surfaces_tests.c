/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "surfaces.h"

#define BUFFERSIZE 2
#define NB_SURFS   2
#define NB_POLYS  10
#define NB_POINTS  5


/* Various tests for the surface routines. */
static gboolean test_visu_surface_points_init(VisuSurfacePoints *points);
static gboolean test_visu_surface_points_allocate_0(VisuSurfacePoints *points);
static gboolean test_visu_surface_points_allocate(VisuSurfacePoints *points);
static gboolean test_visu_surface_points_remove_1(VisuSurfacePoints *points);
static gboolean test_visu_surface_points_remove_all(VisuSurfacePoints *points);
static void local_populate(VisuSurfacePoints *points);
static gboolean test_visu_surface_new(VisuSurface **surf);

int main(int argc, char** argv)
{
  VisuSurfacePoints points;
  VisuSurface *surf;

  /* To be removed later, needed for the link. */
  getGeneralPaths();

  /* Test the VisuSurfacePoints structure. */
  visu_surface_points_init(&points, BUFFERSIZE);
  g_return_val_if_fail(test_visu_surface_points_init(&points), 1);

  /* Try to allocate a points with zero points. */
  visu_surface_points_allocate(&points, NB_SURFS + 1, 0, 0);
  g_return_val_if_fail(test_visu_surface_points_allocate_0(&points), 1);
  visu_surface_points_free(&points);
  /* We try to allocate with some points this time. */
  visu_surface_points_allocate(&points, NB_SURFS + 1, NB_POLYS, NB_POINTS);
  g_return_val_if_fail(test_visu_surface_points_allocate(&points), 1);

  /* We populate the surfaces. */
  local_populate(&points);

  /* We try to remove the second surface. */
  g_message("IsosurfacesPoints: remove one surface.");
  visu_surface_points_remove(&points, 1);
  g_return_val_if_fail(test_visu_surface_points_remove_1(&points), 1);

  /* We try to remove the last two surfaces. */
  g_message("IsosurfacesPoints: remove all remaining surfaces.");
  visu_surface_points_remove(&points, 1);
  visu_surface_points_remove(&points, 0);
  g_return_val_if_fail(test_visu_surface_points_remove_all(&points), 1);
  visu_surface_points_free(&points);

  /* Test the VisuSurface structure. */
  g_return_val_if_fail(test_visu_surface_new(&surf), 1);

  return 0;
}

static gboolean test_visu_surface_points_init(VisuSurfacePoints *points)
{
  /* We check all the scalars. */
  g_return_val_if_fail(points->nsurf      == 0, FALSE);
  g_return_val_if_fail(points->bufferSize == BUFFERSIZE, FALSE);
  g_return_val_if_fail(points->num_polys  == 0, FALSE);
  g_return_val_if_fail(points->num_points == 0, FALSE);

  /* We check that all the pointers are null. */
  g_return_val_if_fail(!points->num_polys_surf, FALSE);
  g_return_val_if_fail(!points->poly_surf_index, FALSE);
  g_return_val_if_fail(!points->poly_num_vertices, FALSE);
  g_return_val_if_fail(!points->poly_vertices, FALSE);
  g_return_val_if_fail(!points->poly_points, FALSE);
  g_return_val_if_fail(!points->poly_normals, FALSE);

  return TRUE;
}

static gboolean test_visu_surface_points_allocate_0(VisuSurfacePoints *points)
{
  /* We check the allocated arrays. */
  g_return_val_if_fail(points->num_polys_surf, FALSE);
  g_return_val_if_fail(points->num_polys_surf[0] == 0 &&
		       points->num_polys_surf[1] == 0 &&
		       points->num_polys_surf[2] == 0, FALSE);

  /* We check that all other pointers are null. */
  g_return_val_if_fail(!points->poly_surf_index, FALSE);
  g_return_val_if_fail(!points->poly_num_vertices, FALSE);
  g_return_val_if_fail(!points->poly_vertices, FALSE);
  g_return_val_if_fail(!points->poly_points, FALSE);
  g_return_val_if_fail(!points->poly_normals, FALSE);

  return TRUE;
}
static gboolean test_visu_surface_points_allocate(VisuSurfacePoints *points)
{
  /* We check the allocated arrays. */
  g_return_val_if_fail(points->num_polys_surf, FALSE);
  g_return_val_if_fail(points->num_polys_surf[0] == 0 &&
		       points->num_polys_surf[1] == 0 &&
		       points->num_polys_surf[2] == 0, FALSE);

  /* We check that all other pointers are allocated. */
  g_return_val_if_fail(points->poly_surf_index, FALSE);
  g_return_val_if_fail(points->poly_num_vertices, FALSE);
  g_return_val_if_fail(points->poly_vertices, FALSE);
  g_return_val_if_fail(points->poly_points, FALSE);
  g_return_val_if_fail(points->poly_normals, FALSE);
  
  return TRUE;
}

static void local_populate(VisuSurfacePoints *points)
{
  int i, j;

  points->num_polys_surf[0] = NB_POLYS / 2 - 2;
  points->num_polys_surf[1] = NB_POLYS / 2 - 1;
  points->num_polys_surf[2] = 3;

  for (i = 0; i < NB_POLYS / 2 - 2; i++)
    points->poly_surf_index[i] = 1;
  for (i = NB_POLYS / 2 - 2; i < NB_POLYS - 3; i++)
    points->poly_surf_index[i] = 2;
  for (i = NB_POLYS - 3; i < NB_POLYS; i++)
    points->poly_surf_index[i] = 3;

  for (i = 0; i < NB_POLYS / 2 + 1; i++)
    points->poly_num_vertices[i] = 4;
  for (i = NB_POLYS / 2 + 1; i < NB_POLYS; i++)
    points->poly_num_vertices[i] = 3;

  for (i = 0; i < NB_POLYS / 2 + 1; i++)
    {
      points->poly_vertices[i] = g_malloc(sizeof(int) * 4);
      for (j = 0; j < 4; j++)
	points->poly_vertices[i][j] = (i + j) % 5;
    }
  for (i = NB_POLYS / 2 + 1; i < NB_POLYS; i++)
    {
      points->poly_vertices[i] = g_malloc(sizeof(int) * 3);
      for (j = 0; j < 3; j++)
	points->poly_vertices[i][j] = (i - j) % 5;
    }

  for (i = 0; i < NB_POINTS; i++)
    for (j = 0; j < 3 + BUFFERSIZE; j++)
      points->poly_points[i][j] = i + j;

  for (i = 0; i < NB_POINTS; i++)
    for (j = 0; j < 3; j++)
      points->poly_normals[i][j] = i + j;
}

static gboolean test_visu_surface_points_remove_1(VisuSurfacePoints *points)
{
  gboolean valid;
  int i, j;

  /* We check the scalar values. */
  g_message(" | check scalar values.");
  g_return_val_if_fail(points->nsurf == NB_SURFS, FALSE);
  g_return_val_if_fail(points->bufferSize == BUFFERSIZE, FALSE);
  g_return_val_if_fail(points->num_polys == NB_POLYS / 2 + 1, FALSE);
  g_return_val_if_fail(points->num_points == NB_POINTS, FALSE);

  /* We check 1D arrays. */
  g_message(" | check 1D arrays.");
  g_return_val_if_fail(points->num_polys_surf[0] == NB_POLYS / 2 - 2 &&
		       points->num_polys_surf[1] == 3, FALSE);
  g_return_val_if_fail(points->poly_surf_index[0] == 1 &&
		       points->poly_surf_index[1] == 1 &&
		       points->poly_surf_index[2] == 1 &&
		       points->poly_surf_index[3] == 2 &&
		       points->poly_surf_index[4] == 2 &&
		       points->poly_surf_index[5] == 2, FALSE);
  g_return_val_if_fail(points->poly_num_vertices[0] == 4 &&
		       points->poly_num_vertices[1] == 4 &&
		       points->poly_num_vertices[2] == 4 &&
		       points->poly_num_vertices[3] == 3 &&
		       points->poly_num_vertices[4] == 3 &&
		       points->poly_num_vertices[5] == 3, FALSE);

  /* We check 2D arrays. */
  valid = TRUE;
  for (i = 0; i < NB_POLYS / 2 - 2; i++)
    for (j = 0; j < 4; j++)
      valid = valid && (points->poly_vertices[i][j] == (i + j) % 5);
  for (i = NB_POLYS / 2 - 2; i < NB_POLYS / 2 + 1; i++)
    for (j = 0; j < 3; j++)
      valid = valid && (points->poly_vertices[i][j] == (i - j - 1) % 5);
  g_message(" | check 2D arrays.");
  g_return_val_if_fail(valid, FALSE);

  /* We check the points and normal values. */
  valid = TRUE;
  for (i = 0; i < NB_POINTS; i++)
    for (j = 0; j < 3 + BUFFERSIZE; j++)
      valid = valid && (points->poly_points[i][j] == i + j);

  for (i = 0; i < NB_POINTS; i++)
    for (j = 0; j < 3; j++)
      valid = valid && (points->poly_normals[i][j] == i + j);
  g_message(" | check points and normals.");
  g_return_val_if_fail(valid, FALSE);

  return TRUE;
}
static gboolean test_visu_surface_points_remove_all(VisuSurfacePoints *points)
{
  /* We check the scalar values. */
  g_message(" | check scalar values.");
  g_return_val_if_fail(points->nsurf      == 0, FALSE);
  g_return_val_if_fail(points->bufferSize == BUFFERSIZE, FALSE);
  g_return_val_if_fail(points->num_polys  == 0, FALSE);
  g_return_val_if_fail(points->num_points == 0, FALSE);

  /* We check that all arrays have been nullified. */
  g_message(" | check null arrays.");
  g_return_val_if_fail(!points->poly_surf_index,   FALSE);
  g_return_val_if_fail(!points->poly_num_vertices, FALSE);
  g_return_val_if_fail(!points->poly_vertices,     FALSE);
  g_return_val_if_fail(!points->poly_points,       FALSE);
  g_return_val_if_fail(!points->poly_normals,      FALSE);

  return TRUE;
}

static gboolean test_visu_surface_new(VisuSurface **surf)
{
  *surf = visu_surface_new(BUFFERSIZE);

  g_return_val_if_fail(test_visu_surface_points_init(&(*surf)->basePoints), FALSE);
  g_return_val_if_fail(test_visu_surface_points_init(&(*surf)->volatilePlanes), FALSE);

  /* We check the null pointers. */
  g_return_val_if_fail(!(*surf)->ids, FALSE);
  g_return_val_if_fail(!(*surf)->resources, FALSE);
  
  /* We check the associated pointers. */
  g_return_val_if_fail((*surf)->properties, FALSE);
  
  return TRUE;
}
