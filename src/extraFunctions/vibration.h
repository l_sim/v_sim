/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mÃ¨l :
	BILLARD, non joignable par mÃ¨l ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant Ã  visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusÃ©e par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VIBRATION_H
#define VIBRATION_H

#include <extraFunctions/vectorProp.h>
#include <visu_data.h>

G_BEGIN_DECLS

/**
 * VISU_TYPE_VIBRATION:
 *
 * return the type of #VisuVibration.
 */
#define VISU_TYPE_VIBRATION	     (visu_vibration_get_type ())
/**
 * VISU_VIBRATION:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuVibration type.
 */
#define VISU_VIBRATION(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_VIBRATION, VisuVibration))
/**
 * VISU_VIBRATION_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuVibrationClass.
 */
#define VISU_VIBRATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_VIBRATION, VisuVibrationClass))
/**
 * VISU_IS_VIBRATION:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuVibration object.
 */
#define VISU_IS_VIBRATION(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_VIBRATION))
/**
 * VISU_IS_VIBRATION_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuVibrationClass class.
 */
#define VISU_IS_VIBRATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_VIBRATION))
/**
 * VISU_VIBRATION_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_VIBRATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_VIBRATION, VisuVibrationClass))

/**
 * VisuVibrationPrivate:
 * 
 * Private data for #VisuVibration objects.
 */
typedef struct _VisuVibrationPrivate VisuVibrationPrivate;

/**
 * VisuVibration:
 * 
 * Common name to refer to a #_VisuVibration.
 */
typedef struct _VisuVibration VisuVibration;
struct _VisuVibration
{
  VisuNodeValuesVector parent;

  VisuVibrationPrivate *priv;
};

/**
 * VisuVibrationClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuVibrationClass.
 */
typedef struct _VisuVibrationClass VisuVibrationClass;
struct _VisuVibrationClass
{
  VisuNodeValuesVectorClass parent;
};

/**
 * visu_vibration_get_type:
 *
 * This method returns the type of #VisuVibration, use
 * VISU_TYPE_VIBRATION instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuVibration.
 */
GType visu_vibration_get_type(void);

VisuVibration* visu_vibration_new(VisuData *data, const gchar *label, guint n);

gboolean visu_vibration_setDisplacements(VisuVibration *vib, guint iph,
                                         const GArray *vibes, gboolean complex);
gboolean visu_vibration_setCurrentMode(VisuVibration *vib,
                                       guint iph, GError **error);

gboolean visu_vibration_setCharacteristic(VisuVibration *vib, guint iph,
                                          const float q[3], float en, float omega);
gboolean visu_vibration_getCharacteristic(const VisuVibration *vib, guint iph,
                                          float q[3], float *en, float *omega);
gboolean visu_vibration_setUserFrequency(VisuVibration *vib, float freq);
gboolean visu_vibration_setAmplitude(VisuVibration *vib, float ampl);
gboolean visu_vibration_setTime(VisuVibration *vib, gfloat at);

void visu_vibration_resetPosition(VisuVibration *vib);
void visu_vibration_setZeroTime(VisuVibration *vib);

guint visu_vibration_getNPhonons(VisuVibration *vib);

void visu_vibration_animate(VisuVibration *vib);

VisuVibration* visu_data_getVibration(VisuData *dataObj, guint nModes);

G_END_DECLS

#endif
