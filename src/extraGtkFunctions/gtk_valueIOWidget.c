/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include <gtk/gtk.h>

#include "gtk_valueIOWidget.h"
#include <support.h>
#include <visu_tools.h>
#include <visu_gtk.h>
#include <gtk_main.h>

/**
 * SECTION:gtk_valueIOWidget
 * @short_description: Defines a specialised #GtkHBox with three
 * button to open, save and save as XML value files.
 *
 * <para></para>
 *
 * Since: 3.5
 */

enum
  {
    PROP_0,
    SAVE_SENSITIVE_PROP,
    N_PROP
  };
static GParamSpec *properties[N_PROP];

static void visu_ui_value_io_dispose (GObject *obj);
static void visu_ui_value_io_finalize(GObject *obj);
static void visu_ui_value_io_get_property(GObject* obj, guint property_id,
                                          GValue *value, GParamSpec *pspec);
static void visu_ui_value_io_set_property(GObject* obj, guint property_id,
                                          const GValue *value, GParamSpec *pspec);

/**
 * VisuUiValueIo:
 *
 * Private structure to store informations of a #VisuUiValueIo object.
 *
 * Since: 3.5
 */
struct _VisuUiValueIo
{
  GtkBox hbox;

  GtkWidget *btExportOpen, *btExportSave, *btExportSaveAs;
  GtkWindow *parent;

  VisuUiValueIoCallback ioOpen;
  gulong openSignalId;
  VisuUiValueIoCallback ioSave;
  gulong saveSignalId;
  gulong saveAsSignalId;

  /* Memory gestion. */
  gboolean dispose_has_run;
};
/**
 * VisuUiValueIoClass
 *
 * Private structure to store informations of a #VisuUiValueIoClass object.
 *
 * Since: 3.5
 */
struct _VisuUiValueIoClass
{
  GtkBoxClass parent_class;

  void (*valueio) (VisuUiValueIo *hbox);
};

/* Local callbacks. */
static void onImportXML(GtkFileChooserButton *filechooserbutton, gpointer data);
static void onExportXML(GtkButton *button, gpointer data);

/* Local methods. */

/**
 * visu_ui_value_io_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuUiValueIo objects.
 *
 * Since: 3.5
 */
G_DEFINE_TYPE(VisuUiValueIo, visu_ui_value_io, GTK_TYPE_BOX)

static void visu_ui_value_io_class_init(VisuUiValueIoClass *klass)
{
  g_debug("Gtk VisuUiValueIo: creating the class of the widget.");
  
  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_value_io_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_value_io_finalize;
  G_OBJECT_CLASS(klass)->set_property = visu_ui_value_io_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_ui_value_io_get_property;

  /**
   * VisuUiValueIo::sensitive-save:
   *
   * The save and save-as buttons sensitivity.
   *
   * Since: 3.8
   */
  properties[SAVE_SENSITIVE_PROP] = g_param_spec_boolean("sensitive-save", "Sensitive save",
                                                         "sensitivity of save buttons",
                                                         FALSE, G_PARAM_READWRITE |
                                                         G_PARAM_STATIC_STRINGS);
  g_object_class_install_property(G_OBJECT_CLASS(klass), SAVE_SENSITIVE_PROP,
				  properties[SAVE_SENSITIVE_PROP]);
}

static void visu_ui_value_io_dispose(GObject *obj)
{
  g_debug("Gtk VisuUiValueIo: dispose object %p.", (gpointer)obj);

  if (VISU_UI_VALUE_IO(obj)->dispose_has_run)
    return;

  VISU_UI_VALUE_IO(obj)->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_value_io_parent_class)->dispose(obj);
}
static void visu_ui_value_io_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  g_debug("Gtk VisuUiValueIo: finalize object %p.", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_value_io_parent_class)->finalize(obj);

  g_debug(" | freeing ... OK.");
}
static void visu_ui_value_io_get_property(GObject* obj, guint property_id,
                                          GValue *value, GParamSpec *pspec)
{
  VisuUiValueIo *self = VISU_UI_VALUE_IO(obj);

  g_debug("Gtk VisuUiValueIo: get property '%s'.",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SAVE_SENSITIVE_PROP:
      g_value_set_boolean(value, gtk_widget_get_sensitive(self->btExportSaveAs));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_ui_value_io_set_property(GObject* obj, guint property_id,
                                          const GValue *value, GParamSpec *pspec)
{
  VisuUiValueIo *self = VISU_UI_VALUE_IO(obj);

  g_debug("Gtk VisuUiValueIo: set property '%s'.",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case SAVE_SENSITIVE_PROP:
      visu_ui_value_io_setSensitiveSave(self, g_value_get_boolean(value));
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}


static void visu_ui_value_io_init(VisuUiValueIo *valueio)
{
  g_debug("Gtk VisuUiValueIo: initializing new object (%p).",
	      (gpointer)valueio);

  valueio->ioOpen         = (VisuUiValueIoCallback)0;
  valueio->openSignalId   = 0;
  valueio->ioSave         = (VisuUiValueIoCallback)0;
  valueio->saveSignalId   = 0;
  valueio->saveAsSignalId = 0;
}

static void buildWidgets(VisuUiValueIo *valueio, GtkWindow *parent,
			 const gchar*tipsOpen, const gchar*tipsSave,
			 const gchar*tipsSaveAs)
{
  GtkFileFilter *filter1, *filter2;
  const gchar *directory;

  gtk_box_set_spacing(GTK_BOX(valueio), 2);
  
  valueio->parent = parent;

  /* The save as button. */
  valueio->btExportSaveAs = gtk_button_new();
  gtk_widget_set_tooltip_text(valueio->btExportSaveAs, tipsSaveAs);
  gtk_widget_set_sensitive(valueio->btExportSaveAs, FALSE);
  gtk_container_add(GTK_CONTAINER(valueio->btExportSaveAs),
		    gtk_image_new_from_icon_name("document-save-as", GTK_ICON_SIZE_MENU));
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportSaveAs, FALSE, FALSE, 0);

  /* The save button. */
  valueio->btExportSave = gtk_button_new();
  gtk_widget_set_tooltip_text(valueio->btExportSave, tipsSave);
  gtk_widget_set_sensitive(valueio->btExportSave, FALSE);
  gtk_container_add(GTK_CONTAINER(valueio->btExportSave),
		    gtk_image_new_from_icon_name("document-save", GTK_ICON_SIZE_MENU));
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportSave, FALSE, FALSE, 0);

  /* The open button. */
  filter1 = gtk_file_filter_new();
  gtk_file_filter_set_name(filter1, _("V_Sim value file (*.xml)"));
  gtk_file_filter_add_pattern(filter1, "*.xml");
  filter2 = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter2, _("All files"));
  gtk_file_filter_add_pattern (filter2, "*");
  valueio->btExportOpen = gtk_file_chooser_button_new(_("Open a V_Sim value file"),
						      GTK_FILE_CHOOSER_ACTION_OPEN);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->btExportOpen), filter1);
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(valueio->btExportOpen), filter2);
  directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(valueio->btExportOpen),
					directory);
  gtk_widget_set_tooltip_text(valueio->btExportOpen, tipsOpen);
  gtk_widget_set_sensitive(valueio->btExportOpen, FALSE);
  gtk_box_pack_end(GTK_BOX(valueio), valueio->btExportOpen, TRUE, TRUE, 0);

  /* The label. */
  gtk_box_pack_end(GTK_BOX(valueio), gtk_label_new(_("I/O:")), FALSE, FALSE, 0);
}

/**
 * visu_ui_value_io_new:
 * @parent: the parent used to show the file dialog.
 * @tipsOpen: a tooltip to show on open button.
 * @tipsSave: a tooltip to show on save button.
 * @tipsSaveAs: a tooltip to show on save-as button.
 *
 * A #VisuUiValueIo widget is like a #GtkComboBox widget, but it is already filled
 * with predefined line patterns (call stipple). Using this widget is
 * a convienient way to share stipples between all part of V_Sim and
 * to give a consistent look of all stipple selection.
 *
 * Since: 3.5
 *
 * Returns: (transfer full): a newly created #VisuUiValueIo widget.
 */
GtkWidget* visu_ui_value_io_new(GtkWindow *parent, const gchar*tipsOpen,
                                const gchar*tipsSave, const gchar*tipsSaveAs)
{
  VisuUiValueIo *valueio;

  g_debug("Gtk VisuUiValueIo: creating new object.");

  valueio = VISU_UI_VALUE_IO(g_object_new(visu_ui_value_io_get_type (), NULL));

  g_debug("Gtk VisuUiValueIo: build widgets.");

  buildWidgets(valueio, parent, tipsOpen, tipsSave, tipsSaveAs);

  return GTK_WIDGET(valueio);
}
/**
 * visu_ui_value_io_connectOnOpen:
 * @valueio: a #VisuUiValueIo widget.
 * @open: a method.
 * 
 * Set the function to call when the open button is clicked.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_connectOnOpen(VisuUiValueIo *valueio, VisuUiValueIoCallback open)
{
  g_return_if_fail(VISU_IS_UI_VALUE_IO(valueio));

  if (valueio->openSignalId)
    {
      g_signal_handler_disconnect(G_OBJECT(valueio->btExportOpen),
				  valueio->openSignalId);
    }
  
  valueio->ioOpen = open;
  valueio->openSignalId =
    g_signal_connect(G_OBJECT(valueio->btExportOpen), "file-set",
		     G_CALLBACK(onImportXML), (gpointer)valueio);
}
/**
 * visu_ui_value_io_connectOnSave:
 * @valueio: a #VisuUiValueIo widget.
 * @save: a method.
 * 
 * Set the function to call when the save or save-as button is clicked.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_connectOnSave(VisuUiValueIo *valueio, VisuUiValueIoCallback save)
{
  g_return_if_fail(VISU_IS_UI_VALUE_IO(valueio));

  if (valueio->saveSignalId)
    g_signal_handler_disconnect(G_OBJECT(valueio->btExportSave),
				valueio->saveSignalId);
  if (valueio->saveAsSignalId)
    g_signal_handler_disconnect(G_OBJECT(valueio->btExportSaveAs),
				valueio->saveAsSignalId);
  
  valueio->ioSave = save;
  valueio->saveAsSignalId = 
    g_signal_connect(G_OBJECT(valueio->btExportSaveAs), "clicked",
		     G_CALLBACK(onExportXML), (gpointer)valueio);
  valueio->saveSignalId =
    g_signal_connect(G_OBJECT(valueio->btExportSave), "clicked",
		     G_CALLBACK(onExportXML), (gpointer)valueio);
}

static gboolean loadXMLFile(gpointer data)
{
  gchar *filename;
  GError *error;

  g_return_val_if_fail(VISU_IS_UI_VALUE_IO(data), FALSE);
  g_return_val_if_fail(VISU_UI_VALUE_IO(data)->ioOpen, FALSE);

  filename = g_object_get_data(G_OBJECT(data), "filename");
  g_return_val_if_fail(filename, FALSE);

  error = (GError*)0;
  if (!VISU_UI_VALUE_IO(data)->ioOpen(filename, &error))
    {
      visu_ui_raiseWarning
	(_("Import V_Sim values from a file."),
	 error->message, VISU_UI_VALUE_IO(data)->parent);
      g_error_free(error);
      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen));
    }
  g_free(filename);
  g_object_set_data(G_OBJECT(data), "filename", (gpointer)0);

  return FALSE;
}

static void onImportXML(GtkFileChooserButton *filechooserbutton, gpointer data)
{
  gchar *filename;

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserbutton));
  if (!filename)
    return;
  
  g_object_set_data(G_OBJECT(data), "filename", filename);
  g_idle_add(loadXMLFile, (gpointer)data);
}
/**
 * visu_ui_value_io_getFilename:
 * @parent: a parent to display the dialog on.
 *
 * Open a save dialog window  with XML filter to choose the name of a
 * file. This is the default action that can be connect to a #VisuUiValueIo
 * widget using visu_ui_value_io_connectOnSave().
 *
 * Since: 3.5
 *
 * Returns: a filename that should be freed later with g_free() by the caller.
 */
gchar* visu_ui_value_io_getFilename(GtkWindow *parent)
{
  GtkWidget *saveDialog;
  gchar *filename;
  const gchar *directory;
  GtkFileFilter *filter;

  saveDialog = gtk_file_chooser_dialog_new
    (_("Export V_Sim values to a file."), parent,
     GTK_FILE_CHOOSER_ACTION_SAVE,
     _("_Cancel"), GTK_RESPONSE_CANCEL,
     _("_Save"), GTK_RESPONSE_ACCEPT, NULL);
  gtk_window_set_modal(GTK_WINDOW(saveDialog), TRUE);
  gtk_window_set_transient_for(GTK_WINDOW(saveDialog), GTK_WINDOW(parent));
  gtk_window_set_position(GTK_WINDOW(saveDialog), GTK_WIN_POS_CENTER_ON_PARENT);
  directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(saveDialog), directory);
  gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(saveDialog), FALSE);
  gtk_dialog_set_default_response(GTK_DIALOG(saveDialog), GTK_RESPONSE_ACCEPT);

  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("V_Sim value files (*.xml)"));
  gtk_file_filter_add_pattern(filter, "*.xml");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(saveDialog), filter);
  filter = gtk_file_filter_new ();
  gtk_file_filter_set_name(filter, _("All files"));
  gtk_file_filter_add_pattern (filter, "*");
  gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(saveDialog), filter);

  /* A suggested filename. */
  gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(saveDialog),
				    _("values.xml"));

  if (gtk_dialog_run(GTK_DIALOG(saveDialog)) != GTK_RESPONSE_ACCEPT)
    {
      gtk_widget_destroy(saveDialog);
      return (gchar*)0;
    }
  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(saveDialog));
  gtk_widget_destroy(saveDialog);
  
  return filename;
}

static gboolean saveXMLFile(gpointer data)
{
  gchar *filename;
  GError *error;

  g_return_val_if_fail(VISU_IS_UI_VALUE_IO(data), FALSE);
  g_return_val_if_fail(VISU_UI_VALUE_IO(data)->ioSave, FALSE);

  filename = g_object_get_data(G_OBJECT(data), "filename");
  g_return_val_if_fail(filename, FALSE);

  error = (GError*)0;
  if (!VISU_UI_VALUE_IO(data)->ioSave(filename, &error))
    {
      visu_ui_raiseWarning
	(_("Export V_Sim values to a file."),
	 error->message, VISU_UI_VALUE_IO(data)->parent);
      g_error_free(error);
    }
  else
    {
      g_signal_handler_block(G_OBJECT(VISU_UI_VALUE_IO(data)->btExportOpen),
			     VISU_UI_VALUE_IO(data)->openSignalId);
      gtk_file_chooser_set_filename
	(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen), filename);
      g_signal_handler_unblock(G_OBJECT(VISU_UI_VALUE_IO(data)->btExportOpen),
			       VISU_UI_VALUE_IO(data)->openSignalId);
    }
  g_free(filename);
  g_object_set_data(G_OBJECT(data), "filename", (gpointer)0);

  return FALSE;
}

static void onExportXML(GtkButton *button, gpointer data)
{
  gchar *filename;

  if (button == GTK_BUTTON(VISU_UI_VALUE_IO(data)->btExportSave))
    filename =
      gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(VISU_UI_VALUE_IO(data)->btExportOpen));
  else
    filename =
      visu_ui_value_io_getFilename(VISU_UI_VALUE_IO(data)->parent);

  if (filename)
    {
      g_object_set_data(G_OBJECT(data), "filename", filename);
      g_idle_add(saveXMLFile, (gpointer)data);
    }
}
/**
 * visu_ui_value_io_setSensitiveOpen:
 * @valueio: a #VisuUiValueIo widget.
 * @status: a boolean.
 *
 * Modify the sensitivity of the open button, depending on @status.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_setSensitiveOpen(VisuUiValueIo *valueio, gboolean status)
{
  g_return_if_fail(VISU_IS_UI_VALUE_IO(valueio));

  gtk_widget_set_sensitive(valueio->btExportOpen, status);
}
/**
 * visu_ui_value_io_setSensitiveSave:
 * @valueio: a #VisuUiValueIo widget.
 * @status: a boolean.
 *
 * Modify the sensitivity of the save button, depending on @status.
 * 
 * Since: 3.5
 */
void visu_ui_value_io_setSensitiveSave(VisuUiValueIo *valueio, gboolean status)
{
  gchar *filename;

  g_return_if_fail(VISU_IS_UI_VALUE_IO(valueio));

  filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(valueio->btExportOpen));
  gtk_widget_set_sensitive(valueio->btExportSave, status && filename);
  if (filename)
    g_free(filename);
  gtk_widget_set_sensitive(valueio->btExportSaveAs, status);
  g_object_notify_by_pspec(G_OBJECT(valueio), properties[SAVE_SENSITIVE_PROP]);
}
/**
 * visu_ui_value_io_setFilename:
 * @valueio: a #VisuUiValueIo widget.
 * @filename: a location on disk.
 *
 * Call the open routine previously set by visu_ui_value_io_connectOnOpen() on
 * @filename and update the buttons accordingly.
 *
 * Since: 3.5
 *
 * Returns: TRUE on success of the open routine.
 */
gboolean visu_ui_value_io_setFilename(VisuUiValueIo *valueio, const gchar *filename)
{
  gboolean valid;
  GError *error;

  g_return_val_if_fail(VISU_IS_UI_VALUE_IO(valueio), FALSE);
  g_return_val_if_fail(valueio->ioOpen, FALSE);
  
  error = (GError*)0;
  valid = valueio->ioOpen(filename, &error);
  if (!valid)
    {
      visu_ui_raiseWarning
	(_("Export V_Sim values to a file."),
	 error->message, valueio->parent);
      g_error_free(error);
      gtk_file_chooser_unselect_all(GTK_FILE_CHOOSER(valueio->btExportOpen));
      gtk_widget_set_sensitive(valueio->btExportSave, FALSE);
    }
  else
    {
      gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(valueio->btExportOpen), filename);
      gtk_widget_set_sensitive(valueio->btExportSave, TRUE);
      gtk_widget_set_sensitive(valueio->btExportSaveAs, TRUE);
    }

  return valid;
}
