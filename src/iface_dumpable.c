/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2023)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2023)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_dumpable.h"

#include "config.h"

/**
 * SECTION:iface_dumpable
 * @short_description: an interface for dumpable objects.
 *
 * <para>This interface describes #VisuData objects that can be saved
 * to disk.</para> 
 */

/* Dumpable interface. */
G_DEFINE_INTERFACE(VisuDumpable, visu_dumpable, G_TYPE_OBJECT)

static void visu_dumpable_default_init(VisuDumpableInterface *iface _U_)
{
}

/**
 * visu_dumpable_save:
 * @dumpable: a #VisuDumpable object.
 * @error: an error location.
 *
 * Dump @dumpable to disk.
 *
 * Since: 3.9
 *
 * Returns: TRUE on success.
 **/
gboolean visu_dumpable_save(const VisuDumpable *dumpable, GError **error)
{
  g_return_val_if_fail(VISU_IS_DUMPABLE(dumpable), FALSE);

  if (!VISU_DUMPABLE_GET_INTERFACE(dumpable)->save)
    return FALSE;

  return VISU_DUMPABLE_GET_INTERFACE(dumpable)->save(dumpable, error);
}
