/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_nodemasker.h"

#include "config.h"

/**
 * SECTION:iface_nodemasker
 * @short_description: an interface for objects with masking
 * capabilities on #VisuNode.
 *
 * <para>Implementers of this interface are objects that can change
 * the visibility of #VisuNode through the visu_node_masker_apply()
 * routine. When an implementer has its masking parameters that have
 * changed, it should call visu_node_masker_emitDirty().</para>
 */


/* enum { */
/*   PROP_0, */
/*   N_PROPS */
/* }; */
/* static GParamSpec *properties[N_PROPS]; */

enum
  {
    DIRTY_SIGNAL,
    NB_SIGNAL
  };
static guint _signals[NB_SIGNAL] = { 0 };

/* NodeMasker interface. */
G_DEFINE_INTERFACE(VisuNodeMasker, visu_node_masker, G_TYPE_OBJECT)

static void visu_node_masker_default_init(VisuNodeMaskerInterface *iface)
{
  /**
   * VisuNodeMasker::masking-dirty:
   * @masker: the object emitting the signal.
   *
   * This signal is emitted when some masking parameters changed.
   *
   * Since: 3.8
   */
  _signals[DIRTY_SIGNAL] =
    g_signal_new("masking-dirty", G_TYPE_FROM_INTERFACE(iface),
                 G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                 0, NULL, NULL, g_cclosure_marshal_VOID__VOID,
                 G_TYPE_NONE, 0, NULL);
}

/**
 * visu_node_masker_setMaskFunc:
 * @masker: a #VisuNodeMasker object.
 * @func: (closure data) (scope notified) (allow-none): a #VisuNodeMaskerFunc object.
 * @data: (closure): some data.
 * @destroy: (destroy data): destroy function.
 *
 * If the implementation provides a user defined masking function,
 * this calls the #VisuNodeMaskerInterface::set_mask_func() routine.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_node_masker_setMaskFunc(VisuNodeMasker *masker, VisuNodeMaskerFunc func,
                                      gpointer data, GDestroyNotify destroy)
{
  gboolean res;

  g_return_val_if_fail(VISU_IS_NODE_MASKER(masker), FALSE);

  if (!VISU_NODE_MASKER_GET_INTERFACE(masker)->set_mask_func)
    return FALSE;

  res = VISU_NODE_MASKER_GET_INTERFACE(masker)->set_mask_func(masker, func, data, destroy);
  if (res)
    visu_node_masker_emitDirty(masker);
    
  return res;
}

/**
 * visu_node_masker_apply:
 * @masker: a #VisuNodeMasker object.
 * @redraw: (out caller-allocates): a location for a boolean
 * @array: a #VisuNodeArray object.
 *
 * Apply the masking properties of @masker over @array. If any node
 * visibility has changed, @redraw is set to %TRUE.
 *
 * Since: 3.8
 **/
void visu_node_masker_apply(const VisuNodeMasker *masker,
                            gboolean *redraw, VisuNodeArray *array)
{
  g_return_if_fail(VISU_IS_NODE_MASKER(masker));

  if (!VISU_NODE_MASKER_GET_INTERFACE(masker)->apply)
    return;

  if (VISU_NODE_MASKER_GET_INTERFACE(masker)->apply(masker, array) && redraw)
    *redraw = TRUE;
}

/**
 * visu_node_masker_emitDirty:
 * @masker: a #VisuNodeMasker object.
 *
 * Emits the "masking-dirty" signal. To be used by implementation of
 * this interface to signal that some masking properties have changed.
 *
 * Since: 3.8
 **/
void visu_node_masker_emitDirty(VisuNodeMasker *masker)
{
  g_debug("Visu Node Masker: emitting dirty signal for %p.",
              (gpointer)masker);
  g_signal_emit(masker, _signals[DIRTY_SIGNAL], 0);
}
