/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "atomic_ascii.h"

#include "atomic_yaml.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <visu_dataloadable.h>
#include <visu_dataatomic.h>
#include <coreTools/toolPhysic.h>
#include <coreTools/toolFiles.h>
#include <extraFunctions/vibration.h>

/**
 * SECTION:atomic_ascii
 * @short_description: Method to load ASCII position file.
 *
 * <para>ASCII file format is a plain text format. The mandatory
 * information are a box description and a list of coordinates with
 * symbols. Additional information can be added by keywords or meta
 * data, like forces, units or total energy from a DFT calculation.</para>
 */

typedef struct AsciiKeywordData_
{
  gboolean reduced;
  gboolean angdeg;
  ToolUnits unit;
  VisuBoxBoundaries bc;
} AsciiKeywordData;
typedef struct AsciiMetaData_
{
  guint nqpt;
  gdouble totalEnergy;
  gboolean forces;
} AsciiMetaData;

static VisuDataLoader *asciiLoader = NULL;

static double readTotalEnergy(const gchar *str);
static gboolean loadAscii(VisuDataLoader *self, VisuDataLoadable *data,
                          guint type, guint nSet,
                          GCancellable *cancel, GError **error);
static int read_ascii_file(VisuDataLoadable *data, guint iType, GError **error);
static gboolean readFile_is_comment(char *str, AsciiKeywordData *kw,
				    AsciiMetaData *md);

/**
 * visu_data_loader_ascii_getStatic:
 *
 * Retrieve the instance of the atomic loader used to parse ASCII files.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a #VisuDataLoader owned by V_Sim.
 **/
VisuDataLoader* visu_data_loader_ascii_getStatic(void)
{
  const gchar *typeASCII[] = {"*.ascii", (char*)0};

  if (asciiLoader)
    return asciiLoader;

  return asciiLoader = visu_data_loader_new_compressible
      (_("'x y z Element' format"), typeASCII, loadAscii, 50);
}

/******************************************************************************/

static gboolean readFile_is_comment(char *str, AsciiKeywordData *kw,
				    AsciiMetaData *md)
{
  gchar **tokens;
  int i;

  if (kw && !strncmp(str + 1, "keyword", 7))
    {
      tokens = g_strsplit_set(str + 9, " ,\n\t\r", -1);
      for (i = 0; tokens[i]; i++)
	if (tokens[i][0] != '\0')
	  {
/* 	    tokens[i] = g_strstrip(tokens[i]); */
	    g_debug("Atomic ASCII: get keyword '%s'.", tokens[i]);
	    if (!g_ascii_strcasecmp(tokens[i], "reduced"))
	      kw->reduced = TRUE;
	    else if (!g_ascii_strcasecmp(tokens[i], "angdeg"))
	      kw->angdeg = TRUE;
	    else if (!g_ascii_strcasecmp(tokens[i], "atomic") ||
		     !g_ascii_strcasecmp(tokens[i], "atomicd0") ||
		     !g_ascii_strcasecmp(tokens[i], "bohr") ||
		     !g_ascii_strcasecmp(tokens[i], "bohrd0"))
	      kw->unit = TOOL_UNITS_BOHR;
	    else if (!g_ascii_strcasecmp(tokens[i], "angstroem") ||
		     !g_ascii_strcasecmp(tokens[i], "angstroemd0"))
	      kw->unit = TOOL_UNITS_ANGSTROEM;
	    else if (!g_ascii_strcasecmp(tokens[i], "periodic"))
	      kw->bc = VISU_BOX_PERIODIC;
	    else if (!g_ascii_strcasecmp(tokens[i], "surface"))
	      kw->bc = VISU_BOX_SURFACE_ZX;
	    else if (!g_ascii_strcasecmp(tokens[i], "surfaceXY"))
	      kw->bc = VISU_BOX_SURFACE_XY;
	    else if (!g_ascii_strcasecmp(tokens[i], "freeBC"))
	      kw->bc = VISU_BOX_FREE;
	  }
      g_strfreev(tokens);
    }
  if (md && !g_ascii_strncasecmp(str + 1, "metaData", 8))
    {
      tokens = g_strsplit_set(str + 10, " ,\n\t\r", -1);
      for (i = 0; tokens[i]; i++)
	if (tokens[i][0] != '\0')
	  {
/* 	    tokens[i] = g_strstrip(tokens[i]); */
	    g_debug("Atomic ASCII: get meta data '%s'.", tokens[i]);
	    if (!g_ascii_strncasecmp(tokens[i], "qpt", 3))
	      md->nqpt += 1;
	    else if (!g_ascii_strncasecmp(tokens[i], "totalEnergy", 11))
	      md->totalEnergy = readTotalEnergy(str + 10);
	    else if (!g_ascii_strncasecmp(tokens[i], "forces", 6))
	      md->forces = TRUE;
	  }
      g_strfreev(tokens);
    }
  if(str[0] == '#' || str[0] == '!')
    return TRUE;
  str = g_strstrip(str);
  return (str[0] == '\0');
}

static double readTotalEnergy(const gchar *str)
{
  gchar *chr, *tmpStr;
  int res, pos;
  double val;

  chr = strstr(str, "totalEnergy");
  chr = strchr(chr, '=');
  chr += 1;

  val = 999.;
  res = (chr)?sscanf(chr, "%lf%n", &val, &pos):0;
  if (res != 1)
    g_warning("syntax error for meta data 'totalEnergy' in '%s'.",
	      chr);
  else if (chr[pos] != '\0')
    {
      tmpStr = g_strdup(chr + pos);
      tmpStr = g_strstrip(tmpStr);
      g_debug("Atomic ASCII: testing unit '%s'.", tmpStr);
      if (!g_ascii_strncasecmp(tmpStr, "Ht", 2))
	val *= 27.21138386;
      else if (!g_ascii_strncasecmp(tmpStr, "Ry", 2))
	val *= .5 * 27.21138386;
      g_free(tmpStr);
    }
  g_debug("Atomic ASCII: found 'totalEnergy' at %geV.", val);
  return val;
}

static gboolean readQpt(gchar *str, float qpt[4], GArray *disp, guint natom)
{
  gchar **values;
  gchar *ptr;
  guint j, k;
  gfloat f;

  ptr = strstr(str, "qpt");
  if (!ptr)
    return FALSE;
  ptr = strstr(ptr, "=");
  if (!ptr)
    {
      g_warning("syntax error for meta data 'qpt' in '%s'.", str);
      return FALSE;
    }

  g_debug("Atomic ASCII: read qpt from '%s'", ptr);
  g_array_set_size(disp, 0);
  values = g_strsplit_set(ptr + 1, " [];\n\r\\\t", -1);
  k = 0;
  for (j = 0; values[j] && k < 4; j++)
    if (values[j][0] != '\0')
      {
	if (sscanf(values[j], "%f", qpt + k) != 1)
	  {
	    g_warning("Can't read a float value from qpt"
		      " keyword in '%s'.", values[j]);
	    qpt[k] = 0.f;
	  }
	g_debug(" | qpt[%d] = %g", k, qpt[k]);
	k += 1;
      }
  if (k != 4)
    g_warning("Can't read 4 float values from qpt"
	      " keyword in '%s'.", ptr);
  for (; values[j]; j++)
    if (values[j][0] != '\0' && sscanf(values[j], "%f", &f) == 1)
      g_array_append_val(disp, f);
  if (disp->len != natom * 6)
    g_warning("Can't read enough displacement values,"
	      " %d read while %d were awaited.", disp->len, natom * 6);
  g_strfreev(values);

  return TRUE;
}

static gboolean readForces(gchar *str, GArray *forces, guint natom)
{
  gchar **values;
  gchar *ptr;
  guint j, k;
  gfloat f;

  ptr = strstr(str, "forces");
  if (!ptr)
    return FALSE;
  ptr = strstr(ptr, "=");
  if (!ptr)
    {
      g_warning("syntax error for meta data 'forces' in '%s'.", str);
      return FALSE;
    }

  g_debug("Atomic ASCII: read forces from '%s'", ptr);
  g_array_set_size(forces, 0);
  values = g_strsplit_set(ptr + 1, " [];\n\r\\\t", -1);
  for (k = 0, j = 0; values[j] && k < 3 * natom; j++)
    if (values[j][0] != '\0')
      {
	if (sscanf(values[j], "%f", &f) == 1)
          {
            g_array_append_val(forces, f);
            k += 1;
          }
      }
  if (k != natom * 3)
    g_warning("Can't read enough force values,"
	      " %d read while %d were awaited.", k, natom * 3);
  g_strfreev(values);

  return TRUE;
}

static gboolean continuousLine(GString *line)
{
  gsize len;

  len = line->len - 1;
  while (line->str[len] == '\n' || line->str[len] == '\r' ||
	 line->str[len] == '\t' || line->str[len] == ' ')
    len -= 1;

  return (line->str[len] == '\\');
}

/******************************************************************************/

static gboolean loadAscii(VisuDataLoader *self _U_, VisuDataLoadable *data,
                          guint type, guint nSet _U_,
                          GCancellable *cancel _U_, GError **error)
{
  g_return_val_if_fail(error && *error == (GError*)0, FALSE);

  /* Do the job of parsing. */
  return (read_ascii_file(data, type, error) >= 0);
}


static int read_ascii_file(VisuDataLoadable *data, guint iType, GError **error)
{
  gchar *info;
  GString *line, *bufLine;
  double xu, yu, zu;
  guint i, iph;
  int pos;
  char nomloc[TOOL_MAX_LINE_LENGTH];
  VisuElement *type;
  VisuDataLoaderIter *iter;
  guint nbLine, natom;
  double boxGeometry[6], abc[3], ang[3];
  float  xyz[3];
  gchar *infoUTF8, **nodeComments;
  gboolean hasNodeComments;
  GIOStatus status;
  ToolFiles *flux;
  float qpt[4];
  GArray *forces, *dcoord;
  AsciiKeywordData kw;
  AsciiMetaData md;
  VisuBox *box;
  VisuVibration *vib;
  VisuNodeValuesString *labels;
    
  g_return_val_if_fail(error && *error == (GError*)0, FALSE);
  g_return_val_if_fail(VISU_IS_DATA_ATOMIC(data), FALSE);

  flux = tool_files_new();
  if (!tool_files_open(flux, visu_data_loadable_getFilename(data, iType), error))
    {
      g_object_unref(flux);
      return -1;
    }

  line = g_string_new("");

  /* The first line is a commentry. */
  if (tool_files_read_line_string(flux, line, NULL, error) != G_IO_STATUS_NORMAL)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -1;
    }
  info = g_strdup(line->str);
  /* The second line contains xx, xy and yy */
  if (tool_files_read_line_string(flux, line, NULL, error) != G_IO_STATUS_NORMAL)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -2;
    }
  if(sscanf(line->str, "%lf %lf %lf",
	    &(boxGeometry[0]), &(boxGeometry[1]), &(boxGeometry[2])) != 3)
    {
      *error = g_error_new(VISU_DATA_LOADABLE_ERROR, DATA_LOADABLE_ERROR_FORMAT,
			   _("Cannot read dxx dyx dyy"
			     " on 2nd line of ASCII file.\n"));
      g_object_unref(flux);
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -3;
    }
  /* The third line contains xz, yz and zz */
  if (tool_files_read_line_string(flux, line, NULL, error) != G_IO_STATUS_NORMAL)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -4;
    }
  if(sscanf(line->str, "%lf %lf %lf",
	    &boxGeometry[3], &boxGeometry[4], &boxGeometry[5]) != 3)
    {
      *error = g_error_new(VISU_DATA_LOADABLE_ERROR, DATA_LOADABLE_ERROR_FORMAT,
			   _("Cannot read dzx dzy dzz"
			     " on 3rd line of ASCII file.\n"));
      g_object_unref(flux);
      g_string_free(line, TRUE);
      g_free(info);
      /* The file doesn't conform to ascii norm, we exit with a wrong type flag. */
      return -5;
    }
   
  /* Ok, from this point we assert that the file is in ascii format.
     All following errors are treated as syntax errors and the corresponding
     flag is return. */
  g_debug("Atomic ASCII: read box is %f x %f x %f\n"
	      "                          %f x %f x %f\n",
	      boxGeometry[0], boxGeometry[1], boxGeometry[2],
	      boxGeometry[3], boxGeometry[4], boxGeometry[5]);

  /* Set the commentary. */
  g_strstrip(info);
  if (info[0] == '#')
    infoUTF8 = g_locale_to_utf8(info + 1, -1, NULL, NULL, NULL);
  else
    infoUTF8 = g_locale_to_utf8(info, -1, NULL, NULL, NULL);
  if (infoUTF8)
    {
      visu_data_setDescription(VISU_DATA(data), infoUTF8);
      g_free(infoUTF8);
    }
  else
    g_warning("Can't convert '%s' to UTF8.\n", info);

  /* 1st pass to count ntype */
  kw.reduced     = FALSE;
  kw.angdeg      = FALSE;
  kw.unit        = TOOL_UNITS_UNDEFINED;
  kw.bc          = VISU_BOX_PERIODIC;
  md.nqpt        = 0;
  md.totalEnergy = 999.;
  md.forces      = FALSE;
  dcoord  = (GArray*)0;
  forces  = (GArray*)0;
  nbLine  = 4;
  iter    = visu_data_loader_iter_new();

  for(status = tool_files_read_line_string(flux, line, NULL, error);
      status == G_IO_STATUS_NORMAL;
      status = tool_files_read_line_string(flux, line, NULL, error))
    {
      nbLine += 1;
      if(readFile_is_comment(line->str, &kw, &md)) continue;
      if(sscanf(line->str, "%lf %lf %lf %s", 
		&xu, &yu, &zu, nomloc) != 4)
	{
	  *error = g_error_new(VISU_DATA_LOADABLE_ERROR, DATA_LOADABLE_ERROR_FORMAT,
			       _("Cannot read x, y, z,"
				 " name in ASCII file at line %d.\n\n"
				 "Quoting '%s'.\n"), nbLine, line->str);
          g_object_unref(flux);
	  g_string_free(line, TRUE);
	  return 1;
	}
      nomloc[8] = '\0';
      visu_data_loader_iter_addNode(iter, visu_element_retrieveFromName(nomloc,
                                                                        (gboolean*)0));
    }
  if (status != G_IO_STATUS_EOF)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      visu_data_loader_iter_unref(iter);
      return 1;
    }

  natom = visu_data_loader_iter_allocate(iter, VISU_NODE_ARRAY(data));
  visu_data_loader_iter_unref(iter);
  if (natom <= 0)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      *error = g_error_new(VISU_DATA_LOADABLE_ERROR, DATA_LOADABLE_ERROR_FORMAT,
			   _("The file contains no atom coordinates.\n"));
      return 1;
    }

  /* We build boxGeometry according to angdeg flag. */
  g_debug("Atomic ASCII: build the box if any.");
  if (kw.angdeg)
    {
      abc[0] = boxGeometry[0];
      abc[1] = boxGeometry[1];
      abc[2] = boxGeometry[2];
      ang[0] = boxGeometry[3];
      ang[1] = boxGeometry[4];
      ang[2] = boxGeometry[5];

      if (sin(ang[2]) == 0.f)
	{
	  *error = g_error_new(VISU_DATA_LOADABLE_ERROR, DATA_LOADABLE_ERROR_FORMAT,
			       _("Wrong (ab) angle, should be"
				 " different from 0[pi].\n\n"
				 " Quoting '%g'.\n"), ang[2]);
          g_object_unref(flux);
	  g_string_free(line, TRUE);
	  return 1;
	}

      boxGeometry[0] = abc[0];
      boxGeometry[1] = abc[1] * cos(G_PI * ang[2] / 180.);
      boxGeometry[2] = abc[1] * sin(G_PI * ang[2] / 180.);
      boxGeometry[3] = abc[2] * cos(G_PI * ang[1] / 180.);
      boxGeometry[4] = abc[2] * (cos(G_PI * ang[0] / 180.) -
				 cos(G_PI * ang[1] / 180.) *
				 cos(G_PI * ang[2] / 180.)) /
	sin(G_PI * ang[2] / 180.);
      boxGeometry[5] = sqrt(abc[2] * abc[2] - boxGeometry[3] * boxGeometry[3] -
			    boxGeometry[4] * boxGeometry[4]);
      boxGeometry[5] *= (ang[1] < 0.)?-1.:+1.;
    }
  /* Always set the given box, in case coordinates are reduced. */
  box = visu_box_new(boxGeometry, kw.bc);
  visu_box_setUnit(box, kw.unit);
  visu_boxed_setBox(VISU_BOXED(data), VISU_BOXED(box));
  g_object_unref(box);
  /* Test phonons. */
  vib = (VisuVibration*)0;
  if (md.nqpt > 0)
    {
      g_debug("Atomic ASCII: initialise phonon storage for %d data.",
		  natom * 6);
      vib = visu_data_getVibration(VISU_DATA(data), md.nqpt);
      dcoord = g_array_sized_new(FALSE, FALSE, sizeof(float), natom * 6);
    }
  if (md.forces)
    {
      g_debug("Atomic ASCII: initialise forces storage for %d data.",
		  natom * 3);
      forces = g_array_sized_new(FALSE, FALSE, sizeof(float), natom * 3);
    }

  g_debug("Atomic ASCII: reread file for node positions.");
  /* Allocate the storage of the node comments. */
  nodeComments = g_malloc(sizeof(gchar*) * natom);
  hasNodeComments = FALSE;

  if (tool_files_rewind(flux, error) != G_IO_STATUS_NORMAL)
    {
      g_object_unref(flux);
      g_string_free(line, TRUE);
      return 1;
    }
  /* reread the file to store the coordinates */
  tool_files_read_line_string(flux, line, NULL, error);
  tool_files_read_line_string(flux, line, NULL, error);
  tool_files_read_line_string(flux, line, NULL, error);
  i = 0;
  iph = 0;
  bufLine = g_string_new((gchar*)0);
  for(status = tool_files_read_line_string(flux, line, NULL, error);
      status == G_IO_STATUS_NORMAL;
      status = tool_files_read_line_string(flux, line, NULL, error))
    {
      if(readFile_is_comment(line->str, (AsciiKeywordData*)0, (AsciiMetaData*)0))
	{
	  while (continuousLine(line) && status == G_IO_STATUS_NORMAL)
	    {
	      status = tool_files_read_line_string(flux, bufLine, NULL, error);
	      g_string_append(line, bufLine->str);
	    }
	  
	  if (vib && readQpt(line->str, qpt, dcoord, natom))
	    {
	      visu_vibration_setCharacteristic(vib, iph, qpt, qpt[3], 1.f);
	      visu_vibration_setDisplacements(vib, iph, dcoord, TRUE);
	      iph += 1;
	    }

          if (md.forces && readForces(line->str, forces, natom))
            visu_node_values_vector_set
              (visu_data_atomic_getForces(VISU_DATA_ATOMIC(data), TRUE), forces);
	  
	  continue;
	}
     
      pos = 0;
      sscanf(line->str, "%lf %lf %lf %s %n", &xu, &yu, &zu, nomloc, &pos);
      nomloc[8] = '\0';
      type = visu_element_lookup(nomloc);
      if (!type)
	{
	  g_warning("The input file must"
		    " have been modified when loading since"
		    " at the second reading the element '%s'"
		    " seems to be new.\n", nomloc);
          g_object_unref(flux);
	  g_string_free(line, TRUE);
	  g_string_free(bufLine, TRUE);
	  return 1;
	}
      xyz[0] = (float)xu;
      if (!(kw.bc & 1) && kw.reduced) xyz[0] /= boxGeometry[0];
      xyz[1] = (float)yu;
      if (!(kw.bc & 2) && kw.reduced) xyz[1] /= boxGeometry[2];
      xyz[2] = (float)zu;
      if (!(kw.bc & 4) && kw.reduced) xyz[2] /= boxGeometry[5];
      visu_data_addNodeFromElement(VISU_DATA(data), type, xyz, kw.reduced);

      /* Store a possible comment. */
      if (line->str[pos] != '\0')
	{
	  nodeComments[i] = (line->str[pos] == '#')?g_strdup(line->str + pos + 1):
	    g_strdup(line->str + pos);
	  g_strstrip(nodeComments[i]);
	  if (nodeComments[i][0] == '\0')
	    {
	      g_free(nodeComments[i]);
	      nodeComments[i] = (gchar*)0;
	    }
	  else
	    hasNodeComments = TRUE;
	}
      else
	nodeComments[i] = (gchar*)0;
      i++;
    }
  g_string_free(line, TRUE);
  g_string_free(bufLine, TRUE);
  g_object_unref(flux);

  /* In free boundary conditions, find the bounding box. */
  if (kw.bc != VISU_BOX_PERIODIC)
    visu_data_setTightBox(VISU_DATA(data));

  /* We apply the comments, if any. */
  if (hasNodeComments)
    {
      labels = visu_data_getNodeLabels(VISU_DATA(data));
      for (i = 0; i < natom; i++)
        if (nodeComments[i])
          {
            if (nodeComments[i][0] == '{' &&
                nodeComments[i][strlen(nodeComments[i]) - 1] == '}')
              visu_data_loader_yaml_setNodeProp
                (VISU_DATA(data), visu_node_array_getFromId(VISU_NODE_ARRAY(data), i),
                 nodeComments[i]);
            else
              visu_node_values_string_setAt
                (labels, visu_node_array_getFromId(VISU_NODE_ARRAY(data), i),
                 nodeComments[i]);
            g_free(nodeComments[i]);
          }
    }
  g_free(nodeComments);

  /* Add some other meta data. */
  if (md.totalEnergy != 999.)
    g_object_set(G_OBJECT(data), "totalEnergy", md.totalEnergy, NULL);
  
  if (dcoord)
    g_array_free(dcoord, TRUE);
  if (forces)
    g_array_free(forces, TRUE);

  g_debug("Atomic ASCII: parse done.");

  return 0;
}

