/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "cylinder_renderer.h"
#include "iface_cylinder.h"

#include <epoxy/gl.h>
#include <math.h>

#include <coreTools/toolShape.h>
#include <renderingMethods/elementAtomic.h>

/**
 * SECTION:cylinder_renderer
 * @short_description: a class to render #VisuPairLink as cylinders.
 *
 * <para>This class is used to render #VisuPairLink as cylinders.</para>
 */

struct _ElementRenderer
{
  VisuElementRenderer *ele;
  gulong mat_sig, col_sig, siz_sig;
};

struct _NodeColorizer
{
  VisuDataColorizer *colorizer;
  gulong dirty_sig;
};

struct _CacheGlShader
{
  guint nVertices;
  float rgba[4];
  float material[5];
};

struct _VisuPairCylinderRendererPrivate
{
  gboolean dispose_has_run;

  guint nlat;
  gfloat radius, ratio;
  GArray *cacheEle1, *cacheEle2; /* To be removed when using shaders. */
  GArray *current;  /* To be removed when using shaders. */
  GHashTable *cache; /* To be removed when using shaders. */

  struct _NodeColorizer nodes;
  struct _ElementRenderer ele1;
  struct _ElementRenderer ele2;

  VisuGlView *view;
  gulong detail_signal;
};

static void visu_pair_cylinder_renderer_dispose(GObject* obj);

static void _rebuild(VisuPairLinkRenderer *self, VisuGlExt *ext, guint shaderId);
static VisuGlExtAttrib* _attribs(VisuPairLinkRenderer *self);
static void _start(VisuPairLinkRenderer *self, VisuPairLink *data,
                   VisuElementRenderer *ele1, VisuElementRenderer *ele2,
                   VisuDataColorizer *colorizer);
static void _stop(VisuPairLinkRenderer *self, const VisuPairLink *data, GArray *gpuData);
static void _draw(VisuPairLinkRenderer *self, const VisuPairLinkIter *iter,
                  GArray *gpuData);
static void _render(const VisuPairLinkRenderer *self, const VisuPairLink *data,
                    const VisuGlExt *ext);
static gboolean _set_view(VisuPairLinkRenderer *renderer, VisuGlView *view);
static void _setElementRenderer(VisuPairCylinderRenderer *self,
                                struct _ElementRenderer *ele,
                                VisuElementRenderer *renderer);
static void _setNodeColorizer(VisuPairCylinderRenderer *self,
                              struct _NodeColorizer *node,
                              VisuDataColorizer *colorizer);

enum
  {
    PROP_0,
    ID_PROP,
    LABEL_PROP,
    DESCR_PROP,
    N_PROP
  };

G_DEFINE_TYPE_WITH_CODE(VisuPairCylinderRenderer, visu_pair_cylinder_renderer, VISU_TYPE_PAIR_LINK_RENDERER,
                        G_ADD_PRIVATE(VisuPairCylinderRenderer))

static void visu_pair_cylinder_renderer_class_init(VisuPairCylinderRendererClass *klass)
{
  g_debug("Visu Cylinder Renderer: creating the class of the object.");

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_pair_cylinder_renderer_dispose;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->rebuild = _rebuild;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->attribs = _attribs;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->start = _start;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->stop = _stop;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->draw = _draw;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->render = _render;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->set_view = _set_view;
}
static void visu_pair_cylinder_renderer_init(VisuPairCylinderRenderer *obj)
{
  g_debug("Visu Cylinder Renderer: initializing a new object (%p).",
	      (gpointer)obj);

  obj->priv = visu_pair_cylinder_renderer_get_instance_private(obj);

  obj->priv->dispose_has_run = FALSE;
  obj->priv->ele1.ele = (VisuElementRenderer*)0;
  obj->priv->ele2.ele = (VisuElementRenderer*)0;
  obj->priv->view = (VisuGlView*)0;
  obj->priv->cache = g_hash_table_new_full(g_direct_hash, g_direct_equal,
                                           NULL, (GDestroyNotify)g_array_unref);
}
static void visu_pair_cylinder_renderer_dispose(GObject* obj)
{
  VisuPairCylinderRenderer *data;

  data = VISU_PAIR_CYLINDER_RENDERER(obj);
  if (data->priv->dispose_has_run)
    return;
  data->priv->dispose_has_run = TRUE;

  _set_view(VISU_PAIR_LINK_RENDERER(data), (VisuGlView*)0);
  _setNodeColorizer(data, &data->priv->nodes, (VisuDataColorizer*)0);
  _setElementRenderer(data, &data->priv->ele1, (VisuElementRenderer*)0);
  _setElementRenderer(data, &data->priv->ele2, (VisuElementRenderer*)0);
  g_hash_table_destroy(data->priv->cache);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_pair_cylinder_renderer_parent_class)->dispose(obj);
}
/**
 * visu_pair_cylinder_renderer_new:
 *
 * The default renderer to draw linsk as cylinders.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): the default #VisuPairCylinderRenderer object.
 */
VisuPairCylinderRenderer* visu_pair_cylinder_renderer_new()
{
  return VISU_PAIR_CYLINDER_RENDERER(g_object_new(VISU_TYPE_PAIR_CYLINDER_RENDERER, "id", "Cylinder pairs", "label", _("Cylinder pairs"), "description", _("Pairs are rendered by cylinders."
                                                                                                                                                           " The color and the width can by chosen."), NULL));
}

static void _setElementRenderer(VisuPairCylinderRenderer *self,
                                struct _ElementRenderer *ele,
                                VisuElementRenderer *renderer)
{
  if (ele->ele == renderer)
    return;

  if (ele->ele)
    {
      g_signal_handler_disconnect(ele->ele, ele->mat_sig);
      g_signal_handler_disconnect(ele->ele, ele->col_sig);
      g_signal_handler_disconnect(ele->ele, ele->siz_sig);
      g_object_unref(ele->ele);
    }
  ele->ele = renderer;
  if (renderer)
    {
      g_object_ref(renderer);
      ele->mat_sig = g_signal_connect_swapped(renderer, "notify::material",
                                              G_CALLBACK(visu_pair_link_renderer_emitDirty), self);
      ele->col_sig = g_signal_connect_swapped(renderer, "notify::color",
                                              G_CALLBACK(visu_pair_link_renderer_emitDirty), self);
      ele->siz_sig = g_signal_connect_swapped(renderer, "size-changed",
                                              G_CALLBACK(visu_pair_link_renderer_emitDirty), self);
    }
}

static void _setNodeColorizer(VisuPairCylinderRenderer *self,
                              struct _NodeColorizer *node,
                              VisuDataColorizer *colorizer)
{
  if (node->colorizer == colorizer)
    return;

  if (node->colorizer)
    {
      g_signal_handler_disconnect(node->colorizer, node->dirty_sig);
      g_object_unref(node->colorizer);
    }
  node->colorizer = colorizer;
  if (colorizer)
    {
      g_object_ref(colorizer);
      node->dirty_sig = g_signal_connect_swapped(colorizer, "dirty",
                                                 G_CALLBACK(visu_pair_link_renderer_emitDirty), self);
    }
}

static void _rebuild(VisuPairLinkRenderer *self _U_, VisuGlExt *ext, guint shaderId)
{
  GError *error;

  error = (GError*)0;
  if (!visu_gl_ext_setShaderById(ext, shaderId, VISU_GL_SHADER_MATERIAL, &error))
    {
      g_warning("Cannot create cylinder shader: %s", error->message);
      g_clear_error(&error);
    }
}

static void _start(VisuPairLinkRenderer *self, VisuPairLink *data,
                   VisuElementRenderer *ele1, VisuElementRenderer *ele2,
                   VisuDataColorizer *colorizer)
{
  VisuPairCylinderRenderer *renderer;
  VisuPairCylinderColorId id;
  const ToolColor *color;
  const float *material;

  renderer = VISU_PAIR_CYLINDER_RENDERER(self);

  renderer->priv->ratio = visu_element_renderer_getExtent(ele1) /
      (visu_element_renderer_getExtent(ele1) +
       visu_element_renderer_getExtent(ele2));
  renderer->priv->radius = visu_pair_cylinder_getRadius(VISU_PAIR_CYLINDER(data));
  renderer->priv->nlat = visu_gl_view_getDetailLevel(renderer->priv->view,
                                                     renderer->priv->radius);
  renderer->priv->current = (GArray*)g_hash_table_lookup(renderer->priv->cache, data);
  if (!renderer->priv->current)
    {
      renderer->priv->current = g_array_new(FALSE, FALSE, sizeof(struct _CacheGlShader));
      g_hash_table_insert(renderer->priv->cache, data, renderer->priv->current);
    }
  g_array_set_size(renderer->priv->current, 0);

  id = visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(data));
  if (id == VISU_CYLINDER_COLOR_ELEMENT)
    {
      struct _CacheGlShader cache[2];
      renderer->priv->cacheEle1 = g_array_new(FALSE, FALSE, sizeof(GLfloat));
      renderer->priv->cacheEle2 = g_array_new(FALSE, FALSE, sizeof(GLfloat));
      color = visu_element_renderer_getColor(ele1);
      memcpy(cache[0].rgba, color->rgba, sizeof(float) * 4);
      material = visu_element_renderer_getMaterial(ele1);
      memcpy(cache[0].material, material, sizeof(float) * 5);
      color = visu_element_renderer_getColor(ele2);
      memcpy(cache[1].rgba, color->rgba, sizeof(float) * 4);
      material = visu_element_renderer_getMaterial(ele2);
      memcpy(cache[1].material, material, sizeof(float) * 5);
      g_array_append_vals(renderer->priv->current, cache, 2);
    }
  if (id == VISU_CYLINDER_COLOR_ELEMENT || id == VISU_CYLINDER_COLOR_NODE)
    {
      _setElementRenderer(renderer, &renderer->priv->ele1, ele1);
      _setElementRenderer(renderer, &renderer->priv->ele2, ele2);
    }
  else
    {
      _setElementRenderer(renderer, &renderer->priv->ele1, (VisuElementRenderer*)0);
      _setElementRenderer(renderer, &renderer->priv->ele2, (VisuElementRenderer*)0);
    }
  if (id == VISU_CYLINDER_COLOR_NODE)
    {
      _setNodeColorizer(renderer, &renderer->priv->nodes, colorizer);
    }
  else
    {
      _setNodeColorizer(renderer, &renderer->priv->nodes, (VisuDataColorizer*)0);
    }
}

static void _stop(VisuPairLinkRenderer *self, const VisuPairLink *data, GArray *gpuData)
{
  VisuPairCylinderRenderer *renderer;
  VisuPairCylinderColorId id;
  ToolShade *shade;

  renderer = VISU_PAIR_CYLINDER_RENDERER(self);
  shade = visu_pair_link_getShade(data);
  id = visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(data));
  if (id == VISU_CYLINDER_COLOR_ELEMENT)
    {
      g_array_append_vals(gpuData,
                          renderer->priv->cacheEle1->data, renderer->priv->cacheEle1->len);
      g_array_index(renderer->priv->current, struct _CacheGlShader, 0).nVertices =
          renderer->priv->cacheEle1->len / 6;
      g_array_free(renderer->priv->cacheEle1, TRUE);
      g_array_append_vals(gpuData,
                          renderer->priv->cacheEle2->data, renderer->priv->cacheEle2->len);
      g_array_index(renderer->priv->current, struct _CacheGlShader, 1).nVertices =
          renderer->priv->cacheEle2->len / 6;
      g_array_free(renderer->priv->cacheEle2, TRUE);
    }
  else if (id == VISU_CYLINDER_COLOR_USER && !shade)
    {
      const float mm[5] = {0.5f, 0.5f, 0.f, 0.f, 0.f};
      struct _CacheGlShader cache;
      const ToolColor *color;
      color = visu_pair_link_getColor(data);
      memcpy(cache.rgba, color->rgba, sizeof(float) * 4);
      memcpy(cache.material, mm, sizeof(float) * 5);
      cache.nVertices = gpuData->len / 6;
      g_array_append_val(renderer->priv->current, cache);
    }
}

static void _setNodeColor(struct _CacheGlShader *shader,
                          VisuElementRenderer *ele, VisuDataColorizer *colorizer,
                          float coeff, VisuData *data, VisuNode *node)
{
  const ToolColor *color;
  const float *material;

  if (!colorizer || !visu_data_colorizer_getColor(colorizer, shader->rgba, data, node))
    {
      color = visu_element_renderer_getColor(ele);
      shader->rgba[0] = color->rgba[0];
      shader->rgba[1] = color->rgba[1];
      shader->rgba[2] = color->rgba[2];
      shader->rgba[3] = color->rgba[3];
    }
  shader->rgba[3] *= coeff;
  material = visu_element_renderer_getMaterial(ele);
  memcpy(shader->material, material, sizeof(float) * 5);
}

static void _setShadeColor(struct _CacheGlShader *shader, const ToolShade *shade,
                           const VisuPairLinkIter *iter)
{
  float mM[2];
  const float mm[5] = {0.5f, 0.5f, 0.f, 0.f, 0.f};
  
  mM[0] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MIN);
  mM[1] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MAX);
  tool_shade_valueToRGB(shade, shader->rgba, (sqrt(iter->d2) - mM[0]) / (mM[1] - mM[0]));
  memcpy(shader->material, mm, sizeof(float) * 5);
}

static VisuGlExtAttrib attribs[] = {{"position", 3, GL_FLOAT, 6 * sizeof(GLfloat), 0},
                                    {"normal", 3, GL_FLOAT, 6 * sizeof(GLfloat), GINT_TO_POINTER(3 * sizeof(GLfloat))},
                                    VISU_GL_EXT_NULL_ATTRIB};
static VisuGlExtAttrib* _attribs(VisuPairLinkRenderer *self _U_)
{
    return attribs;
}

static void _draw(VisuPairLinkRenderer *self, const VisuPairLinkIter *iter,
                  GArray *gpuData)
{
  VisuPairCylinderRenderer *renderer;
  gboolean direct;
  ToolShade *shade;

  renderer = VISU_PAIR_CYLINDER_RENDERER(self);
  shade = visu_pair_link_getShade(iter->parent);
  direct = visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_USER;
  if (iter->periodic || !direct)
    {
      gboolean cached = visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_ELEMENT;
      float dxyz[3];
      guint old = gpuData->len;
      struct _CacheGlShader shader[2];
      dxyz[0] = iter->xyz1[0] + renderer->priv->ratio * iter->dxyz[0];
      dxyz[1] = iter->xyz1[1] + renderer->priv->ratio * iter->dxyz[1];
      dxyz[2] = iter->xyz1[2] + renderer->priv->ratio * iter->dxyz[2];
      tool_drawCylinder(!cached ? gpuData : renderer->priv->cacheEle1, dxyz, iter->xyz1,
                        renderer->priv->radius, renderer->priv->nlat, iter->periodic);
      if (visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_NODE || shade)
        {
          shader[0].nVertices = (gpuData->len - old) / 6;
          if (visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_NODE)
            _setNodeColor(shader, renderer->priv->ele1.ele, renderer->priv->nodes.colorizer,
                          iter->coeff, iter->data, iter->iter1.node);
          else
            _setShadeColor(shader, shade, iter);
          old = gpuData->len;
        }
      dxyz[0] = iter->xyz2[0] - (1.f - renderer->priv->ratio) * iter->dxyz[0];
      dxyz[1] = iter->xyz2[1] - (1.f - renderer->priv->ratio) * iter->dxyz[1];
      dxyz[2] = iter->xyz2[2] - (1.f - renderer->priv->ratio) * iter->dxyz[2];
      tool_drawCylinder(!cached ? gpuData : renderer->priv->cacheEle2, dxyz, iter->xyz2,
                        renderer->priv->radius, renderer->priv->nlat, iter->periodic);
      if (visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_NODE || shade)
        {
          shader[1].nVertices = (gpuData->len - old) / 6;
          if (visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(iter->parent)) == VISU_CYLINDER_COLOR_NODE)
            _setNodeColor(shader + 1, renderer->priv->ele2.ele, renderer->priv->nodes.colorizer,
                        iter->coeff, iter->data, iter->iter2.node);
          else
            _setShadeColor(shader + 1, shade, iter);
          g_array_append_vals(renderer->priv->current, shader, 2);
        }
    }
  else if (shade)
    {
      guint old = gpuData->len;
      struct _CacheGlShader shader;
      tool_drawCylinder(gpuData, iter->xyz1, iter->xyz2,
                        renderer->priv->radius, renderer->priv->nlat, FALSE);
      shader.nVertices = (gpuData->len - old) / 6;
      _setShadeColor(&shader, shade, iter);
      g_array_append_vals(renderer->priv->current, &shader, 1);
    }
  else
    tool_drawCylinder(gpuData, iter->xyz1, iter->xyz2,
                      renderer->priv->radius, renderer->priv->nlat, FALSE);
}
static void _render(const VisuPairLinkRenderer *self,
                    const VisuPairLink *data, const VisuGlExt *ext)
{
  VisuPairCylinderRenderer *renderer;
  guint i, n;
  GArray *shader;

  if (!visu_pair_link_getDrawn(data))
    return;

  renderer = VISU_PAIR_CYLINDER_RENDERER(self);

  g_return_if_fail(renderer->priv->view);
  shader = (GArray*)g_hash_table_lookup(renderer->priv->cache, data);
  for (i = 0, n = 0; i < shader->len; i++)
    {
      struct _CacheGlShader *cache = &g_array_index(shader, struct _CacheGlShader, i);

      g_debug("Visu Cylinder: drawing %d quads.", cache->nVertices / 4);
      visu_gl_ext_setUniformRGBA(ext, cache->rgba);
      visu_gl_ext_setUniformMaterial(ext, cache->material);
      glDrawArrays(GL_TRIANGLES, n, cache->nVertices);
      n += cache->nVertices;
    }
}


static gboolean _set_view(VisuPairLinkRenderer *renderer, VisuGlView *view)
{
  VisuPairCylinderRenderer *self;

  g_return_val_if_fail(VISU_IS_PAIR_CYLINDER_RENDERER(renderer), FALSE);

  self = VISU_PAIR_CYLINDER_RENDERER(renderer);

  if (self->priv->view == view)
    return FALSE;

  if (self->priv->view)
    {
      g_signal_handler_disconnect(G_OBJECT(self->priv->view), self->priv->detail_signal);
      g_object_unref(self->priv->view);
    }
  if (view)
    {
      g_object_ref(view);
      self->priv->detail_signal =
        g_signal_connect_swapped(G_OBJECT(view), "DetailLevelChanged",
                                 G_CALLBACK(visu_pair_link_renderer_emitDirty), (gpointer)self);
    }

  self->priv->view = view;
  return TRUE;
}
