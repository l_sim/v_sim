/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "iface_cylinder.h"

#include <visu_configFile.h>
#include <visu_pairset.h>

/**
 * SECTION:iface_cylinder
 * @short_description: An interface defining all the properties
 * required to draw a #VisuPair as a cylinder.
 *
 * <para>#VisuPairCylinder interface introduces the "radius" property
 * for the cylinder rendering. Cylinders can also be rendered with a
 * solid color or with colors depending on the #VisuElement
 * their are linking.</para>
 */

#define FLAG_RESOURCES_PAIR_RADIUS "pairCylinder_pairRadius"
#define DESC_RESOURCES_PAIR_RADIUS "This value is the radius for specific pairs drawn as cylinders ; element1 elemen2 0 < real < 10"
#define FLAG_RESOURCES_LINK_RADIUS "pairCylinder_linkRadius"
#define DESC_RESOURCES_LINK_RADIUS "This value is the radius for specific drawn link as cylinders ; [element1] [element2] [min] [max] [0 < real < 10]"
#define FLAG_RESOURCES_CYLINDER_RADIUS "pairCylinder_radius"
#define DESC_RESOURCES_CYLINDER_RADIUS "This value is the default radius of the pairs drawn as cylinders ; 0 < real < 10"
#define _RADIUS_DEFAULT 0.1f
static float cylinderRadius, _linkRadius;

#define FLAG_RESOURCES_CYLINDER_COLOR_TYPE "cylinder_colorType"
#define DESC_RESOURCES_CYLINDER_COLOR_TYPE "It chooses the colors of the cylinders according differents criterion ;"
#define FLAG_RESOURCES_LINK_COLOR_TYPE "pairCylinder_linkColorType"
#define DESC_RESOURCES_LINK_COLOR_TYPE "It chooses the colors of the cylinders according differents criterion for each link ; [element1] [element2] [min] [max] [int id]"
#define _COLOR_TYPE_DEFAULT VISU_CYLINDER_COLOR_USER
static int cylinderColorType, _linkColorType;

static void onEntryRadius(VisuConfigFile *obj, VisuConfigFileEntry *entry, gpointer data);
static void onEntryType(VisuConfigFile *obj, VisuConfigFileEntry *entry, gpointer data);

/* This function save the resources. */
static void exportResourcesCylinder(GString *data, VisuData *dataObj);

enum {
  PROP_0,
  PROP_RADIUS,
  PROP_TYPE,
  N_PROPS
};
static GParamSpec *_properties[N_PROPS];

/* Boxed interface. */
G_DEFINE_INTERFACE(VisuPairCylinder, visu_pair_cylinder, G_TYPE_OBJECT)

static void visu_pair_cylinder_default_init(VisuPairCylinderInterface *iface)
{
  VisuConfigFileEntry *resourceEntry, *oldEntry;
  gfloat rgRadius[2] = {VISU_PAIR_CYLINDER_RADIUS_MIN, VISU_PAIR_CYLINDER_RADIUS_MAX};
  int rgType[2] = {VISU_CYLINDER_COLOR_USER, VISU_CYLINDER_COLOR_ELEMENT};

  /**
   * VisuPairCylinder::radius:
   *
   * The cylinder radius.
   *
   * Since: 3.8
   */
  _properties[PROP_RADIUS] =
    g_param_spec_float("radius", "Radius",
                      "cylinder radius", VISU_PAIR_CYLINDER_RADIUS_MIN, VISU_PAIR_CYLINDER_RADIUS_MAX, _RADIUS_DEFAULT, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, _properties[PROP_RADIUS]);
  /**
   * VisuPairCylinder::color-type:
   *
   * The cylinder color type.
   *
   * Since: 3.8
   */
  _properties[PROP_TYPE] =
    g_param_spec_uint("color-type", "Color type",
                      "cylinder color type", 0, VISU_CYLINDER_N_COLOR - 1, _COLOR_TYPE_DEFAULT, G_PARAM_READWRITE);
  g_object_interface_install_property(iface, _properties[PROP_TYPE]);

  resourceEntry = visu_config_file_addIntegerArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                        FLAG_RESOURCES_CYLINDER_COLOR_TYPE,
                                                        DESC_RESOURCES_CYLINDER_COLOR_TYPE,
                                                        1, &cylinderColorType, rgType, FALSE);
  resourceEntry = visu_config_file_addIntegerArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                        FLAG_RESOURCES_LINK_COLOR_TYPE,
                                                        DESC_RESOURCES_LINK_COLOR_TYPE,
                                                        1, &_linkColorType, rgType, TRUE);
  visu_config_file_entry_setVersion(resourceEntry, 3.8f);

  cylinderRadius = _RADIUS_DEFAULT;
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCES_CYLINDER_RADIUS,
                                                      DESC_RESOURCES_CYLINDER_RADIUS,
                                                      1, &cylinderRadius, rgRadius, FALSE);
  oldEntry = visu_config_file_addEntry(VISU_CONFIG_FILE_RESOURCE,
				     FLAG_RESOURCES_PAIR_RADIUS,
				     DESC_RESOURCES_PAIR_RADIUS,
				     1, NULL);
  visu_config_file_entry_setVersion(resourceEntry, 3.1f);
  resourceEntry = visu_config_file_addFloatArrayEntry(VISU_CONFIG_FILE_RESOURCE,
                                                      FLAG_RESOURCES_LINK_RADIUS,
                                                      DESC_RESOURCES_LINK_RADIUS,
                                                      1, &_linkRadius, rgRadius, TRUE);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_entry_setReplace(resourceEntry, oldEntry);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE,
                                     exportResourcesCylinder);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCES_LINK_RADIUS,
                   G_CALLBACK(onEntryRadius), (gpointer)0);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCES_LINK_COLOR_TYPE,
                   G_CALLBACK(onEntryType), (gpointer)0);
}

/*****************************************/
/* Dealing with parameters and resources */
/*****************************************/
static void onEntryRadius(VisuConfigFile *obj _U_, VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuPairLink *link;
  gchar *errMess;
  
  if (!visu_pair_pool_readLinkFromLabel(visu_config_file_entry_getLabel(entry), &link, &errMess))
    {
      visu_config_file_entry_setErrorMessage(entry, errMess);
      g_free(errMess);
      return;
    }
  visu_pair_cylinder_setRadius(VISU_PAIR_CYLINDER(link), _linkRadius);
}
static void onEntryType(VisuConfigFile *obj _U_, VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuPairLink *link;
  gchar *errMess;
  
  if (!visu_pair_pool_readLinkFromLabel(visu_config_file_entry_getLabel(entry), &link, &errMess))
    {
      visu_config_file_entry_setErrorMessage(entry, errMess);
      g_free(errMess);
      return;
    }
  visu_pair_cylinder_setColorType(VISU_PAIR_CYLINDER(link), _linkColorType);
}
static void exportRadius(VisuPair *pair, VisuPairLink *data, gpointer userData)
{
  struct _VisuConfigFileForeachFuncExport *str;
  VisuElement *ele1, *ele2;
  gchar *buf;

  str = ( struct _VisuConfigFileForeachFuncExport*)userData;
  visu_pair_getElements(pair, &ele1, &ele2);
  /* We export the resource only if the elements are
     part of the given VisuData. */
  if (str->dataObj &&
      (!visu_node_array_containsElement(VISU_NODE_ARRAY(str->dataObj), ele1) ||
       !visu_node_array_containsElement(VISU_NODE_ARRAY(str->dataObj), ele2)))
    return;
  if (visu_pair_link_getDistance(data, VISU_DISTANCE_MIN) == 0.f &&
      visu_pair_link_getDistance(data, VISU_DISTANCE_MAX) == 0.f)
    return;

  buf = g_strdup_printf("%s %s  %4.3f %4.3f", ele1->name, ele2->name,
                        visu_pair_link_getDistance(data, VISU_DISTANCE_MIN),
                        visu_pair_link_getDistance(data, VISU_DISTANCE_MAX));
  if (visu_pair_cylinder_getRadius(VISU_PAIR_CYLINDER(data)) !=
      visu_pair_cylinder_getDefaultRadius())
    visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_RADIUS, buf, "%4.3f",
                                 visu_pair_cylinder_getRadius(VISU_PAIR_CYLINDER(data)));

  if (visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(data)) !=
      visu_pair_cylinder_getDefaultColorType())
    visu_config_file_exportEntry(str->data, FLAG_RESOURCES_LINK_COLOR_TYPE, buf, "%d",
                                 visu_pair_cylinder_getColorType(VISU_PAIR_CYLINDER(data)));
  g_free(buf);
}
static void exportPair(VisuPair *pair, gpointer data)
{
  visu_pair_foreach(pair, exportRadius, data);
}
static void exportResourcesCylinder(GString *data, VisuData *dataObj)
{
  struct _VisuConfigFileForeachFuncExport str;
  gchar *buf;

  buf = g_strdup_printf("%s 0 <= integer < %d", DESC_RESOURCES_CYLINDER_COLOR_TYPE,
                        VISU_CYLINDER_N_COLOR);
  visu_config_file_exportComment(data, buf);
  g_free(buf);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_CYLINDER_COLOR_TYPE, NULL,
                               "%d", cylinderColorType);
  visu_config_file_exportComment(data, DESC_RESOURCES_CYLINDER_RADIUS);
  visu_config_file_exportEntry(data, FLAG_RESOURCES_CYLINDER_RADIUS, NULL,
                               "%f", cylinderRadius);
  str.data           = data;
  str.dataObj        = dataObj;
  visu_config_file_exportComment(data, DESC_RESOURCES_PAIR_RADIUS);
  visu_pair_pool_foreach(exportPair, &str);
  visu_config_file_exportComment(data, "");
}

/**
 * visu_pair_cylinder_setRadius:
 * @data: a #VisuPairCylinder object ;
 * @val: a float value.
 *
 * This method allows to change the radius value of a specific pair.
 * When a pair is rendered via a cylinder, it first checks if that pairs has
 * a specific radius value set by this method. If not, it uses the default value.
 *
 * Returns: TRUE if the value is changed.
 */
gboolean visu_pair_cylinder_setRadius(VisuPairCylinder *data, float val)
{
  gboolean res;

  res = VISU_PAIR_CYLINDER_GET_INTERFACE(data)->set_radius(data, CLAMP(val, VISU_PAIR_CYLINDER_RADIUS_MIN, VISU_PAIR_CYLINDER_RADIUS_MAX));
  if (res)
    g_object_notify_by_pspec(G_OBJECT(data), _properties[PROP_RADIUS]);
  return res;
}
/**
 * visu_pair_cylinder_getRadius:
 * @data: a #VisuPairCylinder object.
 *
 * Get the radius value for the specified pair.
 *
 * Returns: the radius value.
 */
gfloat visu_pair_cylinder_getRadius(VisuPairCylinder *data)
{
  return VISU_PAIR_CYLINDER_GET_INTERFACE(data)->get_radius(data);
}

/**
 * visu_pair_cylinder_getDefaultRadius:
 *
 * Get the default value for cylinder radius.
 *
 * Returns: the default value for cylinder radius.
 */
float visu_pair_cylinder_getDefaultRadius()
{
  return cylinderRadius;
}

/**
 * visu_pair_cylinder_setColorType:
 * @data: a #VisuPairCylinder object.
 * @val: a integer that identify the color scheme.
 *
 * It set the color scheme for cylinder pairs. It can be 0 or 1.
 *
 * Returns: TRUE if the calling method should take care of
 * #VisuGlExtPairs objects, FALSE if not.
 */
gboolean visu_pair_cylinder_setColorType(VisuPairCylinder *data, VisuPairCylinderColorId val)
{
  gboolean res;

  res = VISU_PAIR_CYLINDER_GET_INTERFACE(data)->set_colorType(data, MIN(val, VISU_CYLINDER_N_COLOR - 1));
  if (res)
    g_object_notify_by_pspec(G_OBJECT(data), _properties[PROP_TYPE]);
  return res;
}
/**
 * visu_pair_cylinder_getColorType:
 * @data: a #VisuPairCylinder object.
 *
 * Get the color type value for the specified pair.
 *
 * Returns: the color type value.
 */
VisuPairCylinderColorId visu_pair_cylinder_getColorType(VisuPairCylinder *data)
{
  return VISU_PAIR_CYLINDER_GET_INTERFACE(data)->get_colorType(data);
}
/**
 * visu_pair_cylinder_getDefaultColorType:
 *
 * Get the color scheme.
 *
 * Returns: an integer corresponding to the color scheme (0 or 1).
 */
VisuPairCylinderColorId visu_pair_cylinder_getDefaultColorType()
{
  return cylinderColorType;
}
