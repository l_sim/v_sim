/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_CYLINDER_H
#define IFACE_CYLINDER_H

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

/**
 * VisuPairCylinderColorId:
 * @VISU_CYLINDER_COLOR_USER: color is chosen by the user.
 * @VISU_CYLINDER_COLOR_ELEMENT: color is chosen according to
 * the color of the #VisuElement the pair is linked to.
 * @VISU_CYLINDER_COLOR_NODE: color is chosen according to
 * the color of the #VisuNode the pair is linked to (relevant whenever
 * nodes are colorized).
 * @VISU_CYLINDER_COLOR_LENGTH: color is chosen according to the
 * length of the link
 * @VISU_CYLINDER_N_COLOR: number of choices for the colourisation.
 *
 * Possible flags to colourise the cylinder pairs.
 */
typedef enum {
  VISU_CYLINDER_COLOR_USER,
  VISU_CYLINDER_COLOR_ELEMENT,
  VISU_CYLINDER_COLOR_NODE,
  VISU_CYLINDER_COLOR_LENGTH,
  VISU_CYLINDER_N_COLOR
} VisuPairCylinderColorId;

/**
 * VISU_PAIR_CYLINDER_RADIUS_MIN:
 *
 * Minimum value for the radius of cylinder pairs.
 */
#define VISU_PAIR_CYLINDER_RADIUS_MIN 0.01f
/**
 * VISU_PAIR_CYLINDER_RADIUS_MAX:
 *
 * Maximum value for the radius of cylinder pairs.
 */
#define VISU_PAIR_CYLINDER_RADIUS_MAX 3.f

/* Cylinder interface. */
#define VISU_TYPE_PAIR_CYLINDER                (visu_pair_cylinder_get_type())
#define VISU_PAIR_CYLINDER(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_PAIR_CYLINDER, VisuPairCylinder))
#define VISU_IS_PAIR_CYLINDER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_PAIR_CYLINDER))
#define VISU_PAIR_CYLINDER_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE((inst), VISU_TYPE_PAIR_CYLINDER, VisuPairCylinderInterface))

typedef struct _VisuPairCylinder VisuPairCylinder; /* dummy object */
typedef struct _VisuPairCylinderInterface VisuPairCylinderInterface;

/**
 * VisuPairCylinder:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuPairCylinderInterface:
 * @parent: its parent.
 * @set_radius: a method to change the radius of drawn cylinder
 * #VisuPairLink.
 * @get_radius: a method to get the radius.
 * @get_colorType: a method to get the coloring scheme.
 * @set_colorType: a method to change the coloring scheme.
 *
 * Interface for class that can represent #VisuPairLink as cylinders.
 *
 * Since: 3.8
 */
struct _VisuPairCylinderInterface
{
  GTypeInterface parent;

  gfloat     (*get_radius) (VisuPairCylinder *self);
  gboolean   (*set_radius) (VisuPairCylinder *self, gfloat val);

  VisuPairCylinderColorId (*get_colorType) (VisuPairCylinder *self);
  gboolean                (*set_colorType) (VisuPairCylinder *self, VisuPairCylinderColorId val);
};

GType visu_pair_cylinder_get_type (void);

gfloat                  visu_pair_cylinder_getDefaultRadius();
VisuPairCylinderColorId visu_pair_cylinder_getDefaultColorType();

gboolean                visu_pair_cylinder_setColorType(VisuPairCylinder *data,
                                                        VisuPairCylinderColorId val);
VisuPairCylinderColorId visu_pair_cylinder_getColorType(VisuPairCylinder *data);

gboolean   visu_pair_cylinder_setRadius(VisuPairCylinder *data, gfloat val);
gfloat     visu_pair_cylinder_getRadius(VisuPairCylinder *data);

G_END_DECLS

#endif
