/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef IFACE_WIRE_H
#define IFACE_WIRE_H

#include <glib.h>
#include <glib-object.h>

#include <coreTools/toolShade.h>

G_BEGIN_DECLS

/* Boxed interface. */
#define VISU_TYPE_PAIR_WIRE                (visu_pair_wire_get_type())
#define VISU_PAIR_WIRE(obj)                (G_TYPE_CHECK_INSTANCE_CAST((obj), VISU_TYPE_PAIR_WIRE, VisuPairWire))
#define VISU_IS_PAIR_WIRE(obj)             (G_TYPE_CHECK_INSTANCE_TYPE((obj), VISU_TYPE_PAIR_WIRE))
#define VISU_PAIR_WIRE_GET_INTERFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE((inst), VISU_TYPE_PAIR_WIRE, VisuPairWireInterface))

typedef struct _VisuPairWire VisuPairWire; /* dummy object */
typedef struct _VisuPairWireInterface VisuPairWireInterface;

/**
 * VisuPairWire:
 *
 * Interface object.
 *
 * Since: 3.8
 */

/**
 * VisuPairWireInterface:
 * @parent: its parent.
 * @set_width: a method to change the width of drawn wire
 * #VisuPairLink.
 * @get_width: a method to get the width.
 * @set_stipple: a method to change the stipple scheme of drawn wire
 * #VisuPairLink.
 * @get_stipple: a method to get the stipple scheme.
 *
 * Interface for class that can represent #VisuPairLink as flat wires.
 *
 * Since: 3.8
 */
struct _VisuPairWireInterface
{
  GTypeInterface parent;

  guint      (*get_width) (VisuPairWire *self);
  gboolean   (*set_width) (VisuPairWire *self, guint val);
  guint16    (*get_stipple) (VisuPairWire *self);
  gboolean   (*set_stipple) (VisuPairWire *self, guint16 val);
};

GType visu_pair_wire_get_type (void);

guint      visu_pair_wire_getDefaultWidth();
guint16    visu_pair_wire_getDefaultStipple();

gboolean   visu_pair_wire_setWidth(VisuPairWire *data, guint val);
guint      visu_pair_wire_getWidth(VisuPairWire *data);
gboolean   visu_pair_wire_setStipple(VisuPairWire *data, guint16 stipple);
guint16    visu_pair_wire_getStipple(VisuPairWire *data);

G_END_DECLS

#endif
