/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_LINK
#define VISU_LINK

#include <glib.h>
#include <glib-object.h>

#include <coreTools/toolColor.h>
#include <coreTools/toolShade.h>
#include <coreTools/toolPhysic.h>
#include <visu_elements.h>
#include <visu_data.h>

G_BEGIN_DECLS

/**
 * VisuPairLinkDistances:
 * @VISU_DISTANCE_MIN: Flag used to define the minimum length to draw pair. 
 * @VISU_DISTANCE_MAX: Flag used to define the maximum length to draw pair.
 *
 * This is useful with the visu_pair_link_getDistance() and the
 * visu_pair_link_setDistance() methods.
 *
 * Since: 3.8
 */
typedef enum {
  VISU_DISTANCE_MIN,
  VISU_DISTANCE_MAX
} VisuPairLinkDistances;

/**
 * VISU_TYPE_PAIR_LINK:
 *
 * return the type of #VisuPairLink.
 *
 * Since: 3.7
 */
#define VISU_TYPE_PAIR_LINK	     (visu_pair_link_get_type ())
/**
 * VISU_PAIR_LINK:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPairLink type.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PAIR_LINK, VisuPairLink))
/**
 * VISU_PAIR_LINK_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPairLinkClass.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR_LINK, VisuPairLinkClass))
/**
 * VISU_IS_PAIR_LINK:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPairLink object.
 *
 * Since: 3.7
 */
#define VISU_IS_PAIR_LINK(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PAIR_LINK))
/**
 * VISU_IS_PAIR_LINK_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPairLinkClass class.
 *
 * Since: 3.7
 */
#define VISU_IS_PAIR_LINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR_LINK))
/**
 * VISU_PAIR_LINK_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.7
 */
#define VISU_PAIR_LINK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR_LINK, VisuPairLinkClass))

typedef struct _VisuPairLink        VisuPairLink;
typedef struct _VisuPairLinkPrivate VisuPairLinkPrivate;
typedef struct _VisuPairLinkClass   VisuPairLinkClass;

/**
 * VisuPairLink:
 *
 * An opaque structure.
 */
struct _VisuPairLink
{
  VisuObject parent;

  VisuPairLinkPrivate *priv;
};

/**
 * VisuPairLinkClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuPairLinkClass structure.
 */
struct _VisuPairLinkClass
{
  VisuObjectClass parent;
};

/**
 * visu_pair_link_get_type:
 *
 * This method returns the type of #VisuPairLink, use
 * VISU_TYPE_PAIR_LINK instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuPairLink.
 */
GType visu_pair_link_get_type(void);

VisuPairLink* visu_pair_link_new(VisuElement *ele1, VisuElement *ele2,
                                 const float minMax[2], ToolUnits units);

VisuElement* visu_pair_link_getFirstElement(VisuPairLink *data);
VisuElement* visu_pair_link_getSecondElement(VisuPairLink *data);
gboolean   visu_pair_link_setDrawn      (VisuPairLink *data, gboolean drawn);
gboolean   visu_pair_link_getDrawn      (const VisuPairLink *data);
gboolean   visu_pair_link_setColor      (VisuPairLink *data, const ToolColor* destColor);
ToolColor* visu_pair_link_getColor      (const VisuPairLink *data);
gboolean   visu_pair_link_setShade      (VisuPairLink *data, ToolShade *shade);
ToolShade* visu_pair_link_getShade      (const VisuPairLink *data);
gboolean   visu_pair_link_setPrintLength(VisuPairLink *data, gboolean status);
gboolean   visu_pair_link_getPrintLength(const VisuPairLink *data);
gboolean   visu_pair_link_setDistance   (VisuPairLink *data, float val,
                                         VisuPairLinkDistances minOrMax);
float      visu_pair_link_getDistance   (const VisuPairLink *data,
                                         VisuPairLinkDistances minOrMax);
gboolean   visu_pair_link_setUnits      (VisuPairLink *data, ToolUnits units);
ToolUnits  visu_pair_link_getUnits      (const VisuPairLink *data);
gboolean   visu_pair_link_match         (const VisuPairLink *data, const float minMax[2], ToolUnits units);

gboolean   visu_pair_link_isDrawn       (const VisuPairLink *data);

typedef struct _VisuPairLinkIter VisuPairLinkIter;
struct _VisuPairLinkIter
{
  VisuPairLink *parent;

  VisuData *data;
  VisuNodeArrayIter iter1, iter2;

  float buffer;
  float l2[2], l2_buffered[2];

  VisuBox *box;
  gboolean periodic;
  float xyz1[3], xyz2[3], dxyz[3];
  float d2, coeff;
};
gboolean visu_pair_link_iter_new(VisuPairLink *link, VisuData *data,
                                 VisuPairLinkIter *iter,
                                 gboolean usePeriodicty, gfloat buffer);
gboolean visu_pair_link_iter_next(VisuPairLinkIter *iter);

G_END_DECLS

#endif
