/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "wire_renderer.h"
#include "iface_wire.h"
#include <coreTools/toolShade.h>

#include <epoxy/gl.h>
#include <math.h>

/**
 * SECTION:wire_renderer
 * @short_description: a class to render #VisuPairLink as wires.
 *
 * <para>This class is used to render #VisuPairLink as wires.</para>
 */

static VisuGlExtAttrib* _attribs(VisuPairLinkRenderer *self);
static void _rebuild(VisuPairLinkRenderer *self, VisuGlExt *ext, guint shaderId);
static void _draw(VisuPairLinkRenderer *self, const VisuPairLinkIter *iter,
                  GArray *vertices);
static void _render(const VisuPairLinkRenderer *self,
                    const VisuPairLink *data, const VisuGlExt *ext);

G_DEFINE_TYPE(VisuPairWireRenderer, visu_pair_wire_renderer,
              VISU_TYPE_PAIR_LINK_RENDERER)

static void visu_pair_wire_renderer_class_init(VisuPairWireRendererClass *klass)
{
  g_debug("Visu Wire Renderer: creating the class of the object.");

  /* Connect the overloading methods. */
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->attribs = _attribs;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->rebuild = _rebuild;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->draw = _draw;
  VISU_PAIR_LINK_RENDERER_CLASS(klass)->render = _render;
}
static void visu_pair_wire_renderer_init(VisuPairWireRenderer *obj _U_)
{
  g_debug("Visu Wire Renderer: initializing a new object (%p).",
	      (gpointer)obj);
}
/**
 * visu_pair_wire_renderer_new:
 *
 * Creates a renderer to draw links as wires.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuPairWireRenderer object.
 */
VisuPairWireRenderer* visu_pair_wire_renderer_new()
{
  return VISU_PAIR_WIRE_RENDERER(g_object_new(VISU_TYPE_PAIR_WIRE_RENDERER,
                                              "id", "Wire pairs",
                                              "label", _("Wire pairs"),
                                              "description", _("Pairs are rendered by flat lines."
                                                               " The color and the width can by chosen."), NULL));
}

static void _rebuild(VisuPairLinkRenderer *self _U_, VisuGlExt *ext, guint shaderId)
{
  GError *error;

  error = (GError*)0;
  if (!visu_gl_ext_setShaderById(ext, shaderId, VISU_GL_SHADER_COLORED_LINE, &error))
    {
      g_warning("Cannot create wire shader: %s", error->message);
      g_clear_error(&error);
    }
}

static VisuGlExtAttrib attribs[] = {{"color", 4, GL_FLOAT, 7 * sizeof(GLfloat), 0},
                                    {"position", 3, GL_FLOAT, 7 * sizeof(GLfloat), GINT_TO_POINTER(4 * sizeof(GLfloat))},
                                    VISU_GL_EXT_NULL_ATTRIB};
static VisuGlExtAttrib* _attribs(VisuPairLinkRenderer *self _U_)
{
    return attribs;
}

static void _draw(VisuPairLinkRenderer *self _U_,
                  const VisuPairLinkIter *iter, GArray *vertices)
{
  float ratio;
  ToolColor *color;
  float rgba[4], mM[2];
  ToolShade *shade;

  color = visu_pair_link_getColor(iter->parent);
  shade = visu_pair_link_getShade(iter->parent);
  if (shade)
    {
      mM[0] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MIN);
      mM[1] = visu_pair_link_getDistance(iter->parent, VISU_DISTANCE_MAX);
      ratio = (sqrt(iter->d2) - mM[0]) / (mM[1] - mM[0]);
      tool_shade_valueToRGB(shade, rgba, ratio);
    }
  else
    {
      rgba[0] = color->rgba[0];
      rgba[1] = color->rgba[1];
      rgba[2] = color->rgba[2];
    }
  rgba[3] = iter->coeff * color->rgba[3];

  if (iter->periodic)
    {
      GLfloat xyz1[3] = {iter->xyz2[0] - iter->dxyz[0] / 2.f,
          iter->xyz2[1] - iter->dxyz[1] / 2.f,
          iter->xyz2[2] - iter->dxyz[2] / 2.f};
      GLfloat xyz2[3] = {iter->xyz1[0] + iter->dxyz[0] / 2.f,
          iter->xyz1[1] + iter->dxyz[1] / 2.f,
          iter->xyz1[2] + iter->dxyz[2] / 2.f};
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, iter->xyz1, 3);
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, xyz2, 3);
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, xyz1, 3);
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, iter->xyz2, 3);
    }
  else
    {
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, iter->xyz1, 3);
      g_array_append_vals(vertices, rgba, 4);
      g_array_append_vals(vertices, iter->xyz2, 3);
    }
}

static void _render(const VisuPairLinkRenderer *self _U_,
                    const VisuPairLink *data, const VisuGlExt *ext)
{
  const guint nVertices = visu_gl_ext_getBufferDimension(ext) / 7;

  glDisable(GL_DITHER);

  glLineWidth(visu_pair_wire_getWidth(VISU_PAIR_WIRE(data)));
  visu_gl_ext_setUniformStipple(ext, visu_pair_wire_getStipple(VISU_PAIR_WIRE(data)));

  g_debug("Visu Wire: drawing %d links.", nVertices / 2);
  glDrawArrays(GL_LINES, 0, nVertices);

  glEnable(GL_DITHER);
}
