/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef WIRE_RENDERER_H
#define WIRE_RENDERER_H

#include <glib-object.h>

#include <visu_tools.h>
#include "linkRenderer.h"

/**
 * VISU_TYPE_PAIR_WIRE_RENDERER:
 *
 * return the type of #VisuPairWireRenderer.
 *
 * Since: 3.8
 */
#define VISU_TYPE_PAIR_WIRE_RENDERER	     (visu_pair_wire_renderer_get_type ())
/**
 * VISU_PAIR_WIRE_RENDERER:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuPairWireRenderer type.
 *
 * Since: 3.8
 */
#define VISU_PAIR_WIRE_RENDERER(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_PAIR_WIRE_RENDERER, VisuPairWireRenderer))
/**
 * VISU_PAIR_WIRE_RENDERER_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuPairWireRendererClass.
 *
 * Since: 3.8
 */
#define VISU_PAIR_WIRE_RENDERER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_PAIR_WIRE_RENDERER, VisuPairWireRendererClass))
/**
 * VISU_IS_PAIR_WIRE_RENDERER:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuPairWireRenderer object.
 *
 * Since: 3.8
 */
#define VISU_IS_PAIR_WIRE_RENDERER(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_PAIR_WIRE_RENDERER))
/**
 * VISU_IS_PAIR_WIRE_RENDERER_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuPairWireRendererClass class.
 *
 * Since: 3.8
 */
#define VISU_IS_PAIR_WIRE_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_PAIR_WIRE_RENDERER))
/**
 * VISU_PAIR_WIRE_RENDERER_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 *
 * Since: 3.8
 */
#define VISU_PAIR_WIRE_RENDERER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_PAIR_WIRE_RENDERER, VisuPairWireRendererClass))

typedef struct _VisuPairWireRenderer        VisuPairWireRenderer;
typedef struct _VisuPairWireRendererClass   VisuPairWireRendererClass;

/**
 * VisuPairWireRenderer:
 *
 * An opaque structure.
 */
struct _VisuPairWireRenderer
{
  VisuPairLinkRenderer parent;
};

/**
 * VisuPairWireRendererClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuPairWireRendererClass structure.
 */
struct _VisuPairWireRendererClass
{
  VisuPairLinkRendererClass parent;
};

/**
 * visu_pair_wire_renderer_get_type:
 *
 * This method returns the type of #VisuPairWireRenderer, use
 * VISU_TYPE_PAIR_WIRE_RENDERER instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuPairWireRenderer.
 */
GType visu_pair_wire_renderer_get_type(void);

VisuPairWireRenderer* visu_pair_wire_renderer_new();

#endif
