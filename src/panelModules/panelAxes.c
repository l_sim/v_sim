/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelAxes.h"

#include <support.h>
#include <gtk_main.h>
#include <uiElements/ui_axes.h>
#include <uiElements/ui_box.h>
#include <uiElements/ui_scale.h>
#include <extensions/legend.h>

/**
 * SECTION: panelAxes
 * @short_description: The tab where axes, box, scale and legend are
 * setup.
 *
 * <para>Nothing tunable here.</para>
 */

/* Sensitive widget in this subpanel. */
static GtkWidget *panelAxes = NULL;
static GtkWidget *lineBox;
static GtkWidget *lineAxes;
static GtkWidget *lineScales;

static GtkWidget *checkLegend;
static GBinding *bind_legend = NULL;

/* Private functions. */
static GtkWidget *createInteriorAxes(VisuGlNodeScene *scene);

/* Local callbacks. */

/**
 * visu_ui_panel_axes_init:
 * @main: a #VisuUiMain object.
 *
 * This routine will create the #VisuUiPanel where the axes and the
 * label stuffs can be tuned, such as their colour, create scales...
 *
 * Returns: (transfer full): the #VisuUiPanel object dealing with axes.
 */
VisuUiPanel* visu_ui_panel_axes_init(VisuUiMain *main)
{
  VisuGlNodeScene *scene;

  if (panelAxes)
    {
      g_object_ref(G_OBJECT(panelAxes));
      return VISU_UI_PANEL(panelAxes);
    }

  panelAxes = visu_ui_panel_newWithIconFromPath("Panel_axes", _("Box, axes and labels"),
                                                _("Frames/labels"), "stock-axes_20.png");
  g_return_val_if_fail(panelAxes, (VisuUiPanel*)0);

  scene = visu_ui_rendering_window_getGlScene(visu_ui_main_getRendering(main));
  gtk_container_add(GTK_CONTAINER(panelAxes), createInteriorAxes(scene));
  visu_ui_panel_setDockable(VISU_UI_PANEL(panelAxes), TRUE);

  g_object_ref(panelAxes);
  g_debug("Panel Axes: new panel with ref count %d",
              G_OBJECT(panelAxes)->ref_count);
  return VISU_UI_PANEL(panelAxes);
}
/**
 * visu_ui_panel_axes_setAxesExtension:
 * @axes: (transfer full) (allow-none): a #VisuGlExtAxes object.
 *
 * Set the current axes extension handled by this #VisuUiPanel.
 *
 * Since: 3.7
 **/
void visu_ui_panel_axes_setAxesExtension(VisuGlExtAxes *axes)
{
  visu_ui_axes_bind(VISU_UI_AXES(lineAxes), axes);
}
/**
 * visu_ui_panel_axes_setBoxExtension:
 * @box: (transfer full) (allow-none): a #VisuGlExtBox object.
 * @legend: (transfer full) (allow-none): a #VisuGlExtBoxLegend object.
 *
 * Set the current box extension handled by this #VisuUiPanel.
 *
 * Since: 3.8
 **/
void visu_ui_panel_axes_setBoxExtension(VisuGlExtBox *box, VisuGlExtBoxLegend *legend)
{
  visu_ui_box_bind(VISU_UI_BOX(lineBox), box);
  visu_ui_box_bindLegend(VISU_UI_BOX(lineBox), legend);
}
/**
 * visu_ui_panel_axes_setLegendExtension:
 * @legend: (transfer full) (allow-none): a #VisuGlExtLegend object.
 *
 * Set the current legend extension handled by this #VisuUiPanel.
 * To be activated later ...
 *
 * Since: 3.8
 **/
static void visu_ui_panel_axes_setLegendExtension(VisuGlExtLegend *legend)
{
  if (bind_legend)
    g_object_unref(bind_legend);
  if (legend)
    bind_legend =
      g_object_bind_property(legend, "active", checkLegend, "active",
                             G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
}
/**
 * visu_ui_panel_axes_setScaleExtension:
 * @scale: (transfer full) (allow-none): a #VisuGlExtScale object.
 *
 * Set the current scale extension handled by this #VisuUiPanel.
 *
 * Since: 3.8
 **/
void visu_ui_panel_axes_setScaleExtension(VisuGlExtScale *scale)
{
  visu_ui_scale_bind(VISU_UI_SCALE(lineScales), scale);
}

static GtkWidget *createInteriorAxes(VisuGlNodeScene *scene)
{
  GtkWidget *vbox, *hbox, *scrollView;
  GtkWidget *label;

  scrollView = gtk_scrolled_window_new((GtkAdjustment*)0,
				       (GtkAdjustment*)0);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollView),
                                 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrollView), GTK_SHADOW_NONE);

  vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_widget_set_margin_top(vbox, 5);
  gtk_widget_set_margin_start(vbox, 5);
  gtk_widget_set_margin_end(vbox, 5);
  gtk_container_add(GTK_CONTAINER(scrollView), vbox);

  /*********************/
  /* The Bounding box. */
  /*********************/
  lineBox = visu_ui_box_new();
  gtk_box_pack_start(GTK_BOX(vbox), lineBox, FALSE, FALSE, 0);
  visu_ui_panel_axes_setBoxExtension(visu_gl_node_scene_getBox(scene),
                                     visu_gl_node_scene_getBoxLegend(scene));

  /*************/
  /* The Axes. */
  /*************/
  lineAxes = visu_ui_axes_new();
  gtk_widget_set_margin_top(lineAxes, 15);
  gtk_box_pack_start(GTK_BOX(vbox), lineAxes, FALSE, FALSE, 0);
  visu_ui_panel_axes_setAxesExtension(visu_gl_node_scene_getAxes(scene));
  g_object_bind_property(scene, "axes-from-box",
                         visu_ui_axes_getBasisCheckButton(VISU_UI_AXES(lineAxes)), "active",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);

  /**************/
  /* The legend */
  /**************/
  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_widget_set_margin_top(hbox, 15);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

  /* The drawn checkbox. */
  checkLegend = gtk_check_button_new();
  gtk_box_pack_start(GTK_BOX(hbox), checkLegend, FALSE, FALSE, 0);

  /* The label. */
  label = gtk_label_new(_("<b>Legend</b>"));
  gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_widget_set_name(label, "label_head");
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
  visu_ui_panel_axes_setLegendExtension(visu_gl_node_scene_getLegend(scene));

  /*************************/
  /* Adding scale widgets. */
  /*************************/
  lineScales = visu_ui_scale_new();
  gtk_widget_set_margin_top(lineScales, 15);
  gtk_box_pack_start(GTK_BOX(vbox), lineScales, FALSE, FALSE, 0);
  visu_ui_panel_axes_setScaleExtension(visu_gl_node_scene_getScales(scene));

  gtk_widget_show_all(scrollView);

  return scrollView;
}
