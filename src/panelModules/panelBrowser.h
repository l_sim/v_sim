/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef PANELBROWSER_H
#define PANELBROWSER_H

#include <glib.h>
#include <gtk/gtk.h>

#include <extraGtkFunctions/gtk_toolPanelWidget.h>

/**
 * VISU_UI_PANEL_BROWSER_PREVIOUS:
 *
 * Value that give the direction when the selector is moved around file list.
 * See visu_ui_panel_browser_getNextSelected().
 */
#define VISU_UI_PANEL_BROWSER_PREVIOUS 0
/**
 * VISU_UI_PANEL_BROWSER_NEXT:
 *
 * Value that give the direction when the selector is moved around file list.
 * See visu_ui_panel_browser_getNextSelected().
 */
#define VISU_UI_PANEL_BROWSER_NEXT     1

/**
 * visu_ui_panel_browser_init: (skip)
 *
 * Should be used in the list declared in externalModules.h to be loaded by
 * V_Sim on start-up. This routine will create the #VisuUiPanel handling
 * the browser.
 *
 * Returns: a newly created #VisuUiPanel object.
 */
VisuUiPanel* visu_ui_panel_browser_init();

/**
 * visu_ui_panel_browser_getNextSelected:
 * @path: a pointer to returned the path of the newly selected file ;
 * @iterSelected: a pointer to store the newly selected iter ;
 * @direction: VISU_UI_PANEL_BROWSER_NEXT or VISU_UI_PANEL_BROWSER_PREVIOUS.
 *
 * Change the selected file in the browser given the direction.
 *
 * Returns: TRUE if one exists.
 */
gboolean visu_ui_panel_browser_getNextSelected(GtkTreePath **path,
				      GtkTreeIter *iterSelected, int direction);
/**
 * visu_ui_panel_browser_getCurrentSelected:
 * @path: a pointer to returned the path of the currently selected file ;
 * @iterSelected: a pointer to store the currently selected iter.
 *
 * Get iter and path of the currently sleected file.
 *
 * Returns: TRUE if one exists.
 */
gboolean visu_ui_panel_browser_getCurrentSelected(GtkTreePath **path,
					 GtkTreeIter *iterSelected);
/**
 * visu_ui_panel_browser_setCurrentDirectory:
 * @dir: the path of a directory.
 *
 * Change the directory for the browser. The directory is not parsed immediately
 * but only when the subpanel becomes visible.
 */
void visu_ui_panel_browser_setCurrentDirectory(const gchar *dir);
/**
 * visu_ui_panel_browser_setCurrentDirectories:
 * @dirs: a NULL terminated array of directories to be loaded.
 *
 * Change the directories for the browser. It is the same routine than 
 * visu_ui_panel_browser_setCurrentDirectory(), but several directories can be loaded
 * at once. But internally, contrary to visu_ui_panel_browser_setCurrentDirectory()
 * the given array must not be freed since it is not copied.
 */
void visu_ui_panel_browser_setCurrentDirectories(gchar **dirs);

void visu_ui_panel_browser_setMessage(const gchar* message, GtkMessageType message_type);


#endif

