/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD, Damien
	CALISTE, Olivier D'Astier, laboratoire L_Sim, (2001-2005)
  
	Adresses mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD and Damien
	CALISTE and Olivier D'Astier, laboratoire L_Sim, (2001-2005)

	E-mail addresses :
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.
	D'ASTIER, dastier AT iie P cnam P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "panelSurfacesTools.h"
#include "panelSurfaces.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <support.h>
#include <visu_gtk.h>
#include <visu_basic.h>
#include <extraFunctions/pot2surf.h>
#include <extraFunctions/surfaces.h>
#include <gtk_renderingWindowWidget.h>

/**
 * SECTION:panelSurfacesTools
 * @short_description: Gtk interface to manage isosurfaces.
 *
 * <para>This module contains the panel used to manage your .surf
 * files. At the moment, only two tools are availiable for you to
 * use. <link linkend="v-sim-pot2surf">pot2surf</link> will allow you
 * to create .surf files from .pot files. There is also a .surf file
 * merger which is so short that we beliedved it didn't need it's own
 * module.</para>
 */

static GtkWidget *convert_window;
static gboolean convert_w_opened = FALSE;

static GtkListStore* list_store = NULL;
static GtkListStore* list_store_target = NULL;

static GtkWidget* tree_view_source = NULL;
static GtkWidget* tree_view_target = NULL;

enum {
  MERGE_SURF_NAME,
  MERGE_NB_POINTS,
  MERGE_NB_VERTEX,
  MERGE_FILENAME,
  MERGE_NEW_SURF_NAME,
  MERGE_SHORT_FILENAME,
  MERGE_N_COLUMNS
};

static GtkWidget *surfmerge_source_ddd[6];
static GtkWidget *surfmerge_target_ddd[6];

static GtkWidget *entry_current_file;
static GtkWidget *entry_target_file;

void surf_add_surf_to_target();

/* Parses target surf file to retrieve dxx, ..., dzz,
   as well as each surface name, nb of vertex and nb of points. */
gboolean surf_simply_parse_file(char *file_name)
{
  GString *line = g_string_new("");
  gchar **surf_names = NULL;
  gsize terminator_pos;
  int last_op_status;
  int number_of_surfaces;
  int i;
  int *nb_vertex, *nb_points;
  float ddd[6];
  char float_to_string[128];
  gboolean ddd_are_null = TRUE;
  gboolean ddd_are_equal = TRUE;

  GIOChannel* file_to_parse = g_io_channel_new_file(file_name, "r", NULL);

  if(file_to_parse == NULL)
    return FALSE;

  /* First line is arbitrary */
  last_op_status = 
    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
  if(last_op_status != G_IO_STATUS_NORMAL)
    return FALSE;
     
  /* Second line contains dxx, dyx and dyy values */
  last_op_status = 
    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
  if(last_op_status != G_IO_STATUS_NORMAL ||
     sscanf(line->str, "%f %f %f", &ddd[0], &ddd[1], &ddd[2]) != 3)
    return FALSE;

  /* Third line contains dzx, dzy and dzz values */
  last_op_status = 
    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
  if(last_op_status != G_IO_STATUS_NORMAL ||
     sscanf(line->str, "%f %f %f", &ddd[3], &ddd[4], &ddd[5]) != 3)
    return FALSE;

  /* Fourth line contains nsurf values (as well as two other values) */
  last_op_status = 
    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
  if(last_op_status != G_IO_STATUS_NORMAL ||
     sscanf(line->str, "%d", &number_of_surfaces) != 1)
    return FALSE;

  if(number_of_surfaces <= 0)
    return FALSE;
  
  surf_names = g_malloc(number_of_surfaces * sizeof(gchar *));
  nb_vertex = g_malloc(number_of_surfaces * sizeof(int));
  nb_points = g_malloc(number_of_surfaces * sizeof(int));
  
  for(i=0; i<number_of_surfaces; i++)
    {
      int j;

      do
	{
	  last_op_status = 
	    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
	  if(last_op_status != G_IO_STATUS_NORMAL)
	    return FALSE;
	}
      while (line->str[0] == '#');

      surf_names[i] = g_strdup(line->str);
      g_strdelimit(surf_names[i], "\n", ' ');       
      g_strstrip(surf_names[i]);

      last_op_status = 
	g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL ||
	 sscanf(line->str, "%d %d", &nb_vertex[i], &nb_points[i]) != 2)
	return FALSE;

      for(j=0; j<(nb_vertex[i]+nb_points[i]); j++)
	{
	  last_op_status = 
	    g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
	  if(last_op_status != G_IO_STATUS_NORMAL)
	    return FALSE;
	}
    }
      
  gtk_list_store_clear(list_store); 

  for(i=0; i<6; i++)
    {
      sprintf(float_to_string, "%f", ddd[i]);
      gtk_entry_set_text(GTK_ENTRY(surfmerge_source_ddd[i]), float_to_string);

      ddd_are_null = ddd_are_null 
	&& (atof(gtk_entry_get_text(GTK_ENTRY(surfmerge_target_ddd[i]))) == 0.);
      ddd_are_equal = ddd_are_equal
	&& (atof(gtk_entry_get_text(GTK_ENTRY(surfmerge_target_ddd[i]))) == ddd[i]);
    }
  if(ddd_are_null)
    for(i=0; i<6; i++)
      gtk_entry_set_text(GTK_ENTRY(surfmerge_target_ddd[i]), 
			 gtk_entry_get_text(GTK_ENTRY(surfmerge_source_ddd[i])));
  else if(!ddd_are_equal)
    {
      GtkWidget* choice =  
	gtk_dialog_new_with_buttons(_("Found different dxx, ..., dzz"),
				    GTK_WINDOW(convert_window), GTK_DIALOG_MODAL,
				    _("_Cancel"), GTK_RESPONSE_CANCEL,
				    _("Keep current values"), GTK_RESPONSE_YES,
				    _("Change values to new ones"), GTK_RESPONSE_NO, NULL);
      
      gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(choice))), 
			 gtk_label_new(_("Current dxx, ..., dzz, doesn't match the ones used in the "
					 "file you are trying to load. Do you want to keep old "
					 "dxx, ..., dzz ? (if you don't know exactly what you're doing, "
					 "just cancel and load another file)")),
			 TRUE, TRUE, 0);
      gtk_widget_show_all(choice);
      switch(gtk_dialog_run (GTK_DIALOG (choice)))
	{
	case GTK_RESPONSE_CANCEL:
	  gtk_widget_destroy(choice);
	  return FALSE;
	  break;
	case GTK_RESPONSE_NO:
	  for(i=0; i<6; i++)
	    gtk_entry_set_text(GTK_ENTRY(surfmerge_target_ddd[i]),
			       gtk_entry_get_text(GTK_ENTRY(surfmerge_source_ddd[i])));
	  gtk_widget_destroy(choice);
	  break;
	case GTK_RESPONSE_YES:
	default:
	  gtk_widget_destroy(choice);
	  break;
	}
    }
  
  for(i=0; i<number_of_surfaces; i++)
    {
      GtkTreeIter iter;
      gtk_list_store_insert(list_store, &iter, i);
      gtk_list_store_set(list_store, &iter,
			 MERGE_SURF_NAME, surf_names[i],
			 MERGE_NB_VERTEX, nb_vertex[i],
			 MERGE_NB_POINTS, nb_points[i], -1);
      g_free(surf_names[i]);
    }
  
  g_io_channel_unref(file_to_parse);
  return TRUE;
}

/* Opens a file chooser asking the user to select a surf file and then parses
   this file using surf_simply_parse_file(). If info can be retrieved from the selected
   file, set the correspondig entry to selected file. */
void surf_open_file_chooser()
{
  GtkFileFilter *filter_surf = gtk_file_filter_new();
  GtkFileFilter *filter_none = gtk_file_filter_new();
  GtkWidget* file_chooser = 
    gtk_file_chooser_dialog_new("Open .surf file", GTK_WINDOW(convert_window),
				GTK_FILE_CHOOSER_ACTION_OPEN,
				_("_Cancel"), GTK_RESPONSE_CANCEL,
                                _("_Open"), GTK_RESPONSE_ACCEPT,
				NULL);
  const gchar *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());

  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);

  gtk_file_filter_add_pattern(filter_surf, "*.surf");
  gtk_file_filter_set_name(filter_surf, "Isosurfaces files (*.surf)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_surf);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    {
      gchar *current_file;
      switch(gtk_dialog_run (GTK_DIALOG (file_chooser)))
	{	  
	case GTK_RESPONSE_ACCEPT:
	  current_file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_chooser));
	  if(surf_simply_parse_file(current_file))
	    {
	      gtk_entry_set_text(GTK_ENTRY(entry_current_file), current_file);
	      gtk_widget_destroy (file_chooser);
	      return;	  
	    }
	  else
	    {
	      gtk_entry_set_text(GTK_ENTRY(entry_current_file), "");
	      gtk_list_store_clear(list_store); 
	      visu_ui_raiseWarning(_("Loading a file"),
				   _("Unable to parse the selected file."),
				   (GtkWindow*)0);
	    }
	  break;
	case GTK_RESPONSE_CANCEL:
	default:
	  gtk_widget_destroy (file_chooser);
	  return;	  
	}
    }
}

/* Opens a file chooser asking the user to select a surf file and 
   set the correspondig entry to selected file. */
void surfmerge_target_file_chooser(GtkWidget *widget _U_, gpointer data _U_)
{
  GtkFileFilter *filter_surf = gtk_file_filter_new();
  GtkFileFilter *filter_none = gtk_file_filter_new();
  GtkWidget* file_chooser = 
    gtk_file_chooser_dialog_new("Save as .surf file", GTK_WINDOW(convert_window),
				GTK_FILE_CHOOSER_ACTION_SAVE,
				_("_Cancel"), GTK_RESPONSE_CANCEL,
				_("_Save"), GTK_RESPONSE_ACCEPT,
				NULL);
  const gchar *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);


  gtk_file_filter_add_pattern(filter_surf, "*.surf");
  gtk_file_filter_set_name(filter_surf, "Isosurfaces files (*.surf)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_surf);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    {
      gchar *target_file;
      switch(gtk_dialog_run (GTK_DIALOG (file_chooser)))
	{	  
	case GTK_RESPONSE_ACCEPT:
	  target_file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (file_chooser));
	  gtk_entry_set_text(GTK_ENTRY(entry_target_file), target_file);
	  gtk_widget_destroy (file_chooser);
	  return;	  
	  break;
	case GTK_RESPONSE_CANCEL:
	default:
	  gtk_widget_destroy (file_chooser);
	  return;	  
	}
    }
}

/* Callback for when a user specifies a custom name for a surface.
   Register the given name as the new name of the surface. */
void surfmerge_update_surf_name(GtkCellRendererText *cellrenderertext _U_,
			       gchar *path, gchar *text, gpointer user_data _U_)
{
  GtkTreeIter iter;
  if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(list_store_target),
					 &iter, path))
      gtk_list_store_set(list_store_target, &iter,
			 MERGE_NEW_SURF_NAME, g_strdelimit(g_strstrip(text), " ", '_'), -1);
}

/* Callback for when a user removes a surface from the list */  
void surfmerge_remove_surf(GtkWidget *button _U_, gpointer data _U_)
{
  GtkTreeIter iter, iter2;
  GtkTreeModel *model;
  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_target));
  gboolean list_empty = TRUE;

  if (gtk_tree_selection_get_selected (tree_selection, &model, &iter))
    {
      iter2 = iter;
      if(gtk_tree_model_iter_previous(model, &iter2))
	list_empty = FALSE;
      if(gtk_list_store_remove(list_store_target, &iter))
	gtk_tree_selection_select_iter(tree_selection, &iter);
      else if(list_empty == FALSE)
	gtk_tree_selection_select_iter(tree_selection, &iter2);
    }
}


/* Callback for when a user decides to move up or down a surface.
   data == GINT_TO_POINTER(1) : move up
   data == GINT_TO_POINTER(-1) : move down
*/
void surfmerge_move_surf(GtkWidget *button _U_, gpointer data)
{
  GtkTreeIter iter, iter2;
  GtkTreeModel *model;
  int direction;

  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_target));

  if(data == NULL)
    return;

  direction = GPOINTER_TO_INT(data);
  if(direction != -1 && direction != 1)
    return;

  if(!gtk_tree_selection_get_selected(tree_selection, &model, &iter))
    return;

  iter2 = iter;
  if(direction == 1)	
    {
      if(gtk_tree_model_iter_next(model, &iter2))
	gtk_list_store_move_after(list_store_target, &iter, &iter2);
    }
  else if(direction == -1)
    {
      if(gtk_tree_model_iter_previous(model, &iter2))
	gtk_list_store_move_before(list_store_target, &iter, &iter2);
    }
}	  

/* Sets up the model/view used to represent the surfaces present in a file,
   as well as the surfaces the user wants to put in the files he builds. */
void surf_create_tree_views()
{
  GtkCellRenderer* surf_name = gtk_cell_renderer_text_new ();  
  GtkTreeViewColumn* column = NULL;
  GtkTreeSelection *tree_selection;

  list_store_target = 
    gtk_list_store_new (MERGE_N_COLUMNS, G_TYPE_STRING, G_TYPE_INT, 
			G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  list_store = 
    gtk_list_store_new (MERGE_N_COLUMNS -3, G_TYPE_STRING, G_TYPE_INT,
			G_TYPE_INT/*, G_TYPE_STRING*/);
  g_object_set(G_OBJECT(surf_name), "editable", TRUE, NULL);

  tree_view_source = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list_store));
  tree_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_source));
  gtk_tree_selection_set_mode(tree_selection, GTK_SELECTION_MULTIPLE);

  column = 
    gtk_tree_view_column_new_with_attributes(_("Surface name"), gtk_cell_renderer_text_new(),
					     "text", MERGE_SURF_NAME, NULL);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_source), column);
  
/*   column = */
/*     gtk_tree_view_column_new_with_attributes(_("n points"), gtk_cell_renderer_text_new(), */
/* 					      "text", MERGE_NB_POINTS, NULL); */
/*   gtk_tree_view_column_set_expand(column, FALSE); */
/*   gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_source), column); */

/*   column = */
/*     gtk_tree_view_column_new_with_attributes(_("n vertex"), gtk_cell_renderer_text_new(), */
/* 					     "text", MERGE_NB_VERTEX, NULL); */
/*   gtk_tree_view_column_set_expand(column, FALSE); */
/*   gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_source), column); */

  g_signal_connect(G_OBJECT(tree_view_source), "row_activated", surf_add_surf_to_target, NULL);

  tree_view_target = gtk_tree_view_new_with_model(GTK_TREE_MODEL(list_store_target));
  
  column = 
    gtk_tree_view_column_new_with_attributes(_("New name"), surf_name,
					      "text", MERGE_NEW_SURF_NAME, NULL);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_target), column);

  g_signal_connect(G_OBJECT(surf_name), "edited",
		   G_CALLBACK(surfmerge_update_surf_name), NULL);
  column = 
    gtk_tree_view_column_new_with_attributes(_("old name"), gtk_cell_renderer_text_new(),
					      "text", MERGE_SURF_NAME, NULL);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_target), column);

  column = 
    gtk_tree_view_column_new_with_attributes(_("from file"), gtk_cell_renderer_text_new(),
					      "text", MERGE_SHORT_FILENAME, NULL);
  gtk_tree_view_column_set_expand(column, TRUE);
  gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_target), column);

/*   column =  */
/*     gtk_tree_view_column_new_with_attributes(_("n points"), gtk_cell_renderer_text_new(), */
/* 					      "text", MERGE_NB_POINTS, NULL); */
/*   gtk_tree_view_column_set_expand(column, FALSE); */
/*   gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_target), column); */

/*   column= */
/*     gtk_tree_view_column_new_with_attributes(_("n vertex"), gtk_cell_renderer_text_new(), */
/* 					      "text", MERGE_NB_VERTEX, NULL); */
/*   gtk_tree_view_column_set_expand(column, FALSE); */
/*   gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view_target), column); */



}
  
/* Adds selected source isosurface to the list of isosurfaces to put in the file to create. */
void surf_add_surf_to_target()
{
  GtkTreeIter iter_source, iter_target, iter_target_new;
  GtkTreeModel *model_source, *model_target;
  GtkTreeSelection* tree_selection_source = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_source));
  GtkTreeSelection* tree_selection_target = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view_target));
  char *surf_name;
  char *short_filename;
  int nb_points, nb_vertex;  
  GList *rows_start, *rows_browse;
  
  rows_browse = rows_start = gtk_tree_selection_get_selected_rows(tree_selection_source, 
								  &model_source);

  while(rows_browse != NULL)
    {
      if(!gtk_tree_model_get_iter(model_source, &iter_source, rows_browse->data))
	break;
      if(gtk_tree_selection_get_selected(tree_selection_target, &model_target, &iter_target))
	{
	  gtk_list_store_insert_after(list_store_target, &iter_target_new, &iter_target);
	  gtk_tree_selection_select_iter(tree_selection_target, &iter_target_new);
	}
      else
	{
	  iter_target_new = iter_target;
	  gtk_list_store_append(list_store_target, &iter_target_new);
	}
      
      gtk_tree_model_get(GTK_TREE_MODEL(list_store), &iter_source,
			 MERGE_SURF_NAME, &surf_name,
			 MERGE_NB_POINTS, &nb_points,
			 MERGE_NB_VERTEX, &nb_vertex, -1);
      short_filename = g_path_get_basename(gtk_entry_get_text(GTK_ENTRY(entry_current_file)));
      g_strdelimit(surf_name, " ", '_');
      gtk_list_store_set(list_store_target, &iter_target_new,
			 MERGE_SURF_NAME, surf_name,
			 MERGE_NB_POINTS, nb_points,
			 MERGE_NB_VERTEX, nb_vertex,
			 MERGE_FILENAME, gtk_entry_get_text(GTK_ENTRY(entry_current_file)),
			 MERGE_NEW_SURF_NAME, surf_name, 
			 MERGE_SHORT_FILENAME, short_filename, -1);
      g_free(surf_name);
      g_free(short_filename);
      gtk_tree_path_free(rows_browse->data);
      rows_browse = rows_browse->next;
    }

  g_list_free(rows_start);  
}
    
/* Only used with a gtk_tree_model_foreach()
   Used to count the total of surfaces, nb_vertex, and nb_points to build a correct .surf file. */
gboolean surfmerge_init_export(GtkTreeModel *model, GtkTreePath *path _U_,
			  GtkTreeIter *iter, gpointer data)
{
  int *total = data;
  int nb_points;
  int nb_vertex;

  gtk_tree_model_get(GTK_TREE_MODEL(model), iter,
		     MERGE_NB_POINTS, &nb_points,
		     MERGE_NB_VERTEX, &nb_vertex, -1);

  total[0]++;
  total[1] += nb_vertex;
  total[2] += nb_points;
  
  return FALSE;
}

/* Only used with a gtk_tree_model_foreach()
   Used to write a surface in the file the user wants to write. */
gboolean surf_export_surf(GtkTreeModel *model, GtkTreePath *path _U_,
			  GtkTreeIter *iter, gpointer data)
{
  FILE *file_to_write = data;
  gchar *old_surf_name;
  gchar * file_name;
  gchar * surf_name;
  int nb_vertex, nb_points, i, j;
  GIOChannel* file_to_read;
  gsize terminator_pos;
  GString *line = g_string_new("");
  int last_op_status;

  gtk_tree_model_get(GTK_TREE_MODEL(model), iter,
		     MERGE_SURF_NAME, &old_surf_name,
		     MERGE_FILENAME, &file_name,
		     MERGE_NEW_SURF_NAME, &surf_name,
		     MERGE_NB_VERTEX, &nb_vertex, 
		     MERGE_NB_POINTS, &nb_points, -1);

  file_to_read = g_io_channel_new_file(file_name, "r", NULL);
  
  g_debug("Surface Merger : writing surface '%s' info ", surf_name);

  for(i=0; i<4; i++)
    {
      last_op_status = 
	g_io_channel_read_line_string (file_to_read, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL)
	{	  
	  g_free(file_name);
	  g_free(surf_name);
	  g_free(old_surf_name);
	  g_string_free(line, TRUE);
	  g_io_channel_unref(file_to_read);
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("An unknown error occured. Your surf"
				 " file is corrupted.\n"), (GtkWindow*)0);
	  return TRUE;
	}
    }

  for(i=0; i>-1; i++)
    {
      gchar *cur_surf_name;
      int surf_nb_vertex, surf_nb_points;
      last_op_status = 
	g_io_channel_read_line_string (file_to_read, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL)
	{
	  g_free(file_name);
	  g_free(surf_name);
	  g_free(old_surf_name);
	  g_string_free(line, TRUE);
	  g_io_channel_unref(file_to_read);
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("An unknown error occured. Your surf"
				 " file is corrupted.\n"), (GtkWindow*)0);
	  return TRUE;
	}
      cur_surf_name = g_strdup(line->str);
      g_strdelimit(cur_surf_name, "\n", ' ');       
      g_strstrip(cur_surf_name);
      
      g_debug("%s, old:%s", cur_surf_name, old_surf_name);
      if(g_strcmp0(cur_surf_name, old_surf_name) == 0)
	  fprintf(file_to_write, "%s\n", surf_name);

      last_op_status = 
	g_io_channel_read_line_string (file_to_read, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL ||
	 sscanf(line->str, "%d %d", &surf_nb_vertex, &surf_nb_points) != 2)
	{
	  g_free(file_name);
	  g_free(surf_name);
	  g_free(old_surf_name);
	  g_string_free(line, TRUE);
	  g_io_channel_unref(file_to_read);
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("An unknown error occured. Your surf"
				 " file is corrupted.\n"), (GtkWindow*)0);
	  return TRUE;
	}
      g_debug("%s, old:%s", cur_surf_name, old_surf_name);
      if(g_strcmp0(cur_surf_name, old_surf_name) == 0)
      	fprintf(file_to_write, "%s", line->str);

      for(j=0; j<(surf_nb_vertex+surf_nb_points); j++)
	{
	  last_op_status = 
	    g_io_channel_read_line_string (file_to_read, line, &terminator_pos, NULL);
	  if(last_op_status != G_IO_STATUS_NORMAL)
	    {
	      g_free(file_name);
	      g_free(surf_name);
	      g_free(old_surf_name);
	      g_string_free(line, TRUE);
	      g_io_channel_unref(file_to_read);
	      visu_ui_raiseWarning(_("Loading a file"),
				   _("An unknown error occured. Your surf"
				     " file is corrupted.\n"), (GtkWindow*)0);
	      return TRUE;
	    }
	  if(g_strcmp0(cur_surf_name, old_surf_name) == 0)
	    fprintf(file_to_write, "%s", line->str);
	}
      g_debug("%s, old:%s", cur_surf_name, old_surf_name);
      if(g_strcmp0(cur_surf_name, old_surf_name) == 0)
	{
	  g_free(cur_surf_name);
	  break;
	}
      g_free(cur_surf_name);
      g_debug("%s, old:%s", cur_surf_name, old_surf_name);
    }
  g_free(file_name);
  g_free(old_surf_name);
  g_free(surf_name);
  g_string_free(line, TRUE);
  g_io_channel_unref(file_to_read);
  return FALSE;
}

/* If all parameters are correctly set, tries to write a valid .surf file
   from the surfaces selected by the user. */
void surf_gogogo()
{
  FILE* file_to_write = fopen(gtk_entry_get_text(GTK_ENTRY(entry_target_file)), "wb");
  int i;
  int total[3];

  if(file_to_write == NULL)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please choose a surf file to write\n"), (GtkWindow*)0);
      return;
    }

  for(i=0; i<3; i++)
    total[i] = 0;

  gtk_tree_model_foreach(GTK_TREE_MODEL(list_store_target),
			 surfmerge_init_export, total); 

  if(total[0] == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("No surface to write\n"), (GtkWindow*)0);
      return;
    }


  if(fprintf(file_to_write, "surf file generated by v_sim's merger\n") < 0)
    return;

  for(i=0; i<6; i++)
    {
      if(fprintf(file_to_write, "%s ", 
		 gtk_entry_get_text(GTK_ENTRY(surfmerge_target_ddd[i]))) < 0)
	return;
      if((i+1)%3==0)
	if(fprintf(file_to_write, "\n") < 0)
	  return;
    }


  if(fprintf(file_to_write, "%d %d %d\n", total[0], total[1], total[2]) < 0)
    return;

  gtk_tree_model_foreach(GTK_TREE_MODEL(list_store_target),
			 surf_export_surf, file_to_write);

  fclose(file_to_write);

  g_debug("Surface Merger : wrote file %s",
	      gtk_entry_get_text(GTK_ENTRY(entry_target_file)));
}

/* See header file for more info */
GtkWidget *visu_ui_panel_surfaces_tools_fileWidget()
{
  enum { DXX, DYX, DYY, DZX, DZY, DZZ };
  GtkWidget* scrolled_window_top = gtk_scrolled_window_new(NULL, NULL);
  GtkWidget* scrolled_window_bot = gtk_scrolled_window_new(NULL, NULL);
  GtkWidget *global_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *hbox1 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *hbox_top_ddd = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
  GtkWidget *hbox_top_ddd2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
  GtkWidget *hbox_bot_ddd = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
  GtkWidget *hbox_bot_ddd2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
  GtkWidget *hbox_go = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *bottom_frame = gtk_frame_new(_("Target info : "));
  GtkWidget *top_frame = gtk_frame_new(_("Source info : "));
  GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
  GtkWidget *vbox_r = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
  GtkWidget *add_surf_button = gtk_button_new();
  GtkWidget *go_button = gtk_button_new_with_label("Build");
  GtkWidget *save_file = gtk_button_new();
  GtkWidget *hbox_target_file = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  char *ddd[] = {"dxx :", "dyx :", "dyy :",
	       "dzx :", "dzy :", "dzz :"};
  GtkWidget *label;
  GtkWidget *remove_surf_button = gtk_button_new();
  GtkWidget *move_down_surf_button = gtk_button_new();
  GtkWidget *move_up_surf_button = gtk_button_new();
  int i;
  GtkWidget *open_file = gtk_button_new();
  GtkWidget *manage_top_tview_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *manage_bot_tview_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *top_tview_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *bot_tview_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *image_add = gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_remove = gtk_image_new_from_icon_name("list-remove", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_go_down = gtk_image_new_from_icon_name("go-down", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_go_up = gtk_image_new_from_icon_name("go-up", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_open = gtk_image_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);

  entry_current_file = gtk_entry_new();
  entry_target_file = gtk_entry_new();

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window_top),
				 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window_bot),
				 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);

  gtk_container_add (GTK_CONTAINER (add_surf_button), image_add);
  gtk_container_add (GTK_CONTAINER (remove_surf_button), image_remove);
  gtk_container_add (GTK_CONTAINER (move_down_surf_button), image_go_down);
  gtk_container_add (GTK_CONTAINER (move_up_surf_button), image_go_up);
  gtk_container_add (GTK_CONTAINER (open_file), image_open);
  image_open = gtk_image_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);
  gtk_container_add (GTK_CONTAINER (save_file), image_open);

  gtk_widget_set_tooltip_text(go_button, 
		       _("Build specified new .surf file"));
  gtk_widget_set_tooltip_text(entry_current_file, 
		       _("Contains the full path to the currently .surf selected file"));
  gtk_widget_set_tooltip_text(entry_target_file, 
		       _("Contains the full path to the .surf file you want to build"));
  gtk_widget_set_tooltip_text(open_file, 
		       _("Allows you to select a .surf file"));
  gtk_widget_set_tooltip_text(save_file, 
		       _("Selects the .surf file to write"));
  gtk_widget_set_tooltip_text(add_surf_button, 
		       _("Moves selected surface to the list of surfaces to build"));
  gtk_widget_set_tooltip_text(move_down_surf_button, 
		       _("Moves down selected surface in the list of surfaces to build"));
  gtk_widget_set_tooltip_text(move_up_surf_button, 
		       _("Moves up selected surface in the list of surfaces to build"));
  gtk_widget_set_tooltip_text(remove_surf_button, 
		       _("Removes selected surface from the list of surfaces to build"));
  
  for(i=0; i<6; i++)
    {
      surfmerge_source_ddd[i] = gtk_entry_new();
      gtk_entry_set_text(GTK_ENTRY(surfmerge_source_ddd[i]), "0.0000000");
      g_object_set(G_OBJECT(surfmerge_source_ddd[i]), "width-chars", 9, NULL);
      gtk_editable_set_editable(GTK_EDITABLE(surfmerge_source_ddd[i]), FALSE);
      gtk_widget_set_tooltip_text(surfmerge_source_ddd[i],
			   _("The d__ of the current selected file"));
    }

  for(i=0; i<6; i++)
    {
      surfmerge_target_ddd[i] = gtk_entry_new();
      gtk_entry_set_text(GTK_ENTRY(surfmerge_target_ddd[i]), "0.0000000");
      g_object_set(G_OBJECT(surfmerge_target_ddd[i]), "width-chars", 9, NULL);
      gtk_widget_set_tooltip_text(surfmerge_target_ddd[i],
			   _("The d__ of the file to build"));
    }
      
  surf_create_tree_views();

  gtk_box_pack_start(GTK_BOX(global_vbox), top_frame, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(global_vbox), bottom_frame, TRUE, TRUE, 0);

  gtk_container_add(GTK_CONTAINER(top_frame), vbox);
  gtk_container_add(GTK_CONTAINER(scrolled_window_top), tree_view_source);

  gtk_box_pack_start(GTK_BOX(vbox), hbox1, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox_top_ddd, FALSE, FALSE, 0); 
  gtk_box_pack_start(GTK_BOX(vbox), hbox_top_ddd2, FALSE, FALSE, 0); 
  
  gtk_box_pack_start(GTK_BOX(vbox), top_tview_hbox, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(hbox1), gtk_label_new(_("Current file : ")), FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox1), entry_current_file, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox1), open_file, FALSE, FALSE, 0);

  for(i=0; i<3; i++)
    {
      label = gtk_label_new(ddd[i]);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);
      gtk_box_pack_start(GTK_BOX(hbox_top_ddd), label, i==0 ? FALSE : TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(hbox_top_ddd), surfmerge_source_ddd[i], FALSE, FALSE, 0);
    }      

  for(i=3; i<6; i++)
    {
      label = gtk_label_new(ddd[i]);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);
      gtk_box_pack_start(GTK_BOX(hbox_top_ddd2), label, i==3 ? FALSE : TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(hbox_top_ddd2), surfmerge_source_ddd[i], FALSE, FALSE, 0);
    }

  gtk_box_pack_start(GTK_BOX(top_tview_hbox), scrolled_window_top, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(top_tview_hbox), manage_top_tview_vbox, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(manage_top_tview_vbox), add_surf_button, TRUE, FALSE, 0);

  gtk_container_add(GTK_CONTAINER(bottom_frame), vbox_r);
  gtk_container_add(GTK_CONTAINER(scrolled_window_bot), tree_view_target);

  gtk_box_pack_start(GTK_BOX(vbox_r), hbox_bot_ddd, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox_r), hbox_bot_ddd2, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox_r), bot_tview_hbox, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(vbox_r), hbox_target_file, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox_r), hbox_go, FALSE, FALSE, 0);

  for(i=0; i<3; i++)
    {
      label = gtk_label_new(ddd[i]);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);
      gtk_box_pack_start(GTK_BOX(hbox_bot_ddd), label, i==0 ? FALSE : TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(hbox_bot_ddd), surfmerge_target_ddd[i], FALSE, FALSE, 0);
    }      

  for(i=3; i<6; i++)
    {
      label = gtk_label_new(ddd[i]);
      gtk_label_set_xalign(GTK_LABEL(label), 1.);
      gtk_box_pack_start(GTK_BOX(hbox_bot_ddd2), label, i==3 ? FALSE : TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(hbox_bot_ddd2), surfmerge_target_ddd[i], FALSE, FALSE, 0);
    }      

  gtk_box_pack_start(GTK_BOX(bot_tview_hbox), scrolled_window_bot, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(bot_tview_hbox), manage_bot_tview_vbox, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(manage_bot_tview_vbox), remove_surf_button, FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(manage_bot_tview_vbox), move_down_surf_button, FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(manage_bot_tview_vbox), move_up_surf_button, FALSE, FALSE, 0);

  gtk_box_pack_start(GTK_BOX(hbox_target_file), gtk_label_new(_("Target file : ")), FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(hbox_target_file), entry_target_file, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox_target_file), save_file, FALSE, FALSE, 0);

  gtk_box_pack_end(GTK_BOX(hbox_go), go_button, FALSE, FALSE, 0);

  /* Signal connexion */
  g_signal_connect(G_OBJECT(open_file), "clicked",
		   G_CALLBACK(surf_open_file_chooser), entry_current_file);
  g_signal_connect(G_OBJECT(add_surf_button), "clicked",
		   G_CALLBACK(surf_add_surf_to_target), NULL);
  g_signal_connect(G_OBJECT(go_button), "clicked",
		   G_CALLBACK(surf_gogogo), NULL);
  g_signal_connect(G_OBJECT(remove_surf_button), "clicked",
		   G_CALLBACK(surfmerge_remove_surf), NULL);
  g_signal_connect(G_OBJECT(move_up_surf_button), "clicked",
		   G_CALLBACK(surfmerge_move_surf), GINT_TO_POINTER(-1));
  g_signal_connect(G_OBJECT(move_down_surf_button), "clicked",
		   G_CALLBACK(surfmerge_move_surf), GINT_TO_POINTER(1));
  g_signal_connect(G_OBJECT(save_file), "clicked",
		   G_CALLBACK(surfmerge_target_file_chooser), NULL);

  return global_vbox;
}
  
/*****************************************************************************/
/*                                                                           */
/*                               POT2SURF                                    */
/*                                                                           */
/*****************************************************************************/

static GtkListStore *pot2surf_list_store = NULL;
static GtkWidget *pot2surf_tree_view = NULL;

static GtkWidget *pot2surf_entry_source_pot_file = NULL;
static GtkWidget *pot2surf_entry_target_surf_file = NULL;

static GtkWidget *pot2surf_potmax = NULL;
static GtkWidget *pot2surf_potmin = NULL;

enum {
  POT2SURF_NAME,
  POT2SURF_VALUE,
  POT2SURF_N_COLUMNS
};

/* Pops up a window containing info about the pot2surf routine 
   Currently unused. */
void pot2surf_popup_help()
{
  GtkWidget *window;
  gsize length;
  gchar *text = NULL;
  GtkTextBuffer *text_buf;
  GtkWidget *text_view;
  GtkWidget *scrolled_window;
  gchar *chemin = g_build_filename(V_SIM_LEGAL_DIR, "pot2surf_help", NULL);

  if(!g_file_get_contents(chemin, &text, &length, (GError**)0))
    {
      g_free(chemin);
      return;
    }
  g_free(chemin);

  text_buf = gtk_text_buffer_new(NULL);
  text_view = gtk_text_view_new_with_buffer(text_buf);
  scrolled_window = gtk_scrolled_window_new(NULL, NULL);

  gtk_text_buffer_insert_at_cursor(text_buf, text, -1);
  g_free(text);

  /* Create a new dialog window for the scrolled window to be
   * packed into.  */
  window = gtk_dialog_new_with_buttons(_("pot2surf_help"), NULL, 0, 
				       _("_Open"), GTK_RESPONSE_ACCEPT, NULL);
  gtk_container_set_border_width (GTK_CONTAINER (window), 0);
  gtk_widget_set_size_request (window, 600, 300);
    
  /* create a new scrolled window. */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

  /* The dialog window is created with a vbox packed into it. */
  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(window))),
		     scrolled_window, TRUE, TRUE, 0);

  gtk_container_add(GTK_CONTAINER(scrolled_window), text_view);
    
  gtk_widget_show_all (window);

  switch(gtk_dialog_run(GTK_DIALOG(window)))
    {
    default:
      gtk_widget_destroy(window);
    }
}

/* Parses target to file to retrieve pot_min and pot_max 
 Returns TRUE in case of success, FALSE otherwise. */
gboolean pot2surf_quick_parse(char *file_name, float *pot_min, float *pot_max)
{
  GString *line = g_string_new("");
  int last_op_status;
  int na, nb, nc;
  int i, j;
  float current_value;
  gsize terminator_pos;
  gchar **float_tab;
  GIOChannel* file_to_parse = g_io_channel_new_file(file_name, "r", NULL);

  if(file_to_parse == NULL)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Can't open given file for reading"), (GtkWindow*)0);
      g_io_channel_unref(file_to_parse);
      return FALSE;
    }

  /* Read the first five lines */
  for(i=0; i<5; i++)
    {
      last_op_status = 
	g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL)
	{
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("This file doesn't seem to be a correct pot file"),
			       (GtkWindow*)0);	 
	  g_io_channel_unref(file_to_parse);
	  return FALSE;
	}
      if(i==1)
	{
	  sscanf(line->str, "%d %d %d", &na, &nb, &nc);
	  if(na <= 0 || nb <= 0 || nc <= 0)
	    {
	      visu_ui_raiseWarning(_("Loading a file"),
				   _("Second line seem to contain incorrect values"),
				   (GtkWindow*)0);	  
	      g_io_channel_unref(file_to_parse);
	      return FALSE;
	    }
	}	    
    }

  /* Retrieve pot values to find min & max in the following lines.
     According to .pot files specifications, the following lines are in free format */
  for(i=0; i<na*nb*nc; )
    {
      last_op_status = 
	g_io_channel_read_line_string (file_to_parse, line, &terminator_pos, NULL);
      if(last_op_status != G_IO_STATUS_NORMAL)
	{
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("This file doesn't seem to be a correct pot file"),
			       (GtkWindow*)0);	  
	  g_io_channel_unref(file_to_parse);
	  return FALSE;
	}

      float_tab = g_strsplit(line->str, " ", -1);
      for(j=0; float_tab[j] != NULL; j++)
	{
	  if(sscanf(float_tab[j], "%f", &current_value) == 1)
	    {
	      if(i==0) *pot_max = *pot_min = current_value;
	      else
		if(current_value < *pot_min) *pot_min = current_value;
		else if(current_value > *pot_max) *pot_max = current_value;
	      i++;
	    }
	}
      g_strfreev(float_tab);

    }
  g_io_channel_unref(file_to_parse);

  return TRUE;
}

/* Opens a file chooser for the user to select a pot file.
   This file is parsed using  pot2surf_quick_parse() to try to retrieve pot_in and pot_max.
   If parsing is successful, set entries to default values (ie : file_selected.surf and
   file_selected.instruc) */
void pot2surf_select_pot_file(GtkWidget *widget _U_, gpointer data _U_)
{
  GtkFileFilter *filter_pot = gtk_file_filter_new();
  GtkFileFilter *filter_none = gtk_file_filter_new();
  GtkWidget* file_chooser = 
    gtk_file_chooser_dialog_new("Select a .pot file", GTK_WINDOW(convert_window),
				GTK_FILE_CHOOSER_ACTION_OPEN,
				_("_Cancel"), GTK_RESPONSE_CANCEL,
/* 				GTK_STOCK_HELP, GTK_RESPONSE_HELP, */
				_("_Open"), GTK_RESPONSE_ACCEPT, NULL);
  const gchar *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  gchar *dir;
  
  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);

  gtk_file_filter_add_pattern(filter_pot, "*.pot");
  gtk_file_filter_add_pattern(filter_pot, "*.dat");
  gtk_file_filter_set_name(filter_pot, "Potential files (*.pot, *.dat)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_pot);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    {
      switch(gtk_dialog_run(GTK_DIALOG(file_chooser)))
	{
	  float pot_min, pot_max;
	  char float_to_string[128];
	  gchar *selected_file;
	  GString *other_files;
	case GTK_RESPONSE_ACCEPT:
	  selected_file = 
	    gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));
	  
	  if(pot2surf_quick_parse(selected_file, &pot_min, &pot_max))
	    {
	      sprintf(float_to_string, "%f", pot_min);
	      gtk_entry_set_text(GTK_ENTRY(pot2surf_potmin), float_to_string);
	      sprintf(float_to_string, "%f", pot_max);
	      gtk_entry_set_text(GTK_ENTRY(pot2surf_potmax), float_to_string);
	      gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_source_pot_file), selected_file);

	      other_files = g_string_new(selected_file);
	      if(g_str_has_suffix(other_files->str, ".pot"))
		 g_string_erase(other_files, other_files->len-4, -1);
	      g_string_append(other_files, ".surf");
	      gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_target_surf_file), other_files->str);
	      g_string_free(other_files, TRUE);

	      dir = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser));
	      visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                                dir, VISU_UI_DIR_SURFACE);
	      g_free(dir);
	      gtk_list_store_clear(pot2surf_list_store);
	      gtk_widget_destroy(file_chooser);
	      return;
	    }
	  else break;
	case GTK_RESPONSE_HELP:
	  break;
	case GTK_RESPONSE_CANCEL:
	default:
	  dir = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser));
	  visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                            dir, VISU_UI_DIR_SURFACE);
	  g_free(dir);
	  gtk_widget_destroy (file_chooser);
	  return;
	}
    }
}

/* Opens a file chooser to select an instruc file. */
/* void pot2surf_source_instruc_open(GtkWidget *widget, gpointer data) */
/* { */
/*   GtkFileFilter *filter_pot = gtk_file_filter_new (); */
/*   GtkFileFilter *filter_none = gtk_file_filter_new(); */
/*   GtkWidget* file_chooser =  */
/*     gtk_file_chooser_dialog_new("Select an instruction file", GTK_WINDOW(convert_window), */
/* 				GTK_FILE_CHOOSER_ACTION_OPEN, */
/* 				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, */
/* /\* 				GTK_STOCK_HELP, GTK_RESPONSE_HELP, *\/ */
/* 				GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL); */
/*   gchar *directory = visu_ui_getLastOpenDirectory(); */

/*   if (directory) */
/*     gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory); */
/*   gtk_file_filter_add_pattern(filter_pot, "*.instruc"); */
/*   gtk_file_filter_set_name(filter_pot, "Instruction files (*.instruc)");   */
/*   gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_pot); */

/*   gtk_file_filter_add_pattern(filter_none, "*"); */
/*   gtk_file_filter_set_name(filter_none, "All files");   */
/*   gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none); */

/*   while(1) */
/*     { */
/*       switch(gtk_dialog_run(GTK_DIALOG(file_chooser))) */
/* 	{ */
/* 	  gchar *selected_file; */
/* 	case GTK_RESPONSE_ACCEPT: */
/* 	  selected_file =  */
/* 	    gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser)); */
/* 	  gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_source_instruc_file), selected_file); */
/*       directory = (char*)gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser)); */
/*       visu_ui_setLastOpenDirectory(directory); */
/*       g_free(directory); */
/* 	  gtk_widget_destroy(file_chooser); */
/* 	  return; */
/* 	case GTK_RESPONSE_HELP: */
/* 	  break; */
/* 	case GTK_RESPONSE_CANCEL: */
/* 	default: */
/*       directory = (char*)gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser)); */
/*       visu_ui_setLastOpenDirectory(directory); */
/*       g_free(directory); */
/* 	  gtk_widget_destroy (file_chooser); */
/* 	  return; */
/* 	} */
/*     } */
/* } */

/* Opens a file chooser to select a surf file. */
void pot2surf_select_surf_file(GtkWidget *widget _U_, gpointer data _U_)
{
  GtkFileFilter *filter_pot = gtk_file_filter_new ();
  GtkFileFilter *filter_none = gtk_file_filter_new();
  GtkWidget* file_chooser = 
    gtk_file_chooser_dialog_new("Select a .surf file", GTK_WINDOW(convert_window),
				GTK_FILE_CHOOSER_ACTION_SAVE,
				_("_Cancel"), GTK_RESPONSE_CANCEL,
/* 				GTK_STOCK_HELP, GTK_RESPONSE_HELP, */
				_("_Open"), GTK_RESPONSE_ACCEPT, NULL);
  const gchar *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  gchar *dir;

  if (directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);

  gtk_file_filter_add_pattern(filter_pot, "*.surf");
  gtk_file_filter_set_name(filter_pot, "Isosurfaces files (*.surf)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_pot);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    {
      switch(gtk_dialog_run(GTK_DIALOG(file_chooser)))
	{
	  gchar *selected_file;

	case GTK_RESPONSE_ACCEPT:
	  selected_file = 
	    gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));
	  gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_target_surf_file), selected_file);
          dir = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser));
          visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                            dir, VISU_UI_DIR_SURFACE);
          g_free(dir);
	  gtk_widget_destroy(file_chooser);
	  return;
	case GTK_RESPONSE_HELP:
	  break;
	case GTK_RESPONSE_CANCEL:
	default:
          dir = gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser));
          visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                            dir, VISU_UI_DIR_SURFACE);
          g_free(dir);
          gtk_widget_destroy (file_chooser);
	  return;
	}
    }
}

/* Callback for when a surf name is changed in the gtk_tree_view */
void pot2surf_update_surf_name(GtkCellRendererText *cellrenderertext _U_,
			       gchar *path, gchar *text, gpointer user_data _U_)
{
  GtkTreeIter iter;

  g_return_if_fail(gtk_tree_model_get_iter_from_string
		   (GTK_TREE_MODEL(pot2surf_list_store), &iter, path));

  g_debug("Panel VisuSurfaceTools: entered text '%s'.", text);
  if (!g_strcmp0(text, VISU_UI_SURFACE_NAME_STR))
    gtk_list_store_set(pot2surf_list_store, &iter,
		       POT2SURF_NAME, VISU_UI_SURFACE_NAME_CHOOSE, -1);
  else if (text && text[0])
    gtk_list_store_set(pot2surf_list_store, &iter,
		       POT2SURF_NAME, g_strdelimit(text, " ", '_'), -1);
  else
    gtk_list_store_set(pot2surf_list_store, &iter,
		       POT2SURF_NAME, VISU_UI_SURFACE_NAME_CHOOSE, -1);
}

/* Callback for when the pot of a surf to build is changed in the gtk_tree_view */
void pot2surf_update_surf_value(GtkCellRendererText *cellrenderertext _U_,
				gchar *path, gchar *text, gpointer user_data _U_)
{
  GtkTreeIter iter;
  float value;
  float pot_min = atof(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmin)));
  float pot_max = atof(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmax)));

  if(sscanf(text, "%f", &value) != 1)
    return;

  if(value <= pot_min || value >= pot_max)
    return;
  
  if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(pot2surf_list_store),
					 &iter, path))
      gtk_list_store_set(pot2surf_list_store, &iter,
			 POT2SURF_VALUE, value, -1);
}

/* Sets up the model/view used to represent the surfaces to build from
   a .pot file. */
void pot2surf_create_tree_view()
{
  GtkCellRenderer* surf_name = gtk_cell_renderer_text_new ();  
  GtkCellRenderer* surf_value = gtk_cell_renderer_text_new ();  
  GtkTreeViewColumn* column1 
    = gtk_tree_view_column_new_with_attributes(_("Surface name"), surf_name,
					      "markup", POT2SURF_NAME, NULL);
  GtkTreeViewColumn* column2
    = gtk_tree_view_column_new_with_attributes(_("Pot value"), surf_value,
					       "text", POT2SURF_VALUE, NULL);

  pot2surf_list_store = gtk_list_store_new (POT2SURF_N_COLUMNS, G_TYPE_STRING, G_TYPE_FLOAT);
  pot2surf_tree_view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(pot2surf_list_store));

  g_object_set(G_OBJECT(surf_name), "editable", TRUE, "foreground", "blue", NULL);
  g_object_set(G_OBJECT(surf_value), "editable", TRUE, "foreground", "blue", NULL);

  gtk_tree_view_column_set_expand (column1, TRUE);
  gtk_tree_view_column_set_expand(column2, FALSE);
  gtk_tree_view_insert_column(GTK_TREE_VIEW(pot2surf_tree_view), column1, -1);  
  gtk_tree_view_insert_column(GTK_TREE_VIEW(pot2surf_tree_view), column2, -1);  

  g_signal_connect(G_OBJECT(surf_name), "edited",
		   G_CALLBACK(pot2surf_update_surf_name), NULL);
  g_signal_connect(G_OBJECT(surf_value), "edited",
		   G_CALLBACK(pot2surf_update_surf_value), NULL);  
}
  
/* Adds a new surface to the list of surfaces to build from a .pot file. */
void pot2surf_add_surf(GtkWidget *button _U_, gpointer data _U_)
{
  GtkTreeIter iter, new_row;
  GtkTreeModel *model;
  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(pot2surf_tree_view));

  if(g_strcmp0("", gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_source_pot_file))) == 0)
    return;

  tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(pot2surf_tree_view));    

  if (gtk_tree_selection_get_selected (tree_selection, &model, &iter))
    gtk_list_store_insert_after(pot2surf_list_store, &new_row, &iter);
  else
    gtk_list_store_append(pot2surf_list_store, &new_row);

  gtk_tree_selection_select_iter(tree_selection, &new_row);
  gtk_list_store_set(pot2surf_list_store, &new_row, 
		     POT2SURF_NAME, VISU_UI_SURFACE_NAME_CHOOSE,
		     POT2SURF_VALUE, 
		     (atof(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmin))) 
		      + atof(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmax)))) / 2, -1);
}

void onSpecialModeToggled(GtkToggleButton *button, gpointer data)
{
  gtk_widget_set_sensitive(GTK_WIDGET(data), gtk_toggle_button_get_active(button));
}

void pot2surf_special_add_surf(GtkWidget *button _U_, gpointer data _U_)
{
  float *values, pot_min, pot_max;
  int nbValues, i;
  gchar *name;
  GtkTreeIter new_row;
  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(pot2surf_tree_view));
  GtkWidget *dialog;

  if(g_strcmp0("", gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_source_pot_file))) == 0)
    return;
  
  pot_min = g_ascii_strtod(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmin)), NULL);
  pot_max = g_ascii_strtod(gtk_entry_get_text(GTK_ENTRY(pot2surf_potmax)), NULL);

  values = (float*)0;
  name = (gchar*)0;
  dialog = visu_ui_panel_surfaces_generateValues(&nbValues, &values, &name,
					      pot_min, pot_max);

  if (!dialog)
    return;

  gtk_widget_destroy(dialog);
  for (i = 0; i < nbValues; i++)
    {
      gtk_list_store_append(pot2surf_list_store, &new_row);
      gtk_list_store_set(pot2surf_list_store, &new_row, 
			 POT2SURF_NAME, (name && name[0])?name:VISU_UI_SURFACE_NAME_CHOOSE,
			 POT2SURF_VALUE, values[i], -1);
    }
  if (nbValues)
    gtk_tree_selection_select_iter(tree_selection, &new_row);

  if (values)
    g_free(values);
  if (name)
    g_free(name);
}
  

/* Removes selected surface from the list of surfaces to build from a .pot file. */
void pot2surf_remove_surf(GtkWidget *button _U_, gpointer data _U_)
{
  GtkTreeIter iter, iter2;
  GtkTreeModel *model;
  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(pot2surf_tree_view));
  gboolean list_empty = TRUE;

  if (gtk_tree_selection_get_selected (tree_selection, &model, &iter))
    {
      iter2 = iter;
      if(gtk_tree_model_iter_previous(model, &iter2))
	list_empty = FALSE;
      if(gtk_list_store_remove(pot2surf_list_store, &iter))
	gtk_tree_selection_select_iter(tree_selection, &iter);
      else if(list_empty == FALSE)
	gtk_tree_selection_select_iter(tree_selection, &iter2);
    }
}


/* Callback for when a user decides to move up or down a surface.
   data == GINT_TO_POINTER(1) : move up
   data == GINT_TO_POINTER(-1) : move down
*/
void pot2surf_move_surf(GtkWidget *button _U_, gpointer data)
{
  GtkTreeIter iter, iter2;
  GtkTreeModel *model;
  int direction;

  GtkTreeSelection* tree_selection = 
    gtk_tree_view_get_selection(GTK_TREE_VIEW(pot2surf_tree_view));

  if(data == NULL)
    return;

  direction = GPOINTER_TO_INT(data);
  if(direction != -1 && direction != 1)
    return;

  if(!gtk_tree_selection_get_selected(tree_selection, &model, &iter))
    return;

  iter2 = iter;
  if(direction == 1)	
    {
      if(gtk_tree_model_iter_next(model, &iter2))
	gtk_list_store_move_after(pot2surf_list_store, &iter, &iter2);
    }
  else if(direction == -1)
    {
      if(gtk_tree_model_iter_previous(model, &iter2))
	gtk_list_store_move_before(pot2surf_list_store, &iter, &iter2);
    }
}	  

/* Only used through a gtk_tree_model_foreach()
   count the number of element present in a GtkTreeModel */
gboolean pot2surf_count_surf(GtkTreeModel *model _U_, GtkTreePath *path _U_,
			     GtkTreeIter *iter _U_, gpointer data)
{
  int *n = data;
  (*n)++;
  return FALSE;
}

/* Only used through a gtk_tree_model_foreach()
   Writes a surface name and value in an instruc file. */
gboolean pot2surf_write_surf(GtkTreeModel *model, GtkTreePath *path _U_,
			     GtkTreeIter *iter, gpointer data)
{
  FILE *instruc = data;
  char *surf_name=NULL;
  float surf_value;
  
  gtk_tree_model_get(GTK_TREE_MODEL(model), iter,
		     POT2SURF_NAME, &surf_name,
		     POT2SURF_VALUE, &surf_value, -1);
  fprintf(instruc, "%f %s\n", surf_value, surf_name);

  g_free(surf_name);

  return FALSE;
}



/* Tries to build a valid .instruc file. */
/* Returns : 0 in case of success
   n != 0 otherwise */
gboolean pot2surf_build_instruc_file(const gchar *pot_file, const gchar *surf_file, const gchar *instruc_file, int n)
{
  FILE *instruc = NULL;
  if(instruc_file == NULL || g_strcmp0(instruc_file, "") == 0)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Please select an instruc file to write"),
			   (GtkWindow*)0);
      return FALSE;
    }
  if((instruc = fopen(instruc_file, "w")) == NULL)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Can't open selected instruc file for writing"),
			   (GtkWindow*)0);
      return FALSE;
    }

  g_debug("pot2surf : writing/overwriting %s file...", instruc_file);

  fprintf(instruc, "%s\n", pot_file);
  fprintf(instruc, "%s\n", surf_file);
  fprintf(instruc, "%d\n", n);
  
  gtk_tree_model_foreach(GTK_TREE_MODEL(pot2surf_list_store), pot2surf_write_surf, instruc);

  g_debug("pot2surf : ...finished to write file");

  fclose(instruc);

  return TRUE;
}

void pot2surf_select_instruc_file_to_save()
{
  GtkFileFilter *filter_instruc;
  GtkFileFilter *filter_none;
  GtkWidget* file_chooser;
  const char *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());
  int n=0;
  const gchar *pot_file = gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_source_pot_file));
  const gchar *surf_file = gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_target_surf_file));
  GString *default_file = NULL;
   
  gtk_tree_model_foreach(GTK_TREE_MODEL(pot2surf_list_store), pot2surf_count_surf, &n);
  if(n == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please specify surfaces to draw"), (GtkWindow*)0);
      return;
    }
  if(g_strcmp0(pot_file, "") == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please select a source pot file"), (GtkWindow*)0);
      return;
    }
  if(surf_file == NULL || g_strcmp0(surf_file, "") == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please select a target surf file"), (GtkWindow*)0);
      return;
    }

  default_file = g_string_new("");
  g_string_append(default_file, pot_file);
  g_string_append(default_file, ".instruc");

  filter_instruc = gtk_file_filter_new ();
  filter_none = gtk_file_filter_new();
  file_chooser = gtk_file_chooser_dialog_new("Save as a .instruc file", GTK_WINDOW(convert_window),
					     GTK_FILE_CHOOSER_ACTION_SAVE,
					     _("_Cancel"), GTK_RESPONSE_CANCEL,
					     _("_Save"), GTK_RESPONSE_ACCEPT, NULL);
  if(directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);
  
  gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(file_chooser),default_file->str);

  gtk_file_filter_add_pattern(filter_instruc, "*.instruc");
  gtk_file_filter_set_name(filter_instruc, "Instruction files (*.instruc)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_instruc);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    switch(gtk_dialog_run(GTK_DIALOG(file_chooser)))
      {
      case GTK_RESPONSE_ACCEPT:
	if(!pot2surf_build_instruc_file(pot_file, surf_file,
					gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser)), n))
	   
	  break;
	visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                          gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser)), VISU_UI_DIR_SURFACE);
	g_string_free(default_file, TRUE);
	gtk_widget_destroy(file_chooser);
	return;
      case GTK_RESPONSE_CANCEL:
      default:
	g_string_free(default_file, TRUE);
	gtk_widget_destroy (file_chooser);
	return;
      }
}

/* Only used through a gtk_tree_model_foreach()
   Writes a surface name and value in a struct surf array */
gboolean pot2surf_get_surfs(GtkTreeModel *model, GtkTreePath *path _U_,
			     GtkTreeIter *iter, gpointer data)
{
  struct surf {
    char *name;
    float value;
  };
  struct surf *surfaces = data;  
  int i;

  for(i=0; surfaces[i].name != NULL; i++);

  gtk_tree_model_get(GTK_TREE_MODEL(model), iter,
		     POT2SURF_NAME, &surfaces[i].name,
		     POT2SURF_VALUE, &surfaces[i].value, -1);

  return FALSE;
}

/* Tries to build a valid .instruc file and then to executes this file. */
gboolean pot2surf_build_surf_file(GtkWidget *button _U_, gpointer data)
{
  struct surf {
    char *name;
    float value;
  };
  float *values = NULL;
  gchar **names = NULL;
  struct surf *surfaces = NULL;
  int n=0, i;
  GtkTreeIter iter;
  const gchar *pot_file = gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_source_pot_file));
  const gchar *surf_file = gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_target_surf_file));

  gtk_tree_model_foreach(GTK_TREE_MODEL(pot2surf_list_store), pot2surf_count_surf, &n);
  if(n == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please specify surfaces to draw"), (GtkWindow*)0);
      return FALSE;
    }
  if(pot_file == NULL || g_strcmp0(pot_file, "") == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please select a source pot file"), (GtkWindow*)0);
      return FALSE;
    }
  if(surf_file == NULL || g_strcmp0(surf_file, "") == 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Please select a target surf file"), (GtkWindow*)0);
      return FALSE;
    }

  surfaces = g_malloc(n*sizeof(struct surf));
  values = g_malloc(n*sizeof(float));
  names = g_malloc(n*sizeof(gchar *));

  for(i=0; i<n; i++)
    surfaces[i].name = NULL;

  gtk_tree_model_foreach(GTK_TREE_MODEL(pot2surf_list_store), pot2surf_get_surfs, surfaces);

  for(i=0; i<n; i++)
    {
/*       g_debug("%s %f", surfaces[i].name, surfaces[i].value); */
      values[i] = surfaces[i].value;
      if (g_strcmp0(surfaces[i].name, VISU_UI_SURFACE_NAME_CHOOSE))
	names[i] = surfaces[i].name;
      else
	names[i] = (gchar*)0;
    }  

  if(visu_surface_createFromPotentialFile(surf_file, pot_file, n, values, (const gchar **)names) != 0)
    {
      visu_ui_raiseWarning(_("Saving a file"),
			   _("Error"), (GtkWindow*)0);
      for(i=0; i<n; i++)
	g_free(names[i]);
      g_free(names);
      g_free(values);
      g_free(surfaces);
      return FALSE;
    }

  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data)))
    {
      if(visu_ui_panel_surfaces_loadFile(surf_file, &iter,
                                         (GHashTable*)0, (VisuScalarFieldMethod*)0) == 0)
	{
	  for(i=0; i<n; i++)
	    g_free(names[i]);
	  g_free(names);
	  g_free(values);
	  g_free(surfaces);
	  return FALSE;
	}
    }

  for(i=0; i<n; i++)
    g_free(names[i]);
  g_free(names);
  g_free(values);
  g_free(surfaces);
  return TRUE;
}

/* Tries to execute a .instruc file
   ie : given a list of potenetials, tries to build a .surf file from a .pot file */
/* gboolean pot2surf_execute_instruc(GtkWidget *button, gpointer data) */
/* { */
/*   const gchar *instruc_file = gtk_entry_get_text(GTK_ENTRY(pot2surf_entry_source_instruc_file)); */
/*   GString *surf_file_wrote = g_string_new(""); */

/*   if(instruc_file == NULL || g_strcmp0(instruc_file, "") == 0) */
/*     { */
/*       raiseAlertDialog(_("Please select an instruction file")); */
/*       g_string_free(surf_file_wrote, TRUE); */
/*       return FALSE; */
/*     } */

/*   if(pot2surf(instruc_file, surf_file_wrote) != 0) */
/*     { */
/*       g_string_free(surf_file_wrote, TRUE); */
/*       return FALSE; */
/*     } */

/*   if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data))) */
/*     if(isosurfaces_try_file(surf_file_wrote->str, 0) == 0) */
/*       { */
/* 	g_string_free(surf_file_wrote, TRUE); */
/* 	return FALSE; */
/*       } */

/*   return TRUE; */
/* }       */
  

/* void pot2surf_select_build_surf(GtkWidget *radio_button, GtkWidget *hbox) */
/* { */
/*   gtk_widget_set_sensitive(hbox, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio_button))); */
/* } */

/* void pot2surf_select_build_instruc(GtkWidget *radio_button, GtkWidget *hbox) */
/* { */
/*   gtk_widget_set_sensitive(hbox, gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio_button))); */
/* } */

gboolean pot2surf_load_instruc_file(const gchar *instruc_filename)
{
  GIOChannel* instruc_file=NULL;
  GString *line=NULL;
  gchar *surf_file=NULL;
  gchar *pot_file=NULL;
  int n=0, i, j;
  gchar **surf_names;
  float *surf_values;
  GtkTreeIter iter;
  float pot_min, pot_max;
  char float_to_string[128];

  instruc_file = g_io_channel_new_file(instruc_filename, "r", NULL);
  line = g_string_new("");
  if(instruc_file == NULL)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Can't open selected file"), (GtkWindow*)0);
      return FALSE;
    }

  if(g_io_channel_read_line_string(instruc_file, line, NULL, NULL) 
     != G_IO_STATUS_NORMAL)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Line 1 must contain the full path to the .pot"
			     " file to read\n"), (GtkWindow*)0);
      g_string_free(line, TRUE);
      return FALSE;
    }

  pot_file = g_strstrip(g_strdup(line->str));

  if(g_io_channel_read_line_string(instruc_file, line, NULL, NULL) 
     != G_IO_STATUS_NORMAL)
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Line 2 must contain the full path to the"
			     " .surf file to read\n"), (GtkWindow*)0);
      g_string_free(line, TRUE);
      g_free(instruc_file);
      return FALSE;
    }
  
  surf_file = g_strstrip(g_strdup(line->str));

  if((g_io_channel_read_line_string(instruc_file, line, NULL, NULL) != G_IO_STATUS_NORMAL)
     || (sscanf(line->str, "%d", &n) != 1))
    {
      visu_ui_raiseWarning(_("Loading a file"),
			   _("Line 3 must contain the number of"
			     " surfaces to build\n"), (GtkWindow*)0);
      g_string_free(line, TRUE);
      g_free(instruc_file);
      g_free(surf_file);
      return FALSE;
    }

  surf_names = g_malloc(n*sizeof(gchar *));
  surf_values = g_malloc(n*sizeof(gchar *));
  for(i=0; i<n; i++)
    {
      surf_names[i] = g_malloc(TOOL_MAX_LINE_LENGTH*sizeof(gchar));
      if((g_io_channel_read_line_string(instruc_file, line, NULL, NULL) != G_IO_STATUS_NORMAL)
	 || (sscanf(line->str, "%f %s", &surf_values[i], surf_names[i]) != 2))
	{      
	  visu_ui_raiseWarning(_("Loading a file"),
			       _("Lines must contain the value of the"
				 " surface to build and its name\n"), (GtkWindow*)0);
	  g_string_free(line, TRUE);
	  g_free(instruc_file);
	  g_free(surf_file);
	  for(j=0; j<i; j++)
	    g_free(surf_names[j]);
	  g_free(surf_names);
	  return FALSE;
	}
    }      
	  
  gtk_list_store_clear(pot2surf_list_store);
  for(i=0; i<n; i++)
    {
      gtk_list_store_insert(pot2surf_list_store, &iter, i);
      gtk_list_store_set(pot2surf_list_store, &iter,
			 POT2SURF_NAME, surf_names[i],
			 POT2SURF_VALUE, surf_values[i], -1);
    }

  if(pot2surf_quick_parse(pot_file, &pot_min, &pot_max))
    {
      sprintf(float_to_string, "%f", pot_min);
      gtk_entry_set_text(GTK_ENTRY(pot2surf_potmin), float_to_string);
      sprintf(float_to_string, "%f", pot_max);
      gtk_entry_set_text(GTK_ENTRY(pot2surf_potmax), float_to_string);
    }
  gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_source_pot_file), pot_file);
  gtk_entry_set_text(GTK_ENTRY(pot2surf_entry_target_surf_file), surf_file);

  g_string_free(line, TRUE);
  g_free(pot_file);
  g_free(surf_file);
  for(j=0; j<i; j++)
    g_free(surf_names[j]);
  g_free(surf_names);
  g_io_channel_unref(instruc_file);
  return TRUE;
}

void pot2surf_select_instruc_file_to_load()
{
  GtkFileFilter *filter_instruc = gtk_file_filter_new ();
  GtkFileFilter *filter_none = gtk_file_filter_new();
  GtkWidget* file_chooser = 
    gtk_file_chooser_dialog_new("Opens a .instruc file", GTK_WINDOW(convert_window),
				GTK_FILE_CHOOSER_ACTION_OPEN,
				_("_Cancel"), GTK_RESPONSE_CANCEL,
				_("_Open"), GTK_RESPONSE_ACCEPT, NULL);
  const char *directory = visu_ui_main_getLastOpenDirectory(visu_ui_main_class_getCurrentPanel());

  if(directory)
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser), directory);

  gtk_file_filter_add_pattern(filter_instruc, "*.instruc");
  gtk_file_filter_set_name(filter_instruc, "Instruction files (*.instruc)");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_instruc);

  gtk_file_filter_add_pattern(filter_none, "*");
  gtk_file_filter_set_name(filter_none, "All files");  
  gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter_none);

  while(1)
    switch(gtk_dialog_run(GTK_DIALOG(file_chooser)))
      {
      case GTK_RESPONSE_ACCEPT:
	if(!pot2surf_load_instruc_file(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser))))
	  break;
	visu_ui_main_setLastOpenDirectory(visu_ui_main_class_getCurrentPanel(),
                                          gtk_file_chooser_get_current_folder(GTK_FILE_CHOOSER (file_chooser)), VISU_UI_DIR_SURFACE);
	gtk_widget_destroy(file_chooser);
	return;
      case GTK_RESPONSE_CANCEL:
      default:
	gtk_widget_destroy (file_chooser);
	return;
      }
}

/* See header file for more info */
GtkWidget* visu_ui_panel_surfaces_tools_convertWidget()
{
  GtkToolItem *instruc_open = gtk_tool_button_new(NULL, NULL);
  GtkToolItem *instruc_save = gtk_tool_button_new(NULL, NULL);
  GtkToolItem *instruc_help = gtk_tool_button_new(NULL, NULL);
  GtkToolItem* separator = gtk_separator_tool_item_new();
  GtkWidget *main_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

  GtkWidget* scrolled_window = gtk_scrolled_window_new(NULL, NULL);

  GtkWidget *pot_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *pot_open_button = gtk_button_new();

  GtkWidget *tview_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

  GtkWidget *manage_tview_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *add_surf_button = gtk_button_new();
  GtkWidget *special_add_button = gtk_button_new();
  GtkWidget *remove_surf_button = gtk_button_new();
  GtkWidget *move_down_button = gtk_button_new();
  GtkWidget *move_up_button = gtk_button_new();

  GtkWidget *surf_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *surf_open_button = gtk_button_new();

  GtkWidget *min_max_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

  GtkWidget *build_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget *build_surf_button = gtk_button_new_with_label(_("Build"));
  GtkWidget *autoload_checkbox = gtk_check_button_new_with_label (_("Autoload in V_Sim"));

  GtkWidget *image_execute = gtk_image_new_from_icon_name("system-execute", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_add = gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_remove = gtk_image_new_from_icon_name("list-remove", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_go_up = gtk_image_new_from_icon_name("go-up", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_go_down = gtk_image_new_from_icon_name("go-down", GTK_ICON_SIZE_BUTTON);
  GtkWidget *image_open = gtk_image_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);

  GtkWidget *toolbar = gtk_toolbar_new();

  gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(instruc_open), "document-open");
  gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(instruc_save), "document-save-as");
  gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(instruc_help), "help-browser");

  gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH_HORIZ);
  gtk_widget_set_name(toolbar, "message_toolbar");
  gtk_widget_set_name(autoload_checkbox, "message_tog");
  
  pot2surf_create_tree_view();

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(autoload_checkbox), TRUE);

  pot2surf_potmin = gtk_entry_new ();
  pot2surf_potmax = gtk_entry_new ();
  gtk_editable_set_editable(GTK_EDITABLE(pot2surf_potmin), FALSE);
  gtk_editable_set_editable(GTK_EDITABLE(pot2surf_potmax), FALSE);
/*   gtk_widget_set_sensitive(pot2surf_potmin, FALSE); */
/*   gtk_widget_set_sensitive(pot2surf_potmax, FALSE); */
  gtk_entry_set_text(GTK_ENTRY(pot2surf_potmin), "0.0000000");
  gtk_entry_set_text(GTK_ENTRY(pot2surf_potmax), "0.0000000");
  g_object_set(G_OBJECT(pot2surf_potmin), "width-chars", 9, NULL);
  g_object_set(G_OBJECT(pot2surf_potmax), "width-chars", 9, NULL);

  pot2surf_entry_source_pot_file = gtk_entry_new();
  gtk_editable_set_editable(GTK_EDITABLE(pot2surf_entry_source_pot_file), FALSE);
  pot2surf_entry_target_surf_file = gtk_entry_new();
  gtk_editable_set_editable(GTK_EDITABLE(pot2surf_entry_target_surf_file), FALSE);

  /* Widget properties edition */
  gtk_widget_set_tooltip_text(autoload_checkbox, 
		       _("If checked, autoloads newly built file in V_Sim"));
  gtk_widget_set_tooltip_text(pot2surf_entry_source_pot_file, 
		       _("Contains the full path to the currently .pot selected file"));
  gtk_widget_set_tooltip_text(pot2surf_entry_target_surf_file, 
		       _("Contains the full path to the .surf file you want to build"));
  gtk_widget_set_tooltip_text(pot2surf_potmin, 
		       _("Contains the minimal value found in the selected .pot file"));
  gtk_widget_set_tooltip_text(pot2surf_potmax, 
		       _("Contains the maximal value found in the selected .pot file"));
  gtk_widget_set_tooltip_text(build_surf_button, 
		       _("Tries to build specified .surf file using specified parameters"));
  gtk_widget_set_tooltip_text(add_surf_button, 
		       _("Adds a new surface to the list of surfaces to build"));
  gtk_widget_set_tooltip_text(special_add_button, 
		       _("Adds many surfaces to the list of surfaces to build"));
  gtk_widget_set_tooltip_text(remove_surf_button, 
		       _("Removes selected surface from the list of surfaces to build"));
  gtk_widget_set_tooltip_text(move_down_button, 
		       _("Moves down selected surface in the list of surfaces to build"));
  gtk_widget_set_tooltip_text(move_up_button, 
		       _("Moves up selected surface in the list of surfaces to build"));
  gtk_widget_set_tooltip_text(pot_open_button, 
		       _("Selects the .pot file to use"));
  gtk_widget_set_tooltip_text(surf_open_button, 
		       _("Selects the .surf file to write"));
  gtk_tool_item_set_tooltip_text(instruc_open,
			    _("Opens a .instruc file"));
  gtk_tool_item_set_tooltip_text(instruc_save,
			    _("Saves current settings as .instruc file"));
  gtk_tool_item_set_tooltip_text(instruc_help,
			    _("Show help"));

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window),
				 GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
  gtk_container_add (GTK_CONTAINER (add_surf_button), image_add);
  gtk_container_add (GTK_CONTAINER (special_add_button), image_execute);
  gtk_container_add (GTK_CONTAINER (remove_surf_button), image_remove);
  gtk_container_add (GTK_CONTAINER (move_down_button), image_go_down);
  gtk_container_add (GTK_CONTAINER (move_up_button), image_go_up);
  gtk_container_add (GTK_CONTAINER (pot_open_button), image_open);
  image_open = gtk_image_new_from_icon_name("document-open", GTK_ICON_SIZE_BUTTON);
  gtk_container_add (GTK_CONTAINER (surf_open_button), image_open);

  gtk_toolbar_insert(GTK_TOOLBAR(toolbar), instruc_open, 0);
  gtk_toolbar_insert(GTK_TOOLBAR(toolbar), instruc_save, 1);
  gtk_tool_item_set_expand(separator, TRUE); 
  gtk_separator_tool_item_set_draw(GTK_SEPARATOR_TOOL_ITEM(separator), FALSE);
  gtk_toolbar_insert(GTK_TOOLBAR(toolbar), separator, 2);
  gtk_toolbar_insert(GTK_TOOLBAR(toolbar), instruc_help, 3);

  /* Widget packing */
  gtk_container_add(GTK_CONTAINER(scrolled_window), pot2surf_tree_view);

  gtk_box_pack_start(GTK_BOX(main_vbox), toolbar, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(main_vbox), pot_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(main_vbox), min_max_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(main_vbox), tview_hbox, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(main_vbox), surf_hbox, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(main_vbox), build_hbox, FALSE, FALSE, 1);

  gtk_box_pack_start(GTK_BOX(tview_hbox), scrolled_window, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(tview_hbox), manage_tview_vbox, FALSE, FALSE, 0);
  
  gtk_box_pack_start(GTK_BOX(pot_hbox), gtk_label_new(_("Source .pot file :")), FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(pot_hbox), pot2surf_entry_source_pot_file, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(pot_hbox), pot_open_button, FALSE, FALSE, 1);  

  gtk_box_pack_start(GTK_BOX(surf_hbox), gtk_label_new(_("Target .surf file :")), FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(surf_hbox), pot2surf_entry_target_surf_file, TRUE, TRUE, 1);
  gtk_box_pack_start(GTK_BOX(surf_hbox), surf_open_button, FALSE, FALSE, 1);  

  gtk_box_pack_start(GTK_BOX(manage_tview_vbox), add_surf_button, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(manage_tview_vbox), special_add_button, FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(manage_tview_vbox), remove_surf_button, FALSE, FALSE, 1);
  gtk_box_pack_end(GTK_BOX(manage_tview_vbox), move_down_button, FALSE, FALSE, 1);
  gtk_box_pack_end(GTK_BOX(manage_tview_vbox), move_up_button, FALSE, FALSE, 1);
							     
  gtk_box_pack_start(GTK_BOX(min_max_hbox), gtk_label_new(_("Pot min : ")), FALSE, FALSE, 1);
  gtk_box_pack_start(GTK_BOX(min_max_hbox), pot2surf_potmin, FALSE, FALSE, 1);
  gtk_box_pack_end(GTK_BOX(min_max_hbox), pot2surf_potmax, FALSE, FALSE, 1);
  gtk_box_pack_end(GTK_BOX(min_max_hbox), gtk_label_new(_("Pot max : ")), FALSE, FALSE, 1);
 
  gtk_box_pack_end(GTK_BOX(build_hbox), build_surf_button, FALSE, FALSE, 1);
  gtk_box_pack_end(GTK_BOX(build_hbox), autoload_checkbox, FALSE, FALSE, 1);

  /* Signal connexion */
  g_signal_connect(instruc_open, "clicked", 
		   G_CALLBACK(pot2surf_select_instruc_file_to_load), NULL);
  g_signal_connect(instruc_save, "clicked", 
		   G_CALLBACK(pot2surf_select_instruc_file_to_save), NULL);
  g_signal_connect(instruc_help, "clicked", 
		   G_CALLBACK(pot2surf_popup_help), NULL);
  g_signal_connect(build_surf_button, "clicked", 
		   G_CALLBACK(pot2surf_build_surf_file), autoload_checkbox);
  g_signal_connect(pot_open_button, "clicked",
		   G_CALLBACK(pot2surf_select_pot_file), NULL);
  g_signal_connect(surf_open_button, "clicked",
		   G_CALLBACK(pot2surf_select_surf_file), NULL);
  g_signal_connect(add_surf_button, "clicked", 
		   G_CALLBACK(pot2surf_add_surf), NULL);
  g_signal_connect(special_add_button, "clicked", 
		   G_CALLBACK(pot2surf_special_add_surf), NULL);
  g_signal_connect(remove_surf_button, "clicked", 
		   G_CALLBACK(pot2surf_remove_surf), NULL);
  g_signal_connect(move_down_button, "clicked", 
		   G_CALLBACK(pot2surf_move_surf), GINT_TO_POINTER(1));
  g_signal_connect(move_up_button, "clicked", 
		   G_CALLBACK(pot2surf_move_surf), GINT_TO_POINTER(-1));
  
  return main_vbox;
}

void visu_ui_panel_surfaces_tools_init_destroy()
{
  convert_w_opened = FALSE;
  gtk_widget_destroy(convert_window);
}

/* See header file for more info */
void visu_ui_panel_surfaces_tools_init()
{
  GtkWidget *main_vbox;
  GtkWidget *convert_notebook;
  GtkWidget *close_button;
  GtkWidget *merge_label;
  GtkWidget *pot2surf_label;

  if(convert_w_opened)
    return;
  convert_w_opened = TRUE;
  
  main_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  convert_notebook = gtk_notebook_new();
  close_button = gtk_button_new_from_icon_name("window-close", GTK_ICON_SIZE_BUTTON);
  merge_label = gtk_label_new(_("Merge"));
  pot2surf_label = gtk_label_new(_("pot2surf"));

  convert_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(convert_window), "File conversion utilities");
/*   gtk_window_set_modal(GTK_WINDOW(convert_window), TRUE); */
/*   gtk_window_set_transient_for(GTK_WINDOW(convert_window), GTK_WINDOW(mainWindow)); */

  gtk_notebook_append_page(GTK_NOTEBOOK(convert_notebook), visu_ui_panel_surfaces_tools_convertWidget(),
			   pot2surf_label);
  gtk_notebook_append_page(GTK_NOTEBOOK(convert_notebook), visu_ui_panel_surfaces_tools_fileWidget(), 
			   merge_label);
 
  gtk_container_add(GTK_CONTAINER(convert_window), main_vbox);
  gtk_widget_set_name(convert_window, "message");
  gtk_widget_set_name(convert_notebook, "message_notebook");


  gtk_box_pack_start(GTK_BOX(main_vbox), convert_notebook, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(main_vbox), close_button, FALSE, FALSE, 1);

  g_signal_connect(convert_window, "destroy",
		   G_CALLBACK(visu_ui_panel_surfaces_tools_init_destroy), NULL);
  g_signal_connect(close_button, "clicked",
		   G_CALLBACK(visu_ui_panel_surfaces_tools_init_destroy), NULL);

  gtk_widget_show_all(convert_window);
}


/* void open_pot2surf(GtkWidget *widget, gpointer data) */
/* { */
/*   char *file_to_convert = NULL; */
/*   GtkFileFilter *filter = gtk_file_filter_new (); */
/*   GtkWidget* file_chooser =  */
/*     gtk_file_chooser_dialog_new("Select a .instruc file", GTK_WINDOW(convert_window), */
/* 				GTK_FILE_CHOOSER_ACTION_OPEN, */
/* 				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, */
/* 				GTK_STOCK_HELP, GTK_RESPONSE_HELP, */
/* 				"Convert", GTK_RESPONSE_ACCEPT, */
/* 				NULL); */
/*   gint r_value; */

/*   gtk_file_filter_add_pattern(filter, "*.instruc"); */
/*   gtk_file_filter_set_name(filter, "Instructions files (*.instruc)");   */
/*   gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(file_chooser), filter); */

/*   while((r_value = gtk_dialog_run(GTK_DIALOG(file_chooser)))) */
/*     switch(r_value) */
/*       { */
/*       case GTK_RESPONSE_ACCEPT: */
/* 	file_to_convert =  */
/* 	  gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser)); */
/* 	g_debug("\n\n%s\n", file_to_convert); */
/* 	pot2surf(file_to_convert, NULL); */
/* 	gtk_widget_destroy(file_chooser); */
/* 	return; */
/*       case GTK_RESPONSE_HELP: */
/* 	pot2surf_popup_help(); */
/* 	break; */
/*       case GTK_RESPONSE_CANCEL: */
/*       default: */
/* 	gtk_widget_destroy (file_chooser); */
/* 	return; */
/*       }   */
/* }   */

