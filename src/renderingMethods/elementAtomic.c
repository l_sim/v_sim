/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016-2021)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016-2021)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#include "elementAtomic.h"

#include <math.h>

#include <visu_configFile.h>
#include <visu_basic.h>
#include <coreTools/toolShape.h>

/**
 * SECTION:elementAtomic
 * @short_description: a class implementing rendering for
 * #VisuDataAtomic objects.
 *
 * <para>This class implements the virtual method of
 * #VisuElementRenderer class to display nodes as 0D objects at a
 * given point. The specific shape is defined by
 * #VisuElementAtomicShapeId enumeration. The size of the 0D object is
 * given by the "radius" property which is a length with a given
 * "units" property.</para>
 * <para>visu_element_atomic_getFromPool() is a specific function to
 * associate a unique #VisuElementAtomic to a given #VisuElement.</para>
 */

/**
 * VisuElementAtomicClass:
 * @parent: its parent.
 *
 * Interface for class that can represent #VisuElement of the same
 * kind in the same way (usually spheres).
 *
 * Since: 3.8
 */

#define _DEFAULT_RADIUS 1.f

#define FLAG_RESOURCE_RADIUS_SHAPE "atomic_radius_shape"
#define DESC_RESOURCE_RADIUS_SHAPE "The radius of the element and its shape, a real > 0. & [Sphere Cube Elipsoid Point Torus] [units]"
/* These functions write all the element list to export there associated resources. */
static void exportAtomic(GString *data, VisuData* dataObj);
#define FLAG_PARAMETER_SHAPE "atomic_sphere_method"
#define DESC_PARAMETER_SHAPE "The sphere drawing method, [GluSphere Icosahedron]"
static void exportAtomicShape(GString *data, VisuData* dataObj);

/* Read routines for the config file. */
static void onEntryRadiusShape(VisuConfigFile *object, VisuConfigFileEntry *entry, gpointer data);
static void onEntryUnit(VisuElementAtomic *ele, VisuConfigFileEntry *entry, VisuConfigFile *obj);

enum
  {
    sphere_glu,
    sphere_icosahedron,
    sphere_nb
  };
static guint _sphereMethod = sphere_glu;
static const char* _sphereName[sphere_nb + 1] = {"GluSphere", "Icosahedron", (const char*)0};

static const char* _shapeName[VISU_ELEMENT_ATOMIC_N_SHAPES + 1] =
  {"Sphere", "Cube", "Elipsoid", "Point", "Torus", (const char*)0};
static const char* _shapeNameI18n[VISU_ELEMENT_ATOMIC_N_SHAPES + 1];

/**
 * VisuElementAtomic:
 *
 * Structure used to define #VisuElementAtomic objects.
 *
 * Since: 3.8
 */
struct _VisuElementAtomicPrivate
{
  gfloat radius;
  ToolUnits units;
  VisuElementAtomicShapeId shape;
  gfloat ratio, phi, theta;
};

enum {
  PROP_0,
  PROP_RADIUS,
  PROP_UNITS,
  PROP_SHAPE,
  PROP_RATIO,
  PROP_PHI,
  PROP_THETA,
  N_PROPS
};
static GParamSpec *_properties[N_PROPS];

static GList *_pool;

static gboolean _sphereFromName(const gchar *name, guint *value);
static gboolean _shapeFromName(const gchar *name, VisuElementAtomicShapeId *shape);

static GObject* visu_element_atomic_constructor(GType gtype, guint nprops, GObjectConstructParam *props);
static void visu_element_atomic_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec);
static void visu_element_atomic_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec);
static void             _getLayout  (const VisuElementRenderer *element,
                                     guint id,
                                     VisuGlExtPackingModels *packing,
                                     VisuGlExtPrimitives *primitive);
static void             _addVertices(const VisuElementRenderer *element,
                                     const VisuDataColorizer *colorizer,
                                     const VisuData *dataObj, const VisuNode *node,
                                     const gfloat xyz[3], gfloat scale,
                                     GArray **vertices,
                                     VisuCommitVertices commitFunc, gpointer data);
static gfloat           _getExtent  (const VisuElementRenderer *self);

G_DEFINE_TYPE_WITH_CODE(VisuElementAtomic, visu_element_atomic, VISU_TYPE_ELEMENT_RENDERER,
                        G_ADD_PRIVATE(VisuElementAtomic))

static void visu_element_atomic_class_init(VisuElementAtomicClass *klass)
{
  VisuConfigFileEntry *resourceEntry;

  /* Connect the overloading methods. */
  G_OBJECT_CLASS(klass)->constructor  = visu_element_atomic_constructor;
  G_OBJECT_CLASS(klass)->set_property = visu_element_atomic_set_property;
  G_OBJECT_CLASS(klass)->get_property = visu_element_atomic_get_property;
  VISU_ELEMENT_RENDERER_CLASS(klass)->getLayout   = _getLayout;
  VISU_ELEMENT_RENDERER_CLASS(klass)->addVertices = _addVertices;
  VISU_ELEMENT_RENDERER_CLASS(klass)->getExtent   = _getExtent;

  /**
   * VisuElementAtomic::radius:
   *
   * The atomic radius.
   *
   * Since: 3.8
   */
  _properties[PROP_RADIUS] =
    g_param_spec_float("radius", "Radius", "atomic radius",
                       0.001f, G_MAXFLOAT, _DEFAULT_RADIUS, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_RADIUS,
                                  _properties[PROP_RADIUS]);
  /**
   * VisuElementAtomic::units:
   *
   * The unit in which the radius is defined.
   *
   * Since: 3.8
   */
  _properties[PROP_UNITS] =
    g_param_spec_uint("units", "Units", "radius units",
                      0, TOOL_UNITS_N_VALUES - 1, TOOL_UNITS_UNDEFINED, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_UNITS,
                                  _properties[PROP_UNITS]);
  /**
   * VisuElementAtomic::shape:
   *
   * The shape used to represent a given element.
   *
   * Since: 3.8
   */
  _properties[PROP_SHAPE] =
    g_param_spec_uint("shape", "Shape", "atomic shape",
                      0, VISU_ELEMENT_ATOMIC_N_SHAPES - 1, VISU_ELEMENT_ATOMIC_SPHERE,
                      G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_SHAPE,
                                  _properties[PROP_SHAPE]);
  /**
   * VisuElementAtomic::elipsoid-ratio:
   *
   * The ratio used to represent an elongated shape like an elipsoid.
   *
   * Since: 3.8
   */
  _properties[PROP_RATIO] =
    g_param_spec_float("elipsoid-ratio", "Elipsoid ratio", "Elipsoid ratio",
                       0.f, G_MAXFLOAT, 1.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_RATIO,
                                  _properties[PROP_RATIO]);
  /**
   * VisuElementAtomic::elipsoid-angle-phi:
   *
   * The angle phi used to aligned an elongated shape like an elipsoid.
   *
   * Since: 3.8
   */
  _properties[PROP_PHI] =
    g_param_spec_float("elipsoid-angle-phi", "Elipsoid angle phi", "Elipsoid angle phi",
                       0.f, 360.f, 0.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_PHI,
                                  _properties[PROP_PHI]);
  /**
   * VisuElementAtomic::elipsoid-angle-theta:
   *
   * The angle tetha used to aligned an elongated shape like an elipsoid.
   *
   * Since: 3.8
   */
  _properties[PROP_THETA] =
    g_param_spec_float("elipsoid-angle-theta", "Elipsoid angle theta", "Elipsoid angle theta",
                       0.f, 180.f, 90.f, G_PARAM_READWRITE);
  g_object_class_install_property(G_OBJECT_CLASS(klass), PROP_THETA,
                                  _properties[PROP_THETA]);

  resourceEntry = visu_config_file_addTokenizedEntry(VISU_CONFIG_FILE_RESOURCE,
                                                     FLAG_RESOURCE_RADIUS_SHAPE,
                                                     DESC_RESOURCE_RADIUS_SHAPE,
                                                     TRUE);
  g_signal_connect(VISU_CONFIG_FILE_RESOURCE, "parsed::" FLAG_RESOURCE_RADIUS_SHAPE,
                   G_CALLBACK(onEntryRadiusShape), (gpointer)0);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_RESOURCE, exportAtomic);
  resourceEntry = visu_config_file_addEnumEntry(VISU_CONFIG_FILE_PARAMETER,
                                                FLAG_PARAMETER_SHAPE, DESC_PARAMETER_SHAPE,
                                                &_sphereMethod, _sphereFromName, FALSE);
  visu_config_file_entry_setVersion(resourceEntry, 3.4f);
  visu_config_file_addExportFunction(VISU_CONFIG_FILE_PARAMETER,
                                     exportAtomicShape);

  _pool = (GList*)0;

  _shapeNameI18n[0] = _("Sphere");
  _shapeNameI18n[1] = _("Cube");
  _shapeNameI18n[2] = _("Elipsoid");
  _shapeNameI18n[3] = _("Point");
  _shapeNameI18n[4] = _("Torus");
  _shapeNameI18n[5] = (const char*)0;
}
static GObject* visu_element_atomic_constructor(GType gtype, guint nprops, GObjectConstructParam *props)
{
  guint i;

  for (i = 0; i < nprops; ++i)
    {
      if (!g_strcmp0(g_param_spec_get_name(props[i].pspec), "buffer-dimension") &&
          g_value_get_uint(props[i].value) < 1)
        g_value_set_uint(props[i].value, 1);
    }

  return G_OBJECT_CLASS(visu_element_atomic_parent_class)->constructor(gtype, nprops, props);
}
static void visu_element_atomic_init(VisuElementAtomic *obj)
{
  g_debug("Visu Pair Atomic: initializing a new object (%p).",
	      (gpointer)obj);
  
  obj->priv = visu_element_atomic_get_instance_private(obj);

  /* Private data. */
  obj->priv->radius            = _DEFAULT_RADIUS;
  obj->priv->units             = visu_basic_getPreferedUnit();
  obj->priv->shape             = VISU_ELEMENT_ATOMIC_SPHERE;
  obj->priv->ratio             = 1.f;
  obj->priv->phi               = 0.f;
  obj->priv->theta             = 90.f;

  g_signal_connect_object(VISU_CONFIG_FILE_PARAMETER, "parsed::main_unit",
                          G_CALLBACK(onEntryUnit), (gpointer)obj, G_CONNECT_SWAPPED);
}
static void visu_element_atomic_get_property(GObject* obj, guint property_id,
                                             GValue *value, GParamSpec *pspec)
{
  VisuElementAtomic *self = VISU_ELEMENT_ATOMIC(obj);

  g_debug("Visu Element Atomic: get property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_RADIUS:
      g_value_set_float(value, self->priv->radius);
      g_debug("%g.", self->priv->radius);
      break;
    case PROP_UNITS:
      g_value_set_uint(value, self->priv->units);
      g_debug("%d.", self->priv->units);
      break;
    case PROP_SHAPE:
      g_value_set_uint(value, self->priv->shape);
      g_debug("%d.", self->priv->shape);
      break;
    case PROP_RATIO:
      g_value_set_float(value, self->priv->ratio);
      g_debug("%g.", self->priv->ratio);
      break;
    case PROP_PHI:
      g_value_set_float(value, self->priv->phi);
      g_debug("%g.", self->priv->phi);
      break;
    case PROP_THETA:
      g_value_set_float(value, self->priv->theta);
      g_debug("%g.", self->priv->theta);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}
static void visu_element_atomic_set_property(GObject* obj, guint property_id,
                                             const GValue *value, GParamSpec *pspec)
{
  VisuElementAtomic *self = VISU_ELEMENT_ATOMIC(obj);

  g_debug("Visu Element Atomic: set property '%s' -> ",
	      g_param_spec_get_name(pspec));
  switch (property_id)
    {
    case PROP_RADIUS:
      g_debug("%g.", self->priv->radius);
      visu_element_atomic_setRadius(self, g_value_get_float(value));
      break;
    case PROP_UNITS:
      g_debug("%d.", g_value_get_uint(value));
      visu_element_atomic_setUnits(self, g_value_get_uint(value));
      break;
    case PROP_SHAPE:
      visu_element_atomic_setShape(self, g_value_get_uint(value));
      g_debug("%d.", self->priv->shape);
      break;
    case PROP_RATIO:
      visu_element_atomic_setElipsoidRatio(self, g_value_get_float(value));
      g_debug("%g.", self->priv->ratio);
      break;
    case PROP_PHI:
      visu_element_atomic_setElipsoidPhi(self, g_value_get_float(value));
      g_debug("%g.", self->priv->phi);
      break;
    case PROP_THETA:
      visu_element_atomic_setElipsoidTheta(self, g_value_get_float(value));
      g_debug("%g.", self->priv->theta);
      break;
    default:
      /* We don't have any other property... */
      G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, property_id, pspec);
      break;
    }
}

/**
 * visu_element_atomic_new:
 * @element: a #VisuElement object.
 *
 * Creates a new #VisuElementAtomic object used to render @element.
 *
 * Since: 3.8
 *
 * Returns: (transfer full): a newly created #VisuElementAtomic object.
 **/
VisuElementAtomic* visu_element_atomic_new(VisuElement *element)
{
  return VISU_ELEMENT_ATOMIC(g_object_new(VISU_TYPE_ELEMENT_ATOMIC, "element", element, NULL));
}
/**
 * visu_element_atomic_getFromPool:
 * @element: a #VisuElement object.
 *
 * Retrieve a #VisuElementAtomic representing @element. This
 * #VisuElementAtomic is unique and its parent properties are bound to
 * the unique #VisuElementRenderer for @element.
 *
 * Since: 3.8
 *
 * Returns: (transfer none): a #VisuElementAtomic for @element.
 **/
VisuElementAtomic* visu_element_atomic_getFromPool(VisuElement *element)
{
  GList *lst;
  VisuElementAtomic *atomic;

  for (lst = _pool; lst; lst = g_list_next(lst))
    if (visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(lst->data)) == element)
      return VISU_ELEMENT_ATOMIC(lst->data);
  
  atomic = visu_element_atomic_new(element);
  visu_element_renderer_bindToPool(VISU_ELEMENT_RENDERER(atomic));
  _pool = g_list_prepend(_pool, atomic);  
  return atomic;
}
/**
 * visu_element_atomic_bindToPool:
 * @atomic: a #VisuElementAtomic object.
 *
 * Bind all properties of @atomic to the #VisuElementAtomic object
 * corresponding to the same #VisuElement from the pool. The binding
 * is bidirectional. This method is usefull to create a pool of
 * objects inheriting from #VisuElementAtomic.
 *
 * Since: 3.8
 **/
void visu_element_atomic_bindToPool(VisuElementAtomic *atomic)
{
  VisuElementAtomic *pool;

  visu_element_renderer_bindToPool(VISU_ELEMENT_RENDERER(atomic));
  pool = visu_element_atomic_getFromPool(visu_element_renderer_getElement(VISU_ELEMENT_RENDERER(atomic)));
  g_object_bind_property(pool, "radius", atomic, "radius",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "units", atomic, "units",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "shape", atomic, "shape",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "elipsoid-ratio", atomic, "elipsoid-ratio",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "elipsoid-angle-phi", atomic, "elipsoid-angle-phi",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
  g_object_bind_property(pool, "elipsoid-angle-theta", atomic, "elipsoid-angle-theta",
                         G_BINDING_SYNC_CREATE | G_BINDING_BIDIRECTIONAL);
}
/**
 * visu_element_atomic_pool_finalize: (skip)
 *
 * Destroy the internal list of known #VisuElementAtomic objects, see
 * visu_element_atomic_getFromPool().
 *
 * Since: 3.8
 **/
void visu_element_atomic_pool_finalize(void)
{
  g_list_free_full(_pool, (GDestroyNotify)g_object_unref);
  _pool = (GList*)0;
}

static gfloat _getExtent(const VisuElementRenderer *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), 0.f);

  return VISU_ELEMENT_ATOMIC(self)->priv->radius;
}
/**
 * visu_element_atomic_getRadius:
 * @self: a #VisuElementAtomic object.
 *
 * Retrieve the radius used to draw @self. The unit of the value is
 * given by visu_element_atomic_getUnits().
 *
 * Since: 3.8
 *
 * Returns: a radius value.
 **/
gfloat visu_element_atomic_getRadius(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), 0.f);

  return self->priv->radius;
}
/**
 * visu_element_atomic_setRadius:
 * @self: a #VisuElementAtomic object.
 * @val: a positive float value.
 *
 * Change the radius (or long axe) of the representation of @self.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setRadius(VisuElementAtomic *self, gfloat val)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  if (self->priv->radius == val)
    return FALSE;

  g_debug("Element Atomic: changing radius from %g to %g.", self->priv->radius, val);
  self->priv->radius = val;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_RADIUS]);
  g_signal_emit_by_name(self, "size-changed", _getExtent(VISU_ELEMENT_RENDERER(self)));

  return TRUE;
}
/**
 * visu_element_atomic_getUnits:
 * @self: a #VisuElementAtomic object.
 *
 * The units in which the radius value is given.
 *
 * Since: 3.8
 *
 * Returns: a unit.
 **/
ToolUnits visu_element_atomic_getUnits(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), TOOL_UNITS_UNDEFINED);

  return self->priv->units;
}
/**
 * visu_element_atomic_setUnits:
 * @self: a #VisuElementAtomic object.
 * @val: a #ToolUnits value.
 *
 * Change the unit in wich the radius is given, see
 * visu_element_atomic_setRadius().
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setUnits(VisuElementAtomic *self, ToolUnits val)
{
  ToolUnits unit_;
  double fact;

  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  g_debug("Element Atomic: caught 'unit-changed' signal (%p).", (gpointer)self);
  if (self->priv->units == val)
    return FALSE;

  unit_ = self->priv->units;
  self->priv->units = val;
  g_debug("Element Atomic: changed unit (from %s) to %s.",
          tool_physic_getUnitLabel(unit_), tool_physic_getUnitLabel(val));

  if (unit_ != TOOL_UNITS_UNDEFINED && val != TOOL_UNITS_UNDEFINED)
    {
      fact = (double)tool_physic_getUnitValueInMeter(unit_) /
        tool_physic_getUnitValueInMeter(val);

      self->priv->radius *= fact;
      g_debug("Element Atomic: emitting radius changed to %g.", self->priv->radius);

      g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_UNITS]);
      g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_RADIUS]);
    }
  else
    g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_UNITS]);

  return TRUE;
}
/**
 * visu_element_atomic_getShape:
 * @self: a #VisuElementAtomic object.
 *
 * Retrieve the #VisuElementAtomicShapeId that @self is using for representation.
 *
 * Since: 3.8
 *
 * Returns: the shape used by @self.
 **/
VisuElementAtomicShapeId visu_element_atomic_getShape(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), VISU_ELEMENT_ATOMIC_SPHERE);

  return self->priv->shape;
}
/**
 * visu_element_atomic_setShape:
 * @self: a #VisuElementAtomic object.
 * @val: a #VisuElementAtomicShapeId value.
 *
 * Change the representation shape of @self.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setShape(VisuElementAtomic *self,
                                      VisuElementAtomicShapeId val)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  if (self->priv->shape == val)
    return FALSE;

  self->priv->shape = val;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_SHAPE]);

  return TRUE;
}
/**
 * visu_element_atomic_getElipsoidRatio:
 * @self: a #VisuElementAtomic object.
 *
 * When @self is used to draw constant elipsoid or torus, this value
 * is used to adjust the ratio between the two angles.
 *
 * Since: 3.8
 *
 * Returns: a ratio value.
 **/
gfloat visu_element_atomic_getElipsoidRatio(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), 1.f);

  return self->priv->ratio;
}
/**
 * visu_element_atomic_setElipsoidRatio:
 * @self: a #VisuElementAtomic object.
 * @val: a positive float value.
 *
 * Change the ratio between the long axe and the short axe of the
 * representation for shapes that are not isotropic.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setElipsoidRatio(VisuElementAtomic *self, gfloat val)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  if (self->priv->ratio == val)
    return FALSE;

  self->priv->ratio = val;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_RATIO]);

  return TRUE;
}
/**
 * visu_element_atomic_getElipsoidPhi:
 * @self: a #VisuElementAtomic object.
 *
 * Retrieve the phi angle used to draw elipsoid shape with this renderer.
 *
 * Since: 3.8
 *
 * Returns: the phi angle.
 **/
gfloat visu_element_atomic_getElipsoidPhi(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), 0.f);

  return self->priv->phi;
}
/**
 * visu_element_atomic_setElipsoidPhi:
 * @self: a #VisuElementAtomic object.
 * @val: a float value.
 *
 * Change the phi angle of the representation for shapes that are not isotropic.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setElipsoidPhi(VisuElementAtomic *self, gfloat val)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  if (self->priv->phi == val)
    return FALSE;

  self->priv->phi = val;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_PHI]);

  return TRUE;
}
/**
 * visu_element_atomic_getElipsoidTheta:
 * @self: a #VisuElementAtomic object.
 *
 * Retrieve the theta angle used to draw elipsoid shape with this renderer.
 *
 * Since: 3.8
 *
 * Returns: a theta value.
 **/
gfloat visu_element_atomic_getElipsoidTheta(const VisuElementAtomic *self)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), 90.f);

  return self->priv->theta;
}
/**
 * visu_element_atomic_setElipsoidTheta:
 * @self: a #VisuElementAtomic object.
 * @val: a float value.
 *
 * Change the theta angle of the representation for shapes that are not isotropic.
 *
 * Since: 3.8
 *
 * Returns: TRUE if value is actually changed.
 **/
gboolean visu_element_atomic_setElipsoidTheta(VisuElementAtomic *self, gfloat val)
{
  g_return_val_if_fail(VISU_IS_ELEMENT_ATOMIC(self), FALSE);

  if (self->priv->theta == val)
    return FALSE;

  self->priv->theta = val;
  g_object_notify_by_pspec(G_OBJECT(self), _properties[PROP_THETA]);

  return TRUE;
}

static void _getLayout(const VisuElementRenderer *element,
                       guint id _U_,
                       VisuGlExtPackingModels *packing,
                       VisuGlExtPrimitives *primitive)
{
  g_return_if_fail(VISU_IS_ELEMENT_ATOMIC(element));

  switch (VISU_ELEMENT_ATOMIC(element)->priv->shape)
    {
    case VISU_ELEMENT_ATOMIC_SPHERE:
    case VISU_ELEMENT_ATOMIC_ELLIPSOID:
    case VISU_ELEMENT_ATOMIC_CUBE:
    case VISU_ELEMENT_ATOMIC_TORUS:
      *packing = VISU_GL_XYZ_NRM;
      *primitive = VISU_GL_TRIANGLES;
      break;
    case VISU_ELEMENT_ATOMIC_POINT:
      *packing = VISU_GL_XYZ;
      *primitive = VISU_GL_POINTS;
      break;
    default:
      g_return_if_reached();
      break;
    }
}
static void _addVertices(const VisuElementRenderer *element,
                         const VisuDataColorizer *colorizer,
                         const VisuData *dataObj, const VisuNode *node,
                         const gfloat xyz[3], gfloat scale,
                         GArray **vertices, VisuCommitVertices commitFunc, gpointer data)
{
  int nlat;
  VisuGlView *view;
  VisuElementAtomic *atomic;
  gfloat rgba[4];
  const gfloat *material;
  const ToolColor *color = (const ToolColor*)0;

  g_return_if_fail(commitFunc);

  if (!vertices[0])
    return;
  view = visu_element_renderer_getGlView(VISU_ELEMENT_RENDERER(element));
  if (!view)
    return;
  g_return_if_fail(VISU_IS_ELEMENT_ATOMIC(element));
  atomic = VISU_ELEMENT_ATOMIC(element);

  material = visu_element_renderer_getMaterial(element);
  if (!colorizer || !visu_data_colorizer_getColor(colorizer, rgba, dataObj, node))
    color =  visu_element_renderer_getColor(element);

  nlat = visu_gl_view_getDetailLevel(view, atomic->priv->radius * scale);
  switch (atomic->priv->shape)
    {
    case VISU_ELEMENT_ATOMIC_SPHERE:
      if (_sphereMethod == sphere_glu)
        tool_drawSphere(*vertices, xyz, atomic->priv->radius * scale, nlat);
      else
        tool_drawIcosahedron(*vertices, xyz, atomic->priv->radius * scale, nlat);
      break;
    case VISU_ELEMENT_ATOMIC_ELLIPSOID:
      tool_drawEllipsoid(*vertices, xyz, atomic->priv->theta * G_PI / 180.f,
                         atomic->priv->phi * G_PI / 180.f,
                         atomic->priv->radius * scale, atomic->priv->ratio, nlat);
      break;
    case VISU_ELEMENT_ATOMIC_CUBE:
      tool_drawCube(*vertices, xyz, atomic->priv->radius * scale);
      break;
    case VISU_ELEMENT_ATOMIC_TORUS:
      tool_drawTorus(*vertices, xyz, atomic->priv->theta * G_PI / 180.f,
                     atomic->priv->phi * G_PI / 180.f,
                     atomic->priv->radius * scale, atomic->priv->ratio, nlat);
      break;
    case VISU_ELEMENT_ATOMIC_POINT:
      g_array_append_vals(*vertices, xyz, 3);
      break;
    default:
      /* g_return_if_reached(); */
      break;
    }
  commitFunc(*vertices, 0, color ? color->rgba : rgba, material, data);
}

/**
 * visu_element_atomic_getShapeNames:
 * @asLabel: a boolean.
 *
 * Get the string defining #VisuElementAtomicShapeId. If @asLabel is
 * %TRUE, then the string are translated and stored in UTF8.
 *
 * Since: 3.8
 *
 * Returns: (transfer none) (array zero-terminated=1): strings
 * representing #VisuElementAtomicShapeId.
 **/
const gchar ** visu_element_atomic_getShapeNames(gboolean asLabel)
{
  if (asLabel)
    return _shapeNameI18n;
  else
    return _shapeName;
}

/*****************************************/
/* Dealing with parameters and resources */
/*****************************************/
static gboolean _sphereFromName(const gchar *name, guint *value)
{
  g_return_val_if_fail(name && value, FALSE);

  for (*value = 0; *value < sphere_nb; *value += 1)
    if (!g_strcmp0(name, _sphereName[*value]))
      return TRUE;
  return FALSE;
}
static gboolean _shapeFromName(const gchar *name, VisuElementAtomicShapeId *shape)
{
  g_return_val_if_fail(name && shape, FALSE);

  for (*shape = 0; *shape < VISU_ELEMENT_ATOMIC_N_SHAPES; *shape += 1)
    if (!g_strcmp0(name, _shapeName[*shape]))
      return TRUE;
  return FALSE;
}
static VisuElementAtomic* _fromEntry(VisuConfigFileEntry *entry)
{
  const gchar *label;
  VisuElement *ele;

  label = visu_config_file_entry_getLabel(entry);
  ele = visu_element_retrieveFromName(label, (gboolean*)0);
  if (!ele)
    {
      visu_config_file_entry_setErrorMessage(entry, _("'%s' wrong element name"), label);
      return (VisuElementAtomic*)0;
    }
  return visu_element_atomic_getFromPool(ele);
}
static void onEntryRadiusShape(VisuConfigFile *obj _U_,
                               VisuConfigFileEntry *entry, gpointer data _U_)
{
  VisuElementAtomic *ele;
  float rgRadius[2] = {0.f, G_MAXFLOAT};
  float radius;
  VisuElementAtomicShapeId shape;
  gchar *remains;
  ToolUnits units = TOOL_UNITS_UNDEFINED;

  g_debug("Rendering Atomic: parse line.");
  /* Get the element. */
  ele = _fromEntry(entry);
  if (!ele)
    return;

  /* Read 1 float. */
  if (!visu_config_file_entry_popTokenAsFloat(entry, 1, &radius, rgRadius))
    return;

  /* Read 1 string. */
  if (!visu_config_file_entry_popTokenAsEnum(entry, &shape, _shapeFromName))
    return;

  /* Read 1 optional string. */
  remains = visu_config_file_entry_popAllTokens(entry);
  if (remains && remains[0])
    units = tool_physic_getUnitFromName(remains);
  g_free(remains);

  g_debug("Rendering Atomic: store values.");
  visu_element_atomic_setUnits(ele, units);
  visu_element_atomic_setRadius(ele, radius);
  visu_element_atomic_setShape(ele, shape);
}
static void onEntryUnit(VisuElementAtomic *ele,
                        VisuConfigFileEntry *entry _U_, VisuConfigFile *obj _U_)
{
  if (ele->priv->units != TOOL_UNITS_UNDEFINED)
    return;

  visu_element_atomic_setUnits(ele, visu_basic_getPreferedUnit());
}
/* These functions write all the element list to export there associated resources. */
static void exportAtomic(GString *data, VisuData *dataObj)
{
  GList *pos;
  VisuElementAtomic *ele;
  const VisuElement *element;

  visu_config_file_exportComment(data, DESC_RESOURCE_RADIUS_SHAPE);
  for (pos = _pool; pos; pos = g_list_next(pos))
    {
      ele = VISU_ELEMENT_ATOMIC(pos->data);
      element = visu_element_renderer_getConstElement(VISU_ELEMENT_RENDERER(ele));
      if (!dataObj || visu_node_array_containsElement(VISU_NODE_ARRAY(dataObj), element))
        visu_config_file_exportEntry(data, FLAG_RESOURCE_RADIUS_SHAPE,
                                     visu_element_getName(element),
                                     "%10.3f %s %s", ele->priv->radius,
                                     _shapeName[ele->priv->shape],
                                     tool_physic_getUnitLabel(ele->priv->units));
    }
  visu_config_file_exportComment(data, "");
}

static void exportAtomicShape(GString *data, VisuData* dataObj _U_)
{
  g_string_append_printf(data, "# %s\n", DESC_PARAMETER_SHAPE);
  g_string_append_printf(data, "%s: %s\n\n", FLAG_PARAMETER_SHAPE,
			 _sphereName[_sphereMethod]);
}
