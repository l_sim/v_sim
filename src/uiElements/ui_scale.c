/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2001-2014)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#include "ui_scale.h"

#include <support.h>
#include <visu_tools.h>
#include <coreTools/toolMatrix.h>

/**
 * SECTION:ui_scale
 * @short_description: Defines a widget to setup scale.
 *
 * <para>A set of widgets to setup the rendring of a scale.</para>
 */

/**
 * VisuUiScaleClass:
 * @parent: the parent class;
 *
 * A short way to identify #_VisuUiScaleClass structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiScale:
 *
 * An opaque structure.
 *
 * Since: 3.8
 */
/**
 * VisuUiScalePrivate:
 *
 * Private fields for #VisuUiScale objects.
 *
 * Since: 3.8
 */
struct _VisuUiScalePrivate
{
  gboolean dispose_has_run;

  GtkWidget *lblScales;
  GtkWidget *spinScaleLength;
  GtkWidget *entryScale;
  GtkWidget *originespin[3];
  GtkWidget *orientationspin[3];
  
  VisuGlExtScale *model;
  GBinding *bind_warn;
  GBinding *bind_lg, *bind_lbl;
  GBinding *bind_orig[3];
  GBinding *bind_dir[3];
};

static void visu_ui_scale_finalize(GObject* obj);
static void visu_ui_scale_dispose(GObject* obj);

G_DEFINE_TYPE_WITH_CODE(VisuUiScale, visu_ui_scale, VISU_TYPE_UI_LINE,
                        G_ADD_PRIVATE(VisuUiScale))

static void visu_ui_scale_class_init(VisuUiScaleClass *klass)
{
  g_debug("Ui Scale: creating the class of the widget.");
  g_debug("                     - adding new signals ;");

  /* Connect freeing methods. */
  G_OBJECT_CLASS(klass)->dispose = visu_ui_scale_dispose;
  G_OBJECT_CLASS(klass)->finalize = visu_ui_scale_finalize;
}
static void visu_ui_scale_dispose(GObject *obj)
{
  g_debug("Ui Scale: dispose object %p.", (gpointer)obj);

  if (VISU_UI_SCALE(obj)->priv->dispose_has_run)
    return;

  visu_ui_scale_bind(VISU_UI_SCALE(obj), (VisuGlExtScale*)0);

  VISU_UI_SCALE(obj)->priv->dispose_has_run = TRUE;
  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_scale_parent_class)->dispose(obj);
}
static void visu_ui_scale_finalize(GObject *obj)
{
  g_return_if_fail(obj);

  g_debug("Ui Scale: finalize object %p.", (gpointer)obj);

  /* Chain up to the parent class */
  G_OBJECT_CLASS(visu_ui_scale_parent_class)->finalize(obj);

  g_debug(" | freeing ... OK.");
}

static void visu_ui_scale_init(VisuUiScale *obj)
{
  GtkWidget *vbox, *hbox;
  GtkWidget *table, *label;
  gchar *lblXYZ[3] = {"X", "Y", "Z"};
  int i;

  obj->priv = visu_ui_scale_get_instance_private(obj);
  obj->priv->dispose_has_run = FALSE;

  obj->priv->model = (VisuGlExtScale*)0;

  vbox = visu_ui_line_getOptionBox(VISU_UI_LINE(obj));

  /* To be removed. */
  obj->priv->lblScales =
    gtk_label_new(_("<i><b>Several scales are defined from resource files, "
                    "but only one is editable.</b></i>"));
  gtk_label_set_line_wrap(GTK_LABEL(obj->priv->lblScales), TRUE);
  gtk_label_set_use_markup(GTK_LABEL(obj->priv->lblScales), TRUE);
  gtk_box_pack_end(GTK_BOX(vbox), obj->priv->lblScales, FALSE, FALSE, 10);
  gtk_widget_set_no_show_all(obj->priv->lblScales, TRUE);

  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
  gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,5);
  label = gtk_label_new(_("Legend:"));
  gtk_box_pack_start(GTK_BOX(hbox),label, FALSE, FALSE, 0); 
  obj->priv->entryScale = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(obj->priv->entryScale),
                                 _("Default legend displays the length."));
  gtk_widget_set_tooltip_text(obj->priv->entryScale,
                              _("Use blank legend to print the default"
                                " value with the distance."));
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->entryScale, TRUE, TRUE, 0);
  
  hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
  label = gtk_label_new(_("Length:"));
  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
 
  obj->priv->spinScaleLength = gtk_spin_button_new_with_range(0., 1000., 1.);
  gtk_spin_button_set_digits(GTK_SPIN_BUTTON(obj->priv->spinScaleLength), 2);
  gtk_widget_set_halign(obj->priv->spinScaleLength, GTK_ALIGN_END);
  gtk_box_pack_start(GTK_BOX(hbox), obj->priv->spinScaleLength, TRUE, TRUE, 0); 

  table = gtk_grid_new();
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5); 

  label = gtk_label_new(_("Origin:"));
  gtk_widget_set_hexpand(label, TRUE);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_grid_attach(GTK_GRID(table), label, 0, 0, 1, 1);
  for (i = 0; i < 3; i++)
    {
      gtk_grid_attach(GTK_GRID(table), gtk_label_new(lblXYZ[i]), 1, i, 1, 1);

      obj->priv->originespin[i] = gtk_spin_button_new_with_range(-999., 999., 1.);
      gtk_spin_button_set_digits(GTK_SPIN_BUTTON(obj->priv->originespin[i]), 2);
      gtk_grid_attach(GTK_GRID(table), obj->priv->originespin[i], 2, i, 1, 1);
    }

  table = gtk_grid_new();
  gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5); 

  label = gtk_label_new(_("Orientation:"));
  gtk_widget_set_hexpand(label, TRUE);
  gtk_label_set_xalign(GTK_LABEL(label), 0.);
  gtk_grid_attach(GTK_GRID(table), label, 0, 0, 1, 1);
  for (i = 0; i < 3; i++)
    {
      gtk_grid_attach(GTK_GRID(table), gtk_label_new(lblXYZ[i]), 1, i, 1, 1);

      obj->priv->orientationspin[i] = gtk_spin_button_new_with_range(-999., 999., 1.);
      gtk_spin_button_set_digits(GTK_SPIN_BUTTON(obj->priv->orientationspin[i]), 2);
      gtk_grid_attach(GTK_GRID(table), obj->priv->orientationspin[i], 2, i, 1, 1);
    }

  gtk_widget_show_all(GTK_WIDGET(vbox));
}


/**
 * visu_ui_scale_new:
 *
 * Creates a new #VisuUiScale to allow to setup scale rendering characteristics.
 *
 * Since: 3.8
 *
 * Returns: a pointer to the newly created widget.
 */
GtkWidget* visu_ui_scale_new()
{
  g_debug("Ui Scale: new object.");
  
  return GTK_WIDGET(g_object_new(VISU_TYPE_UI_SCALE, "label", _("Label"), NULL));
}

static gboolean showWarning(GBinding *bind _U_, const GValue *source,
                            GValue *target, gpointer user_data _U_)
{
  g_value_set_boolean(target, g_value_get_uint(source) > 1);
  return TRUE;
}
/**
 * visu_ui_scale_bind:
 * @scale: a #VisuUiScale object.
 * @model: (transfer full): a #VisuGlExtScale object.
 *
 * Bind the properties of @model to be displayed by @scale.
 *
 * Since: 3.8
 **/
void visu_ui_scale_bind(VisuUiScale *scale, VisuGlExtScale *model)
{
  const gchar *orig[3] = { "current-origin-x", "current-origin-y", "current-origin-z" };
  const gchar *dir[3] = { "current-orientation-x", "current-orientation-y", "current-orientation-z" };
  guint i;

  g_return_if_fail(VISU_IS_UI_SCALE(scale));

  if (scale->priv->model == model)
    return;

  visu_ui_line_bind(VISU_UI_LINE(scale), VISU_GL_EXT_LINED(model));
  if (scale->priv->model)
    {
      g_object_unref(G_OBJECT(scale->priv->bind_lg));
      g_object_unref(G_OBJECT(scale->priv->bind_lbl));
      g_object_unref(G_OBJECT(scale->priv->bind_orig[0]));
      g_object_unref(G_OBJECT(scale->priv->bind_orig[1]));
      g_object_unref(G_OBJECT(scale->priv->bind_orig[2]));
      g_object_unref(G_OBJECT(scale->priv->bind_dir[0]));
      g_object_unref(G_OBJECT(scale->priv->bind_dir[1]));
      g_object_unref(G_OBJECT(scale->priv->bind_dir[2]));
      g_object_unref(G_OBJECT(scale->priv->bind_warn));
      g_object_unref(G_OBJECT(scale->priv->model));
    }
  scale->priv->model = model;
  if (model)
    {
      g_object_ref(G_OBJECT(model));
      scale->priv->bind_lg = g_object_bind_property(model, "current-length",
                                                    scale->priv->spinScaleLength, "value",
                                                    G_BINDING_BIDIRECTIONAL |
                                                    G_BINDING_SYNC_CREATE);
      scale->priv->bind_lbl = g_object_bind_property(model, "current-label",
                                                     scale->priv->entryScale, "text",
                                                     G_BINDING_BIDIRECTIONAL |
                                                     G_BINDING_SYNC_CREATE);
      for (i = 0; i < 3; i++)
        {
          scale->priv->bind_orig[i] =
            g_object_bind_property(model, orig[i], scale->priv->originespin[i], "value",
                                   G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
          scale->priv->bind_dir[i] =
            g_object_bind_property(model, dir[i], scale->priv->orientationspin[i], "value",
                                   G_BINDING_BIDIRECTIONAL | G_BINDING_SYNC_CREATE);
        }
      scale->priv->bind_warn =
        g_object_bind_property_full(model, "n-arrows",
                                    scale->priv->lblScales, "visible",
                                    G_BINDING_DEFAULT | G_BINDING_SYNC_CREATE,
                                    showWarning, NULL, NULL, NULL);
    }
}
