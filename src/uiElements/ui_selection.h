/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_UI_SELECTION_H
#define VISU_UI_SELECTION_H

#include <glib.h>
#include <glib-object.h>

#include <gtk/gtk.h>

#include <visu_data.h>
#include <extensions/marks.h>

/***************/
/* Public part */
/***************/

/**
 * VISU_TYPE_UI_SELECTION:
 *
 * return the type of #VisuUiSelection.
 */
#define VISU_TYPE_UI_SELECTION	     (visu_ui_selection_get_type ())
/**
 * VISU_UI_SELECTION:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuUiSelection type.
 */
#define VISU_UI_SELECTION(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_UI_SELECTION, VisuUiSelection))
/**
 * VISU_UI_SELECTION_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuUiSelectionClass.
 */
#define VISU_UI_SELECTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_UI_SELECTION, VisuUiSelectionClass))
/**
 * VISU_IS_UI_SELECTION:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuUiSelection object.
 */
#define VISU_IS_UI_SELECTION(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_UI_SELECTION))
/**
 * VISU_IS_UI_SELECTION_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuUiSelectionClass class.
 */
#define VISU_IS_UI_SELECTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_UI_SELECTION))
/**
 * VISU_UI_SELECTION_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_UI_SELECTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_UI_SELECTION, VisuUiSelectionClass))


/**
 * VisuUiSelectionPrivate:
 * 
 * Private data for #VisuUiSelection objects.
 */
typedef struct _VisuUiSelectionPrivate VisuUiSelectionPrivate;

/**
 * VisuUiSelection:
 * 
 * Common name to refer to a #_VisuUiSelection.
 */
typedef struct _VisuUiSelection VisuUiSelection;
struct _VisuUiSelection
{
  GtkListStore parent;

  VisuUiSelectionPrivate *priv;
};

/**
 * VisuUiSelectionClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuUiSelectionClass.
 */
typedef struct _VisuUiSelectionClass VisuUiSelectionClass;
struct _VisuUiSelectionClass
{
  GtkListStoreClass parent;
};

/**
 * visu_ui_selection_get_type:
 *
 * This method returns the type of #VisuUiSelection, use
 * VISU_TYPE_UI_SELECTION instead.
 *
 * Since: 3.8
 *
 * Returns: the type of #VisuUiSelection.
 */
GType visu_ui_selection_get_type(void);

/**
 * VisuUiSelectionColumnId:
 * @VISU_UI_SELECTION_NUMBER: column displaying the node id.
 * @VISU_UI_SELECTION_HIGHLIGHT:column checked when the node is highlighted.
 * @VISU_UI_SELECTION_N_COLUMNS: the number of columns.
 *
 * Thesse are the description of the columns stored in the object.
 */
typedef enum
  {
    VISU_UI_SELECTION_NUMBER,
    VISU_UI_SELECTION_HIGHLIGHT,
    VISU_UI_SELECTION_N_COLUMNS
  } VisuUiSelectionColumnId;

VisuUiSelection* visu_ui_selection_new();

gboolean visu_ui_selection_setNodeModel(VisuUiSelection *selection, VisuData *data);
gboolean visu_ui_selection_setHighlightModel(VisuUiSelection *selection,
                                             VisuGlExtMarks *marks);

gboolean visu_ui_selection_add(VisuUiSelection *selection, guint id);
void visu_ui_selection_append(VisuUiSelection *selection, const GArray *ids);
void visu_ui_selection_appendHighlightedNodes(VisuUiSelection *selection);
/* GtkBox* visu_ui_selection_getControls(VisuUiSelection *list); */

void visu_ui_selection_highlight(VisuUiSelection *selection,
                                 GtkTreeIter *iter, VisuGlExtMarksStatus status);

void visu_ui_selection_remove(VisuUiSelection *selection, const GArray *ids);
void visu_ui_selection_removePaths(VisuUiSelection *selection, const GList *paths);
void visu_ui_selection_clear(VisuUiSelection *selection);

gboolean visu_ui_selection_removeAt(VisuUiSelection *selection, GtkTreeIter *iter);
gboolean visu_ui_selection_at(VisuUiSelection *selection, GtkTreeIter *iter, guint id);

GArray* visu_ui_selection_get(const VisuUiSelection *selection);
void visu_ui_selection_set(VisuUiSelection *selection, const GArray *ids);

#endif
