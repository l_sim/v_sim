/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2017)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2017)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_ANIMATION_H
#define VISU_ANIMATION_H

#include <glib.h>
#include <glib-object.h>

#include <visu_tools.h>

G_BEGIN_DECLS

/***************/
/* Public part */
/***************/

/**
 * VisuAnimationType:
 * @VISU_ANIMATION_LINEAR: a linear evolution.
 * @VISU_ANIMATION_QUAD: a quadratic evolution.
 * @VISU_ANIMATION_SIN: a sinusoid evolution.
 *
 * Various type of time evolution.
 */
typedef enum _VisuAnimationType
{
  VISU_ANIMATION_LINEAR,
  VISU_ANIMATION_QUAD,
  VISU_ANIMATION_SIN
} VisuAnimationType;

/**
 * VISU_TYPE_ANIMATION:
 *
 * return the type of #VisuAnimation.
 */
#define VISU_TYPE_ANIMATION	     (visu_animation_get_type ())
/**
 * VISU_ANIMATION:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuAnimation type.
 */
#define VISU_ANIMATION(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_ANIMATION, VisuAnimation))
/**
 * VISU_ANIMATION_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuAnimationClass.
 */
#define VISU_ANIMATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_ANIMATION, VisuAnimationClass))
/**
 * VISU_IS_ANIMATION:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuAnimation object.
 */
#define VISU_IS_ANIMATION(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_ANIMATION))
/**
 * VISU_IS_ANIMATION_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuAnimationClass class.
 */
#define VISU_IS_ANIMATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_ANIMATION))
/**
 * VISU_ANIMATION_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_ANIMATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_ANIMATION, VisuAnimationClass))

typedef struct _VisuAnimationPrivate VisuAnimationPrivate;

/**
 * VisuAnimation:
 * 
 * Common name to refer to a #_VisuAnimation.
 */
typedef struct _VisuAnimation VisuAnimation;
struct _VisuAnimation
{
  VisuObject parent;

  VisuAnimationPrivate *priv;
};

/**
 * VisuAnimationClass:
 * @parent: private.
 * 
 * Common name to refer to a #_VisuAnimationClass.
 */
typedef struct _VisuAnimationClass VisuAnimationClass;
struct _VisuAnimationClass
{
  VisuObjectClass parent;
};

GType visu_animation_get_type(void);

VisuAnimation* visu_animation_new(GObject *obj, const gchar *property);

gboolean visu_animation_start(VisuAnimation *anim, const GValue *to, gulong tick,
                              gulong duration, gboolean loop, VisuAnimationType type);

gboolean visu_animation_animate(VisuAnimation *anim, gulong tick);

gboolean visu_animation_isRunning(const VisuAnimation *anim);

void visu_animation_getFrom(const VisuAnimation *anim, GValue *from);
void visu_animation_getTo(const VisuAnimation *anim, GValue *to);

void visu_animation_abort(VisuAnimation *anim);

G_END_DECLS

#endif
