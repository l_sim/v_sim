/* -*- mode: C; c-basic-offset: 2 -*- */
/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2021)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2021)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATA_H
#define VISU_DATA_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"
#include "visu_elements.h"
#include "visu_nodes.h"
#include "extraFunctions/nodeProp.h"
#include "extraFunctions/stringProp.h"
#include "visu_box.h"
#include "iface_boxed.h"
#include "iface_pointset.h"
#include "coreTools/toolPhysic.h"
#include "coreTools/toolFileFormat.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA:
 *
 * return the type of #VisuData.
 */
#define VISU_TYPE_DATA	     (visu_data_get_type ())
/**
 * VISU_DATA:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuData type.
 */
#define VISU_DATA(obj)	     (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_DATA, VisuData))
/**
 * VISU_DATA_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuDataClass.
 */
#define VISU_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_DATA, VisuDataClass))
/**
 * VISU_IS_DATA:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuData object.
 */
#define VISU_IS_DATA(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_DATA))
/**
 * VISU_IS_DATA_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuDataClass class.
 */
#define VISU_IS_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_DATA))
/**
 * VISU_DATA_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_DATA_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA, VisuDataClass))

typedef struct _VisuDataPrivate VisuDataPrivate;
typedef struct _VisuData VisuData;

/**
 * VisuData:
 *
 * Opaque structure for #VisuData objects.
 */
struct _VisuData
{
  VisuNodeArray parent;

  VisuDataPrivate *priv;
};

/**
 * VisuDataClass:
 * @parent: the parent class.
 *
 * A short way to identify #_VisuDataClass structure.
 */
typedef struct _VisuDataClass VisuDataClass;
struct _VisuDataClass
{
  VisuNodeArrayClass parent;
};

/**
 * visu_data_get_type:
 *
 * This method returns the type of #VisuData, use VISU_TYPE_DATA instead.
 *
 * Returns: the type of #VisuData.
 */
GType visu_data_get_type(void);

VisuData* visu_data_new(void);
void visu_data_freePopulation(VisuData *data);

void visu_data_setDescription(VisuData *data, const gchar* commentary);
const gchar* visu_data_getDescription(const VisuData *data);

gfloat visu_data_getAllNodeExtens(VisuData *dataObj, VisuBox *box);

VisuNode* visu_data_addNodeFromElement(VisuData *data, VisuElement *ele,
                                       float xyz[3], gboolean reduced);
VisuNode* visu_data_addNodeFromElementName(VisuData *data, const gchar *name,
                                           float xyz[3], gboolean reduced);
VisuNode* visu_data_addNodeFromIndex(VisuData *data, guint position,
                                     float xyz[3], gboolean reduced);

VisuBox* visu_data_setTightBox(VisuData *data);

gboolean visu_data_getNodeBoxFromNumber(VisuData *data, guint nodeId, int nodeBox[3]);
gboolean visu_data_getNodeBoxFromCoord(VisuData *data, float xcart[3], int nodeBox[3]);

void visu_data_getNodeUserPosition(const VisuData *data, const VisuNode *node, float coord[3]);

gboolean visu_data_setNewBasisFromNodes(VisuData *data, guint nO, guint nA, guint nB, guint nC);
gboolean visu_data_setNewBasis(VisuData *data, float matA[3][3], float O[3]);
gboolean visu_data_reorder(VisuData *data, const VisuData *dataRef);

gboolean visu_data_addNodeProperties(VisuData *data, VisuNodeValues *values);
gboolean visu_data_removeNodeProperties(VisuData *data, const gchar *label);
GList* visu_data_getAllNodeProperties(VisuData *data);
VisuNodeValues* visu_data_getNodeProperties(VisuData *data, const gchar *label);
VisuNodeValuesString* visu_data_getNodeLabels(VisuData *data);
const gchar* visu_data_getNodeLabelAt(const VisuData *data, const VisuNode *node);

gboolean visu_data_applyTransformationsFromCLI(VisuData *data, GError **error);

void visu_data_rescale(VisuData *data, VisuBox *box);

/**
 * VisuDataIter:
 * @parent: the parent #VisuNodeArrayIter.
 * @xyz: the current node position, in cartesian coordinates.
 * @boxXyz: the current node position, in reduced box coordinates.
 *
 * A structure, expanding #VisuNodeArrayIter, computing the current node
 * position in cartesian or box coordinates.
 *
 * Since: 3.9
 */
typedef struct _VisuDataIter VisuDataIter;
struct _VisuDataIter
{
  VisuNodeArrayIter parent;
  gfloat xyz[3], boxXyz[3];
};
void visu_data_iter_new(VisuData *data, VisuDataIter *iter, VisuNodeArrayIterType type);
void visu_data_iter_new_forElement(VisuData *data, VisuDataIter *iter,
                                   const VisuElement *element);
gboolean visu_data_iter_isValid(const VisuDataIter *iter);
gboolean visu_data_iter_isVisible(const VisuDataIter *iter);
gboolean visu_data_iter_next(VisuDataIter *iter);


G_END_DECLS

#endif
