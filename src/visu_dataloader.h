/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2016)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2016)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DATA_LOADER_H
#define VISU_DATA_LOADER_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "coreTools/toolFileFormat.h"
#include "visu_dataloadable.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DATA_LOADER:
 *
 * Return the associated #GType to the VisuDataLoader objects.
 */
#define VISU_TYPE_DATA_LOADER         (visu_data_loader_get_type ())
/**
 * VISU_DATA_LOADER:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuDataLoader object.
 */
#define VISU_DATA_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_DATA_LOADER, VisuDataLoader))
/**
 * VISU_DATA_LOADER_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuDataLoaderClass object.
 */
#define VISU_DATA_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_DATA_LOADER, VisuDataLoaderClass))
/**
 * VISU_IS_DATA_LOADER:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuDataLoader object.
 */
#define VISU_IS_DATA_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_DATA_LOADER))
/**
 * VISU_IS_DATA_LOADER_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuDataLoaderClass class.
 */
#define VISU_IS_DATA_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_DATA_LOADER))
/**
 * VISU_DATA_LOADER_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_DATA_LOADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DATA_LOADER, VisuDataLoaderClass))

/**
 * VisuDataLoader:
 *
 * An opaque structure.
 */
typedef struct _VisuDataLoader VisuDataLoader;
typedef struct _VisuDataLoaderPrivate VisuDataLoaderPrivate;
struct _VisuDataLoader
{
  ToolFileFormat parent;
  
  VisuDataLoaderPrivate *priv;
};
/**
 * VisuDataLoaderClass:
 * @parent: the parent class.
 *
 * An opaque structure.
 */
typedef struct _VisuDataLoaderClass VisuDataLoaderClass;
struct _VisuDataLoaderClass
{
  ToolFileFormatClass parent;
};

/**
 * VisuDataLoaderFunc:
 * @self: a #VisuDataLoader object.
 * @data: the #VisuDataLoadable object to populate.
 * @type: the type of file to populate in @data.
 * @iSet: an integer specifying the set to load, if any.
 * @cancel: (allow-none): a #GCancellable object.
 * @error: (allow-none): an error location.
 *
 * Prototype of function to load a file and populate @data
 * accordingly. The file is taken from
 * visu_data_loadable_getFilename() using @type.
 *
 * Since: 3.8
 *
 * Returns: TRUE if the file matches the format defined by @self,
 * even if some parsing errors may occur.
 */
typedef gboolean (*VisuDataLoaderFunc)(VisuDataLoader *self, VisuDataLoadable *data,
                                       guint type, guint iSet,
                                       GCancellable *cancel, GError **error);

/**
 * visu_data_loader_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuDataLoader objects.
 */
GType visu_data_loader_get_type(void);

VisuDataLoader* visu_data_loader_new(const gchar* descr,
                                     const gchar** patterns, gboolean restricted,
                                     VisuDataLoaderFunc func, guint priority);
VisuDataLoader* visu_data_loader_new_compressible(const gchar* descr,
                                                  const gchar** patterns,
                                                  VisuDataLoaderFunc func,
                                                  guint priority);
VisuDataLoader* visu_data_loader_new_full(const gchar* descr, const gchar** patterns,
                                          gboolean restricted, gboolean allowCompression,
                                          VisuDataLoaderFunc func, guint priority,
                                          GDestroyNotify notify, gpointer data);

gint visu_data_loader_comparePriority(const VisuDataLoader *a, const VisuDataLoader *b);

gboolean visu_data_loader_load(VisuDataLoader *loader, VisuDataLoadable *data,
                               guint type, guint iSet,
                               GCancellable *cancel, GError **error);
void visu_data_loader_setStatus(VisuDataLoader *loader, const gchar *status);

typedef struct _VisuDataLoaderIter VisuDataLoaderIter;

GType visu_data_loader_iter_get_type(void);

VisuDataLoaderIter* visu_data_loader_iter_new();
void visu_data_loader_iter_addNode(VisuDataLoaderIter *iter, const VisuElement *element);
guint visu_data_loader_iter_allocate(VisuDataLoaderIter *iter, VisuNodeArray *array);
void visu_data_loader_iter_unref(VisuDataLoaderIter *iter);
VisuDataLoaderIter* visu_data_loader_iter_ref(VisuDataLoaderIter *iter);

G_END_DECLS

#endif
