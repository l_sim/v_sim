/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_DUMP_H
#define VISU_DUMP_H

#include <glib.h>
#include <glib-object.h>

#include "visu_tools.h"
#include "coreTools/toolFileFormat.h"
#include "visu_data.h"

G_BEGIN_DECLS

/**
 * VISU_TYPE_DUMP:
 *
 * Return the associated #GType to the VisuDump objects.
 */
#define VISU_TYPE_DUMP         (visu_dump_get_type ())
/**
 * VISU_DUMP:
 * @obj: the widget to cast.
 *
 * Cast the given object to a #VisuDump object.
 */
#define VISU_DUMP(obj)         (G_TYPE_CHECK_INSTANCE_CAST ((obj), VISU_TYPE_DUMP, VisuDump))
/**
 * VISU_DUMP_CLASS:
 * @klass: the class to cast.
 *
 * Cast the given class to a #VisuDumpClass object.
 */
#define VISU_DUMP_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), VISU_TYPE_DUMP, VisuDumpClass))
/**
 * VISU_IS_DUMP:
 * @obj: the object to test.
 *
 * Return if the given object is a valid #VisuDump object.
 */
#define VISU_IS_DUMP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VISU_TYPE_DUMP))
/**
 * VISU_IS_DUMP_CLASS:
 * @klass: the class to test.
 *
 * Return if the given class is a valid #VisuDumpClass class.
 */
#define VISU_IS_DUMP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VISU_TYPE_DUMP))
/**
 * VISU_DUMP_GET_CLASS:
 * @obj: the widget to get the class of.
 *
 * Get the class of the given object.
 */
#define VISU_DUMP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_DUMP, VisuDumpClass))

typedef struct _VisuDump VisuDump;
struct _VisuDump
{
  ToolFileFormat parent;
};

typedef struct _VisuDumpClass VisuDumpClass;
/**
 * VisuDumpClass:
 * @parent: the parent class.
 *
 * An opaque structure.
 */
struct _VisuDumpClass
{
  ToolFileFormatClass parent;
};

/**
 * visu_dump_get_type
 *
 * #GType are unique numbers to identify objects.
 *
 * Returns: the #GType associated with #VisuDump objects.
 */
GType visu_dump_get_type(void);

/**
 * VISU_DUMP_ERROR: (skip)
 *
 * Internal function for error handling.
 */
#define VISU_DUMP_ERROR visu_dump_getQuark()
GQuark visu_dump_getQuark();
/**
 * VisuDumpErrorFlag:
 * @DUMP_ERROR_OPENGL: Error with OpenGL dumping.
 * @DUMP_ERROR_FILE: Error when opening.
 * @DUMP_ERROR_ENCODE: Wrongness when computing the encoding format.
 *
 * These are flags used when dumping to a file.
 */
typedef enum
  {
    DUMP_ERROR_OPENGL,
    DUMP_ERROR_FILE,
    DUMP_ERROR_ENCODE
  } VisuDumpErrorFlag;


GList* visu_dump_pool_getAllModules();
void visu_dump_pool_finalize(void);

G_END_DECLS

#endif
