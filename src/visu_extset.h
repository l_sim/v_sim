/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Damien
	CALISTE, laboratoire L_Sim, (2015)
  
	Adresse mèl :
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Damien
	CALISTE, laboratoire L_Sim, (2015)

	E-mail address:
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/
#ifndef VISU_GL_EXT_SET_H
#define VISU_GL_EXT_SET_H

#include <glib.h>
#include <glib-object.h>

#include "opengl.h"
#include "visu_extension.h"
#include "openGLFunctions/view.h"
#include "coreTools/toolColor.h"

G_BEGIN_DECLS

/***************/
/* Public part */
/***************/

/**
 * VISU_TYPE_GL_EXT_SET:
 *
 * return the type of #VisuGlExtSet.
 */
#define VISU_TYPE_GL_EXT_SET	     (visu_gl_ext_set_get_type ())
/**
 * VISU_GL_EXT_SET:
 * @obj: a #GObject to cast.
 *
 * Cast the given @obj into #VisuGlExtSet type.
 */
#define VISU_GL_EXT_SET(obj)	        (G_TYPE_CHECK_INSTANCE_CAST(obj, VISU_TYPE_GL_EXT_SET, VisuGlExtSet))
/**
 * VISU_GL_EXT_SET_CLASS:
 * @klass: a #GObjectClass to cast.
 *
 * Cast the given @klass into #VisuGlExtSetClass.
 */
#define VISU_GL_EXT_SET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST(klass, VISU_TYPE_GL_EXT_SET, VisuGlExtSetClass))
/**
 * VISU_IS_GL_EXT_SET:
 * @obj: a #GObject to test.
 *
 * Test if the given @ogj is of the type of #VisuGlExtSet object.
 */
#define VISU_IS_GL_EXT_SET(obj)    (G_TYPE_CHECK_INSTANCE_TYPE(obj, VISU_TYPE_GL_EXT_SET))
/**
 * VISU_IS_GL_EXT_SET_CLASS:
 * @klass: a #GObjectClass to test.
 *
 * Test if the given @klass is of the type of #VisuGlExtSetClass class.
 */
#define VISU_IS_GL_EXT_SET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE(klass, VISU_TYPE_GL_EXT_SET))
/**
 * VISU_GL_EXT_SET_GET_CLASS:
 * @obj: a #GObject to get the class of.
 *
 * It returns the class of the given @obj.
 */
#define VISU_GL_EXT_SET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS(obj, VISU_TYPE_GL_EXT_SET, VisuGlExtSetClass))

/**
 * VisuGlExtSetPrivate:
 * 
 * Private data for #VisuGlExtSet objects.
 */
typedef struct _VisuGlExtSetPrivate VisuGlExtSetPrivate;

/**
 * VisuGlExtSet:
 * 
 * Common name to refer to a #_VisuGlExtSet.
 */
typedef struct _VisuGlExtSet VisuGlExtSet;
struct _VisuGlExtSet
{
  VisuGl parent;

  VisuGlExtSetPrivate *priv;
};

/**
 * VisuGlExtSetClass:
 * @parent: private.
 * @added: executed when @ext is added.
 * @removed: executed when @ext is removed.
 * 
 * Common name to refer to a #_VisuGlExtSetClass.
 */
typedef struct _VisuGlExtSetClass VisuGlExtSetClass;
struct _VisuGlExtSetClass
{
  VisuGlClass parent;

  void (*added)(VisuGlExtSet *self, VisuGlExt *ext);
  void (*removed)(VisuGlExtSet *self, VisuGlExt *ext);
};

/**
 * visu_gl_ext_set_get_type:
 *
 * This method returns the type of #VisuGlExtSet, use
 * VISU_TYPE_GL_EXT_SET instead.
 *
 * Since: 3.7
 *
 * Returns: the type of #VisuGlExtSet.
 */
GType visu_gl_ext_set_get_type(void);

VisuGlExtSet* visu_gl_ext_set_new();

gboolean visu_gl_ext_set_add(VisuGlExtSet *set, VisuGlExt *ext);
gboolean visu_gl_ext_set_remove(VisuGlExtSet *set, VisuGlExt *ext);

gboolean visu_gl_ext_set_setGlView(VisuGlExtSet *set, VisuGlView *view);

VisuGlExt* visu_gl_ext_set_getByName(const VisuGlExtSet *set, const gchar *name);
GList* visu_gl_ext_set_getAll(VisuGlExtSet *set);

GArray* visu_gl_ext_set_getPixmapData(VisuGlExtSet *set, guint width,
                                      guint height, gboolean hasAlpha);

G_END_DECLS

#endif
