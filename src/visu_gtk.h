/*   EXTRAITS DE LA LICENCE
	Copyright CEA, contributeurs : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)
  
	Adresse mèl :
	BILLARD, non joignable par mèl ;
	CALISTE, damien P caliste AT cea P fr.

	Ce logiciel est un programme informatique servant à visualiser des
	structures atomiques dans un rendu pseudo-3D. 

	Ce logiciel est régi par la licence CeCILL soumise au droit français et
	respectant les principes de diffusion des logiciels libres. Vous pouvez
	utiliser, modifier et/ou redistribuer ce programme sous les conditions
	de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
	sur le site "http://www.cecill.info".

	Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
	pris connaissance de la licence CeCILL, et que vous en avez accepté les
	termes (cf. le fichier Documentation/licence.fr.txt fourni avec ce logiciel).
*/

/*   LICENCE SUM UP
	Copyright CEA, contributors : Luc BILLARD et Damien
	CALISTE, laboratoire L_Sim, (2001-2005)

	E-mail address:
	BILLARD, not reachable any more ;
	CALISTE, damien P caliste AT cea P fr.

	This software is a computer program whose purpose is to visualize atomic
	configurations in 3D.

	This software is governed by the CeCILL  license under French law and
	abiding by the rules of distribution of free software.  You can  use, 
	modify and/ or redistribute the software under the terms of the CeCILL
	license as circulated by CEA, CNRS and INRIA at the following URL
	"http://www.cecill.info". 

	The fact that you are presently reading this means that you have had
	knowledge of the CeCILL license and that you accept its terms. You can
	find a copy of this licence shipped with this software at Documentation/licence.en.txt.
*/

#ifndef VISU_GTK_H
#define VISU_GTK_H

#include <glib.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "coreTools/toolColor.h"
#include "coreTools/toolFileFormat.h"
#include "visu_data.h"
#include "gtk_renderingWindowWidget.h"

/**
 * VisuUiNewWidgetFunc:
 *
 * This prototype is used whenever a method is required to create a GtkWidget.
 *
 * Returns: (transfer full): a newly created GtkWidget.
 */
typedef GtkWidget* (*VisuUiNewWidgetFunc)(void);

/**
 * VisuUiInitWidgetFunc:
 * @panel: a location for a #GtkWindow describing the panel.
 * @renderWindow: a location for a #GtkWindow ;
 * @renderArea: a location for a #GtkWidget.
 *
 * This prototype is used whenever a method is required to initialise
 * the GTK interface of V_Sim. @panel must be set to point on the
 * window of the command panel, @renderWindow must point to the window
 * containing the rendering area (can be the same than @panel) and
 * @renderArea contains the widget that does the OpenGL rendering.
 */
typedef void (*VisuUiInitWidgetFunc)(GtkWindow **panel,
                                     GtkWindow **renderWindow, GtkWidget **renderArea);

void visu_ui_raiseWarning(gchar *action, gchar *message, GtkWindow *window);
void visu_ui_raiseWarningLong(gchar *action, gchar *message, GtkWindow *window);

void visu_ui_mainCreate(VisuUiInitWidgetFunc panelFunc);
gboolean visu_ui_runCommandLine(gpointer data);

void visu_ui_wait(void);

/**
 * VisuUiFileFilter:
 * @gtkFilter: the #GtkFileFilter associate to @visuFilter.
 * @visuFilter: a #ToolFileFormat object.
 *
 * Structure used to associate the V_Sim way to store file filters
 * with the Gtk way.
 */
typedef struct _VisuUiFileFilter VisuUiFileFilter;
struct _VisuUiFileFilter
{
  GtkFileFilter *gtkFilter;
  ToolFileFormat* visuFilter;
};

GList* visu_ui_createFilter(GList *list, GtkWidget *fileChooser);

GdkPixbuf* visu_ui_createPixbuf(const gchar *filename);
GdkPixbuf* tool_color_get_stamp(const ToolColor *color, gboolean alpha);

void visu_ui_storeRecent(const gchar *filename);

void visu_ui_createInterface(GtkWindow **panel,
                             GtkWindow **renderWindow, GtkWidget **renderArea);
GtkWidget* visu_ui_buildRenderingWindow(VisuUiRenderingWindow *renderWindow);
GtkWindow* visu_ui_getPanel(void);
GtkWindow* visu_ui_getRenderWindow(void);
GtkWidget* visu_ui_getRenderWidget(void);
void visu_ui_setRenderWidget(VisuUiRenderingWindow *render);

#endif
