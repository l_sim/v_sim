#!/bin/bash

DIR=../tmp/src
EXE=v_sim-dev
VER=3.8

$DIR/$EXE ../examples/aluminium.d3 -r alu.xml -e aluminium-$VER.png > aluminium.log 2>&1

$DIR/$EXE ../examples/aluminium.d3 -p ../examples/planes.xml -f ../examples/density-sih4.dat -v 0.01:0.05 -e field-$VER.png > field.log 2>&1

$DIR/$EXE ../examples/test_isosurfaces.ascii -i ../examples/test_isosurfaces.surf -e iso-$VER.png > iso.log 2>&1

$DIR/$EXE ../examples/demo.ascii -p ../examples/planes.xml -e planes-$VER.png > planes.log 2>&1

$DIR/$EXE ../examples/aluminium.d3 -p ../examples/planes.xml -f ../examples/density-sih4.dat -b 1 -n 10 -e map-$VER.png > map.log 2>&1
