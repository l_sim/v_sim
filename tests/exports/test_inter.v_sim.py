#!/usr/bin/python

def dump(scene):
  scene.dump(v_sim.Dump.png_getStatic(), "out.win.png", 600, 600, None, None)
  v_sim.UiMainClass.getDefaultRendering().setCurrent(True)
  v_sim.UiMainClass.getCurrentPanel().quit(True)

def clickAt(inter, view, x, y, button = 1, shiftMod = False, controlMod = False):
  ev = v_sim.SimplifiedEvents()
  ev.x = x
  ev.y = y
  ev.button = button
  ev.buttonType = v_sim.ButtonActionId.PRESS
  inter.handleEvent(view, ev)
  ev.button = button
  ev.shiftMod = shiftMod
  ev.controlMod = controlMod
  ev.buttonType = v_sim.ButtonActionId.RELEASE
  inter.handleEvent(view, ev)

def _release(inter, view, ev):
  ev.buttonType = v_sim.ButtonActionId.RELEASE
  inter.handleEvent(view, ev)
  return FALSE
def _move(inter, view, ev, disp):
  disp[5] += 1
  x = float(disp[5]) / float(disp[4])
  fx = (2*x)**3/2 if x < 0.5 else 1-(2-2*x)**3/2
  ev.x = disp[0] + disp[2] * fx
  ev.y = disp[1] + disp[3] * fx
  ev.motion = True
  inter.handleEvent(view, ev)
  if (disp[4] == disp[5]):
    GLib.idle_add(_release, inter, view, ev)
    return False
  else:
    return True

def move(inter, view, dx, dy, button = 1, shiftMod = False, controlMod = False, duration = 500, nSteps = 50):
  ev = v_sim.SimplifiedEvents()
  ev.x = 300
  ev.y = 300
  ev.button = button
  ev.buttonType = v_sim.ButtonActionId.PRESS
  inter.handleEvent(view, ev)
  iStep = 0
  GLib.timeout_add(duration / nSteps, _move, inter, view, ev, [ev.x, ev.y, dx, dy, nSteps, iStep])

def test(scene):
  marks = scene.getMarks()
  view = scene.getGlView()
  inter = marks.get_property("interactive")
  inter.highlight(23)
  clickAt(inter, view, 342, 460, 3, shiftMod = True)
  clickAt(inter, view, 278, 377, 3)
  move(inter, view, 142, -75)
  GLib.timeout_add_seconds(1, dump, scene)

from gi.repository import GLib
  
scene = v_sim.UiMainClass.getDefaultRendering().getGlScene()
GLib.idle_add(test, scene)
