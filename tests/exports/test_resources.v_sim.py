#!/usr/bin/python

(valid, nLines) = v_sim.ConfigFile.saveResourcesToXML("./tmp.res.xml", None)

if not(valid):
  raise ValueError("Resources not written")

print nLines

v_sim.UiMainClass.getCurrentPanel().quit(True)
